
package com.mos.eboot.service.platform.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerLog;
import com.mos.eboot.platform.entity.CustomerXnh;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.service.config.RedisUtil;
import com.mos.eboot.service.config.jdbcUtil;
import com.mos.eboot.service.platform.service.ICCallLogService;
import com.mos.eboot.service.platform.service.ICContactPeoRelService;
import com.mos.eboot.service.platform.service.ICustImRoleService;
import com.mos.eboot.service.platform.service.ICustPhoneService;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerLogService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.ICustomerXnhService;
import com.mos.eboot.service.platform.service.ISysUserRoleService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.service.platform.service.ISysUserWxInfoService;
import com.mos.eboot.service.platform.service.ISysuserCustomerMgrService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.service.platform.service.impl.CContactPeoRelService;
import com.mos.eboot.service.platform.service.impl.CContactPeoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.tools.util.PhoneUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.IMCustomerVO;

/**
 * <p>
 *  
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@RestController
@RequestMapping("api/sys/cust")
public class CustomerController {
	
	private Boolean type=true;//标记是否继续进行更新区号
	
	@Autowired
	ICustomerService customerService;
	
	@Autowired
	ICCallLogService callLogService;
	
	@Autowired
	ICustomerXnhService customerXnhService;

	@Autowired
	ICustPhoneService custPhoneService;

 
	
	@Autowired
	ICustImRoleService custImRoleService;

	@Autowired
	ISysuserCustomerMgrService iSysuserCustomerMgrService;
	
	@Autowired
	ISysUserWxInfoService iSysUserWxInfoService;
	
	@Autowired
    ISysUserRoleService userRoleService;
	
	@Autowired
	RedisUtil redisUtil;
	
	@Autowired
    ISysUserService iSysUserService;
	
	@Autowired
	ICContactPeoRelService iCContactPeoRelService;
 
	@Autowired
	ICustWxService iCustWxService;
	
	 @Autowired
	 CContactPeoService cContactPeoService;
	 
	 @Autowired
	 CContactPeoRelService cContactPeoRelService;
	 
	 @Autowired
	 IWxLoginService iWxLoginService;
		
	 @Autowired
	 ICustomerLogService  iCustomerLogService;
	 
	@PostMapping("/query-cust-by-entity")
	List<Customer> getCustByEntity(@RequestBody Customer customer){		
		EntityWrapper<Customer> entity=new EntityWrapper<Customer>();
		entity.eq("sys_wx_id", customer.getSysWxId());
		entity.eq("is_del", "0");
		List<Customer> custList=customerService.selectList(entity );	
		return custList;	
	}
	//关联微信号查询
	@PostMapping("/query-custWx")
	List<CustWx> getCustomerQueryCustWx(@RequestBody CustWx custWx){		
		EntityWrapper<CustWx> entity=new EntityWrapper<CustWx>();
		if(StringUtils.isNotBlank(custWx.getWxRemark())){
			entity.like("wx_remark", custWx.getWxRemark());
		}
		if(StringUtils.isNotBlank(custWx.getWxNickname())){
			entity.like("wx_nickname", custWx.getWxNickname());
		}
		if(StringUtils.isNotBlank(custWx.getcId())){
			entity.eq("c_id", custWx.getcId());
		}
		List<CustWx> custList=iCustWxService.selectList(entity );
		if(custList.size()>0){
			for(CustWx cu:custList){
				if(StringUtil.isNotBlank(cu.getcId())){
					
					cu.setCustName(customerService.selectById(cu.getcId()).getCustName());
				}
				
			}
				
		}
		return custList;	
	}
	//微信号关联-解绑
	@PostMapping("/custWx-save")
	CustWx getCustomercustWxSave(@RequestBody CustWx custWx){	
	
		if(custWx.getType().equals("1")){//取消关联
			custWx.setcId(null);
			custWx.setUserId(null);
			custWx.setCustName(null);
			
			
			Customer cust=customerService.selectById(iCustWxService.selectById(custWx.getId()).getcId()	);
			cust.setWxStatus("缺少微信");
			customerService.updateById(cust);
			
		}else{
			Customer cust=customerService.selectById(custWx.getcId());
			cust.setWxStatus("正常"); 
			custWx.setUserId(cust.getCustAdminUserId());
			customerService.updateById(cust);
		}
		iCustWxService.updateById(custWx);		 
		if(StringUtil.isNotBlank(custWx.getcId())){			
			custWx.setCustName(customerService.selectById(custWx.getcId()).getCustName());
		}			 
		return custWx;	
	}
	
	   /**
     * 虚拟号转换
     * */
     
    @RequestMapping("/customerZh")
    public ResultModel<Boolean> customerZh(@RequestBody List<Customer> culist){
     
    	for(Customer li:culist ){
    	   CustomerXnh reqData = JSON.parseObject(JSON.toJSONString(li), CustomerXnh.class);
          	 customerXnhService.insert(reqData);
          	customerService.deleteById(li.getId());
          	
    	}
    	
    	return new ResultModel<Boolean>(ResultStatus.SUCCESS, true);
    } 
    
 /**
  * 数据合并
  * */
  
 @RequestMapping("/shujuhebing")
 public ResultModel<Boolean> shujuhebing(@RequestBody List<Customer> culist,@RequestParam("uuid")  String uuid){
  
 	for(Customer li:culist ){
 		if(li.getMainData()==false){
 			//更改联系人
 			EntityWrapper<CustWx> CustWxentity=new EntityWrapper<CustWx>();
 			CustWxentity.eq("c_id", li.getId());
 			
 			List<CustWx> CustWxList=iCustWxService.selectList(CustWxentity);		
 			if(CustWxList.size()>0){
 				for(CustWx  cu:CustWxList){
 	 				cu.setcId(uuid);
 	 				iCustWxService.updateById(cu);
 	 			}
 			}
 			//更改通话记录
 			EntityWrapper<CCallLog> callentity=new EntityWrapper<CCallLog>();
 			callentity.eq("CUSTOMER_ID", li.getId());
 			List<CCallLog> callList=callLogService.selectList(callentity);		
 			if(callList.size()>0){
 				for(CCallLog  cu:callList){
 	 				cu.setCustomerId(uuid);
 	 				callLogService.updateById(cu);
 	 			}
 			}
 			 //删除用户数据
 			 li.setIsDel("1");
 			li.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
 			 customerService.updateById(li)	;		 
 		}
 		if(li.getMainData()==true){//保留的数据
 			li.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
 			 customerService.updateById(li);
 		}
 	   //CustomerXnh reqData = JSON.parseObject(JSON.toJSONString(li), CustomerXnh.class);
       	// customerXnhService.insert(reqData);
        	//customerService.deleteById(li.getId());
 	}
 	
 	return new ResultModel<Boolean>(ResultStatus.SUCCESS, true);
 } 
 
 @RequestMapping("/shujuUpdate")
 public ResultModel<Boolean> shujuUpdate(){
	 CustomerLog custLog=null;
	 EntityWrapper<CustomerLog> wrapper= new EntityWrapper<CustomerLog>();
	 List<CustomerLog> list=iCustomerLogService.selectList(wrapper);
 	 if(list.size()>0){
 		custLog=list.get(list.size()-1);
 		List<Customer> listCust=  jdbcUtil.get_update_bcustomer(custLog.getUpdateTime());
 		if(listCust.size()>0){
 			CustomerLog log1=new  CustomerLog();
 			log1.setUpdateNum(listCust.size());
 			log1.setUpdateTime(listCust.get(listCust.size()-1).getRawUpTime());
 			log1.setCreateTime(new Date());
 			iCustomerLogService.insert(log1);
 			
 			 List<SysUser>  userList=iSysUserService.selectList(new EntityWrapper<SysUser>()); 
				 Map<Object,String> usermap=new HashMap<Object, String>();
				 for(SysUser ur:userList){
					 usermap.put(ur.getUsername(),ur.getId());
				 } 
 			for(Customer li:listCust){
 				EntityWrapper<Customer>  custWrapper= new EntityWrapper<Customer>();
 				custWrapper.eq("tid",li.getTid());
 				Customer cust=customerService.selectOne(custWrapper);
 				if(cust!=null){
 					Date date1=DateUtil.string_date(cust.getRawUpTime());//修改时间
 					Date date2=DateUtil.string_date(li.getRawUpTime());//修改时间
 					int compareTo = date1.compareTo(date2);
 					//compareTo()方法的返回值，date1小于date2返回-1，date1大于date2返回1，相等返回0
 					 if(compareTo==-1){
 						 //if(!li.getValidPhone().equals(cust.getValidPhone())){
 							// li.setOtherPhone(li.getOtherPhone()+li.getValidPhone()); 						 
 						 //}
 						cust.setValidPhone(StringUtil.str_Phone(li.getValidPhone()));
 						List<String> strList=StringUtil.str_all_Phone(li.getOtherPhone());
 						if(strList!=null&&strList.size()>0){
 							String st="";
 							for(String str:strList){
 								st=st+str+"  ";
 							}
 							cust.setOtherPhone(st);
 						}
 						cust.setCustName(li.getCustName());
 						cust.setProvinceName(li.getProvinceName());
 						cust.setCityName(li.getCityName());
 						cust.setAreaName(li.getAreaName());
 						cust.setCustType(li.getCustType());
 						cust.setAddressInfo(li.getAddressInfo());
 						cust.setRawUpTime(li.getRawUpTime());
 						 if(StringUtil.isNotBlank(li.getValidPhone())){
 							 if(li.getValidPhone().length()!=11){
 								li.setIfNormal("电话异常");//默   
 							 }else{
 								li.setIfNormal("正常");//默   
 							 }
 							
 						 }else{
 							li.setIfNormal("电话异常");//默   
 						 }
 						 li.setWxStatus("缺少微信");
 						 if(StringUtils.isNotBlank(StringUtil.str_Phone(li.getValidPhone()))){
 							  EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
 							  entityWrapper.like("wx_remark",li.getValidPhone());	 
 							  List<CustWx>  wxFriendsList=iCustWxService.selectList(entityWrapper);
 							  if(wxFriendsList.size()>0){
 								 /* for(CustWx cu:wxFriendsList){
 									   cu.setcId(li.getId());
 									  iCustWxService.updateById(cu);
 								  }*/
 								     
 								 li.setWxStatus("正常");  
 							  } 
 						 }
 						
 						customerService.updateById(cust);
 						
 						//查询通话记录 插入
						 List<CCallLog> calllist=jdbcUtil.get_tbl_bchat(null,li.getTid(),custLog.getUpdateTime()) ;
						 if(calllist.size()>0){
							 for(int i=0;i<calllist.size();i++){
								 CCallLog log=calllist.get(i);
								 EntityWrapper<CCallLog>  clWrapper= new EntityWrapper<CCallLog>();
								 clWrapper.eq("tid",log.getTid());
								 CCallLog log2=callLogService.selectOne(clWrapper);
								 if(log2==null){
									 log.setCustomerId(li.getId());
									 callLogService.insert(log);
								 }
								
								
								
							 } 
						 }
						 //删除现在得联系人    
							EntityWrapper<CContactPeo>  peoWrapper= new EntityWrapper<CContactPeo>();
							peoWrapper.eq("CUSTOMER_ID",cust.getId());
							List<CContactPeo> peoList= cContactPeoService.selectList(peoWrapper);
							if(peoList.size()>0){
								for (int i = 0; i < peoList.size(); i++) {
									cContactPeoService.deleteById(peoList.get(i).getContactId());
								}
								 
							}
							
							
			 				EntityWrapper<CContactPeoRel>  peoRelWrapper= new EntityWrapper<CContactPeoRel>();
			 				peoRelWrapper.eq("CUSTOMER_ID",cust.getId());
			 				List<CContactPeoRel> peoRelList=cContactPeoRelService.selectList(peoRelWrapper);
			 				if(peoRelList.size()>0){
			 					for (int i = 0; i < peoRelList.size(); i++) {
			 						cContactPeoRelService.deleteById(peoRelList.get(i).getContactRelId());
								}
								 
							}
						 
						 
						  
						   
 					 }
 				}else{
 					EntityWrapper<CustomerXnh>  custxnhWrapper= new EntityWrapper<CustomerXnh>();
 					custxnhWrapper.eq("tid",li.getTid());
 					CustomerXnh custxnh=customerXnhService.selectOne(custxnhWrapper);
 					if(custxnh==null){//虚拟号中没有该数据代表事新增的数据
 						//查询系统所有业务员
 					
 						
 						 li.setValidPhone(StringUtil.str_Phone(li.getValidPhone()));
 						 li.setId(UuidUtil.get32UUID());				 
 						 if(usermap.get(li.getCustAdminNickname())!=null){	//存储业务员id				 
 							li.setCustAdminUserId(usermap.get(li.getCustAdminNickname()));
 						 }else{//默认设置为空
 							li.setCustAdminUserId(null); 
 						 }	
 						 
 						 if(StringUtil.isNotBlank(li.getValidPhone())){
 							 if(li.getValidPhone().length()!=11){
 								li.setIfNormal("电话异常");//默   
 							 }else{
 								li.setIfNormal("正常");//默   
 							 }
 							
 						 }else{
 							li.setIfNormal("电话异常");//默   
 						 }
 						  EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
 						  entityWrapper.like("wx_remark",li.getValidPhone());	 
 						  List<CustWx>  wxFriendsList=iCustWxService.selectList(entityWrapper);
 						  if(wxFriendsList.size()>0){
 							  for(CustWx cu:wxFriendsList){
 								   cu.setcId(li.getId());
 								  iCustWxService.updateById(cu);
 							  }
 							   
 							 li.setWxStatus("正常");  
 						  }else{
 							  li.setWxStatus("缺少微信");
 						  }					  
 						customerService.insert(li);
 						//查询通话记录 插入
 						 List<CCallLog> calllist=jdbcUtil.get_tbl_bchat(null,li.getTid(),custLog.getUpdateTime()) ;
 						 if(calllist.size()>0){
 							 for(int i=0;i<calllist.size();i++){
 								 CCallLog log=calllist.get(i);
 								 log.setCustomerId(li.getId());
 								 callLogService.insert(log);
 								
 							 } 
 						 }
 					}
 				}
 				
 			}
 		}
 	 }	
 	return new ResultModel<Boolean>(ResultStatus.SUCCESS, true);
 }   
  
  
	@PostMapping("/query-page")
	ResultModel<Page<Customer>> queryPage(@RequestBody Page<Customer> page,@RequestParam("userId") String userId){		
		EntityWrapper<Customer> wrapper= new EntityWrapper<Customer>();
		 wrapper.eq("is_del", "0");
		 Page<Customer>  dataPage=null;
		if(null != page.getCondition()) {
			Map<String,Object> condition= page.getCondition();
			String  commonName=condition.get("commonName")==null?"":condition.get("commonName").toString();
		    String  commonPhone=condition.get("commonPhone")==null?"":condition.get("commonPhone").toString();
			if(StringUtil.isNotBlank(commonName)||StringUtil.isNotBlank(commonPhone)){
				  List<Customer>  list=customerService.queryNameAndPhone(commonName, commonPhone,page);
				  dataPage=new Page<Customer>();
				  dataPage.setTotal(customerService.queryNameAndPhoneTotal(commonName, commonPhone));
				  dataPage.setRecords(list);
			}else{
				if(condition.get("custName") != null) {
					wrapper.like("cust_name", condition.get("custName").toString());
				}				
				if(condition.get("validPhone") != null) {
					wrapper.like("valid_phone", (String)condition.get("validPhone")).or().like("other_phone", (String)condition.get("validPhone"));
				}			
				if(condition.get("custType") != null) {
					wrapper.eq("cust_type", condition.get("custType"));
				}
				if(condition.get("ifNormal") != null) {
					wrapper.eq("if_normal", condition.get("ifNormal"));
				}
				if(condition.get("wxStatus") != null) {
					wrapper.eq("wx_status", condition.get("wxStatus"));
				}
				if(condition.get("provinceName") != null) {
					wrapper.eq("province_name", condition.get("provinceName"));
				}
				if(condition.get("cityName") != null) {
					wrapper.eq("city_name", condition.get("cityName"));
				}
				if(condition.get("areaName") != null) {
					wrapper.eq("area_name", condition.get("areaName"));
				}
				if(condition.get("remark") != null) {
					wrapper.like("remark", condition.get("remark").toString());
				}
				if(condition.get("custAdminNickname") != null) {
					wrapper.like("cust_admin_nickname", condition.get("custAdminNickname").toString());
				}
				 page.getCondition().clear();
				 dataPage = customerService.selectPage(page,wrapper);
			}
					
		   
		}else{
			 dataPage = customerService.selectPage(page,wrapper);
		}
		
		   
		return new ResultModel<Page<Customer>>(ResultStatus.SUCCESS, dataPage);
	}
	
	
	
	 
	//获取所有的客户
	@PostMapping("/get-all-cust")
	ResultModel<List<Customer>> getAllCust(){		
		EntityWrapper<Customer> wrapper= new EntityWrapper<Customer>(); 
		 wrapper.eq("is_del", "0");
		return new ResultModel<List<Customer>>(ResultStatus.SUCCESS, customerService.selectList(wrapper));
	}
	
	
	
	@RequestMapping(value = "/get-cust-by-rel-phone", method = RequestMethod.GET)
	ResultModel<CContactPeoRel> getCusteByRelPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CContactPeoRel> entityWrapper = new EntityWrapper<CContactPeoRel>();
		entityWrapper.eq("CONTACT_PHONE", phone);
		CContactPeoRel custPhone = cContactPeoRelService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<CContactPeoRel>(ResultStatus.EMPTY_DATA, null);
		}
		return new ResultModel<CContactPeoRel>(ResultStatus.SUCCESS, custPhone);
	}

	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "/get-cust-by-phone", method = RequestMethod.GET)
	public ResultModel<Customer> getCustomerByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CContactPeoRel> entityWrapper = new EntityWrapper<CContactPeoRel>();
		entityWrapper.eq("CONTACT_PHONE", phone);
		CContactPeoRel custPhone = iCContactPeoRelService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<Customer>(ResultStatus.EMPTY_DATA, null);
		}
		Customer cust = customerService.selectById(custPhone.getCustomerId());
		return new ResultModel<Customer>(ResultStatus.SUCCESS, cust);
	}

	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "/get-cust-by-vail-other-phone", method = RequestMethod.GET)
	public ResultModel<Customer> getCustomerByvailOrOherPhone(@RequestParam("phone") String phone) {
			Customer customer=null;
			if(StringUtil.isNotBlank(phone)){
				 EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();
				 curapper.eq("is_del", "0");
				 curapper.like("valid_phone", phone).or().like("other_phone", phone);
				   customer=customerService.selectOne(curapper);
			}
		return new ResultModel<Customer>(ResultStatus.SUCCESS, customer);
	}

	
	
	@GetMapping(value = "/get-cust-by-id")
	public ResultModel<Customer> getCustomerById(@RequestParam("id") String id) {	
		return new ResultModel<Customer>(ResultStatus.SUCCESS,customerService.selectById(id));
	}
   /**
	 * redis数据初始化
	 * 
	 * **/
	@GetMapping(value = "/cust-redis-init")
	public ResultModel<Boolean> custRedisInit() {	
		EntityWrapper<CustWx> cust= new EntityWrapper<CustWx>(); 
	    cust.isNull("c_id");
		List<CustWx> custList=iCustWxService.selectList(cust);
		if(custList.size()>0){
			Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
			for(CustWx cu:custList){
				 String strPhone=StringUtil.str_Phone(cu.getWxRemark());
				 EntityWrapper<Customer> wrapper= new EntityWrapper<Customer>(); 
				 wrapper.eq("is_del", "0");
				 wrapper.like("valid_phone", strPhone);
				 List<Customer> customer=customerService.selectList(wrapper);//针对老数据处理
				 Customer custom=null;
				 if(customer.size()>0){
					 custom=customer.get(0);
				 }else{
					//新数据都会存在c_contact_peo_rel				 
					 EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
					 Wrapper.eq("CONTACT_PHONE", strPhone);
					 CContactPeoRel  dataPage=cContactPeoRelService.selectOne(Wrapper);
					 custom=customerService.selectById(dataPage.getCustomerId());
				 }
				  if(custom!=null){
					 cu.setUserId(custom.getCustAdminUserId());
					 cu.setcId(custom.getId());
					 iCustWxService.updateById(cu);
					 String[] str=new String[2];
		    		 str[0]=custom.getId();
		    		 str[1]=custom.getCustAdminUserId();
		    	     map.put(cu.getWxId()+"-"+cu.getSysWxId(),  JSON.toJSONString(str))	;
		    	     redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1); 
				 }
			 }
		}		
	  	return new ResultModel<Boolean>(ResultStatus.SUCCESS,true);
	}
	
	
	
	/**
	 * 创建人员电话得联系人及备注信息
	 * */
	@PostMapping(value = "/cust-create-peoRel")
	public void createPeoRel(@RequestBody Customer customer){
		  //查询用户数据			 
	  
		 EntityWrapper<CContactPeo> cConWrapper = new EntityWrapper<CContactPeo>();
		  cConWrapper.eq("CUSTOMER_ID", customer.getId());
		  CContactPeo cCon=cContactPeoService.selectOne(cConWrapper);
		  if(cCon==null){
			    CContactPeo  cContactPeo=new CContactPeo();
				cContactPeo.setCreateTime(new Date());
				cContactPeo.setCustomerId(customer.getId());  	
				cContactPeo.setUseFlag(1);
				cContactPeo.setContactName(customer.getCustName());
				cContactPeo.setContactId(UuidUtil.get32UUID());
				cContactPeoService.insert(cContactPeo);
				String vaildPhone=customer.getValidPhone();
				String otherPhone=customer.getOtherPhone();
				List<String> vailStr=new ArrayList<String>();
				List<String> otherStr=new ArrayList<String>();
				
				if(StringUtils.isNotBlank(vaildPhone)){
					 vailStr=StringUtil.str_all_Phone(vaildPhone);
					if(vailStr.size()>0){
						for(int i=0;i<vailStr.size();i++){
							CContactPeoRel  cContactPeoRel=new CContactPeoRel();
							String[] str=PhoneUtil.getAttr(vailStr.get(i));  	    
				    		cContactPeoRel.setPhoneArea(str[0]);
							cContactPeoRel.setPhoneAtt(str[1]);
						 	cContactPeoRel.setContactId(cContactPeo.getContactId());
							cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
							cContactPeoRel.setContactPhone(vailStr.get(i));
							cContactPeoRel.setCustomerId(customer.getId());
							cContactPeoRel.setPhoneDefault("默认");//是否默认手机号 
							cContactPeoRel.setContactState("正常");//状态0分离1正常
							cContactPeoRel.setWxType("企业号");	
							cContactPeoRel.setUseFlag(1);
							 
							iCContactPeoRelService.insert(cContactPeoRel);
						}
						
					}
					 
				}
				if(StringUtils.isNotBlank(otherPhone)){
					 otherStr=StringUtil.str_all_Phone(otherPhone);
					if(otherStr.size()>0){
						for(int i=0;i<otherStr.size();i++){		
							CContactPeoRel  cContactPeoRel=new CContactPeoRel();
		    			     String[] str=PhoneUtil.getAttr(otherStr.get(i));  	
		    			    cContactPeoRel.setPhoneArea(str[0]);
							cContactPeoRel.setPhoneAtt(str[1]);
							cContactPeoRel.setContactId(cContactPeo.getContactId());
							cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
							cContactPeoRel.setContactPhone(otherStr.get(i));
							cContactPeoRel.setCustomerId(customer.getId());
							cContactPeoRel.setPhoneDefault("默认");//是否默认手机号   0 是 1否
							cContactPeoRel.setContactState("正常");//状态0分离1正常
							cContactPeoRel.setWxType("企业号");	
							cContactPeoRel.setUseFlag(1);

							iCContactPeoRelService.insert(cContactPeoRel);
						}
						
					}
					 
				} 
			 
				
			} 
				customer.setIfNormal("正常");
				customerService.updateById(customer);
			 
	}
	
	/**
	 * 异常数据处理
	 * **/
	@GetMapping(value = "/sjcl")
	public ResultModel<Boolean> sjcl() {
		//先查询 电话异常数据		 
		 updateCustData(customerService.queryPhoneCf());		 
		 //处理姓名重复	 
		 updateCustData(customerService.queryNameCf());
     	 return new ResultModel<Boolean>(ResultStatus.SUCCESS,true);
		
	}
	
	public  void updateCustData( List<Customer>  list) {
		if(list.size()>0){
			 for(Customer cu:list){
				 if(cu!=null&&StringUtil.isNotBlank(cu.getValidPhone())){
					  EntityWrapper<Customer> custWrapper = new EntityWrapper<Customer>();
					  custWrapper.eq("is_del", "0");
					  custWrapper.eq("valid_phone",cu.getValidPhone());
					  List<Customer>  custList=customerService.selectList(custWrapper);
					  if(custList.size()>0){
						  String customerId=custList.get(0).getId();
						  for(int i=0;i<custList.size();i++){
							  
							  Customer culi =custList.get(i);							  
							  //首先去查询微信
							  EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
							  entityWrapper.eq("c_id",culi.getId());	 							
							  List<CustWx>  wxFriendsList=iCustWxService.selectList(entityWrapper);
							  if(wxFriendsList.size()>0){
								  for(CustWx wxList:wxFriendsList){
									  wxList.setcId(customerId);
									  iCustWxService.updateById(wxList);
								  }
							  }
							   
							  //去查询通话记录
							  EntityWrapper<CCallLog> logWrapper = new EntityWrapper<CCallLog>();
							  logWrapper.eq("CUSTOMER_ID",culi.getId());
							  List<CCallLog>  callList=callLogService.selectList(logWrapper);
							  if(callList.size()>0){
								  for(CCallLog wxList:callList){
									  wxList.setCustomerId(customerId);
									  callLogService.updateById(wxList);
								  }
							  }
							  
							  if(i!=0){
								  //culi.setIfNormal("正常"); 
								  culi.setIsDel("1");
								  customerService.updateById(culi);
							  }else{
								  //culi.setIfNormal("正常"); 
								  customerService.updateById(culi);
							  }
							  
						  }
					  }
				 }
			 }
		 }
	}
	/**
	 * 配货站人员数据同步
	 * **/
	@GetMapping(value = "/cust-distribution")
	public ResultModel<Boolean> custDistribution() {	
		
	 
		try {
			//查询系统所有业务员
			 List<SysUser>  userList=iSysUserService.selectList(new EntityWrapper<SysUser>()); 
			 Map<Object,String> usermap=new HashMap<Object, String>();
			 for(SysUser ur:userList){
				 usermap.put(ur.getUsername(),ur.getId());
			 }			 
		     //查询配货站里所有的用户数据
			 Integer in=customerService.getMaxTid()==null?0:customerService.getMaxTid();
			 List<Customer> custList=jdbcUtil.get_tbl_all_bcustomer(in+"");	
			 if(custList.size()>0){
				 for(Customer customer:custList){
					 customer.setId(UuidUtil.get32UUID());				 
					 if(usermap.get(customer.getCustAdminNickname())!=null){	//存储业务员id				 
						 customer.setCustAdminUserId(usermap.get(customer.getCustAdminNickname()));
					 }else{//默认设置为空
						 customer.setCustAdminUserId(null); 
					 }	
					 if(StringUtils.isBlank(customer.getCustType())){
						 customer.setCustType("其他");
					 }
					 if(StringUtil.isNotBlank(customer.getValidPhone())){
						 if(customer.getValidPhone().length()!=11){
							 customer.setIfNormal("电话异常");//默   
						 }else{
							 customer.setIfNormal("正常");//默   
						 }
						
					 }else{
						 customer.setIfNormal("电话异常");//默   
					 }
    				 customer.setWxStatus("缺少微信");
					 customerService.insert(customer);				
				 }
			 }  
		
			 	 //根据备注电话 去查询用户微信号  去同步更新微信号 如果能查到说明数据正常
		 	   EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
			  entityWrapper.isNull("c_id").or().eq("c_id","");	 
			  //entityWrapper.eq("wx_id","wxid_ai1lyes8p2x422");	
			 // entityWrapper.eq("sys_wx_id","wxid_wj9qo6gvftnk22");	
			  
			 // entityWrapper.like("wx_remark", "1"); 
			 List<CustWx>  wxFriendsList=iCustWxService.selectList(entityWrapper);
			// Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);  
			 if(wxFriendsList.size()>0){
				 for(CustWx cu:wxFriendsList){
					 String strPhone=StringUtil.str_Phone(cu.getWxRemark());
					 if(!StringUtil.isBlank(strPhone)){
						 EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();
						 curapper.eq("is_del", "0");
						 curapper.like("valid_phone", strPhone).or().like("other_phone", strPhone);
						 Customer customer=customerService.selectOne(curapper);
							 if(customer!=null){
								 String[] custr=new String[2];
								 custr[0]=customer.getId();
								 custr[1]=customer.getCustAdminUserId();
								 //map.put(cu.getWxId(), JSON.toJSONString(custr));	
								 cu.setcId(customer.getId());	
								 cu.setUserId(customer.getCustAdminUserId());
								 iCustWxService.updateById(cu);
								// redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1); 
								 if(customer.getWxStatus().equals("缺少微信")){
									 customer.setWxStatus("正常");
									 customerService.updateById(customer);
								 }
								
								 
								}
							 }						 
					 }
				 }   
		     //查询用户数据	 
				    EntityWrapper<Customer> custWrapper = new EntityWrapper<Customer>();
				    custWrapper.eq("is_del", "0");
				    custWrapper.eq("if_normal", "电话异常");
			   
			   //custWrapper.eq("id", "fffdb3266b5c4558906824d322393eb1");
			  List<Customer>  custPhoneList=customerService.selectList(custWrapper);
			  for(Customer customer:custPhoneList){
				   if(StringUtil.isNotBlank(customer.getValidPhone())){
					   List<String> phoneList=StringUtil.str_all_Phone(customer.getValidPhone());
						  if(phoneList.size()>0){
							  customer.setOtherPhone(customer.getOtherPhone()+"  "+customer.getValidPhone());
							  for(int i=0;i<phoneList.size();i++){
								  if(i==0){
									  customer.setValidPhone(phoneList.get(i));
									  customer.setOtherPhone(customer.getOtherPhone().replaceAll(phoneList.get(i), ""));
								  }else{
									  customer.setOtherPhone(customer.getOtherPhone().replaceAll(phoneList.get(i), "")+"  "+phoneList.get(i));
									 
								  }
							  }
							  customer.setIfNormal("正常");
							  customerService.updateById(customer);
						  }
				   }
				 
				  
			}  
	 	 
		} catch (Exception e) {
			return new ResultModel<Boolean>(ResultStatus.SUCCESS,false);
		}
		
		return new ResultModel<Boolean>(ResultStatus.SUCCESS,true);
	}
 	
	
	@GetMapping(value = "/cust-syn")
	public ResultModel<Boolean> custSyn() {	
		

		return null;
	}
	
	@RequestMapping(value = "/update-cust-by-id", method = RequestMethod.POST)
	public ResultModel<Boolean> updateCustomerById(@RequestBody Customer customer) {			
		return new ResultModel<Boolean>(ResultStatus.SUCCESS, customerService.updateById(customer));
	}
	
	
	@RequestMapping(value = "/get-cust-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerWxByCustId(@RequestParam("cid") String cid,@RequestParam("syswxid") String syswxid) {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		if(StringUtils.isNotBlank(cid)){
			entityWrapper.eq("c_id", cid);
		}
		if(StringUtils.isNotBlank(syswxid)){
			entityWrapper.eq("sys_wx_id", syswxid);
		}
		List<CustWx> list = iCustWxService.selectList(entityWrapper);

		if (null == list || list.size() == 0) {
			return new ResultModel<List<CustWx>>(ResultStatus.EMPTY_DATA, null);
		}

		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}

	@RequestMapping(value = "/get-cust-wx-by-id", method = RequestMethod.GET)
	ResultModel<CustWx> getCustWxById(@RequestParam("id") String id) {
		CustWx custWx = iCustWxService.selectById(id);
		return new ResultModel<CustWx>(ResultStatus.SUCCESS, custWx);
	}

	@RequestMapping(value = "/get-cust-list-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustListWxByCid(@RequestParam("cid") String cid) {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		entityWrapper.eq("c_id", cid);
		List<CustWx> list = iCustWxService.selectList(entityWrapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	@RequestMapping(value = "/get-cust-wx-all", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustWxAll() {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		List<CustWx> list = iCustWxService.selectList(entityWrapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	@RequestMapping(value = "/get-cust-phone-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustPhone>> getCustPhoneByCustId(@RequestParam("cid") String cid) {
		EntityWrapper<CustPhone> entityWrapper = new EntityWrapper<CustPhone>();
		entityWrapper.eq("c_id", cid);
		List<CustPhone> data = custPhoneService.selectList(entityWrapper);

		return new ResultModel<List<CustPhone>>(ResultStatus.SUCCESS, data);
	}

	@RequestMapping(value = "/get-cust-phone-by-id", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneById(@RequestParam("id") String id) {
		CustPhone custPhone = custPhoneService.selectById(id);
		return new ResultModel<CustPhone>(ResultStatus.SUCCESS, custPhone);
	}

	@RequestMapping(value = "/get-cust-phone-by-phone", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CustPhone> entityWrapper = new EntityWrapper<CustPhone>();
		entityWrapper.eq("cust_phone", phone);
		CustPhone custPhone = custPhoneService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<CustPhone>(ResultStatus.EMPTY_DATA, null);
		}
		return new ResultModel<CustPhone>(ResultStatus.SUCCESS, custPhone);
	}
	/**
	 * 用户转交
	 * */
	
	@PostMapping(value = "/user-distri")
	public Boolean usrDistri(@RequestParam("id") String id,@RequestParam("name") String name,@RequestBody List<Customer> list) {
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
		  
		for(Customer li:list){		
		    li.setCustAdminNickname(name);
		    li.setCustAdminUserId(id);		   
			String[] str=new String[2];
			str[0]=li.getId();
			str[1]=li.getCustAdminUserId();
			
			EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
			entityWrapper.eq("c_id", li.getId());
			List<CustWx> wxList=iCustWxService.selectList(entityWrapper);
			if(wxList.size()>0){
				
				for(CustWx cx:wxList){
					 cx.setUserId(li.getCustAdminUserId());
					  iCustWxService.updateById(cx);
					map.put(cx.getWxId()+"-"+cx.getSysWxId(), JSON.toJSONString(str));
				}
				
			}
			
		}
		redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1);
		return customerService.updateAllColumnBatchById(list);
	}
	/**
	 * 保存用户
	 * */
	@PostMapping("/save-cust")
	ResultModel<Customer> saveCustomer(@RequestBody IMCustomerVO customerVO) {
		Customer customer = new Customer();
		BeanCopyUtil.copyProperties(customerVO, customer);
		customer.setId(UuidUtil.get32UUID());
		customer.setValidPhone(customerVO.getCustPhone());
		customer.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));

		boolean cnt = customerService.insert(customer);
		if (!cnt) {
			return new ResultModel<Customer>(ResultStatus.FAIL);
		}

		/*CustPhone custPhone = new CustPhone();
		custPhone.setCustPhone(customerVO.getCustPhone());
		custPhone.setIsDefault("1");
		custPhone.setPhoneType("1");
		custPhone.setcId(customer.getId());
		boolean rs = custPhoneService.insert(custPhone);
		if (!rs) {
			return new ResultModel<Customer>(ResultStatus.FAIL);
		}*/

		return new ResultModel<Customer>(ResultStatus.SUCCESS, customer);
	}
	
	@PostMapping("/get-cust-list-wx-by-phone")
	ResultModel<List<CustWx>> getCustListWxByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CustWx> curapper = new EntityWrapper<CustWx>();
		curapper.like("wx_remark", phone);
		List<CustWx> list=iCustWxService.selectList(curapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	
	@PostMapping("/update-cust-list-cid")
	ResultModel<List<CustWx>> updateCustListCid(@RequestParam("cid") String cid,@RequestBody List<CustWx> list) {
		if(list.size()>0){
			for(CustWx li:list){
				//插入成功后 对于新得聊天人员 更新聊天人员归属关系
			  li.setcId(cid);
			  iCustWxService.updateById(li);
			}
		}
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
		
	}
	
	
	@PostMapping("/save-sample-cust")
	ResultModel<Customer> saveSampleCustomer(@RequestBody IMCustomerVO customerVO) {
		
		//查询所归属的业务员
		/*EntityWrapper<SysUserWxInfo>	wrapper =new EntityWrapper<SysUserWxInfo>();
		wrapper.eq("wx_id", customerVO.getSysWxId());
		 SysUserWxInfo  sysList=iSysUserWxInfoService.selectOne(wrapper);		 
		 customerVO.setCustAdminUserId(sysList.getUserId());
	
		 
		EntityWrapper<SysUser>	userWrapper =new EntityWrapper<SysUser>();
		userWrapper.eq("id", sysList.getUserId());		
		SysUser  user=iSysUserService.selectOne(userWrapper); 
		 
		customerVO.setCustAdminNickname(user.getNickname());*/
		//存在手机号时候  先从数据库查一遍 吐过数据库没有 再去从配货站查一遍
		Customer customer=new Customer();
		 EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();
		 if(StringUtil.isNotBlank(customerVO.getCustPhone())){
			 curapper.eq("is_del", "0");
			 curapper.like("valid_phone", customerVO.getCustPhone()).or().like("other_phone", customerVO.getCustPhone());
			  customer=customerService.selectOne(curapper);
			  if(customer==null){
				  customerVO=jdbcUtil.get_tbl_bcustomer(customerVO, customerVO.getCustPhone()); //去配货站库查询数据 
				  customer=new Customer();
			  }
			  
		 } 
		  BeanCopyUtil.copyProperties(customerVO, customer);	
		  boolean cnt=true;
		  if(customer.getId()==null){
			    customer.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
			    customer.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
			    customer.setIsDel("0");
				customer.setId(UuidUtil.get32UUID());
				customer.setWxStatus("正常");
				cnt = customerService.insert(customer);
		  }else{
			  customer.setCustAdminNickname(customerVO.getCustAdminNickname());
			  customer.setCustAdminUserId(customerVO.getCustAdminUserId());
			  customer.setCustWxId(customerVO.getCustWxId());
			  customer.setSysWxId(customerVO.getSysWxId());	
			  customer.setWxStatus("正常");
			  cnt = customerService.updateById(customer);
		  }
	   if (!cnt) {
			return new ResultModel<Customer>(ResultStatus.FAIL);
		}else{
			//插入成功后 对于新得聊天人员 更新聊天人员归属关系
		/*	EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
			entityWrapper.eq("wx_id", customerVO.getCustWxId());
			entityWrapper.eq("sys_wx_id", customerVO.getSysWxId());
			CustWx csutWx=custWxService.selectOne(entityWrapper);
			if(csutWx!=null){
				csutWx.setcId(customer.getId());
				csutWx.setUserId(customer.getCustAdminUserId());
				custWxService.updateById(csutWx);
			}*/
		
		}

		return new ResultModel<Customer>(ResultStatus.SUCCESS, customer);
	}
	
	/**
	 * 新增微信与客户信息关联关系
	 * @param custWx
	 * @return
	 */
	@PostMapping("/save-cust-wx")
	ResultModel<CustWx> saveCustWx(@RequestBody CustWx custWx){
		boolean cnt = true;
		if(custWx.getId()==null){
			iCustWxService.insert(custWx);
		}else{
			iCustWxService.updateById(custWx);
		}
	 
		if (!cnt) {
			return new ResultModel<CustWx>(ResultStatus.FAIL);
		}
		return new ResultModel<CustWx>(ResultStatus.SUCCESS, custWx);
	}
	
	
	@GetMapping("/get-cust-im-role")
	ResultModel<List<CustImRole>> getCustImRole(@RequestParam("userId") String userId,
			@RequestParam("custType") String custType){
		EntityWrapper<CustImRole> wrapper = new EntityWrapper<CustImRole>();
		wrapper.eq("user_id", userId).and().eq("cust_type", custType);
		wrapper.orderBy("sort", true);
		List<CustImRole> data = custImRoleService.selectList(wrapper);
		return new ResultModel<>(ResultStatus.SUCCESS,data);
	}
	
	
	@RequestMapping(value = "/get-cust-wx-list-by-wxid", method = RequestMethod.GET)
	public ResultModel<List<CustWx>> getCustomerWxListByWxId(@RequestParam("wxid") String wxid,@RequestParam("syswxid") String syswxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		if(StringUtils.isNotBlank(wxid)){
			wrapper.eq("wx_id", wxid);
		}
		if(StringUtils.isNotBlank(syswxid)){
			wrapper.eq("sys_wx_id", syswxid);
		}
		
		
		List<CustWx> data= iCustWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,data);
	}

	@RequestMapping(value = "/get-cust-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<CustWx> getCustomerWxByWxId(@RequestParam("wxid") String wxid,@RequestParam("syswxid") String syswxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid).and().eq("sys_wx_id", syswxid);
 		
		CustWx custWx =iCustWxService.selectOne(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
 
	
	@RequestMapping(value = "/get-cust-listwx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxId(@RequestParam("wxid") String wxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid);
 		
		List<CustWx> custWx =iCustWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
	
	@RequestMapping(value = "/get-cust-list-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxIdAndSysWxId(@RequestParam("wxid") String wxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid);
 		
		List<CustWx> custWx =iCustWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
	@RequestMapping(value = "/modify-wx-remark-phone", method = RequestMethod.POST)
	public void modifyRemarkPhone(@RequestParam("cid") String cid,@RequestParam("value") String value,@RequestParam("type") String type){
		Customer custom=customerService.selectById(cid);
		if(type.equals("0")){
			custom.setCustName(value);	
		}
		if(type.equals("1")){
			custom.setValidPhone(value);	
		}
		customerService.updateById(custom);
		
		 
	}
	
	
	
}
