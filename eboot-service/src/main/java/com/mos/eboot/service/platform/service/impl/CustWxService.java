package com.mos.eboot.service.platform.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.service.platform.mapper.CustWxMapper;
import com.mos.eboot.service.platform.mapper.SysMenuMapper;
import com.mos.eboot.service.platform.service.ICustWxService;

/**
 * <p>
 * 客户微信对应关系表 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@Service
public class CustWxService extends ServiceImpl<CustWxMapper, CustWx> implements ICustWxService {
	  @Resource
	    private CustWxMapper custWxMapper;
	@Override
	public Boolean updateCidByWxRemark(@RequestParam("cid")  String cid,@RequestParam("remark")  String remark) {
		return custWxMapper.updateCidByWxRemark(cid,remark);
	}
	@Override
	public List<CustWx> queryCount(String createTimeStart,
			String createTimeEnd, String wxId,String type) {
		 
		return custWxMapper.queryCount(createTimeStart, createTimeEnd, wxId,type);
	}
	 
}
