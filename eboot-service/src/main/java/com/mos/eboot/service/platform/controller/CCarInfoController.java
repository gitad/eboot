package com.mos.eboot.service.platform.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.service.platform.service.impl.CCarInfoRelService;
import com.mos.eboot.service.platform.service.impl.CCarInfoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/cCarInfo")
public class CCarInfoController {
	@Autowired
	CCarInfoService cCarInfoService;
	@Autowired
	CCarInfoRelService cCarInfoRelService;
	
	 	@PostMapping("/query")
		public ResultModel<List<CCarInfo>> query(@RequestBody Page<CCarInfo> page) {
	 		Map<String,Object> condition= page.getCondition();	
			EntityWrapper<CCarInfo>  Wrapper=new EntityWrapper<CCarInfo>();
			 if(condition!=null){
				    if(condition.get("customerId")!=null){
				    	 Wrapper.eq("CUSTOMER_ID", condition.get("customerId").toString());
				    	 condition.remove("customerId");
					}
			 }
		    Wrapper.eq("USE_FLAG", 1);
		    List<CCarInfo>  dataPage=cCarInfoService.selectList(Wrapper);
		    for(CCarInfo ca: dataPage){
		    	EntityWrapper<CCarInfoRel>  wap=new EntityWrapper<CCarInfoRel>();
		    	wap.eq("CAR_ID", ca.getCarId());
			     ca.setRelList(cCarInfoRelService.selectList(wap));
		    }
			 return new ResultModel<List<CCarInfo>>(ResultStatus.SUCCESS, dataPage);
	    }
	 	
	 	
	 	
		 @PostMapping("/queryById")
		 public  CCarInfo  queryById(@RequestParam("carId") String carId) {
			 return cCarInfoService.selectById(carId);			 
		 } 
		 @PostMapping("/queryByCustomerId")
		 public  List<CCarInfo>  queryByCustomerId(@RequestParam("customerId") String customerId) {
			 EntityWrapper<CCarInfo>  wap=new EntityWrapper<CCarInfo>();
		    	wap.eq("CUSTOMER_ID", customerId);
			 return cCarInfoService.selectList(wap);			 
		 } 
		 
		  
		 
		 
		 
 		@PostMapping("/relSave")
		public CCarInfoRel relSave (@RequestBody CCarInfoRel cCarInfoRel) {	
 			  if(StringUtil.isBlank(cCarInfoRel.getCarRelId())){
 				 cCarInfoRel.setCarRelId(UuidUtil.get32UUID());
 				 cCarInfoRel.setCreateTime(new Date());
 				 cCarInfoRelService.insert(cCarInfoRel);
 				
 			  }else{
 				 cCarInfoRel.setUpdateTime(new Date());
 				 cCarInfoRelService.updateById(cCarInfoRel);
 			  }			 
			 return cCarInfoRel;
	    }
 		
 		@PostMapping("/save")
		public CCarInfo save (@RequestBody CCarInfo  cCarInfo ) {	
 			  if(StringUtil.isBlank(cCarInfo.getCarId())){
 				 cCarInfo.setCarId(UuidUtil.get32UUID());
 				
 				 cCarInfoService.insert(cCarInfo);			
 			  }else{
 				 cCarInfo.setUpdateTime(new Date());
 				 cCarInfoService.updateById(cCarInfo);
 			  }			 
			 return cCarInfo;
	    }
 		 @PostMapping("/del")
 		 public Boolean del(@RequestBody   CCarInfo cCarInfo) {
 			Boolean bool= cCarInfoService.deleteById(cCarInfo);
 			return bool;
 	    }

 		@PostMapping("/relDel")
		public CCarInfoRel relDel (@RequestBody CCarInfoRel cCarInfoRel) {	
 			  cCarInfoRelService.deleteById(cCarInfoRel.getCarRelId());		 
			 return cCarInfoRel;
	    }
 		
 		
 		@PostMapping("/queryByEntity")
 		public CCarInfo queryByEntity(@RequestBody  CCarInfo  cCarInfo) {
 			CCarInfo  result=null;
 			EntityWrapper<CCarInfo>  wrapper=new EntityWrapper<CCarInfo>();
 			if(cCarInfo!=null){
 				 wrapper.eq("CAR_LINCESE", cCarInfo.getCarLincese()).or().eq("CAR_TRAVEL_NUM", cCarInfo.getCarTravelNum());	
 				 result=cCarInfoService.selectOne(wrapper);
 			}
 			
 			 
 			 return result;
 		}		 
}
