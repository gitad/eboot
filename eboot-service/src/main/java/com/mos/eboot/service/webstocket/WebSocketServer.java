package com.mos.eboot.service.webstocket;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mos.eboot.platform.entity.IncomingPhoneConf;
import com.mos.eboot.service.platform.service.IIncomingPhoneConfService;

@ServerEndpoint("/webSocket/{cid}")
@Component
public class WebSocketServer {

	private static ConcurrentHashMap<String, Session> sessionPools = new ConcurrentHashMap<>();
	
	@Autowired
	private IIncomingPhoneConfService iIncomingPhoneConfService;
	
	public void initIncomingPhoneSocket() {
		List<IncomingPhoneConf> list =iIncomingPhoneConfService.selectList(null);
	}
	
	// 建立连接成功调用
	@OnOpen
	public void onOpen(Session session, @PathParam(value = "cid") String cid) {
		sessionPools.put(cid, session);
		try {
			session.getBasicRemote().sendText("");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 关闭连接时调用
	@OnClose
	public void onClose(@PathParam(value = "cid") String cid) {
		sessionPools.remove(cid);
	}

	// 收到客户端信息
	@OnMessage
	public void onMessage(String message) throws IOException {
		message = "客户端：" + message + ",已收到";
		System.out.println(message);
		for (Session session : sessionPools.values()) {
			try {
				session.getBasicRemote().sendText(message);
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
		}
	}

	// 错误时调用
	@OnError
	public void onError(Session session, Throwable throwable) {
		System.out.println("发生错误");
		throwable.printStackTrace();
	}
}
