package com.mos.eboot.service.platform.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.service.platform.mapper.SysUserWxInfoMapper;
import com.mos.eboot.service.platform.service.ISysUserWxInfoService;

@Service
public class SysUserWxInfoServiceImpl extends ServiceImpl<SysUserWxInfoMapper, SysUserWxInfo>
		implements ISysUserWxInfoService {

	@Autowired
	private SysUserWxInfoMapper sysUserWxInfoMapper;

	@Override
	public List<SysUserWxInfo> getWxByUserId(String userId) {
		// TODO Auto-generated method stub
		return sysUserWxInfoMapper.getWxByUserId(userId);
	}

	@Override
	public Boolean deleteByUserId(String id) {
		// TODO Auto-generated method stub
		return sysUserWxInfoMapper.deleteByUserId(id);
	}

	@Override
	public Boolean updClientIdByTerminalId(String terminalId, String clientId) {
		// TODO Auto-generated method stub
		Map<String,String> map =new HashMap<String,String>();
		map.put("terminalId", terminalId);
		map.put("clientId", clientId);
		return sysUserWxInfoMapper.updClientIdByTerminalId(map);
	}

}
