package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.service.platform.mapper.CCallLogMapper;
import com.mos.eboot.service.platform.mapper.CustomerMapper;
import com.mos.eboot.service.platform.service.ICCallLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCallLogService extends ServiceImpl<CCallLogMapper, CCallLog> implements ICCallLogService {
	@Autowired
	private CCallLogMapper cCallLogMapper;

	@Override
	public int getMaxTid() {
		Integer num=0;
		try {
			  num=cCallLogMapper.getMaxTid();
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
		return num;
	}
}
