package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.service.platform.mapper.CCarInfoRelMapper;
import com.mos.eboot.service.platform.service.ICCarInfoRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCarInfoRelService extends ServiceImpl<CCarInfoRelMapper, CCarInfoRel> implements ICCarInfoRelService {
	
}
