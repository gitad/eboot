package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.UserIncomingPhone;
import com.mos.eboot.service.platform.mapper.UserIncomingPhoneMapper;
import com.mos.eboot.service.platform.service.IUserIncomingPhoneService;

/**
 * <p>
 * 用户来电手机关联关系 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
@Service
public class UserIncomingPhoneService extends ServiceImpl<UserIncomingPhoneMapper, UserIncomingPhone> implements IUserIncomingPhoneService {
	
}
