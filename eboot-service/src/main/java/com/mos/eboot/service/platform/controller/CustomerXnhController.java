package com.mos.eboot.service.platform.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerXnh;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.service.config.RedisUtil;
import com.mos.eboot.service.config.jdbcUtil;
import com.mos.eboot.service.platform.service.ICContactPeoRelService;
import com.mos.eboot.service.platform.service.ICustImRoleService;
import com.mos.eboot.service.platform.service.ICustPhoneService;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.ICustomerXnhService;
import com.mos.eboot.service.platform.service.ISysUserRoleService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.service.platform.service.ISysUserWxInfoService;
import com.mos.eboot.service.platform.service.ISysuserCustomerMgrService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.service.platform.service.impl.CContactPeoRelService;
import com.mos.eboot.service.platform.service.impl.CContactPeoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.tools.util.PhoneUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.IMCustomerVO;

/**
 * <p>
 * 用户虚拟号表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-22
 */
@RestController
@RequestMapping("/customerXnh")
public class CustomerXnhController {

	private Boolean type=true;//标记是否继续进行更新区号
	@Autowired
	private ICustomerXnhService customerService;
	
	@Autowired
	ICustomerService customerSe;

	@Autowired
	private ICustPhoneService custPhoneService;

	@Autowired
	private ICustWxService custWxService;
	
	@Autowired
	private ICustImRoleService custImRoleService;

	@Autowired
	private ISysuserCustomerMgrService iSysuserCustomerMgrService;
	
	@Autowired
	ISysUserWxInfoService iSysUserWxInfoService;
	
	@Autowired
    private ISysUserRoleService userRoleService;
	
	@Autowired
	private RedisUtil redisUtil;
	
	@Autowired
    ISysUserService iSysUserService;
	
	@Autowired
	ICContactPeoRelService iCContactPeoRelService;
 
	@Autowired
	ICustWxService iCustWxService;
	
	 @Autowired
	 CContactPeoService cContactPeoService;
	 
	 @Autowired
	 CContactPeoRelService cContactPeoRelService;
	 
	 @Autowired
	IWxLoginService iWxLoginService;
		
	@PostMapping("/query-cust-by-entity")
	List<CustomerXnh> getCustByEntity(@RequestBody  CustomerXnh customer){		
		EntityWrapper<CustomerXnh> entity=new EntityWrapper<CustomerXnh>();
		 
		entity.eq("sys_wx_id", customer.getSysWxId());
		List<CustomerXnh> custList=customerService.selectList(entity );	
		return custList;	
	}
	
	   /**
  * 虚拟号转换
  * */
  
 @RequestMapping("/customerZh")
 public ResultModel<Boolean> customerZh(@RequestBody List<CustomerXnh> culist){
  
 	for(CustomerXnh li:culist ){
 		Customer reqData = JSON.parseObject(JSON.toJSONString(li), Customer.class);
 	     customerSe.insert(reqData);
       	 customerService.deleteById(li.getId());
 	}
 	
 	return new ResultModel<Boolean>(ResultStatus.SUCCESS, true);
 } 
	@PostMapping("/query-page")
	ResultModel<Page<CustomerXnh>> queryPage(@RequestBody Page<CustomerXnh> page,@RequestParam("userId") String userId){		
		EntityWrapper<CustomerXnh> wrapper= new EntityWrapper<CustomerXnh>();
		if(null != page.getCondition()) {
			Map<String,Object> condition= page.getCondition();
			if(condition.get("custName") != null) {
				wrapper.like("cust_name", condition.get("custName").toString());
			}
			
			if(condition.get("validPhone") != null) {
				wrapper.like("valid_phone", (String)condition.get("validPhone")).or().like("other_phone", (String)condition.get("validPhone"));
			}
			
			if(condition.get("custType") != null) {
				wrapper.eq("cust_type", condition.get("custType"));
			}
			if(condition.get("ifNormal") != null) {
				wrapper.eq("if_normal", condition.get("ifNormal"));
			}
			if(condition.get("provinceName") != null) {
				wrapper.eq("province_name", condition.get("provinceName"));
			}
			if(condition.get("cityName") != null) {
				wrapper.eq("city_name", condition.get("cityName"));
			}
			if(condition.get("areaName") != null) {
				wrapper.eq("area_name", condition.get("areaName"));
			}
			page.getCondition().clear();
		}
		 Page<CustomerXnh>  dataPage = customerService.selectPage(page,wrapper);
		  
		return new ResultModel<Page<CustomerXnh>>(ResultStatus.SUCCESS, dataPage);
	}
	
	//获取所有的客户
	@PostMapping("/get-all-cust")
	ResultModel<List<CustomerXnh>> getAllCust(){		
		EntityWrapper<CustomerXnh> wrapper= new EntityWrapper<CustomerXnh>(); 
		return new ResultModel<List<CustomerXnh>>(ResultStatus.SUCCESS, customerService.selectList(wrapper));
	}
	
	
	
	@RequestMapping(value = "/get-cust-by-rel-phone", method = RequestMethod.GET)
	ResultModel<CContactPeoRel> getCusteByRelPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CContactPeoRel> entityWrapper = new EntityWrapper<CContactPeoRel>();
		entityWrapper.eq("CONTACT_PHONE", phone);
		CContactPeoRel custPhone = cContactPeoRelService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<CContactPeoRel>(ResultStatus.EMPTY_DATA, null);
		}
		return new ResultModel<CContactPeoRel>(ResultStatus.SUCCESS, custPhone);
	}

	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "/get-cust-by-phone", method = RequestMethod.GET)
	public ResultModel<CustomerXnh> getCustomerByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CContactPeoRel> entityWrapper = new EntityWrapper<CContactPeoRel>();
		entityWrapper.eq("CONTACT_PHONE", phone);
		CContactPeoRel custPhone = iCContactPeoRelService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<CustomerXnh>(ResultStatus.EMPTY_DATA, null);
		}
		CustomerXnh cust = customerService.selectById(custPhone.getCustomerId());
		return new ResultModel<CustomerXnh>(ResultStatus.SUCCESS, cust);
	}

	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "/get-cust-by-vail-other-phone", method = RequestMethod.GET)
	public ResultModel<CustomerXnh> getCustomerByvailOrOherPhone(@RequestParam("phone") String phone) {
		 CustomerXnh customer=null;
		if(StringUtil.isNotBlank(phone)){
			 EntityWrapper<CustomerXnh> curapper = new EntityWrapper<CustomerXnh>();
			 curapper.like("valid_phone", phone).or().like("other_phone", phone);
			   customer=customerService.selectOne(curapper);
		}
		
		return new ResultModel<CustomerXnh>(ResultStatus.SUCCESS, customer);
	}

	
	
	@GetMapping(value = "/get-cust-by-id")
	public ResultModel<CustomerXnh> getCustomerById(@RequestParam("id") String id) {	
		return new ResultModel<CustomerXnh>(ResultStatus.SUCCESS,customerService.selectById(id));
	}
	
	
	

	
	
	/**
	 * redis数据初始化
	 * **/
	@GetMapping(value = "/cust-redis-init")
	public ResultModel<Boolean> custRedisInit() {	
		
		EntityWrapper<CustWx> cust= new EntityWrapper<CustWx>(); 
	    cust.isNull("c_id");
		List<CustWx> custList=iCustWxService.selectList(cust);
		if(custList.size()>0){
			 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
			for(CustWx cu:custList){
				 String strPhone=StringUtil.str_Phone(cu.getWxRemark());
				 EntityWrapper<CustomerXnh> wrapper= new EntityWrapper<CustomerXnh>(); 
				 wrapper.like("valid_phone", strPhone);
				 List<CustomerXnh> customer=customerService.selectList(wrapper);//针对老数据处理
				  CustomerXnh custom=null;
				 if(customer.size()>0){
					 custom=customer.get(0);
				 }else{
					//新数据都会存在c_contact_peo_rel				 
					 EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
					 Wrapper.eq("CONTACT_PHONE", strPhone);
					 CContactPeoRel  dataPage=cContactPeoRelService.selectOne(Wrapper);
					 custom=customerService.selectById(dataPage.getCustomerId());
				 }
				
				 if(custom!=null){
					 cu.setUserId(custom.getCustAdminUserId());
					 cu.setcId(custom.getId());
					 iCustWxService.updateById(cu);
					 String[] str=new String[2];
		    		 str[0]=custom.getId();
		    		 str[1]=custom.getCustAdminUserId();
		    	     map.put(cu.getWxId()+"-"+cu.getSysWxId(),  JSON.toJSONString(str))	;
		    	     redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1); 
				 }
				 
			}
		}		
	 
    	
    	return new ResultModel<Boolean>(ResultStatus.SUCCESS,true);
	}
	
	
	
	/**
	 * 创建人员电话得联系人及备注信息
	 * */
	@PostMapping(value = "/cust-create-peoRel")
	public void createPeoRel(@RequestBody  CustomerXnh customer){
		  //查询用户数据			 
	  
		 EntityWrapper<CContactPeo> cConWrapper = new EntityWrapper<CContactPeo>();
		  cConWrapper.eq("CUSTOMER_ID", customer.getId());
		  CContactPeo cCon=cContactPeoService.selectOne(cConWrapper);
		  if(cCon==null){
			    CContactPeo  cContactPeo=new CContactPeo();
				cContactPeo.setCreateTime(new Date());
				cContactPeo.setCustomerId(customer.getId());  	
				cContactPeo.setUseFlag(1);
				cContactPeo.setContactName(customer.getCustName());
				cContactPeo.setContactId(UuidUtil.get32UUID());
				cContactPeoService.insert(cContactPeo);
				String vaildPhone=customer.getValidPhone();
				String otherPhone=customer.getOtherPhone();
				List<String> vailStr=new ArrayList<String>();
				List<String> otherStr=new ArrayList<String>();
				
				if(StringUtils.isNotBlank(vaildPhone)){
					 vailStr=StringUtil.str_all_Phone(vaildPhone);
					if(vailStr.size()>0){
						for(int i=0;i<vailStr.size();i++){
							CContactPeoRel  cContactPeoRel=new CContactPeoRel();
							  String[] str=PhoneUtil.getAttr(vailStr.get(i));  	    
				    			 cContactPeoRel.setPhoneArea(str[0]);
								cContactPeoRel.setPhoneAtt(str[1]);
						 	cContactPeoRel.setContactId(cContactPeo.getContactId());
							cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
							cContactPeoRel.setContactPhone(vailStr.get(i));
							cContactPeoRel.setCustomerId(customer.getId());
							cContactPeoRel.setPhoneDefault("默认");//是否默认手机号 
							cContactPeoRel.setContactState("正常");//状态0分离1正常
							cContactPeoRel.setWxType("企业号");	
							cContactPeoRel.setUseFlag(1);
							 
							iCContactPeoRelService.insert(cContactPeoRel);
						}
						
					}
					 
				}
				if(StringUtils.isNotBlank(otherPhone)){
					 otherStr=StringUtil.str_all_Phone(otherPhone);
					if(otherStr.size()>0){
						for(int i=0;i<otherStr.size();i++){		
							CContactPeoRel  cContactPeoRel=new CContactPeoRel();
		    			     String[] str=PhoneUtil.getAttr(otherStr.get(i));  	
		    			    cContactPeoRel.setPhoneArea(str[0]);
							cContactPeoRel.setPhoneAtt(str[1]);
							cContactPeoRel.setContactId(cContactPeo.getContactId());
							cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
							cContactPeoRel.setContactPhone(otherStr.get(i));
							cContactPeoRel.setCustomerId(customer.getId());
							cContactPeoRel.setPhoneDefault("默认");//是否默认手机号   0 是 1否
							cContactPeoRel.setContactState("正常");//状态0分离1正常
							cContactPeoRel.setWxType("企业号");	
							cContactPeoRel.setUseFlag(1);

							iCContactPeoRelService.insert(cContactPeoRel);
						}
						
					}
					 
				} 
				if(vailStr.size()==0&&otherStr.size()==0){
					customer.setIfNormal("2");
					customerService.updateById(customer);
				}
				
			}
	}
	 	
	
	@GetMapping(value = "/cust-syn")
	public ResultModel<Boolean> custSyn() {	
		

		return null;
	}
	
	@RequestMapping(value = "/update-cust-by-id", method = RequestMethod.POST)
	public ResultModel<Boolean> updateCustomerById(@RequestBody  CustomerXnh customer) {			
		return new ResultModel<Boolean>(ResultStatus.SUCCESS, customerService.updateById(customer));
	}
	
	
	@RequestMapping(value = "/get-cust-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerWxByCustId(@RequestParam("cid") String cid,@RequestParam("syswxid") String syswxid) {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		if(StringUtils.isNotBlank(cid)){
			entityWrapper.eq("c_id", cid);
		}
		if(StringUtils.isNotBlank(syswxid)){
			entityWrapper.eq("sys_wx_id", syswxid);
		}
		List<CustWx> list = custWxService.selectList(entityWrapper);

		if (null == list || list.size() == 0) {
			return new ResultModel<List<CustWx>>(ResultStatus.EMPTY_DATA, null);
		}

		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}

	@RequestMapping(value = "/get-cust-wx-by-id", method = RequestMethod.GET)
	ResultModel<CustWx> getCustWxById(@RequestParam("id") String id) {
		CustWx custWx = custWxService.selectById(id);
		return new ResultModel<CustWx>(ResultStatus.SUCCESS, custWx);
	}

	@RequestMapping(value = "/get-cust-list-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustListWxByCid(@RequestParam("cid") String cid) {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		entityWrapper.eq("c_id", cid);
		List<CustWx> list = custWxService.selectList(entityWrapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	@RequestMapping(value = "/get-cust-wx-all", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustWxAll() {
		EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		List<CustWx> list = custWxService.selectList(entityWrapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	@RequestMapping(value = "/get-cust-phone-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustPhone>> getCustPhoneByCustId(@RequestParam("cid") String cid) {
		EntityWrapper<CustPhone> entityWrapper = new EntityWrapper<CustPhone>();
		entityWrapper.eq("c_id", cid);
		List<CustPhone> data = custPhoneService.selectList(entityWrapper);

		return new ResultModel<List<CustPhone>>(ResultStatus.SUCCESS, data);
	}

	@RequestMapping(value = "/get-cust-phone-by-id", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneById(@RequestParam("id") String id) {
		CustPhone custPhone = custPhoneService.selectById(id);
		return new ResultModel<CustPhone>(ResultStatus.SUCCESS, custPhone);
	}

	@RequestMapping(value = "/get-cust-phone-by-phone", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CustPhone> entityWrapper = new EntityWrapper<CustPhone>();
		entityWrapper.eq("cust_phone", phone);
		CustPhone custPhone = custPhoneService.selectOne(entityWrapper);
		if (null == custPhone) {
			return new ResultModel<CustPhone>(ResultStatus.EMPTY_DATA, null);
		}
		return new ResultModel<CustPhone>(ResultStatus.SUCCESS, custPhone);
	}
	/**
	 * 用户转交
	 * */
	
	@PostMapping(value = "/user-distri")
	public Boolean usrDistri(@RequestParam("id") String id,@RequestParam("name") String name,@RequestBody List<CustomerXnh> list) {
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
		  
		for(CustomerXnh li:list){		
		    li.setCustAdminNickname(name);
		    li.setCustAdminUserId(id);		   
			String[] str=new String[2];
			str[0]=li.getId();
			str[1]=li.getCustAdminUserId();
			
			EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
			entityWrapper.eq("c_id", li.getId());
			List<CustWx> wxList=custWxService.selectList(entityWrapper);
			if(wxList.size()>0){
				
				for(CustWx cx:wxList){
					 cx.setUserId(li.getCustAdminUserId());
					  iCustWxService.updateById(cx);
					map.put(cx.getWxId()+"-"+cx.getSysWxId(), JSON.toJSONString(str));
				}
				
			}
			
		}
		redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1);
		return customerService.updateAllColumnBatchById(list);
	}
	/**
	 * 保存用户
	 * */
	@PostMapping("/save-cust")
	ResultModel<CustomerXnh> saveCustomer(@RequestBody IMCustomerVO customerVO) {
		CustomerXnh customer = new CustomerXnh();
		BeanCopyUtil.copyProperties(customerVO, customer);
		customer.setId(UuidUtil.get32UUID());
		customer.setValidPhone(customerVO.getCustPhone());
		customer.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));

		boolean cnt = customerService.insert(customer);
		if (!cnt) {
			return new ResultModel<CustomerXnh>(ResultStatus.FAIL);
		}
 

		return new ResultModel<CustomerXnh>(ResultStatus.SUCCESS, customer);
	}
	
	@PostMapping("/get-cust-list-wx-by-phone")
	ResultModel<List<CustWx>> getCustListWxByPhone(@RequestParam("phone") String phone) {
		EntityWrapper<CustWx> curapper = new EntityWrapper<CustWx>();
		curapper.like("wx_remark", phone);
		List<CustWx> list=custWxService.selectList(curapper);
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
	}
	
	@PostMapping("/update-cust-list-cid")
	ResultModel<List<CustWx>> updateCustListCid(@RequestParam("cid") String cid,@RequestBody List<CustWx> list) {
		if(list.size()>0){
			for(CustWx li:list){
				//插入成功后 对于新得聊天人员 更新聊天人员归属关系
			  li.setcId(cid);
				custWxService.updateById(li);
			}
		}
		return new ResultModel<List<CustWx>>(ResultStatus.SUCCESS, list);
		
	}
	
	
	@PostMapping("/save-sample-cust")
	ResultModel<CustomerXnh> saveSampleCustomer(@RequestBody IMCustomerVO customerVO) {
		
		//查询所归属的业务员
		/*EntityWrapper<SysUserWxInfo>	wrapper =new EntityWrapper<SysUserWxInfo>();
		wrapper.eq("wx_id", customerVO.getSysWxId());
		 SysUserWxInfo  sysList=iSysUserWxInfoService.selectOne(wrapper);		 
		 customerVO.setCustAdminUserId(sysList.getUserId());
	
		 
		EntityWrapper<SysUser>	userWrapper =new EntityWrapper<SysUser>();
		userWrapper.eq("id", sysList.getUserId());		
		SysUser  user=iSysUserService.selectOne(userWrapper); 
		 
		customerVO.setCustAdminNickname(user.getNickname());*/
		//存在手机号时候  先从数据库查一遍 吐过数据库没有 再去从配货站查一遍
		CustomerXnh customer=new CustomerXnh();
		 EntityWrapper<CustomerXnh> curapper = new EntityWrapper<CustomerXnh>();
		 if(StringUtil.isNotBlank(customerVO.getCustPhone())){
			 curapper.like("valid_phone", customerVO.getCustPhone()).or().like("other_phone", customerVO.getCustPhone());
			  customer=customerService.selectOne(curapper);
			  if(customer==null){
				  customerVO=jdbcUtil.get_tbl_bcustomer(customerVO, customerVO.getCustPhone()); //去配货站库查询数据 
				  customer=new CustomerXnh();
			  }
			  
		 } 
		  BeanCopyUtil.copyProperties(customerVO, customer);	
		  boolean cnt=true;
		  if(customer.getId()==null){
			    customer.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
			    customer.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
				customer.setId(UuidUtil.get32UUID());
				customer.setWxStatus("正常");
				cnt = customerService.insert(customer);
		  }else{
			  customer.setCustAdminNickname(customerVO.getCustAdminNickname());
			  customer.setCustAdminUserId(customerVO.getCustAdminUserId());
			  customer.setCustWxId(customerVO.getCustWxId());
			  customer.setSysWxId(customerVO.getSysWxId());	
			  customer.setWxStatus("正常");
			  cnt = customerService.updateById(customer);
		  }
	   if (!cnt) {
			return new ResultModel<CustomerXnh>(ResultStatus.FAIL);
		}else{
			//插入成功后 对于新得聊天人员 更新聊天人员归属关系
			EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
			entityWrapper.eq("wx_id", customerVO.getCustWxId());
			entityWrapper.eq("sys_wx_id", customerVO.getSysWxId());
			CustWx csutWx=custWxService.selectOne(entityWrapper);
			if(csutWx!=null){
				csutWx.setcId(customer.getId());
				csutWx.setUserId(customer.getCustAdminUserId());
				custWxService.updateById(csutWx);
			}
		
		}

		return new ResultModel<CustomerXnh>(ResultStatus.SUCCESS, customer);
	}
	
	/**
	 * 新增微信与客户信息关联关系
	 * @param custWx
	 * @return
	 */
	@PostMapping("/save-cust-wx")
	ResultModel<CustWx> saveCustWx(@RequestBody CustWx custWx){
		boolean cnt = true;
		if(custWx.getId()==null){
			custWxService.insert(custWx);
		}else{
			custWxService.updateById(custWx);
		}
	 
		if (!cnt) {
			return new ResultModel<CustWx>(ResultStatus.FAIL);
		}
		return new ResultModel<CustWx>(ResultStatus.SUCCESS, custWx);
	}
	
	
	@GetMapping("/get-cust-im-role")
	ResultModel<List<CustImRole>> getCustImRole(@RequestParam("userId") String userId,
			@RequestParam("custType") String custType){
		EntityWrapper<CustImRole> wrapper = new EntityWrapper<CustImRole>();
		wrapper.eq("user_id", userId).and().eq("cust_type", custType);
		wrapper.orderBy("sort", true);
		List<CustImRole> data = custImRoleService.selectList(wrapper);
		return new ResultModel<>(ResultStatus.SUCCESS,data);
	}
	
	
	@RequestMapping(value = "/get-cust-wx-list-by-wxid", method = RequestMethod.GET)
	public ResultModel<List<CustWx>> getCustomerWxListByWxId(@RequestParam("wxid") String wxid,@RequestParam("syswxid") String syswxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		if(StringUtils.isNotBlank(wxid)){
			wrapper.eq("wx_id", wxid);
		}
		if(StringUtils.isNotBlank(syswxid)){
			wrapper.eq("sys_wx_id", syswxid);
		}
		
		
		List<CustWx> data= custWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,data);
	}

	@RequestMapping(value = "/get-cust-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<CustWx> getCustomerWxByWxId(@RequestParam("wxid") String wxid,@RequestParam("syswxid") String syswxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid).and().eq("sys_wx_id", syswxid);
 		
		CustWx custWx =custWxService.selectOne(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
 
	
	@RequestMapping(value = "/get-cust-listwx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxId(@RequestParam("wxid") String wxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid);
 		
		List<CustWx> custWx =custWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
	
	@RequestMapping(value = "/get-cust-list-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxIdAndSysWxId(@RequestParam("wxid") String wxid){
		EntityWrapper<CustWx> wrapper= new EntityWrapper<CustWx>();
		wrapper.eq("wx_id", wxid);
 		
		List<CustWx> custWx =custWxService.selectList(wrapper);
		
		return new ResultModel<>(ResultStatus.SUCCESS,custWx);
	}
	
	@RequestMapping(value = "/modify-wx-remark-phone", method = RequestMethod.POST)
	public void modifyRemarkPhone(@RequestParam("cid") String cid,@RequestParam("value") String value,@RequestParam("type") String type){
		CustomerXnh custom=customerService.selectById(cid);
		if(type.equals("0")){
			custom.setCustName(value);	
		}
		if(type.equals("1")){
			custom.setValidPhone(value);	
		}
		customerService.updateById(custom);
		
		 
	}
	
}
