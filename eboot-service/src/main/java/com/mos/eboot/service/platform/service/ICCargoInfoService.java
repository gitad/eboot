package com.mos.eboot.service.platform.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CCargoInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface ICCargoInfoService extends IService<CCargoInfo> {
	 
}
