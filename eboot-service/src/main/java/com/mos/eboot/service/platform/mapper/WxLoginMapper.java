package com.mos.eboot.service.platform.mapper;

 import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.WxLogin;

/**
 * <p>
  * 微信登录用户表 Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-11
 */
public interface WxLoginMapper extends BaseMapper<WxLogin> {

}