package com.mos.eboot.service.platform.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.service.config.jdbcUtil;
import com.mos.eboot.service.platform.service.ISysUserRoleService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.shiro.utils.PasswordHelper;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.vo.ChartVO;

/**
 * @author 小尘哥
 */
@RestController
@RequestMapping("api/user")
public class UserController  extends BaseController{

    @Resource
    private ISysUserService userService;

    @Resource
    private ISysUserRoleService userRoleService;

    @ApiOperation(value = "获取用户信息",notes = "根据用户名获取用户")
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query")
    @RequestMapping(value = "get-by-name",method = RequestMethod.GET)
    public SysUser getByUsername(@RequestParam("username")String username){
        EntityWrapper<SysUser> entityWrapper = new EntityWrapper<>();
        entityWrapper.eq("username",username);
        SysUser  user= userService.selectOne(entityWrapper);
        return user;
    }
    @RequestMapping(value = "get-by-nick-name",method = RequestMethod.GET)
    public SysUser getByUserNickname(@RequestParam("nickname")String nickname){
        EntityWrapper<SysUser> entityWrapper = new EntityWrapper<>();
        entityWrapper.eq("nickname",nickname);
        SysUser  user= userService.selectOne(entityWrapper);
        return user;
    }
   
	@GetMapping(value = "/get-by-token")
	public SysUser getByUserToken(@RequestParam("token") String token) {
		EntityWrapper<SysUser> entityWrapper = new EntityWrapper<SysUser>();
		entityWrapper.eq("token", token);
		return userService.selectOne(entityWrapper);
	}
	
	@PostMapping(value = "/get-tbl-all-user")
	public List<SysUser> getTblAllUser() {
		List<SysUser> list=jdbcUtil.get_tbl_all_user();
		return list;
	}
	
	
	@PostMapping(value = "/get-all-user")
	public List<SysUser> getAllUser() {
		List<SysUser> list=userService.selectList(new EntityWrapper<SysUser>());
		return list;
	}

	
	@GetMapping(value = "/check-by-name-token")
	public SysUser checkByUserNameToken(@RequestParam("name") String name,@RequestParam("token") String token) {
		EntityWrapper<SysUser> entityWrapper = new EntityWrapper<SysUser>();
		entityWrapper.eq("username", name);
		SysUser user=userService.selectOne(entityWrapper);
		SysUser tbluser=jdbcUtil.get_tbl_user(name, token);
		if(user==null){//为空则创建数据
		    user=tbluser;
		    //user.setId(UuidUtil.get32UUID());;//主键
		    user.setPassword(new PasswordHelper().encrypt(user.getPassword()));
		    user.setIsDel(Constants.NEGATIVE);
    	    user.setCreateTime(new Date());
    	    user.setDisabled(Constants.NEGATIVE);
    	    user.setLocked(Constants.NEGATIVE);
			userService.saveOrUpdate(user);	
			tbluser.setId(user.getId());
		}else{//不为空则进行密码更新
			 user.setPassword(new PasswordHelper().encrypt(tbluser.getPassword()));
			 userService.saveOrUpdate(user);		
		} 
		 
		return  tbluser;
	}
	
 

	@ApiOperation(value = "新增|修改用户信息",notes = "新增|修改用户信息")
    @RequestMapping(value = "save-or-update",method = RequestMethod.POST)
    public ResultModel<String> saveOrUpdate(@ApiParam("用户对象") @RequestBody SysUser sysUser){
		boolean result=userService.saveOrUpdate(sysUser);
        return this.basicResult(result);
    }

    @ApiOperation(value = "删除用户",notes = "根据用户id删除用户")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "String", paramType = "query")
    @RequestMapping(value = "del",method = RequestMethod.POST)
    public ResultModel<String> deleteById(@RequestParam("id")String id){
        return this.basicResult(userService.deleteById(id));
    }

    @ApiOperation(value = "分页查询",notes = "分页查询用户")
    @RequestMapping(value = "query-page",method = RequestMethod.POST)
    public ResultModel<Page<SysUser>> queryPage(@ApiParam(value = "分页对象") @RequestBody Page<SysUser> page){
    	 
    	
        return new ResultModel<>(ResultStatus.SUCCESS,userService.queryPage(page));
    }

    @ApiOperation(value = "获取用户",notes = "根据用户id获取用户")
    @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "String", paramType = "query")
    @RequestMapping(value = "get-by-id",method = RequestMethod.GET)
    public ResultModel<SysUser> getById(@RequestParam("id")String id){
        SysUser user = userService.selectById(id);
        if (user != null){
            user.setRoleIds(userRoleService.getRoleIds(id));
        }
        return new ResultModel<>(ResultStatus.SUCCESS, user);
    }

    @ApiOperation(value = "登录统计",notes = "登录统计")
	@RequestMapping(value = "login-count",method = RequestMethod.GET)
	public ResultModel<List<ChartVO>> loginCount(){
		return new ResultModel<>(ResultStatus.SUCCESS,userService.loginCount());
	}

    @ApiOperation(value = "条件查询用户",notes = "条件查询用户")
	@RequestMapping(value = "all",method = RequestMethod.POST)
	public ResultModel<List<SysUser>> all(@ApiParam @RequestBody SysUser user){
		return new ResultModel<>(ResultStatus.SUCCESS,userService.getAll(user));
	}
    

	
    
}
