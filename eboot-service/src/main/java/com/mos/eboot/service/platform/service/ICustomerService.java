package com.mos.eboot.service.platform.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.Customer;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface ICustomerService extends IService<Customer> {
	
	public int saveCustomer(Customer customer);
	public int updateCustomer(Customer customer);
	public int updateTemCustomer(Customer customer);
	
	public Integer getMaxTid();
	
	
	Integer queryNameAndPhoneTotal(@Param("commonName")   String  commonName,
			 @Param("commonPhone")   String  commonPhone);
	 List<Customer> queryNameAndPhone(@Param("commonName")   String  commonName,
			 @Param("commonPhone")   String  commonPhone, Page<Customer> page);
	 
	 List<Customer> queryNameCf();
	 List<Customer> queryPhoneCf();
	 
}
