package com.mos.eboot.service.platform.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.SysuserCustomerMgr;
import com.mos.eboot.service.platform.service.impl.CContactPeoRelService;
import com.mos.eboot.service.platform.service.impl.CContactPeoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cContactPeoRel")
public class CContactPeoRelController {

	 @Autowired
	 CContactPeoRelService cContactPeoRelService;
	 
	 @PostMapping("/query")
	@ResponseBody
	public ResultModel<Page<CContactPeoRel>> query(@RequestBody  Page<CContactPeoRel> page) {
		Map<String,Object> condition= page.getCondition();
		EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
	 
		Page<CContactPeoRel> dataPage=cContactPeoRelService.selectPage(page, Wrapper);		
		return new ResultModel<Page<CContactPeoRel>>(ResultStatus.SUCCESS, dataPage);
   }
	 @PostMapping("/queryPhone")
	@ResponseBody
	public ResultModel<CContactPeoRel> queryPhone(@RequestParam("phone") String  phone) {
	 
		EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
		Wrapper.eq("CONTACT_PHONE", phone);
		 CContactPeoRel  dataPage=cContactPeoRelService.selectOne(Wrapper);
		return new ResultModel<CContactPeoRel>(ResultStatus.SUCCESS, dataPage);
   }

	
	@PostMapping("/save")
	@ResponseBody
	public CContactPeoRel save(@RequestBody  CContactPeoRel  cContactPeoRel) {	
		 boolean result;
		if(cContactPeoRel.getContactRelId()!=null){
			 result=cContactPeoRelService.updateById(cContactPeoRel);
		}else{
			 result=cContactPeoRelService.insert(cContactPeoRel);	
		}
	     	
	    return  cContactPeoRel;
	}
	
	@PostMapping("/del")
	@ResponseBody
	public ResultModel<Boolean>  del( @RequestBody  CContactPeoRel  cContactPeoRel) {
		 
	    boolean result=cContactPeoRelService.deleteById(cContactPeoRel.getContactRelId());
	    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
	}
	
	
	@PostMapping("/quer-by-entity")
	@ResponseBody
	public ResultModel<CContactPeoRel>  querByEntity( @RequestBody  CContactPeoRel  cContactPeoRel) {
		EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
		 if(cContactPeoRel.getCustomerId()!=null){
			 Wrapper.eq("CUSTOMER_ID", cContactPeoRel.getCustomerId());
		 }
		 if(cContactPeoRel.getContactPhone()!=null){
			 Wrapper.eq("CONTACT_PHONE", cContactPeoRel.getContactPhone());
		 }
	 
	    return new ResultModel<CContactPeoRel>(ResultStatus.SUCCESS, cContactPeoRelService.selectOne(Wrapper));
	}
	
	@PostMapping("/queryById")
	@ResponseBody
	public  CContactPeoRel queryById(@RequestParam("contactRelId")  String  contactRelId) {
		CContactPeoRel  result=cContactPeoRelService.selectById(contactRelId);	
		   return result;
	}
	@PostMapping("/queryByContactId")
	@ResponseBody
	public  List<CContactPeoRel> queryByContactId(@RequestParam("contactId")  String  contactId) {
		 EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
		 Wrapper.eq("CONTACT_ID",contactId );
		 List<CContactPeoRel>  result=cContactPeoRelService.selectList(Wrapper);	
		 return result;
	}

}
