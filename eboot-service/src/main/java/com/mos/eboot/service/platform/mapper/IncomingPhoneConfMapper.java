package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.IncomingPhoneConf;

/**
 * <p>
  * 来电弹屏手机号配置信息 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
public interface IncomingPhoneConfMapper extends BaseMapper<IncomingPhoneConf> {

}