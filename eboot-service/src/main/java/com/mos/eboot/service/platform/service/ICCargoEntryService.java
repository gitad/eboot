package com.mos.eboot.service.platform.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CCargoEntry;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface ICCargoEntryService extends IService<CCargoEntry> {
	 
	List<CCargoEntry> queryByAddress(String address);
	
	List<CCargoEntry> querySource(String  addressDetail,
			String  customerPrice,
			String  carsAndGoods,
			String  planTime_start,
			String  planTime_end,
			String  cargoType, Page<CCargoEntry> page
			);
	Integer querySourceTotal(String  addressDetail,
			String  customerPrice,
			String  carsAndGoods,
			String  planTime_start,
			String  planTime_end,
			String  cargoType
			);
}
