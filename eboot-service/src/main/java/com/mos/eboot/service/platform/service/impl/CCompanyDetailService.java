package com.mos.eboot.service.platform.service.impl;

 
import com.mos.eboot.platform.entity.CCompanyDetail;
import com.mos.eboot.service.platform.mapper.CCompanyDetailMapper;
import com.mos.eboot.service.platform.service.ICCompanyDetailService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@Service
public class CCompanyDetailService extends ServiceImpl<CCompanyDetailMapper, CCompanyDetail> implements ICCompanyDetailService {
	
}
