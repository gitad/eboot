package com.mos.eboot.service.platform.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.service.platform.mapper.CustomerMapper;
import com.mos.eboot.service.platform.service.ICustomerService;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@Service
public class CustomerService extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

	@Autowired
	private CustomerMapper customerMapper;

	@Override
	public int saveCustomer(Customer customer) {
		return customerMapper.saveCustomer(customer);
	}
	@Override
	public int updateCustomer(Customer customer) {
		return customerMapper.updateCustomer(customer);
	}
	@Override
	public int updateTemCustomer(Customer customer) {
		return customerMapper.updateTemCustomer(customer);
	}
	
	@Override
	public List<Customer> queryNameAndPhone(@Param("commonName")   String  commonName,
			 @Param("commonPhone")   String  commonPhone, Page<Customer> page) {
	  return customerMapper.queryNameAndPhone(commonName,commonPhone,page);
	}
	
	@Override
	public Integer queryNameAndPhoneTotal(@Param("commonName")   String  commonName,
			 @Param("commonPhone")   String  commonPhone) {
	  return customerMapper.queryNameAndPhoneTotal(commonName,commonPhone);
	}
	@Override
	public Integer getMaxTid() {
		Integer num=0;
		try {
			  num=customerMapper.getMaxTid();
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
		return num;
	}
	@Override
	public List<Customer> queryNameCf() {
		
		return customerMapper.queryNameCf();
	}
	@Override
	public List<Customer> queryPhoneCf() {
		// TODO Auto-generated method stub
		return customerMapper.queryPhoneCf();
	}
}
