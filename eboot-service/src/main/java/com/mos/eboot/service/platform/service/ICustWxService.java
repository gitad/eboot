package com.mos.eboot.service.platform.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CustWx;

/**
 * <p>
 * 客户微信对应关系表 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface ICustWxService extends IService<CustWx> {
	Boolean updateCidByWxRemark(String cid,String remark);
	List<CustWx> queryCount(@RequestParam("createTimeStart")   String  createTimeStart,
			 @RequestParam("createTimeEnd")   String  createTimeEnd,
			 @RequestParam("wxId")   String  wxId, @RequestParam("type")   String  type);
}
