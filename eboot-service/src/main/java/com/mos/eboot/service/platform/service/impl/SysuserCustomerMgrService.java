package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.SysuserCustomerMgr;
import com.mos.eboot.service.platform.mapper.SysuserCustomerMgrMapper;
import com.mos.eboot.service.platform.service.ISysuserCustomerMgrService;

/**
 * <p>
 * 系统用户管理客户表 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-06-14
 */
@Service
public class SysuserCustomerMgrService extends ServiceImpl<SysuserCustomerMgrMapper, SysuserCustomerMgr> implements ISysuserCustomerMgrService {
	
}
