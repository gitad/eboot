package com.mos.eboot.service.platform.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 微信登录用户表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-11
 */
@Controller
@RequestMapping("/wxLogin")
public class WxLoginController {
	
}
