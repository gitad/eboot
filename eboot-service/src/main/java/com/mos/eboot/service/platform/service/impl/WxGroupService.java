package com.mos.eboot.service.platform.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.WxGroup;
import com.mos.eboot.service.platform.mapper.WxGroupMapper;
import com.mos.eboot.service.platform.service.IWxGroupService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-17
 */
@Service
public class WxGroupService extends ServiceImpl<WxGroupMapper, WxGroup> implements IWxGroupService {
	@Autowired
	private WxGroupMapper wxGroupMapper;

	@Override
	public List<WxGroup> queryGroup(String groupName,String remark,String carLen,
			String carType,String profit,String owerName,Page<WxGroup> page){ 
		 return wxGroupMapper.queryGroup( groupName, remark, carLen, carType, profit, owerName,page);
	}
}
