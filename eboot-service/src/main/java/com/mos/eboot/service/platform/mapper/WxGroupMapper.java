package com.mos.eboot.service.platform.mapper;

 
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.WxGroup;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-17
 */
public interface WxGroupMapper extends BaseMapper<WxGroup> {
	List<WxGroup> queryGroup(@Param("groupName")  String groupName,@Param("remark")  String remark,@Param("carLen")  String carLen,
			@Param("carType")  String carType,@Param("profit")  String profit,@Param("owerName")  String owerName,Page<WxGroup> page);
}