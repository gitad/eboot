package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.PhoneCallOutLog;
import com.mos.eboot.service.platform.mapper.PhoneCallOutLogMapper;
import com.mos.eboot.service.platform.service.IPhoneCallOutLogService;

/**
 * <p>
 * 自动拨打电话记录 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-08-17
 */
@Service
public class PhoneCallOutLogService extends ServiceImpl<PhoneCallOutLogMapper, PhoneCallOutLog> implements IPhoneCallOutLogService {
	
}
