package com.mos.eboot.service.platform.controller;


import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.service.config.RedisUtil;
import com.mos.eboot.service.platform.service.ICustImRoleService;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.service.platform.service.impl.CCustomerMarketService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 * 客户营销表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
@RestController
@RequestMapping("/cCustomerMarket")
public class CCustomerMarketController {
	@Autowired
	CCustomerMarketService customerMarketService;
	
	@Autowired
	IWxLoginService  wxLoginService;
	
	@Autowired
	private ICustomerService customerService;
	@Autowired
	RedisUtil redisUtil;
	@Autowired
	private ICustImRoleService custImRoleService;
	@Autowired
	private ICustWxService custWxService;
	
	 @PostMapping("/query")
		public ResultModel<Page<CCustomerMarket>> query(@RequestBody  Page<CCustomerMarket> page) {
		    Map<String,Object> condition= page.getCondition();	
			EntityWrapper<CCustomerMarket>  Wrapper=new EntityWrapper<CCustomerMarket>();
			  if(condition!=null){
				  if(condition.get("customerId")!=null){
				    	 Wrapper.eq("CUSTOMER_ID", condition.get("customerId").toString());
				    	 condition.remove("customerId");
					} 
				  if(condition.get("useFlag")!=null){
				    	 Wrapper.eq("USE_FLAG", condition.get("useFlag").toString());
				    	 condition.remove("useFlag");
					}
			  }
			Page<CCustomerMarket> dataPage=customerMarketService.selectPage(page, Wrapper);
		 
			return new ResultModel<Page<CCustomerMarket>>(ResultStatus.SUCCESS, dataPage); 
		 
	    }
		 
	 @PostMapping("/save")
		public ResultModel<CCustomerMarket>  save(@RequestBody  CCustomerMarket  customerMarket) {
			 boolean result; 	
			if(StringUtils.isBlank(customerMarket.getMarketId())){			 
				customerMarket.setMarketId(UuidUtil.get32UUID());
				result=customerMarketService.insert(customerMarket);	
			}else{		
				result=customerMarketService.updateById(customerMarket)	;
			}
			 
		    return new ResultModel<CCustomerMarket>(ResultStatus.SUCCESS, customerMarket);
		}
		
		@PostMapping("/del")
		public ResultModel<Boolean>  del(@RequestBody CCustomerMarket  customerMarket) { 
	 
		    boolean result=customerMarketService.deleteById(customerMarket.getMarketId());
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		} 
		
		
	
}
