package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.service.platform.mapper.CustImRoleMapper;
import com.mos.eboot.service.platform.service.ICustImRoleService;

/**
 * <p>
 * IM弹框窗口操作菜单权限 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@Service
public class CustImRoleService extends ServiceImpl<CustImRoleMapper, CustImRole> implements ICustImRoleService {
	
}
