package com.mos.eboot.service.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.platform.entity.CustWx;

/**
 * <p>
  * 客户微信对应关系表 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface CustWxMapper extends BaseMapper<CustWx> {
	Boolean updateCidByWxRemark(@Param("cid")String cid,@Param("remark")String remark);
	
	
	List<CustWx>  queryCount(@Param("createTimeStart")   String  createTimeStart, 
			    @Param("createTimeEnd")   String  createTimeEnd,
			    @Param("wxId")   String wxId,   @Param("type")   String type);

}