package com.mos.eboot.service.platform.service;

import com.mos.eboot.platform.entity.CContactPeo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface ICContactPeoService extends IService<CContactPeo> {
	
}
