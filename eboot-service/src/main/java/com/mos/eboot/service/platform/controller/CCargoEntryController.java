package com.mos.eboot.service.platform.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.service.platform.service.impl.CCargoEntryRelService;
import com.mos.eboot.service.platform.service.impl.CCargoEntryService;
import com.mos.eboot.service.platform.service.impl.CCargoInfoService;
import com.mos.eboot.service.platform.service.impl.CustomerService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/cCargoEntry")
public class CCargoEntryController {
	@Autowired
	CCargoInfoService cCargoInfoService;
	
	@Autowired
	CCargoEntryService cCargoEntryService;
	 
	@Autowired
	CCargoEntryRelService cCargoEntryRelService;
	@Autowired
	CustomerService customerService;
	 @PostMapping("/query")
	 public ResultModel<Page<CCargoEntry>> query(@RequestBody  Page<CCargoEntry> page) {
		 Map<String,Object> condition= page.getCondition();	
		EntityWrapper<CCargoEntry>  Wrapper=new EntityWrapper<CCargoEntry>();	 	
	    Wrapper.eq("USE_FLAG", 1); 
	    if(condition!=null){
	    	 if(condition.get("customerId")!=null){
		    	 Wrapper.eq("CUSTOMER_ID", condition.get("customerId").toString());
		    	 condition.remove("customerId");
			}
	    	 if(condition.get("planTime")!=null){//定时任务用到plantime  根据plantime筛选数据
	    		  String startTime=condition.get("planTime").toString();
		    	   String endTime=condition.get("planTime").toString();
		 		    Date newEnd=DateUtil.string_date(endTime);
		 			Calendar   calendar = new GregorianCalendar(); 
		 			calendar.setTime(newEnd); 
		 			calendar.add(calendar.DATE,1); //把日期往后增加一天,整数  往后推,负数往前移动 
		 			newEnd=calendar.getTime(); //这个时间就是日期往后推一天的结果 
		 			endTime=DateUtil.date_string(newEnd);
		 		 	  
			    	 Wrapper.ge("PLAN_TIME",startTime).
					 and().le("PLAN_TIME",endTime);
			    	 condition.remove("planTime");
			}
	    }
		Page<CCargoEntry> dataPage=cCargoEntryService.selectPage(page, Wrapper);
		  
		return new ResultModel<Page<CCargoEntry>>(ResultStatus.SUCCESS, dataPage);
    }

	 
	 @PostMapping("/queryById")
	 public   CCargoEntry  queryById(@RequestParam("cargoId") String cargoId) {
		 CCargoEntry  dataPage=cCargoEntryService.selectById(cargoId);
		 return dataPage;
    }

	 @PostMapping("/saveCCargoEntry")
	 public   CCargoEntry saveCCargoEntry(@RequestBody CCargoEntry  cargoEntry) {
		 
		 boolean  dataPage=cCargoEntryService.updateById(cargoEntry);
		 return cargoEntry;
    } 
	 
	 	@PostMapping("/queryPirce")
		@ResponseBody
		public  ResultModel<List<CCargoInfo>> queryPirce(@RequestParam Map<String, String> map){		 		 
	 		List<CCargoInfo> list=cCargoInfoService.queryPirce(map.get("start"),map.get("end"),map.get("carLens"),map.get("type"));	 		
	 		return new ResultModel<List<CCargoInfo>>(ResultStatus.SUCCESS, list);
	 	}
	 
		 @PostMapping("/queryForm")
		 public ResultModel<List<CCargoEntryRel>> queryForm( @RequestParam("cargoId") String cargoId,@RequestParam("type") String type) {
			EntityWrapper<CCargoEntryRel>  Wrapper=new EntityWrapper<CCargoEntryRel>();	 	
		    Wrapper.eq("CARGO_ID",cargoId); 
		    Wrapper.eq("TRANSPORT_TYPE",type); 
		    Wrapper.orderBy("ORDER_BY"); 
			List<CCargoEntryRel> list=  cCargoEntryRelService.selectList(Wrapper);
			 
			return new ResultModel<List<CCargoEntryRel>>(ResultStatus.SUCCESS, list);
	    }

	 	
	 	/**
	 	 * 货源调度查询
	 	 * **/
	 	 @PostMapping("/query-source")
		 public ResultModel<Page<CCargoEntry>> querySource(@RequestBody  Page<CCargoEntry> page) {
	 		Map<String, Object> map=page.getCondition();
	 		String addressDetail="";
	 		String customerPrice="";
	 		String carsAndGoods="";
	 		String planTime_start="";
	 		String planTime_end= "";
	 		String cargoType= "";
	 		if(map!=null){
	 			  addressDetail= map.get("addressDetail")==null?"":map.get("addressDetail").toString();
		 		  customerPrice= map.get("customerPrice")==null?"":map.get("customerPrice").toString();
		 		  carsAndGoods= map.get("carsAndGoods")==null?"":map.get("carsAndGoods").toString();
		 		  planTime_start= map.get("planTime_start")==null?"":map.get("planTime_start").toString();
		 		  planTime_end= map.get("planTime_end")==null?"":map.get("planTime_end").toString();
		 		  cargoType= map.get("cargoType")==null?"":map.get("cargoType").toString();		 		
	 		}
	 		List<CCargoEntry> list=cCargoEntryService.querySource(addressDetail,customerPrice,carsAndGoods,planTime_start,planTime_end,cargoType,page);
	 		 page.setTotal(cCargoEntryService.querySourceTotal(addressDetail, customerPrice, carsAndGoods, planTime_start, planTime_end, cargoType));
	 		for(CCargoEntry cr:list){
	 		   //Integer all=cCargoInfoService.queryCars(cr.getCargoId(), "1");
	 	 	   EntityWrapper<CCargoEntryRel>  stwrapper=new EntityWrapper<CCargoEntryRel>();//开始
	    	   stwrapper.eq("CARGO_ID",cr.getCargoId()); 
	    	   stwrapper.eq("TRANSPORT_TYPE","0"); 
	    	   stwrapper.orderBy("ORDER_BY"); 
			   CCargoEntryRel st= cCargoEntryRelService.selectOne(stwrapper); 
			   if(st==null){
				   st=new  CCargoEntryRel();
			   }
			   
			   
			   EntityWrapper<CCargoEntryRel>  endwrapper=new EntityWrapper<CCargoEntryRel>();//开始
			   endwrapper.eq("CARGO_ID",cr.getCargoId()); 
			   endwrapper.eq("TRANSPORT_TYPE","1"); 
			   endwrapper.orderBy("ORDER_BY"); 
			   CCargoEntryRel end= cCargoEntryRelService.selectOne(endwrapper); 
			   if(end==null){
				   end=new  CCargoEntryRel();
			   }
			   
			    
			   EntityWrapper<CCargoInfo>  cargowrapper=new EntityWrapper<CCargoInfo>();//货源匹配   
			   
			   cargowrapper.like("ADDRESS_DETAIL_STR", st.getCityCode());
			   cargowrapper.like("ADDRESS_DETAIL_END", end.getCityCode());
			   cargowrapper.like("CAR_LEN", cr.getCarLen());
			   cargowrapper.like("CAR_TYPE", cr.getCarType());
			   cargowrapper.lt("CREATE_TIME",  new SimpleDateFormat("yyyy-MM-dd").format(new Date()) );
			   List<CCargoInfo> li= cCargoInfoService.selectList(cargowrapper);
			   Integer all=0;
			   if(li!=null){
				   all=li.size(); 
			   }
	 			
	 			
	 			
	 		   Integer  priceAll=cCargoInfoService.queryCars(cr.getCargoId(), "2");
	 		   cr.setpCar(all);
	 		   cr.setpPriceCar(priceAll);	 		  
	 		}	 		 
	 		page.setRecords(list);
			return new ResultModel<Page<CCargoEntry>>(ResultStatus.SUCCESS, page);
	    }
	 	@PostMapping("/saveAll")
		@ResponseBody
		public   ResultModel<CCargoEntry> saveAll(@RequestParam Map<String, String> map){
	 		SysUser user=JSONObject.parseObject(map.get("SysUser"), SysUser.class);
	 		String customerId=map.get("customerId");
	 		String phone=map.get("phone");
	 		String  UUID=UuidUtil.get32UUID();
	 		CCargoEntry cargo=JSONObject.parseObject(map.get("mainData"), CCargoEntry.class);
	 		if(StringUtil.isBlank(customerId)){//代表这条数据是从货源调度修改过来  customerId需要重新赋值
	 			customerId=cargo.getCustomerId();
	 		}
	 		List<CCargoEntryRel> startForm=  JSONArray.parseArray(map.get("startForm"),CCargoEntryRel.class );
	 		List<CCargoEntryRel> endForm=  JSONArray.parseArray(map.get("endForm"),CCargoEntryRel.class );	 		 
	 			if(StringUtil.isNotBlank(cargo.getCargoId())){	
	 				UUID=cargo.getCargoId();
	 			}
	 			//起点
		 		cargo.setCustomerId(customerId);
		 		cargo.setCreateUser(user.getNickname());
		 		cargo.setCreateTime(new Date());		 	
		 		cargo.setUseFlag(1);
		 		Customer cust=	customerService.selectById(customerId);
		 		cargo.setPhoneNum(StringUtil.str_Phone(phone));
		 		cargo.setCustName(cust.getCustName());
		 		String  start="",end="";
		 		for(int i=0;i<startForm.size();i++){//起点
		 			CCargoEntryRel st=startForm.get(i);
		 			st.setCargoId(UUID);
		 			st.setCreateUser(user.getNickname());
		 			st.setCreateTime(new Date());
		 			st.setOrderBy(i);
		 			st.setTransportType(0);
		 			if(start.equals("")){
		 				start+=st.getProvinceCode()+"-"+st.getCityCode()+"-"+st.getAreaCode();
		 			}else{
		 				start+=","+st.getProvinceCode()+"-"+st.getCityCode()+"-"+st.getAreaCode();
		 			}
		 			if(StringUtil.isNotBlank(st.getCargoRelId())){
		 				cCargoEntryRelService.updateById(st);
		 			}else{
		 				st.setCargoRelId(UuidUtil.get32UUID());	
		 				cCargoEntryRelService.insert(st);
		 			}
		 			 
		 		}
		 		for(int i=0;i<endForm.size();i++){//起点
		 			CCargoEntryRel st=endForm.get(i);
		 	 
		 			st.setCargoId(UUID);
		 			st.setCreateUser(user.getNickname());
		 			st.setCreateTime(new Date());
		 			st.setOrderBy(i);
		 			st.setTransportType(1);
		 			if(end.equals("")){
		 				end+=st.getProvinceCode()+"-"+st.getCityCode()+"-"+st.getAreaCode();
		 			}else{
		 				end+=","+st.getProvinceCode()+"-"+st.getCityCode()+"-"+st.getAreaCode();
		 			}
		 			if(StringUtil.isNotBlank(st.getCargoRelId())){
		 				cCargoEntryRelService.updateById(st);
		 			}else{
		 				st.setCargoRelId(UuidUtil.get32UUID());	
		 				cCargoEntryRelService.insert(st);
		 			}
		 			
		 		}
		 		
		 		cargo.setAddressDetailStr(start);
		 		cargo.setAddressDetailEnd(end);
			 	cargo.setCarsAndGoods(cargo.getCarUseType()+";"+cargo.getCarLen()+";"+cargo.getWeightStr()+cargo.getWeightEnd()+";"+cargo.getVolumeStr()+cargo.getVolumeEnd()+";"+cargo.getCarType() );  
		 		
			 	if(StringUtil.isNotBlank(cargo.getCargoId())){			 		
			 		cCargoEntryService.updateById(cargo);
		 		}else{
		 			cargo.setCargoId(UUID);
		 			cCargoEntryService.insert(cargo);
		 		}
			 	
		 	return new ResultModel<CCargoEntry>(ResultStatus.SUCCESS, cargo);
	 	}
	 	@PostMapping("/save")
		@ResponseBody
		public   ResultModel<Boolean> save(@RequestParam Map<String, String> map){
	 		SysUser user=JSONObject.parseObject(map.get("SysUser"), SysUser.class);
	 		String dataList=map.get("dataList");
	 		String goodsInfo=map.get("goodsInfo");
	 		String carsInfo=map.get("carsInfo");
	 		String cCargoEntry=map.get("cCargoEntry");
	 		String customerId=map.get("customerId");
	 		//主表信息
			 CCargoEntry cargo=JSONObject.parseObject(cCargoEntry, CCargoEntry.class);
			 cargo.setCustomerId(customerId);
			 cargo.setCreateUser(user.getNickname());
			 cargo.setCreateTime(new Date());
			 cargo.setCargoId(UuidUtil.get32UUID());
			 cargo.setUseFlag(1);
			 //化工-XXX;1吨-2吨;3方-5方;吨包
			 String[]  goods=goodsInfo.split(";");
			 
		     String[]  catagory=goods[0].split("-");
		     
		     //大类
		     cargo.setCargoCatagory(catagory[0]);		     
		     //长度大于二存在小类 赋值
			 if(catagory.length>1){
				 cargo.setCargoSubCatagory(catagory[1]); 
			 }	
			 
			 
			  //重量
			 String[]  weg=goods[1].split("-");
			 if(weg.length>0){
				 cargo.setWeightStr(weg[0]); 				 
				 if(weg.length>1){
					 cargo.setWeightEnd(weg[1]);; 
				 }
			 }
		
			 //体积
			 String[]  vol=goods[2].split("-");
			 if(vol.length>0){
				 cargo.setVolumeStr(vol[0]); 
			 	if(weg.length>1){
			 		cargo.setVolumeEnd(vol[1]);; 
			 	}		 
			 }
			 //包装方式
			 cargo.setPack(goods[3]);
			 
			 String[] cars=carsInfo.split(";");
			 //用车类型
			 cargo.setCarUseType(cars[0]);
			 //车长	
			 cargo.setCarLen(cars[1]);
			 //车型
			 cargo.setCarType(cars[2]);
			 
			//装货地址详情
		 	List<JSONObject> dataJson=  JSONArray.parseArray(dataList,JSONObject.class );
		 	List<CCargoEntryRel> relList=new ArrayList<CCargoEntryRel>();
			String  start="",end="";
		 	for(JSONObject js: dataJson ){
		 		CCargoEntryRel rel=new CCargoEntryRel();
		 		rel.setCreateUser(user.getNickname());
		 		rel.setCreateTime(new Date());
		 		rel.setCargoRelId(UuidUtil.get32UUID());
		 		rel.setCargoId(cargo.getCargoId());
		 		//获取省市区
	 	 	 	try {
	 	 	 		String[] jpcas=js.get("pca").toString().split("-");
		 			rel.setProvinceCode(jpcas[0]);
			 		rel.setCityCode(jpcas[1]);
			 		rel.setAreaCode(jpcas[2]);
			 		String transportType=js.get("transportType").toString();
	 				if(transportType.equals("0")){//装货地址
						start=start+("起点:"+js.get("pca").toString()).replaceAll("null", " ");						 						 
					}
					if(transportType.equals("1")){//卸货地址
						end=end+("终点:"+js.get("pca").toString()).replaceAll("null", " ");					
					}
					
				} catch (Exception e) {
					// TODO: handle exception
				}	
		 		 
	 	 	 	//获取地址
		 		rel.setAddress(js.get("address").toString());
		 		//获取联系方式
		 		rel.setContacts(js.get("contacts").toString());	 	
		 		//获取货运类型	 
		 		rel.setTransportType(Integer.valueOf(js.get("transportType").toString()));
		 		rel.setUseFlag(1);
		 		relList.add(rel);		 		
		 	}	  
		 	
		 //	cargo.setAddressDetail(start+" "+end);
		 	cargo.setCarsAndGoods(carsInfo+";"+goodsInfo);  
		 	cCargoEntryService.insert(cargo);
		 	cCargoEntryRelService.insertBatch(relList);
			return new ResultModel<Boolean>(ResultStatus.SUCCESS, true);
		  
		}
}
