package com.mos.eboot.service.platform.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.naming.spi.DirStateFactory.Result;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysuserCustomerMgr;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.service.platform.service.ISysuserCustomerMgrService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@RestController
@RequestMapping("api/sysuser/cust")
public class SysUserCustMgrController {

	@Autowired
	private ICustomerService customerService;

	private ISysuserCustomerMgrService iSysuserCustomerMgrService;
	
    @Resource
    private ISysUserService userService;
	
	@PostMapping("/add")
	public ResultModel<String> addUserCust(@RequestParam("cids") String cids, @RequestParam("userId") String userId) {

		String cid[] = cids.split(",");
		SysUser user = userService.selectById(userId);
		for (String id : cid) {
			Customer  customer = customerService.selectById(Integer.parseInt(id));
			if (customer == null) {
				continue;
			}

			SysuserCustomerMgr entity = new SysuserCustomerMgr();
			entity.setcId(Integer.parseInt(id));
			entity.setUserId(userId);
			entity.setUserName(user.getUsername());
			iSysuserCustomerMgrService.insert(entity);
		}
		return ResultModel.defaultSuccess(ResultStatus.SUCCESS.getMsg());
	}

	@GetMapping("/query-by-userid")
	public ResultModel<List<SysuserCustomerMgr>> queryCustByUserId(@RequestParam("userId") String userId) {
		EntityWrapper<SysuserCustomerMgr> wrapper=  new EntityWrapper<SysuserCustomerMgr>();
		wrapper.eq("user_id", userId);
		return new ResultModel<List<SysuserCustomerMgr>>(ResultStatus.SUCCESS,iSysuserCustomerMgrService.selectList(wrapper));
	}

	@PostMapping("/del-by-cids")
	public ResultModel<String> delUserCustByCIds(@RequestParam("cids") String cids,
			@RequestParam("userId") String userId) {
		
		String cid[] = cids.split(",");
		for (String id : cid) {
			EntityWrapper<SysuserCustomerMgr> wrapper = new EntityWrapper<SysuserCustomerMgr>();
			wrapper.eq("user_id", userId);
			wrapper.eq("c_id", id);
			iSysuserCustomerMgrService.delete(wrapper);
		}
		
		return ResultModel.defaultSuccess(ResultStatus.SUCCESS.getMsg());
	}

	@PostMapping("/del-by-userid")
	public ResultModel<String> delUserCustByUserId(@RequestParam("userId") String userId) {
		EntityWrapper<SysuserCustomerMgr> wrapper = new EntityWrapper<SysuserCustomerMgr>();
		wrapper.eq("user_id", userId);
		iSysuserCustomerMgrService.delete(wrapper);
		return ResultModel.defaultSuccess(ResultStatus.SUCCESS.getMsg());
	}

}
