package com.mos.eboot.service.platform.controller;


import java.sql.Timestamp;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
 
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.platform.entity.IncomingPhoneCallLog;
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.service.platform.service.ICCarInfoService;
import com.mos.eboot.service.platform.service.ICCargoEntryRelService;
import com.mos.eboot.service.platform.service.ICCargoEntryService;
import com.mos.eboot.service.platform.service.ICCargoInfoService;
import com.mos.eboot.service.platform.service.ICOrderService;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.ISysEbootLogService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.service.platform.service.impl.IncomingPhoneCallLogService;
import com.mos.eboot.tools.util.DateUtil;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@RestController
@RequestMapping("/pfManceCount")
public class PfmanceCountController {
	
	 @Autowired
	 ICOrderService iCOrderService;
	 
	 @Autowired
	 ICCargoInfoService iCCargoInfoService;
	 
	 @Autowired
	 ICCargoEntryService iCCargoEntryService;
	 
	 @Autowired
	 ICCargoEntryRelService iCCargoEntryRelService;
	 
	 @Autowired
	 ICCarInfoService iCCarInfoService;
	 
	 @Autowired
	 ICustomerService icustomerService;
	 
	 @Autowired
	 IWxLoginService iWxLoginService;
	 
	 @Autowired
	 ICustWxService iCustWxService;
	 
	 @Autowired
	 ISysUserService  iSysUserService;
	 
	 @Autowired
	 ISysEbootLogService iSysEbootLogService;
	 
	 
	 @Autowired
	 IncomingPhoneCallLogService incomingPhoneCallLogService;
	 
	 @PostMapping("/queryCount")
	 public  List<JSONObject> queryCount(@RequestBody Map<String, String> map) {
	 
		List<JSONObject> orderList=new ArrayList<JSONObject>();
		 EntityWrapper<SysUser>  wxwrap=new EntityWrapper<SysUser>();
 		if(map.get("createId")!=null){
 			wxwrap.eq("id", map.get("createId"));
 		}
 		List<SysUser> sysList=iSysUserService.selectList(wxwrap);
		for (int i = 0; i < sysList.size(); i++) {
			String startTime=map.get("createTimeStart");
			String endTime=map.get("createTimeEnd");
		    Date newEnd=DateUtil.string_date(endTime);
			Calendar   calendar = new GregorianCalendar(); 
			calendar.setTime(newEnd); 
			calendar.add(calendar.DATE,1); //把日期往后增加一天,整数  往后推,负数往前移动 
			newEnd=calendar.getTime(); //这个时间就是日期往后推一天的结果 
			endTime=DateUtil.date_string(newEnd);
			SysUser user=sysList.get(i);
			JSONObject  json=new JSONObject();
			json.put("createTimeStart", map.get("createTimeStart"));		
			json.put("createTimeEnd", map.get("createTimeEnd"));
			json.put("createPeo", user.getNickname());
			json.put("addAllPeo",sysLog("t_cust_wx","添加",user.getId(),startTime,endTime));//加好友数
			json.put("addPeo", sysLog("t_cust_wx","新增",user.getId(),startTime,endTime));//新增客户
			json.put("scPeo", sysLog("t_cust_wx","删除",user.getId(),startTime,endTime));//删除
			json.put("lhPeo", sysLog("t_cust_wx","拉黑",user.getId(),startTime,endTime));//拉黑
			json.put("wbzPeo", json.getInteger("addAllPeo")-json.getInteger("addPeo"));//未备注
			json.put("orderNum", orderNum(user.getId(),startTime,endTime));//成单数量
			json.put("entryNum", entryNum(user.getNickname(),startTime,endTime));//录入货源数
			json.put("callIn",callLog(user.getId(),"1",startTime,endTime));
			json.put("callOut", callLog(user.getId(),"2",startTime,endTime));
			orderList.add(json);
		}
		  return   orderList ;
     }
	//String类型转换成Date类型 1 
	 public static Date date(String date_str) {
	          try {
	              Calendar zcal = Calendar.getInstance();//日期类
	              Timestamp timestampnow = new Timestamp(zcal.getTimeInMillis());//转换成正常的日期格式
	              SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");//改为需要的东西
	                ParsePosition pos = new ParsePosition(0);
	               Date current = formatter.parse(date_str, pos);
	               timestampnow = new Timestamp(current.getTime());
	              return timestampnow;
	         }
	          catch (NullPointerException e) {
	              return null;
	          }
	     } 
 
	public Integer  sysLog(String type,String staties,String peoId,String createTimeStart,String createTimeEnd){
		 EntityWrapper<SysEbootLog>  wxwrap=new EntityWrapper<SysEbootLog>();
		 wxwrap.eq("log_business_type", type);
		 wxwrap.eq("log_staties", staties);
		 wxwrap.eq("create_peo_id", peoId);
		 wxwrap.ge("create_time",createTimeStart).
		 and().le("create_time",createTimeEnd);
		
		 List<SysEbootLog> list=iSysEbootLogService.selectList(wxwrap);
		 return list.size();
	}
	 
	public Integer  orderNum(String peoId,String createTimeStart,String createTimeEnd){//成单数量
		
	  
		 EntityWrapper<COrder>  wxwrap=new EntityWrapper<COrder>();
		// wxwrap.eq("create_id", peoId);
		  wxwrap.eq("order_states", "已完成");
		  wxwrap.ge("create_time",createTimeStart).  and().le("create_time",createTimeEnd);
		// wxwrap.apply("  create_time >= '2022-03-15' AND create_time <='2022-03-16' DATE_FORMAT('2022-03-15', 'yyyy-mm-dd') <= DATE_FORMAT(CREATE_TIME, 'yyyy-mm-dd')");
		 List<COrder> list=iCOrderService.selectList(wxwrap);
		 return list.size();
	}
	public Integer   entryNum(String userName,String createTimeStart,String createTimeEnd){//成单数量
		 EntityWrapper<CCargoEntry>  wxwrap=new EntityWrapper<CCargoEntry>();
		 wxwrap.eq("create_user", userName);
		  wxwrap.between("create_time", createTimeStart,createTimeEnd);
			 //wxwrap.ge("DATE_FORMAT(create_time, 'yyyy-mm-dd')","DATE_FORMAT('"+createTimeStart+"', 'yyyy-mm-dd')").
			// and().le("DATE_FORMAT(create_time, 'yyyy-mm-dd')","DATE_FORMAT('"+createTimeEnd+"', 'yyyy-mm-dd')");
			 wxwrap.ge("create_time",createTimeStart).
			 and().le("create_time",createTimeEnd);
			
		 List<CCargoEntry> list=iCCargoEntryService.selectList(wxwrap);
		 return list.size();
	}
	public Integer   callLog(String userName,String type,String createTimeStart,String createTimeEnd){//成单数量
		Integer alltime=0;
		 EntityWrapper<IncomingPhoneCallLog>  wxwrap=new EntityWrapper<IncomingPhoneCallLog>();
		 wxwrap.eq("raw_add_user", userName);
		 wxwrap.eq("call_type", type);
		 // wxwrap.between("raw_add_time", createTimeStart,createTimeEnd);
		  
		  wxwrap.ge("raw_add_time",createTimeStart).
			 and().le("raw_add_time",createTimeEnd);
			
		 List<IncomingPhoneCallLog> list=incomingPhoneCallLogService.selectList(wxwrap);
		 for(IncomingPhoneCallLog li:list){
			 alltime+=Integer.valueOf(li.getDuration());
		 }
		 return alltime;
	}
}
