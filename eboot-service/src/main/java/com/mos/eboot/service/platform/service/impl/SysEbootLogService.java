package com.mos.eboot.service.platform.service.impl;

 
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.service.platform.mapper.SysEbootLogMapper;
import com.mos.eboot.service.platform.service.ISysEbootLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
@Service
public class SysEbootLogService extends ServiceImpl<SysEbootLogMapper, SysEbootLog> implements ISysEbootLogService {
	
	
}
