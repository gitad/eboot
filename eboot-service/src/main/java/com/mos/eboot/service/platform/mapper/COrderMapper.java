package com.mos.eboot.service.platform.mapper;
 
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.COrder;

/**
 * <p>
  * 订单表 Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
public interface COrderMapper extends BaseMapper<COrder> {

	 COrder  queryCount(@Param("createTimeStart")   String  createTimeStart, 
						    @Param("createTimeEnd")   String  createTimeEnd,
						    @Param("payPathId")   String payPathId,
						    @Param("createId")   String createId );
	 
	 
	List<COrder>  queryCountDetail(@Param("createTimeStart")   String  createTimeStart, 
			    @Param("createTimeEnd")   String  createTimeEnd,
			    @Param("payPathId")   String payPathId,
			    @Param("createId")   String createId, @Param("orderNum")   String orderNum);

}