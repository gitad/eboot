package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.service.platform.mapper.CCargoEntryRelMapper;
import com.mos.eboot.service.platform.service.ICCargoEntryRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCargoEntryRelService extends ServiceImpl<CCargoEntryRelMapper, CCargoEntryRel> implements ICCargoEntryRelService {
	
}
