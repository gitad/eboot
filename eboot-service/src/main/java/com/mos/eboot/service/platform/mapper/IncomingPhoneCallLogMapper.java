package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.IncomingPhoneCallLog;

/**
 * <p>
  * 来电-通话记录 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-20
 */
public interface IncomingPhoneCallLogMapper extends BaseMapper<IncomingPhoneCallLog> {

}