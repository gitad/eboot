package com.mos.eboot.service.platform.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.IncomingPhoneCallLog;
import com.mos.eboot.platform.entity.IncomingPhoneConf;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.platform.entity.UserIncomingPhone;
import com.mos.eboot.service.platform.service.ICustPhoneService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.IIncomingPhoneCallLogService;
import com.mos.eboot.service.platform.service.impl.IncomingPhoneConfService;
import com.mos.eboot.service.platform.service.impl.UserIncomingPhoneService;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.vo.UserIncomingPhoneVO;

@RestController
@RequestMapping("sys/incoming/phone")
public class UserInComingPhoneController extends BaseController {

	@Autowired
	private UserIncomingPhoneService userIncomingPhoneService;

	@Autowired
	private IncomingPhoneConfService incomingPhoneConfService;

	@Autowired
	private IIncomingPhoneCallLogService incomingPhoneCallLogService;
	
	@Autowired
	private ICustPhoneService custPhoneService;
	
	@Autowired
	private ICustomerService customerService;

	/**
	 * 根据用户ID，查询来电弹屏配置信息
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/by-user-id")
	public ResultModel<List<UserIncomingPhoneVO>> getIncomingPhoneByUserId(@RequestParam(name = "userId") String userId) {
		List<UserIncomingPhoneVO> data = new ArrayList<UserIncomingPhoneVO>();
		EntityWrapper<UserIncomingPhone> entityWrapper = new EntityWrapper<>();

		entityWrapper.eq("user_id", userId);
		List<UserIncomingPhone> userPhoneList = userIncomingPhoneService.selectList(entityWrapper);
		for (UserIncomingPhone userIncomingPhone : userPhoneList) {
			UserIncomingPhoneVO vo = new UserIncomingPhoneVO();

			IncomingPhoneConf conf = incomingPhoneConfService.selectById(userIncomingPhone.getConfId());

			BeanCopyUtil.copyProperties(conf, vo);
			vo.setUserId(userId);

			data.add(vo);
		}
		return new ResultModel<>(ResultStatus.SUCCESS.getCode(), ResultStatus.SUCCESS.getMsg(), data);
	}

	@PostMapping("/save-phone")
	public  IncomingPhoneConf saveComingPhone(@RequestBody IncomingPhoneConf icomingPhoneConf) {
		if(icomingPhoneConf.getId()!=null){
			incomingPhoneConfService.updateById(icomingPhoneConf);
		}else{
			incomingPhoneConfService.insert(icomingPhoneConf);
		}
		return icomingPhoneConf;
	}
	
	/**
	 * 根据电话id
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/by-conf-id")
	public ResultModel<List<UserIncomingPhoneVO>> getIncomingPhoneByConfId(@RequestParam(name = "confId") String confId) {
		List<UserIncomingPhoneVO> data = new ArrayList<UserIncomingPhoneVO>();
		EntityWrapper<UserIncomingPhone> entityWrapper = new EntityWrapper<>();

		entityWrapper.eq("conf_id", confId);
		List<UserIncomingPhone> userPhoneList = userIncomingPhoneService.selectList(entityWrapper);
		for (UserIncomingPhone userIncomingPhone : userPhoneList) {
			UserIncomingPhoneVO vo = new UserIncomingPhoneVO();

			IncomingPhoneConf conf = incomingPhoneConfService.selectById(userIncomingPhone.getConfId());

			BeanCopyUtil.copyProperties(conf, vo);
		 
			data.add(vo);
		}
		return new ResultModel<>(ResultStatus.SUCCESS.getCode(), ResultStatus.SUCCESS.getMsg(), data);
	}
	/**
	 * 根据conf_id查询电话
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("/by-no-select")
	public ResultModel<List<UserIncomingPhoneVO>> getByNoSelect(@RequestParam(name = "userId") String userId) {
		List<UserIncomingPhoneVO> data = new ArrayList<UserIncomingPhoneVO>();
		EntityWrapper<UserIncomingPhone> entityWrapper = new EntityWrapper<>();
		List<UserIncomingPhone> userPhoneList = userIncomingPhoneService.selectList(entityWrapper);//查询关联 表所有已关联得数据	
		//conf 是唯一得
		Map<Object, Object>  phoneAllmap=userPhoneList.stream().collect(Collectors.toMap(UserIncomingPhone::getConfId, UserIncomingPhone::getUserId));
		
		List<IncomingPhoneConf> inList=incomingPhoneConfService.selectList(new EntityWrapper<>());
		
		for (IncomingPhoneConf phone : inList) {
			if(phoneAllmap.get(phone.getId())==null||phoneAllmap.get(phone.getId()).equals(userId)){
				UserIncomingPhoneVO vo = new UserIncomingPhoneVO();
				 
				BeanCopyUtil.copyProperties(phone, vo);			 
				data.add(vo);
			}
			
			
			
		}
		
		
		
		return new ResultModel<>(ResultStatus.SUCCESS.getCode(), ResultStatus.SUCCESS.getMsg(), data);
	}

	@PostMapping("/save/phone-call-log")
	public ResultModel<String> savePhoneCallLog(@RequestParam(name = "data") String data,
			@RequestParam(name = "userId") String userId) {

		IncomingPhoneCallLog callLog = new IncomingPhoneCallLog();

		JSONObject dataJson = JSONObject.parseObject(data);

		String callId = dataJson.getString("callid");
		callLog.setCreateTime(new Date());
		callLog.setUserId(userId);
		callLog.setCallId(callId);
		callLog.setCallUserId(dataJson.getString("user_id"));
		callLog.setPhoneNum(dataJson.getString("phone"));
		callLog.setCallType(dataJson.getString("type"));
		callLog.setStatus(dataJson.getString("status"));
		try {
			callLog.setCreateTime(DateUtil.parseDate(dataJson.getString("createtime"), "yyyy-MM-dd HH:mm:ss"));
		} catch (ParseException e) {
		}
		callLog.setDuration(dataJson.getString("duration"));
		callLog.setRecordingPath("");
		callLog.setRawAddTime(Calendar.getInstance().getTime());
		callLog.setRawAddUser(userId);
		
		incomingPhoneCallLogService.insert(callLog);
		
		EntityWrapper<CustPhone> wrapper =new EntityWrapper<CustPhone>();
		
		wrapper.eq("cust_phone", callLog.getPhoneNum());
		CustPhone custPhone = custPhoneService.selectOne(wrapper);
		if (null == custPhone) {
			return ResultModel.defaultSuccess("");
		}
		
		Customer customer= customerService.selectById(custPhone.getcId());

		if (null == customer) {
			return ResultModel.defaultSuccess("");
		}

		customer.setLastCallTime(callLog.getCreateTime());
		customerService.updateAllColumnById(customer);
		
		return ResultModel.defaultSuccess("");
	}

	@GetMapping("/all")
	public ResultModel<List<IncomingPhoneConf>> getAllIncomingPhoneConf() {
		EntityWrapper<IncomingPhoneConf> wrapper = new EntityWrapper<IncomingPhoneConf>();
		wrapper.orderBy("id", true);
		List<IncomingPhoneConf> data = incomingPhoneConfService.selectList(wrapper);
		return new ResultModel<List<IncomingPhoneConf>>(ResultStatus.SUCCESS,data);
	}
	

	@GetMapping("/by-phone-num")
	public ResultModel<IncomingPhoneConf> getIncomingPhoneConfByPhoneNum(@RequestParam(name = "phoneNum") String phoneNum){
		EntityWrapper<IncomingPhoneConf> wrapper=new EntityWrapper<IncomingPhoneConf>();
		wrapper.eq("phone_num", phoneNum);
		IncomingPhoneConf data = incomingPhoneConfService.selectOne(wrapper);
		
		return new ResultModel<IncomingPhoneConf>(ResultStatus.SUCCESS, data);
	}
	
	
	/**
	 * 电话关联删除或者新增
	 * @return
	 */
	@GetMapping("/rel/addOrdel")
	public ResultModel<Boolean> getPhoneRelOrDel(@RequestParam(name = "userId") String userId,
												@RequestParam(name = "confId") String confId,
												@RequestParam(name = "type") boolean type){
		Boolean result = true;
		if(type==true){//增加
			UserIncomingPhone UserIncomingPhone=new UserIncomingPhone();
			UserIncomingPhone.setConfId(Integer.valueOf(confId));
			UserIncomingPhone.setUserId(userId);
			result=userIncomingPhoneService.insert(UserIncomingPhone);
		}
		if(type==false){//删除
			EntityWrapper<UserIncomingPhone> wrapper=new EntityWrapper<UserIncomingPhone>();
			wrapper.eq("user_id", userId);
			wrapper.eq("conf_id", confId);
			result=userIncomingPhoneService.delete(wrapper);
		}
		return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		
	}
}
