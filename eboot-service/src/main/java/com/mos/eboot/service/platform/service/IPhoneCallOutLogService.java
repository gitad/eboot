package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.PhoneCallOutLog;

/**
 * <p>
 * 自动拨打电话记录 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-08-17
 */
public interface IPhoneCallOutLogService extends IService<PhoneCallOutLog> {
	
}
