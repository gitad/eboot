package com.mos.eboot.service.platform.controller;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.WxLogin;
import com.mos.eboot.service.platform.service.ICCarInfoService;
import com.mos.eboot.service.platform.service.ICCargoEntryRelService;
import com.mos.eboot.service.platform.service.ICCargoEntryService;
import com.mos.eboot.service.platform.service.ICCargoInfoService;
import com.mos.eboot.service.platform.service.ICOrderService;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.ISysUserService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@RestController
@RequestMapping("/cOrder")
public class COrderController {
	
	 @Autowired
	 ICOrderService iCOrderService;
	 
	 @Autowired
	 ICCargoInfoService iCCargoInfoService;
	 
	 @Autowired
	 ICCargoEntryService iCCargoEntryService;
	 
	 @Autowired
	 ICCargoEntryRelService iCCargoEntryRelService;
	 
	 @Autowired
	 ICCarInfoService iCCarInfoService;
	 
	 @Autowired
	 ICustomerService icustomerService;
	 
	 @Autowired
	 IWxLoginService iWxLoginService;
	 
	 @Autowired
	 ICustWxService iCustWxService;
	 
	 @Autowired
	 ISysUserService  iSysUserService;
	 
	 @PostMapping("/query")
	 public ResultModel<Page<COrder>> query(@RequestBody  Page<COrder> page) {
		Map<String,Object> condition= page.getCondition();	
		EntityWrapper<COrder>  Wrapper=new EntityWrapper<COrder>();
		 if(condition!=null){
			    if(condition.get("customerId")!=null){
			    	 Wrapper.eq("customer_id", condition.get("customerId").toString());
			    	 condition.remove("customerId");
				}
			    if(condition.get("orderNum")!=null&&condition.get("orderNum")!=null){//订单号
			       Wrapper.like("order_num", condition.get("orderNum").toString());
			       condition.remove("orderNum");
				 
				}
			  
			     if(condition.get("payStates")!=null){ 
			    	 Wrapper.eq("pay_states", condition.get("payStates").toString());
			    	 condition.remove("payStates");
				}  
			     if(condition.get("orderStates")!=null){ 
			    	 Wrapper.eq("order_states", condition.get("orderStates").toString());
			    	 condition.remove("orderStates");
				} 
			     if(condition.get("payPath")!=null){ 
			    	 Wrapper.like("pay_path", condition.get("payPath").toString());
			    	 condition.remove("payPath");
				} 
			     if(condition.get("remark")!=null){ 
			    	 Wrapper.like("remark", condition.get("remark").toString());
			    	 condition.remove("remark");
				} 
			     if(condition.get("vaildPhone")!=null){ 
			    	 Wrapper.like("vaild_phone", condition.get("vaildPhone").toString());
			    	 condition.remove("vaildPhone");
				} 
		    }
		
	    Wrapper.eq("USE_FLAG", 1);
	    Page<COrder> dataPage=iCOrderService.selectPage(page, Wrapper);
	    return new ResultModel<Page<COrder>>(ResultStatus.SUCCESS, dataPage);
     }
	 
	 @PostMapping("/queryCountDetail")
	 public  List<COrder> queryCountDetail(@RequestBody  Page<COrder> page) {
		Map<String,Object> condition= page.getCondition();	
		 List<COrder> list=null;
		 String orderNum="";
		 if(condition!=null){
			 orderNum=condition.get("orderNum")==null?"":condition.get("orderNum").toString();
			    if(condition.get("type")!=null){
			    	if(condition.get("type").equals("0")){//微信维度
			    	  list= iCOrderService.queryCountDetail(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), condition.get("payPathId").toString(),null,orderNum);	 
			    	}
			    	 if(condition.get("type").equals("1")){//人员维度
				    	list= iCOrderService.queryCountDetail(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), null,condition.get("createId").toString(),orderNum);	 
				    } 
			    	  
				}
			   
		    }
		
	  
	    return   list ;
     }
	 @PostMapping("/queryCount")
	 public  List<JSONObject> queryCount(@RequestBody  Page<COrder> page) {
		Map<String,Object> condition= page.getCondition();	
	 
		List<JSONObject> orderList=new ArrayList<JSONObject>();
		 if(condition!=null){
			    if(condition.get("type")!=null){
			    	if(condition.get("type").equals("0")){//微信维度
			    		EntityWrapper<WxLogin>  wxwrap=new EntityWrapper<WxLogin>();
			    		if(condition.get("payPathId")!=null){
			    			wxwrap.eq("wx_id", condition.get("payPathId"));
			    		}
			    		List<WxLogin> wxList=iWxLoginService.selectList(wxwrap);
			    		for(WxLogin wx:wxList){
			    			JSONObject json=new JSONObject();
			    			//统计金额
			    			 COrder corder= iCOrderService.queryCount(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), wx.getWxId(),null);
			    			 if(corder==null){
			    				corder=new COrder();
			    				corder.setPayNum("0"); 
			    				corder.setReturnNum("0");
			    			 }
			    			 json.put("createTimeStart", condition.get("createTimeStart").toString());
			    			 json.put("createTimeEnd", condition.get("createTimeEnd").toString());
			    			 json.put("payPath",wx.getWxNickname());
			    			 json.put("payNum",new BigDecimal(corder.getPayNum())); 
			    			 json.put("returnNum",new BigDecimal(corder.getReturnNum())); 
			    			 json.put("trueNum",new BigDecimal(corder.getPayNum()).subtract(new BigDecimal(corder.getReturnNum()))); 
			    			 json.put("createPeo",""); 
			    			 json.put("createId",""); 
			    			 json.put("payPathId",wx.getWxId());
			    			 //统计增加的人员
			    			 List<CustWx> custList= iCustWxService.queryCount(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), wx.getWxId(),null);
			    			 json.put("addWx", custList.size());
			    			 List<CustWx> remarkList= iCustWxService.queryCount(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), wx.getWxId(),"0");
			    			 json.put("noRemark", remarkList.size());	
			    			 orderList.add(json);
			    		}
			    	}
			    	if(condition.get("type").equals("1")){//人维度
			    		 EntityWrapper<SysUser>  wxwrap=new EntityWrapper<SysUser>();
			    		if(condition.get("createId")!=null){
			    			wxwrap.eq("id", condition.get("createId"));
			    		}
			    		List<SysUser> sysList=iSysUserService.selectList(wxwrap);
			    		for(SysUser sy:sysList ){
			    			JSONObject json=new JSONObject();
			    			//统计金额
			    			 COrder corder= iCOrderService.queryCount(condition.get("createTimeStart").toString(), condition.get("createTimeEnd").toString(), null,sy.getId());
			    		 
			    			 if(corder==null){
			    				corder=new COrder();
			    				corder.setPayNum("0"); 
			    				corder.setReturnNum("0");
			    			 }
			    	 
			    			 
			    			 json.put("createTimeStart", condition.get("createTimeStart").toString());
			    			 json.put("createTimeEnd", condition.get("createTimeEnd").toString());
			    			 json.put("payPath","");			 
			    			 json.put("payPathId","");
			    			 json.put("payNum",new BigDecimal(corder.getPayNum())); 
			    			 json.put("returnNum",new BigDecimal(corder.getReturnNum())); 
			    			 json.put("trueNum",new BigDecimal(corder.getPayNum()).subtract(new BigDecimal(corder.getReturnNum()))); 
			    			 json.put("createPeo",sy.getNickname()); 
			    			 json.put("createId",sy.getId()); 
			    			 json.put("addWx", 0);
			    			 json.put("noRemark", 0);	
			    			 orderList.add(json);
			    		}
			    		
			    		
			    		
			    	}
			    	 
			    	  
				}
			   
		    }
		
	  
	    return   orderList ;
     }
	 
	 @PostMapping("/queryByCargoInfoId")
	 public  COrder  queryByCargoInfoId(@RequestParam("cargoInfoId") String cargoInfoId) {
		 EntityWrapper<COrder>  wrapperz=new EntityWrapper<COrder>();
    	 wrapperz.eq("CARGO_INFO_ID", cargoInfoId);
    	 wrapperz.eq("USE_FLAG", 1);
    	 return iCOrderService.selectOne(wrapperz);
		 
	 }
	 @PostMapping("/queryDetail")
	 public  JSONObject  queryDetail(@RequestParam("orderId") String orderId) {
		 
		 JSONObject json=new JSONObject();
	     COrder  order=iCOrderService.selectById(orderId);
	     if(order!=null){
	    	 CCargoInfo cargoInfo= iCCargoInfoService.selectById(order.getCargoInfoId()) ;//车询货得
	    	 if(cargoInfo!=null){
	    		 CCargoEntry cargoEntry= iCCargoEntryService.selectById(cargoInfo.getCargoId());
		    	 
		    	 EntityWrapper<CCargoEntryRel>  wrapperz=new EntityWrapper<CCargoEntryRel>();
		    	 wrapperz.eq("CARGO_ID", cargoEntry.getCargoId());
		    	 wrapperz.eq("TRANSPORT_TYPE", 0);
		    	 EntityWrapper<CCargoEntryRel>  wrapperx=new EntityWrapper<CCargoEntryRel>();
		    	 wrapperx.eq("CARGO_ID", cargoEntry.getCargoId());
		    	 wrapperx.eq("TRANSPORT_TYPE", 1);
		    	
		    	 CCarInfo carInfo= iCCarInfoService.selectById(cargoInfo.getCarId());
		    	 if(carInfo!=null){
		    		 Customer cust= icustomerService.selectById(carInfo.getCustomerId()); 
		    		 carInfo.setCustName(cust.getCustName());
		    		 carInfo.setVaildPhone(cust.getValidPhone());
		    	 } 
		    	 json.put("order",order==null?new COrder():order );
		    	 json.put("carInfo",carInfo==null?new CCarInfo():carInfo);
		    	 json.put("cargoInfo",cargoInfo==null?new CCargoInfo():cargoInfo);
		    	 json.put("cargoEntry",cargoEntry==null?new CCargoEntry():cargoEntry);
		    	 json.put("cargoEntryRelz",iCCargoEntryRelService.selectList(wrapperz));
		    	 json.put("cargoEntryRelx",iCCargoEntryRelService.selectList(wrapperx)); 
	    	 }else{
	    		 json.put("msg","未查询到订单详情");
	    	 }
	    	
	     }
	    return json;
     }
	 @PostMapping("/save")
	 public  COrder  save(@RequestBody   COrder  cOrder) {
		 if(cOrder.getOrderId()==null){
			 cOrder.setCargoId(UuidUtil.get32UUID());
			 cOrder.setUseFlag("1");
			 iCOrderService.insert(cOrder);
		 }else{
			 iCOrderService.updateById(cOrder); 
		 }
		 
		 return cOrder;
	 }	 
}
