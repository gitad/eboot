package com.mos.eboot.service.platform.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

 



import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.CCompanyDetail;
import com.mos.eboot.platform.entity.CCompanyDetailRel;
import com.mos.eboot.service.platform.service.impl.CCompanyDetailRelService;
import com.mos.eboot.service.platform.service.impl.CCompanyDetailService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@RestController
@RequestMapping("/cCompanyDetail")
public class CCompanyDetailController {
	
	@Autowired
	CCompanyDetailService cCompanyDetailService;
	
	@Autowired
	CCompanyDetailRelService cCompanyDetailRelService;
	
    @PostMapping("/query")
	public ResultModel<CCompanyDetail> query(@RequestBody  CCompanyDetail cCompanyDetail) {		 
 		EntityWrapper<CCompanyDetail>  Wrapper=new EntityWrapper<CCompanyDetail>();
 		if(cCompanyDetail.getCustomerId()!=null){
 			Wrapper.eq("customer_id",cCompanyDetail.getCustomerId());
 		}
	    CCompanyDetail detail=cCompanyDetailService.selectOne(Wrapper);		 
		return new ResultModel<CCompanyDetail>(ResultStatus.SUCCESS, detail);
    }
    
    
    @PostMapping("/save")
	public ResultModel<CCompanyDetail> save(@RequestBody  CCompanyDetail cCompanyDetail) {		 
 	 
 		if(StringUtil.isNotBlank(cCompanyDetail.getCompanyId())){
 			cCompanyDetailService.updateById(cCompanyDetail);
 		}else{
 			cCompanyDetail.setCompanyId(UuidUtil.get32UUID());
 			cCompanyDetailService.insert(cCompanyDetail);
 		}
	    	 
		return new ResultModel<CCompanyDetail>(ResultStatus.SUCCESS, cCompanyDetail);
    }
    
    
    @PostMapping("/relQuery")
	public ResultModel<List<CCompanyDetailRel>> relQuery(@RequestParam("companyId") String companyId) {		 
    	EntityWrapper<CCompanyDetailRel>  Wrapper=new EntityWrapper<CCompanyDetailRel>();
    	Wrapper.eq("company_id", companyId);
		return new ResultModel<List<CCompanyDetailRel>>(ResultStatus.SUCCESS, cCompanyDetailRelService.selectList(Wrapper));
    }
    
    @PostMapping("/relDel")
	public ResultModel<Boolean> relDel(@RequestBody CCompanyDetailRel cCompanyDetailRel) {		
    	Boolean bool=cCompanyDetailRelService.deleteById(cCompanyDetailRel);
		return new ResultModel<Boolean>(ResultStatus.SUCCESS,bool);
    }
   
    
    @PostMapping("/relSave")
  	public ResultModel<CCompanyDetailRel> relSave(@RequestBody CCompanyDetailRel cCompanyDetailRel) {	
    	if(StringUtil.isNotBlank(cCompanyDetailRel.getCompanyRelId())){
    		cCompanyDetailRelService.updateById(cCompanyDetailRel);
    	}else{
    		cCompanyDetailRel.setCompanyRelId(UuidUtil.get32UUID());
    		cCompanyDetailRelService.insert(cCompanyDetailRel);
    	}
       
  		return new ResultModel<CCompanyDetailRel>(ResultStatus.SUCCESS,cCompanyDetailRel);
      }
    
     
    
}
