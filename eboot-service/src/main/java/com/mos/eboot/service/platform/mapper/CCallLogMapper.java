package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CCallLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface CCallLogMapper extends BaseMapper<CCallLog> {
	int  getMaxTid();
}