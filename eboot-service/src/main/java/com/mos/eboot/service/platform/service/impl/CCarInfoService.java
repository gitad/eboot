package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.service.platform.mapper.CCarInfoMapper;
import com.mos.eboot.service.platform.service.ICCarInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCarInfoService extends ServiceImpl<CCarInfoMapper, CCarInfo> implements ICCarInfoService {
	
}
