package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.SysuserCustomerMgr;

/**
 * <p>
  * 系统用户管理客户表 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-06-14
 */
public interface SysuserCustomerMgrMapper extends BaseMapper<SysuserCustomerMgr> {

}