package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CustomerLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-06-05
 */
public interface CustomerLogMapper extends BaseMapper<CustomerLog> {

}