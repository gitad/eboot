package com.mos.eboot.service.platform.service.impl;

 
import com.mos.eboot.platform.entity.CCompanyDetailRel;
import com.mos.eboot.service.platform.mapper.CCompanyDetailRelMapper;
import com.mos.eboot.service.platform.service.ICCompanyDetailRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@Service
public class CCompanyDetailRelService extends ServiceImpl<CCompanyDetailRelMapper, CCompanyDetailRel> implements ICCompanyDetailRelService {
	
}
