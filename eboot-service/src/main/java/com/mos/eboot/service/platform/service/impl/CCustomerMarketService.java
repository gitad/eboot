package com.mos.eboot.service.platform.service.impl;

 
 
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.service.platform.mapper.CCustomerMarketMapper;
import com.mos.eboot.service.platform.service.ICCustomerMarketService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户营销表 服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
@Service
public class CCustomerMarketService extends ServiceImpl<CCustomerMarketMapper, CCustomerMarket> implements ICCustomerMarketService {
	
}
