package com.mos.eboot.service.platform.service.impl;

 
 
import com.mos.eboot.platform.entity.CustomerLog;
import com.mos.eboot.service.platform.mapper.CustomerLogMapper;
import com.mos.eboot.service.platform.service.ICustomerLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-06-05
 */
@Service
public class CustomerLogService extends ServiceImpl<CustomerLogMapper, CustomerLog> implements ICustomerLogService {
	
}
