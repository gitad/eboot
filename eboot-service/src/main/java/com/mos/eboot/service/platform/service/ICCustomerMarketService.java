package com.mos.eboot.service.platform.service;

 
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CCustomerMarket;

/**
 * <p>
 * 客户营销表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
public interface ICCustomerMarketService extends IService<CCustomerMarket> {
	
}
