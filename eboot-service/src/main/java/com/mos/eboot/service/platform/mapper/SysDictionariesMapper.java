package com.mos.eboot.service.platform.mapper;

 
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.vo.ChartVO;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
public interface SysDictionariesMapper extends BaseMapper<SysDictionaries> {
	List<SysDictionaries> listSubDictByParentId(@Param("parentId")String parentId);
	List<SysDictionaries> listSubDict();
	 
}