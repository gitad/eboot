package com.mos.eboot.service.platform.service.impl;

 
import com.mos.eboot.platform.entity.WxLogin;
import com.mos.eboot.service.platform.mapper.WxLoginMapper;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 * 微信登录用户表 服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-11
 */
@Service
public class WxLoginService extends ServiceImpl<WxLoginMapper, WxLogin> implements IWxLoginService {
	
}
