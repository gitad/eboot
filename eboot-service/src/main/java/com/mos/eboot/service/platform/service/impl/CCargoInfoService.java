package com.mos.eboot.service.platform.service.impl;

import java.util.List;

import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.service.platform.mapper.CCargoInfoMapper;
import com.mos.eboot.service.platform.service.ICCargoInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCargoInfoService extends ServiceImpl<CCargoInfoMapper, CCargoInfo> implements ICCargoInfoService {
	@Autowired
	CCargoInfoMapper cCargoInfoMapper;
	
	public List<CCargoInfo> queryPirce(String start,String end,String carLens,String type){
		return cCargoInfoMapper.queryPirce(start, end, carLens,type);
	}
	
	public List<CCargoInfo> queryCarGo(String cargoId,String customerId,String type){
		return cCargoInfoMapper.queryCarGo(cargoId, customerId,type);
	}
	 
	public Integer queryCars(@Param("customerId")String customerId,@Param("type")String type){
		return cCargoInfoMapper.queryCars(customerId,type);
	}
}
