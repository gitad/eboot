package com.mos.eboot.service.platform.service;

import java.util.List;

import com.mos.eboot.platform.entity.WxGroup;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-17
 */
public interface IWxGroupService extends IService<WxGroup> {
	public List<WxGroup> queryGroup(String groupName,String remark,String carLen,String carType,String profit,String owerName,Page<WxGroup> page) ;
}
