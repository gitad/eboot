package com.mos.eboot.service.platform.controller;


import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CQuickReply;
import com.mos.eboot.service.platform.service.ICQuickReplyService;
import com.mos.eboot.service.platform.service.impl.CQuickReplyService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-17
 */
@RestController
@RequestMapping("/cQuickReply")
public class CQuickReplyController {
	
	@Autowired
	ICQuickReplyService cQuickReplyService;
	
	 @PostMapping("/query")
		public ResultModel<Page<CQuickReply>> query(@RequestBody  Page<CQuickReply> page) {
			Map<String,Object> condition= page.getCondition();
			EntityWrapper<CQuickReply>  Wrapper=new EntityWrapper<CQuickReply>();
		    Wrapper.eq("USE_FLAG", 1);
		    Wrapper.eq("REPLY_TYPE", condition.get("replyType")); 
		    condition.remove("replyType");
			Page<CQuickReply> dataPage=cQuickReplyService.selectPage(page, Wrapper);		
			return new ResultModel<Page<CQuickReply>>(ResultStatus.SUCCESS, dataPage);
	    }

		
		@PostMapping("/save")
		public ResultModel<Boolean>  save(@RequestBody  CQuickReply  cCQuickReply) {
			 boolean result; 	
			if(StringUtils.isBlank(cCQuickReply.getReplyId())){			 
				cCQuickReply.setReplyId(UuidUtil.get32UUID());
				cCQuickReply.setUseFlag(1);
				//cCallLog.setCreateTime();
			    result=cQuickReplyService.insert(cCQuickReply);	
			}else{
				
				result=cQuickReplyService.updateById(cCQuickReply)	;
			}
			 
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		}
		
		@PostMapping("/del")
		public ResultModel<Boolean>  del(@RequestBody CQuickReply  cCQuickReply) { 
		    boolean result=cQuickReplyService.deleteById(cCQuickReply.getReplyId());
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		}
		
	
}
