package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.IncomingPhoneConf;
import com.mos.eboot.service.platform.mapper.IncomingPhoneConfMapper;
import com.mos.eboot.service.platform.service.IIncomingPhoneConfService;

/**
 * <p>
 * 来电弹屏手机号配置信息 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
@Service
public class IncomingPhoneConfService extends ServiceImpl<IncomingPhoneConfMapper, IncomingPhoneConf>
		implements IIncomingPhoneConfService {

}
