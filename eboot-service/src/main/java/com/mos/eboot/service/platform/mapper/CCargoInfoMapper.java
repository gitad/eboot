package com.mos.eboot.service.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mos.eboot.platform.entity.CCargoInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface CCargoInfoMapper extends BaseMapper<CCargoInfo> {
	List<CCargoInfo> queryPirce(@Param("start")  String start, @Param("end") String end, @Param("carLens")  String carLens, @Param("type")  String type);

	List<CCargoInfo> queryCarGo(@Param("cargoId")  String cargoId,@Param("customerId")  String customerId,@Param("type")  String type);

	Integer queryCars(@Param("cargoId")String cargoId,@Param("type")String type);


}