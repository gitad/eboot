package com.mos.eboot.service.platform.service;

 
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerXnh;

/**
 * <p>
 * 用户虚拟号表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-22
 */
public interface ICustomerXnhService extends IService<CustomerXnh> {
	public int saveCustomer(CustomerXnh customer);
}
