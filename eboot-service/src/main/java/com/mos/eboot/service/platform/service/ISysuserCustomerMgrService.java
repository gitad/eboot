package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.SysuserCustomerMgr;

/**
 * <p>
 * 系统用户管理客户表 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-06-14
 */
public interface ISysuserCustomerMgrService extends IService<SysuserCustomerMgr> {
	
}
