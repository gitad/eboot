package com.mos.eboot.service.platform.service.impl;

import java.util.List;

import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.service.platform.mapper.CCargoEntryMapper;
import com.mos.eboot.service.platform.mapper.SysDictionariesMapper;
import com.mos.eboot.service.platform.service.ICCargoEntryService;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CCargoEntryService extends ServiceImpl<CCargoEntryMapper, CCargoEntry> implements ICCargoEntryService {
	@Autowired
	private CCargoEntryMapper cCargoEntryMapper;

	@Override
	public List<CCargoEntry> queryByAddress(@RequestParam("address")   String  address) {
		return cCargoEntryMapper.queryByAddress(address);
	}

	
 
	@Override
	public List<CCargoEntry> querySource(@RequestParam("addressDetail")   String  addressDetail,
										@RequestParam("customerPrice")   String  customerPrice,
										@RequestParam("carsAndGoods")   String  carsAndGoods,
										@RequestParam("planTime_start") String  planTime_start,
										@RequestParam("planTime_end") String  planTime_end,
										@RequestParam("cargoType") String  cargoType, Page<CCargoEntry> page
										) {
		return cCargoEntryMapper.querySource(addressDetail,customerPrice,carsAndGoods,planTime_start,planTime_end,cargoType,page);
	}
	@Override
	public Integer querySourceTotal(@RequestParam("addressDetail")   String  addressDetail,
										@RequestParam("customerPrice")   String  customerPrice,
										@RequestParam("carsAndGoods")   String  carsAndGoods,
										@RequestParam("planTime_start") String  planTime_start,
										@RequestParam("planTime_end") String  planTime_end,
										@RequestParam("cargoType") String  cargoType 
										) {
		return cCargoEntryMapper.querySourceTotal(addressDetail,customerPrice,carsAndGoods,planTime_start,planTime_end,cargoType);
	}
	
}
