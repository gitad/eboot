package com.mos.eboot.service.platform.controller;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.WxLogin;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.IWxLoginService;
import com.mos.eboot.service.platform.service.impl.CContactPeoRelService;
import com.mos.eboot.service.platform.service.impl.CContactPeoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/cContactPeo")
public class CContactPeoController {
	
	
	 @Autowired
	 CContactPeoService cContactPeoService;
	 
	 @Autowired
	 CContactPeoRelService cContactPeoRelService;
	 
	 @Autowired
	 private ICustomerService customerService;
	
	 @Autowired
	 ICustWxService custWxService;
	 
	 @Autowired 
	 IWxLoginService wxLoginService;
	 
	 @PostMapping("/query")
	 public List<CContactPeo> query( @RequestParam("cid") String cid) {
		EntityWrapper<CContactPeo>  wrapper=new EntityWrapper<CContactPeo>();
		wrapper.eq("CUSTOMER_ID", cid); 
		wrapper.eq("USE_FLAG", "1"); 
		List<CContactPeo> list=cContactPeoService.selectList(wrapper);	
		if(list.size()>0){
			for(CContactPeo li:list){
				//存储子表数据
				EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
				Wrapper.eq("CONTACT_ID", li.getContactId()); 	
				List<CContactPeoRel> relList=cContactPeoRelService.selectList(Wrapper);
				li.setContactPeoList(relList);
					for(CContactPeoRel re:relList){
						//存储
						EntityWrapper<CustWx>  custWrapper=new EntityWrapper<CustWx>();
						List<CustWx> cuList=null;
						if(StringUtil.isNotBlank(re.getContactPhone())){
							custWrapper.like("wx_remark", re.getContactPhone());
							 cuList=custWxService.selectList(custWrapper);	
						}
					
						String wxType="";
						if(cuList!=null&&cuList.size()>0){						
							for(int i=0;i<cuList.size();i++){
								WxLogin login=wxLoginService.selectOne(new EntityWrapper<WxLogin>().eq("wx_id", cuList.get(i).getSysWxId()));
								String wxNickName=login.getWxNickname();
								if(cuList.get(i).getWxStat().equals("1")){//正常
									wxType+=wxNickName+"-正常";
								}
								if(cuList.get(i).getWxStat().equals("2")){//被删除
									wxType+=wxNickName+"-删除";
								}
								if(cuList.get(i).getWxStat().equals("3")){//被拉黑
									wxType+=wxNickName+"-拉黑";
								}
								if(i!=cuList.size()-1){
									wxType+=",";
								}
							}
						}
						
						re.setWx(wxType.equals("")?"无":wxType);
					}
			}
		} 
		return list;
    }

	 @PostMapping("/queryById")
	 public  CContactPeo  queryById( @RequestParam("contactId") String contactId) {
		  CContactPeo  result=cContactPeoService.selectById(contactId);		  
		  return result;
	 }

	 @PostMapping("/queryByCustomerId")
	 public   List<CContactPeo>  queryByCustomerId( @RequestParam("customerId") String customerId) {
	 
		 return cContactPeoService.selectList(new EntityWrapper<CContactPeo>().eq("CUSTOMER_ID", customerId));		  
		  
	 }
	@PostMapping("/queryByEntity")
	public CContactPeo queryByEntity(@RequestBody  CContactPeo  cContactPeo) {
		  CContactPeo  result=null;
		EntityWrapper<CContactPeo>  wrapper=new EntityWrapper<CContactPeo>();
		if(cContactPeo!=null){
		   wrapper.eq("CONTACT_CARD", cContactPeo.getContactCard());	
		   result=cContactPeoService.selectOne(wrapper);
		}
		
		 
		 return result;
	}
	
	@PostMapping("/checkByPhone")
	public ResultModel<String>  checkByPhone(@RequestParam("phone")  String  phone,@RequestParam("customerId")  String  customerId) {
		 String result="";
		 EntityWrapper<CContactPeoRel>  wrapper=new EntityWrapper<CContactPeoRel>();
		 wrapper.eq("CONTACT_PHONE", phone);	
		 List<CContactPeoRel> list=cContactPeoRelService.selectList(wrapper);
		 if(list.size()>0){
			 for(CContactPeoRel li:list){
				 EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();	
				 curapper.eq("is_del", "0");
				  curapper.setSqlSelect("id","cust_name");
				 Customer  customer=customerService.selectById(li.getCustomerId());
				 result+=" 号码:"+phone+"与用户:'"+customer.getCustName()+"'-所拥有号码重复"; 		 
			 } 
		 }else{
			  EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();
			  curapper.eq("is_del", "0");
			  curapper.setSqlSelect("id","cust_name");
			  curapper.like("valid_phone", phone).or().like("other_phone", phone);
			  curapper.ne("id", customerId);
			  Customer customer=customerService.selectOne(curapper);
			  if(customer!=null){
				  result+=" 号码:"+phone+"与用户:'"+customer.getCustName()+"'-所拥有号码重复"; 		 
			  }
		 }
		 return new ResultModel<String>(ResultStatus.SUCCESS, result);
	} 
	
	
	@PostMapping("/save")
	public ResultModel<Boolean>  save(@RequestBody  CContactPeo  cContactPeo) {
		boolean result;
		 cContactPeo.setCreateTime(new Date());
		 result=cContactPeoService.insert(cContactPeo);
		 return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
	}
	
	@PostMapping("/update")
	public ResultModel<Boolean>  update(@RequestBody  CContactPeo  cContactPeo) {
		boolean result;
		result=cContactPeoService.updateById(cContactPeo);
		return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
	}
	
	@PostMapping("/del")
	public ResultModel<Boolean>  del(  @RequestParam("contactId") String contactId) {
	    boolean result=cContactPeoService.deleteById(contactId);
	    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
	}
	
	 
	
}
