 package com.mos.eboot.service.platform.controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.service.platform.service.impl.SysDictionariesService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
@RestController
@RequestMapping("/sysDictionaries")
public class SysDictionariesController {
	
	@Autowired
	SysDictionariesService sysDictionariesService;
 
	 @PostMapping("/query")
		public  List<SysDictionaries> query(@RequestParam("parentId")   String  parentId) {

		 
		 EntityWrapper<SysDictionaries>  wrapper=null;
		 wrapper=new EntityWrapper<SysDictionaries>();
		 wrapper.eq("PARENT_ID", parentId);
		 List<SysDictionaries> dataPage=sysDictionariesService.selectList(wrapper);	
		 return  dataPage;
	   }
		
	 
	 @PostMapping("/queryByparentId")
		public  List<SysDictionaries> queryByparentId(@RequestParam("parentId")   String  parentId) {		 
		 	//List<SysDictionaries> dataPage=sysDictionariesService.listSubDictByParentId(parentId);
		 List<SysDictionaries> list=new ArrayList<SysDictionaries>();
		 List<SysDictionaries> dataPage=sysDictionariesService.listSubDict();
		 
		 	for(SysDictionaries da:dataPage ){	
		 		if(da.getParentId().equals("-1")){
		 			 da.setChildren(getChildren(da.getDictionariesId(),dataPage)); 	
		 			list.add(da);
		 		}
		 		
		 	}
			return  list;
	    }
		
	 public  List<SysDictionaries> getChildren(String  parentId,List<SysDictionaries> dataPage) {		 
		   List<SysDictionaries> list=new ArrayList<SysDictionaries>();	
		 	for(SysDictionaries da:dataPage ){	
		 		if(da.getParentId().equals(parentId)){
		 			 da.setChildren(getChildren(da.getDictionariesId(),dataPage)); 	
		 			list.add(da);
		 		}
		 		
		 	}
			return  list;
	    }
	 
		@PostMapping("/save")
		public ResultModel<Boolean>  save(@RequestBody  List<SysDictionaries> list) {
			 boolean result=true;
			 for(SysDictionaries li:list){
				 if(li.getDictionariesId()==null){
					 li.setDictionariesId(UuidUtil.get32UUID());
					result=sysDictionariesService.insert(li);	
				 }else{
					result=sysDictionariesService.updateById(li);
				 }
					 	 
			 }
			 
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		}
		
		@PostMapping("/del")
		public ResultModel<Boolean>  del(@RequestBody  SysDictionaries  sysDictionaries) { 
			//获取全部下来  采用递归获取出所有节点和子节点数据
			 boolean result=true;
			 List<SysDictionaries> dataPage=sysDictionariesService.listSubDict();
			 getdelChildren(sysDictionaries.getDictionariesId(),dataPage);
		   // boolean result=sysDictionariesService.deleteById(sysDictionaries.getDictionariesId());
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		}
		 public  void getdelChildren(String  parentId,List<SysDictionaries> dataPage) {		 
			   
			 	for(SysDictionaries da:dataPage ){	
			 		if(da.getParentId().equals(parentId)){
			 			  getdelChildren(da.getDictionariesId(),dataPage); 	
			 			  boolean result=sysDictionariesService.deleteById(da.getDictionariesId()); 
			 		}
			 	   if(da.getDictionariesId().equals(parentId)){
			 		  boolean result=sysDictionariesService.deleteById(da.getDictionariesId()); 
			 	   }
			 		
			 	}
				 
		    }	
}
