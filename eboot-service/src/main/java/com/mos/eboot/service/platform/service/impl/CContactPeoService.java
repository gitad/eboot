package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.service.platform.mapper.CContactPeoMapper;
import com.mos.eboot.service.platform.service.ICContactPeoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CContactPeoService extends ServiceImpl<CContactPeoMapper, CContactPeo> implements ICContactPeoService {
	
}
