package com.mos.eboot.service.platform.controller;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.service.platform.service.impl.SysEbootLogService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
@RestController
@RequestMapping("/sysEbootLog")
public class SysEbootLogController {

	@Autowired
	SysEbootLogService sysEbootLogService;
	
	@PostMapping("/queryPage")
	public  List<SysEbootLog> queryPage(@RequestBody  SysEbootLog sysEbootLog) {
		 
			EntityWrapper<SysEbootLog>  Wrapper=new EntityWrapper<SysEbootLog>();
			 if(StringUtil.isNotBlank(sysEbootLog.getLogBusinessId())) {
				Wrapper.eq("log_business_id", sysEbootLog.getLogBusinessId());
			 } 
			 if(StringUtil.isNotBlank(sysEbootLog.getLogBusinessType())) {
				 Wrapper.eq("log_business_type", sysEbootLog.getLogBusinessType());
			 } 
			 if(StringUtil.isNotBlank(sysEbootLog.getLogStaties())) {
				 Wrapper.eq("log_staties", sysEbootLog.getLogStaties());
			 } 
			 if(StringUtil.isNotBlank(sysEbootLog.getCustomerId())) {
				 Wrapper.eq("customer_id", sysEbootLog.getCustomerId());
			 } 
			 List<SysEbootLog>  list=sysEbootLogService.selectList(Wrapper);
		  	return list;
	    }
	
	@PostMapping("/save")
	public SysEbootLog save(@RequestBody  SysEbootLog sysEbootLog) {
		boolean ins=sysEbootLogService.insert(sysEbootLog);
		return sysEbootLog;
	}
	
}
