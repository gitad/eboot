package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.service.platform.mapper.CContactPeoRelMapper;
import com.mos.eboot.service.platform.service.ICContactPeoRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class CContactPeoRelService extends ServiceImpl<CContactPeoRelMapper, CContactPeoRel> implements ICContactPeoRelService {
	
}
