package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.IncomingPhoneConf;

/**
 * <p>
 * 来电弹屏手机号配置信息 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
public interface IIncomingPhoneConfService extends IService<IncomingPhoneConf> {
	
}
