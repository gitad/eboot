package com.mos.eboot.service.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Configuration;

import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.IMCustomerVO;

@Configuration
public  class jdbcUtil  {
	 
	// private static String url="jdbc:mysql://rm-8vb8w350e4h7198fpio.mysql.zhangbei.rds.aliyuncs.com:3306/peihuozhan?characterEncoding=utf8";
	// private static String user="root";
	// static String password="1qaz@WSXE";
	 
	 
	 /* private static String url="jdbc:mysql://127.0.0.1:3306/peihuozhan?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=utf-8&allowPublicKeyRetrieval=true";
	 private static String user="root";
	 private static String password="123456";*/
	 private static String url="jdbc:mysql://192.168.0.58:3306/peihuozhan?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=utf-8&allowPublicKeyRetrieval=true";
	 private static String user="root";
	 private static String password="MSxmlDB123";


    /*静志代码块在类加载时执行，
    		并且只执行一次。*/
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取数据库连接对象
     * @return 连接对象
     * @throws SQLException
     */
    public static Connection getConnection () throws SQLException {
            return DriverManager.getConnection(url,user,password);
    }

    /**
     * 关闭资源
     * @param conn 连接对象
     * @param ps 数据库操作对象
     * @param rs 结果集
     */
    public static void close(Connection conn, Statement ps, ResultSet rs){
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
   public static  IMCustomerVO get_tbl_bcustomer(IMCustomerVO custvo,String phone){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
 
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_bcustomer  where isDel='0' and ValidTel LIKE '%"+phone+"%';";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		  
           while (rs.next()){
        	   custvo.setTid(rs.getString("id"));//主键
        	   custvo.setCustName(rs.getString("CustomerName"));
        	   custvo.setValidPhone(rs.getString("ValidTel"));
        	   custvo.setOtherPhone(rs.getString("AnotherTel"));
        	   custvo.setQq(rs.getString("QQ"));
        	   //custvo.setCustWxId(rs.getString("WeiXinUr"));
        	   //custvo.setSysWxId(rs.getString("WeiXinMy"));
        	  // custvo.setWxStatus(rs.getString("WXStatus"));
        	   custvo.setProvinceName(rs.getString("AreaSheng"));
        	   custvo.setCityName(rs.getString("AreaCity"));
        	   custvo.setAreaName(rs.getString("AreaQu"));
        	   custvo.setCustType(rs.getString("TypeName"));
        	   custvo.setAddressInfo(rs.getString("Address"));
        	   custvo.setIsDel(rs.getString("IsDel"));
        	   custvo.setIsUpload(rs.getString("IsUpload"));
//        	   custvo.setRawAddUser(rs.getString(""));
//        	   custvo.setRawAddTime(rs.getString(""));
//        	   custvo.setRawUpUser(rs.getString(""));
//        	   custvo.getLastCallTime(rs.getString("")) 
        	   custvo.setRemark(rs.getString("Memo"));
        	   custvo.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));//update时间和add时间一样 方便配货站小詹修改数据
        	   custvo.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
        	  
        	  
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return custvo;
   }
   
   public static  List<CCallLog> get_tbl_bchat(String id,String cusId,String createTime){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
       List<CCallLog> list=new ArrayList<CCallLog>();
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_bchat where isDel='0' ";
           if(StringUtils.isNotBlank(id)){
        	   sql +=" AND id>"+id; 
           }
           if(StringUtils.isNotBlank(cusId)){
        	   sql +=" AND CusID="+cusId; 
           }
           if(StringUtils.isNotBlank(createTime)){
        	   sql +=" AND AddDate> '"+createTime+"'"; 
           }
           sql+=" ORDER BY id ASC  ";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		  
           while (rs.next()){
        	   CCallLog custvo=new CCallLog();
        	   custvo.setCallId(UuidUtil.get32UUID());
        	   custvo.settCustId(rs.getString("CusID"));//主键
        	   custvo.setTid(rs.getString("id"));//主键
        	   custvo.setUseFlag(1);
        	   custvo.setCallType(rs.getString("ChatType"));
        	   custvo.setCallRemark(rs.getString("Memo"));
        	   custvo.setCreateUser(rs.getString("AddMan"));     	   
        	   custvo.setCreateTime(Timestamp.valueOf(rs.getString("AddDate").replace("/", "-")));
        	   custvo.setUpdateTime(Timestamp.valueOf(rs.getString("AddDate").replace("/", "-")));
        	   custvo.setUpdateUser(rs.getString("AddMan")); 
        	 //  custvo.setPlanTime(Timestamp.valueOf(rs.getString("RdChatDate")+" 00:00:00".replace("/", "-")));
        	   custvo.setCallContent(rs.getString("ChatContents"));
        	   list.add(custvo);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return list;
   }
   public static  List<Customer> get_tbl_all_bcustomer(String id){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
       List<Customer> list=new ArrayList<Customer>();
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_bcustomer where isDel='0' ";
           if(StringUtils.isNotBlank(id)){
        	   sql +=" AND id>"+id; 
           }
           sql+=" ORDER BY id ASC";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		  
           while (rs.next()){
        	   Customer custvo=new Customer();
        	   custvo.setTid(rs.getString("id"));//主键
        	   custvo.setCustName(rs.getString("CustomerName"));
        	   custvo.setValidPhone(rs.getString("ValidTel"));
        	   custvo.setOtherPhone(rs.getString("AnotherTel"));
        	   custvo.setQq(rs.getString("QQ"));
        	  
        	   //custvo.setCustWxId(rs.getString("WeiXinUr"));
        	   //custvo.setSysWxId(rs.getString("WeiXinMy"));
        	  // custvo.setWxStatus(rs.getString("WXStatus"));
        	   custvo.setRawAddUser(rs.getString("AddMan"));
        	   custvo.setRawUpUser(rs.getString("AddMan"));
        	   custvo.setProvinceName(rs.getString("AreaSheng"));
        	   custvo.setCityName(rs.getString("AreaCity"));
        	   custvo.setAreaName(rs.getString("AreaQu"));
        	   custvo.setCustType(rs.getString("TypeName"));
        	   custvo.setAddressInfo(rs.getString("Address"));
        	   custvo.setIsDel(rs.getString("IsDel"));
        	   custvo.setIsUpload(rs.getString("IsUpload"));
//        	   custvo.setRawAddUser(rs.getString(""));
//        	   custvo.setRawAddTime(rs.getString(""));
//        	   custvo.setRawUpUser(rs.getString(""));
//        	   custvo.getLastCallTime(rs.getString("")) 
        	 //  custvo.setRawUpTime(rs.getString("UpdateTime"));
        	   custvo.setRawUpTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));//update时间和add时间一样 方便配货站小詹修改数据
        	   custvo.setRawAddTime(DateUtil.date_string(Calendar.getInstance().getTime(), "yyyy-MM-dd HH:mm:ss"));
        	   custvo.setRemark(rs.getString("Memo"));
        	   custvo.setCustAdminNickname(rs.getString("AddMan"));
        	   list.add(custvo);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return list;
   }
   
   
   public static  List<Customer> get_update_bcustomer(String updatetime){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
       List<Customer> list=new ArrayList<Customer>();
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_bcustomer where isDel=0 and UpdateTime>='"+updatetime+"'  ORDER BY UpdateTime  ASC";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		    //获取结果集的元数据
           ResultSetMetaData rsm = rs.getMetaData();
           //通过ResultSetMetaData获取结果集中的列数
           int count = rsm.getColumnCount();
 
           while (rs.next()){
        	   Customer custvo=new Customer();
        	   custvo.setTid(rs.getString("id"));//主键
        	   custvo.setCustName(rs.getString("CustomerName"));
        	   custvo.setValidPhone(rs.getString("ValidTel"));
        	   custvo.setOtherPhone(rs.getString("AnotherTel"));
        	   custvo.setQq(rs.getString("QQ"));
        	  
        	   //custvo.setCustWxId(rs.getString("WeiXinUr"));
        	   //custvo.setSysWxId(rs.getString("WeiXinMy"));
        	  // custvo.setWxStatus(rs.getString("WXStatus"));
        	   custvo.setRawAddUser(rs.getString("AddMan"));
        	   custvo.setRawUpUser(rs.getString("AddMan"));
        	   custvo.setProvinceName(rs.getString("AreaSheng"));
        	   custvo.setCityName(rs.getString("AreaCity"));
        	   custvo.setAreaName(rs.getString("AreaQu"));
        	   custvo.setCustType(rs.getString("TypeName"));
        	   custvo.setAddressInfo(rs.getString("Address"));
        	   custvo.setIsDel(rs.getString("IsDel"));
        	   custvo.setIsUpload(rs.getString("IsUpload"));
//        	   custvo.setRawAddUser(rs.getString(""));
//        	   custvo.setRawAddTime(rs.getString(""));
//        	   custvo.setRawUpUser(rs.getString(""));
//        	   custvo.getLastCallTime(rs.getString("")) 
        	 //  custvo.setRawUpTime(rs.getString("UpdateTime"));
        	   custvo.setRawUpTime(rs.getString("UpdateTime"));
        	   custvo.setRawAddTime(rs.getString("AddDate"));
        	   custvo.setRemark(rs.getString("Memo"));
        	   custvo.setCustAdminNickname(rs.getString("AddMan"));
        	   list.add(custvo);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return list;
   }
   
   
   public static  SysUser get_tbl_user(String name,String token){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
       SysUser user=new SysUser();
    		   
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_user WHERE LoginName='"+name+"' and token='"+token+"'";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		  
           while (rs.next()){
        	
        	   user.setUsername(rs.getString("LoginName"));
        	   user.setPassword(rs.getString("Password"));       	
        	   user.setNickname(rs.getString("RealName"));
        	   user.setMobile(rs.getString("Tel"));
               	         	  
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return user;
   }
   
   public static  List<SysUser> get_tbl_all_user(){
	   Connection conn =null;
		Statement  pstmt = null;
       ResultSet rs = null;
      
       List<SysUser> list=new ArrayList<SysUser>();	   
       try {
           //获取连接
           conn = jdbcUtil.getConnection();
           //获取预编译的数据库操作对象
           String sql = "SELECT * from tbl_user";
           pstmt = conn.createStatement();
		   rs = pstmt.executeQuery(sql);	
		  
           while (rs.next()){
        	   SysUser user=new SysUser();
        	   user.setUsername(rs.getString("LoginName"));
        	   user.setPassword(rs.getString("Password"));       	
        	   user.setNickname(rs.getString("RealName"));
        	   user.setMobile(rs.getString("Tel"));
        	   list.add(user);         	  
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }finally {
           //释放资源
       	close(conn, pstmt, rs);
       }
       return list;
   }
    
  
    
    public static void main(String[] args) {
    	System.out.println(Timestamp.valueOf("2021-11-15"));
    }

 
}
