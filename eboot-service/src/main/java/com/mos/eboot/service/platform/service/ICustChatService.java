package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CustChat;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhang
 * @since 2021-08-30
 */
public interface ICustChatService extends IService<CustChat> {
	
}
