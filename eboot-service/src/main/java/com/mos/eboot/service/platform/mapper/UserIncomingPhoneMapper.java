package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.UserIncomingPhone;

/**
 * <p>
  * 用户来电手机关联关系 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
public interface UserIncomingPhoneMapper extends BaseMapper<UserIncomingPhone> {

}