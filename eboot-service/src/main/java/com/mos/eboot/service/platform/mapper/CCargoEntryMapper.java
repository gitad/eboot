package com.mos.eboot.service.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCargoEntry;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface CCargoEntryMapper extends BaseMapper<CCargoEntry> {
	
	List<CCargoEntry> queryByAddress(@Param("address")String address);
	
	
	List<CCargoEntry> querySource(@Param("addressDetail")   String  addressDetail,
			@Param("customerPrice")   String  customerPrice,
			@Param("carsAndGoods")   String  carsAndGoods,
			@Param("planTime_start") String  planTime_start,
			@Param("planTime_end") String  planTime_end,
			@Param("cargoType") String  cargoType, Page<CCargoEntry> page);
	
	Integer querySourceTotal(@Param("addressDetail")   String  addressDetail,
			@Param("customerPrice")   String  customerPrice,
			@Param("carsAndGoods")   String  carsAndGoods,
			@Param("planTime_start") String  planTime_start,
			@Param("planTime_end") String  planTime_end,
			@Param("cargoType") String  cargoType);
	
	
}