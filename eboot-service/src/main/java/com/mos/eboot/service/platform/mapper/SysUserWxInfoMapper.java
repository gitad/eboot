package com.mos.eboot.service.platform.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.SysUserWxInfo;

public interface SysUserWxInfoMapper extends BaseMapper<SysUserWxInfo> {

	List<SysUserWxInfo> getWxByUserId(String userId);

	Boolean deleteByUserId(String id);

	Boolean updClientIdByTerminalId(Map<String, String> map);

}
