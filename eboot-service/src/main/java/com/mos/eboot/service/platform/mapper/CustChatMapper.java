package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CustChat;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-08-30
 */
public interface CustChatMapper extends BaseMapper<CustChat> {

}