package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CustPhone;

/**
 * <p>
 * 客户联系方式表 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface ICustPhoneService extends IService<CustPhone> {
	
}
