package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.PhoneCallOutLog;

/**
 * <p>
 * 自动拨打电话记录 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-08-17
 */
public interface PhoneCallOutLogMapper extends BaseMapper<PhoneCallOutLog> {

}