package com.mos.eboot.service.platform.controller;

 
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.WxGroup;
import com.mos.eboot.platform.entity.WxLogin;
import com.mos.eboot.service.platform.service.impl.CustWxService;
import com.mos.eboot.service.platform.service.impl.WxGroupService;
import com.mos.eboot.service.platform.service.impl.WxLoginService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-17
 */
@RestController
@RequestMapping("/wxGroup") 
public class WxGroupController {
	@Autowired
	WxGroupService wxGroupService;
	
	@Autowired
	WxLoginService wxLoginService;
	
	@Autowired
	CustWxService custWxService;
	
	    @PostMapping("/queryGroup")
		public ResultModel<Page<WxGroup>> queryGroup(@RequestBody  Page<WxGroup> page) {
	    	Map<String, Object> map=page.getCondition();
	    	String groupName="";
	    	String remark="";
	    	String carLen="";
	    	String carType="";
	    	String profit="";
	    	String owerName="";
	    	if(map!=null){
	    		  groupName=map.get("groupName")==null?"":map.get("groupName").toString();
		    	  remark=map.get("remark")==null?"":map.get("remark").toString();
		    	  carLen=map.get("carLen")==null?"":map.get("carLen").toString();
		    	  carType=map.get("carType")==null?"":map.get("carType").toString();
		    	  profit=map.get("profit")==null?"":map.get("profit").toString();
		    	  owerName=map.get("owerName")==null?"":map.get("owerName").toString();
	    	}
		 	EntityWrapper<WxGroup>  Wrapper=new EntityWrapper<WxGroup>();
		 	
		 	List<WxGroup> wxlist=wxGroupService.queryGroup(groupName,remark,carLen,carType,profit,owerName,page);
		  	
				for(int i=0;i<wxlist.size();i++){
					
					WxGroup group=JSONObject.parseObject(JSON.toJSONString(wxlist.get(i)), WxGroup.class);
					if(group.getGroupId()==null){
					    EntityWrapper<CustWx>  wxWrap=new EntityWrapper<CustWx>();
						wxWrap.eq("wx_id", group.getWxId());
						List<CustWx> list=custWxService.selectList(wxWrap);
						if(list.size()>0){
							for(CustWx li: list){
								
								 if(li.getWxType().equals("1")){//群主
									group.setOwerWx(li.getSysWxId());
								 }else
								 group.setGroupId(li.getWxId());
								 WxLogin login=wxLoginService.selectOne(new EntityWrapper<WxLogin>().eq("wx_id", li.getSysWxId()));
								 if(group.getWxNums()==null){
									 group.setWxNums(login.getWxNickname()); 
								 }else{
									 group.setWxNums(group.getWxNums()+","+login.getWxNickname()); 
								 }
								
							}
						}
					 
						WxGroup WxGroup= wxGroupService.selectOne(new EntityWrapper<WxGroup>().eq("group_id", group.getWxId()));	
						if(WxGroup==null){
							group.setGroupId(group.getWxId());
							 wxGroupService.insert(group);
						}
					   
					    wxlist.set(i, group);
				  } 
					 
					
				}
	    
		  
			
			 
			page.setRecords(wxlist);
			return new ResultModel<Page<WxGroup>>(ResultStatus.SUCCESS, page);
	    }
	    
	    
	    @PostMapping("/save")
		public ResultModel<Boolean> save(@RequestBody  WxGroup wxGroup) {
	    
	    	return new ResultModel<Boolean>(ResultStatus.SUCCESS, wxGroupService.updateById(wxGroup));
	    } 
}
