package com.mos.eboot.service.platform.controller;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.service.config.jdbcUtil;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.impl.CCallLogService;
import com.mos.eboot.service.platform.service.impl.CCarInfoRelService;
import com.mos.eboot.service.platform.service.impl.CCarInfoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/cCallLog")
public class CCallLogController {
	@Autowired
	CCarInfoService cCarInfoService;
	@Autowired
	CCallLogService cCallLogService;
	@Autowired
	CCarInfoRelService cCarInfoRelService;
	@Autowired
	private ICustomerService customerService;
	
	 @PostMapping("/query")
	public ResultModel<Page<CCallLog>> query(@RequestBody  Page<CCallLog> page) {
		Map<String,Object> condition= page.getCondition();	
		EntityWrapper<CCallLog>  Wrapper=new EntityWrapper<CCallLog>();
	    Wrapper.eq("USE_FLAG", 1);
	  
	    if(condition!=null){
		    if(condition.get("customerId")!=null){
		    	 Wrapper.eq("CUSTOMER_ID", condition.get("customerId").toString());
		    	 condition.remove("customerId");
			}		    
		     if(condition.get("contactPhone")!=null){//
		    	 Wrapper.eq("CONTACT_PHONE", condition.get("contactPhone").toString());
		    	 condition.remove("contactPhone");
			}
		     if(condition.get("callType")!=null){//
		    	 Wrapper.like("CALL_TYPE", condition.get("callType").toString());
		    	 condition.remove("callType");
			}
	    }
	    page.setSize(100000);
		Page<CCallLog> dataPage=cCallLogService.selectPage(page, Wrapper);
		 
			List<CCallLog>  list=dataPage.getRecords();
			for(CCallLog li:list){
				if(StringUtil.isNotBlank(li.getCustomerId())){
					Customer cust=customerService.selectById(li.getCustomerId());
					li.setCustName(cust.getCustName());
					li.setValidPhone(cust.getValidPhone());
				}else{
					li.setCustName("无");
					li.setValidPhone("无");
				}
			}
		 
		return new ResultModel<Page<CCallLog>>(ResultStatus.SUCCESS, dataPage);
    }
	 
	 
	 @PostMapping("/dataFilterQuery")
		public ResultModel<Page<CCallLog>> dataFilterQuery(@RequestBody  Page<CCallLog> page) {
			Map<String,Object> condition= page.getCondition();	
			EntityWrapper<CCallLog>  Wrapper=new EntityWrapper<CCallLog>();
		    Wrapper.eq("USE_FLAG", 1);
		  
		    if(condition!=null){
			    if(condition.get("customerId")!=null){
			    	 Wrapper.eq("CUSTOMER_ID", condition.get("customerId").toString());
			    	 condition.remove("customerId");
				}
			  
				    if(condition.get("cityName")!=null){
				   	  
				    	 Wrapper.like("CALL_CONTENT", condition.get("cityName").toString());
				    	 condition.remove("cityName");
					}
				    if(condition.get("goodsName")!=null){
				   	  
				    	 Wrapper.like("CALL_CONTENT", condition.get("goodsName").toString());
				    	 condition.remove("goodsName");
					}	
					
				    if(condition.get("carLen")!=null){
					   	  
				    	 Wrapper.like("CALL_CONTENT", condition.get("carLen").toString());
				    	 condition.remove("carLen");
					}
				    if(condition.get("carType")!=null){
					   	  
				    	 Wrapper.like("CALL_CONTENT", condition.get("carType").toString());
				    	 condition.remove("carType");
					}
			     if(condition.get("contactPhone")!=null){//
			    	 Wrapper.eq("CONTACT_PHONE", condition.get("contactPhone").toString());
			    	 condition.remove("contactPhone");
				}
			     if(condition.get("callType")!=null){//
			    	 Wrapper.like("CALL_TYPE", condition.get("callType").toString());
			    	 condition.remove("callType");
				}
		    }
			Page<CCallLog> dataPage=cCallLogService.selectPage(page, Wrapper);
			 
				List<CCallLog>  list=dataPage.getRecords();
				for(CCallLog li:list){
					if(StringUtil.isNotBlank(li.getCustomerId())){
						Customer cust=customerService.selectById(li.getCustomerId());
						li.setCustName(cust.getCustName());
						li.setValidPhone(cust.getValidPhone());
					}else{
						li.setCustName("无");
						li.setValidPhone("无");
					}
				}
			 
			return new ResultModel<Page<CCallLog>>(ResultStatus.SUCCESS, dataPage);
	    }
		 
	 
	 
	 
		@PostMapping("/insertCarInfo")
		@ResponseBody
		public Boolean insertCarInfo( @RequestBody 	Map<String,Object> map){	
		
			
			
			
		 		Object listString =map.get("list");
		 		Object carLen =map.get("selCarLen");
		 		Object carType =map.get("selCarType");
		 	 
		 		Object provinceCode =map.get("provinceCode");
		 		Object cityCode =map.get("cityCode");
		 		Object areaCode =map.get("areaCode");
		 		
		 		
		 		
		 		Object routeType =map.get("routeType");
		 		
		 		Object countryStart =map.get("countryStart");
		 		Object provinceStart =map.get("provinceStart");
		 		Object cityStart =map.get("cityStart");
		 		
		 		Object countryTrans =map.get("countryTrans");
		 		Object provinceTrans =map.get("provinceTrans");
		 		Object cityTrans =map.get("cityTrans");
		 		
		 		Object countryEnd =map.get("countryEnd");
		 		Object provinceEnd =map.get("provinceEnd");
		 		Object cityEnd =map.get("cityEnd");
		 		
		 		List<CCallLog>  list=JSONObject.parseArray(listString.toString(), CCallLog.class);
		 		if(list.size()>0){
		 			for(CCallLog li: list){
		 				if(StringUtil.isNotBlank(li.getCustomerId())){
		 					EntityWrapper<CCarInfo>  Wrapper=new EntityWrapper<CCarInfo>();
		 				    Wrapper.eq("CAR_LEN", carLen);
		 				    Wrapper.eq("CAR_TYPE", carType);
		 				    Wrapper.eq("CUSTOMER_ID", li.getCustomerId());
		 				    CCarInfo carInfo=cCarInfoService.selectOne(Wrapper);
		 				    if(carInfo==null){
		 				    	 carInfo=new CCarInfo();
		 				    	 carInfo.setCarId(UuidUtil.get32UUID());
		 			 
		 				    	 carInfo.setCreateTime(new Date());
		 				    	 carInfo.setCustomerId(li.getCustomerId());
		 				    	 carInfo.setCarLen(carLen.toString());
		 				    	 carInfo.setCarType(carType.toString());
		 				    	 carInfo.setUseFlag(1);
		 		 				 cCarInfoService.insert(carInfo);
		 		 				 
		 		 				 CCarInfoRel rel=new CCarInfoRel();
		 		 				 rel.setCarId(carInfo.getCarId());
		 		 				 rel.setCarRelId(UuidUtil.get32UUID());
		 		 				 rel.setType(1);
		 		 				 rel.setUseFlag(1);
		 		 				 rel.setCreateTime(new Date());
		 		 				 rel.setProvinceStart(provinceCode.toString());
		 		 				 rel.setCityStart(cityCode.toString());
		 		 				 rel.setCountryStart(areaCode.toString());
		 		 				 cCarInfoRelService.insert(rel);
		 		 				 
		 		 				
		 		 				 
		 		 				 
		 		 				 CCarInfoRel rel1=new CCarInfoRel();
	 		 					 rel1.setCarId(carInfo.getCarId());
	 		 					 rel1.setCarRelId(UuidUtil.get32UUID());
	 		 					 rel1.setType(2);
	 		 					 rel1.setUseFlag(1);
	 		 					 rel1.setCreateTime(new Date());
	 		 					 rel1.setRouteType(routeType.toString());
	 		 					 rel1.setProvinceStart(provinceStart.toString() );
	 		 					 rel1.setCityStart( cityStart.toString());
	 		 					 rel1.setCountryStart(countryStart.toString());
	 		 					 
	 		 					 rel1.setProvinceEnd(provinceEnd.toString());
	 		 					 rel1.setCityEnd(cityEnd.toString());
	 		 					 rel1.setCountryEnd(countryEnd.toString());
		 		 				 
		 		 				 if(routeType.equals("中转")){
		 		 				 
		 		 					rel1.setProvinceTrans(provinceTrans.toString());
		 		 					rel1.setCityTrans(cityTrans.toString());
		 		 					rel1.setCountryTrans(countryTrans.toString());
			 		 				cCarInfoRelService.insert(rel1);
		 		 		 
		 		 				 }else{
		 		 					 cCarInfoRelService.insert(rel1);
				 		 				
		 		 				 }
		 		 				 
		 		 				 
		 				    }else{
		 			 	
		 				    	
		 				    	
		 				    	
		 				    	
		 				    	//常跑地点
			 				   	EntityWrapper<CCarInfoRel>  relwrapper=new EntityWrapper<CCarInfoRel>();
			 				    relwrapper.eq("CAR_ID", carInfo.getCarId());
			 				    relwrapper.eq("PROVINCE_START", provinceCode);
			 				    relwrapper.eq("CITY_START", cityCode);
			 				    relwrapper.eq("COUNTRY_START", areaCode);
			 				    CCarInfoRel carRel=cCarInfoRelService.selectOne(relwrapper);
			 				    if(carRel==null){
			 				    	 CCarInfoRel rel=new CCarInfoRel();
			 		 				 rel.setCarId(carInfo.getCarId());
			 		 				 rel.setCarRelId(UuidUtil.get32UUID());
			 		 				 rel.setType(1);
			 		 				 rel.setUseFlag(1);
			 		 				 rel.setCreateTime(new Date());
			 		 				 rel.setProvinceStart(provinceCode.toString());
			 		 				 rel.setCityStart(cityCode.toString());
			 		 				 rel.setCountryStart(areaCode.toString());
			 		 				 cCarInfoRelService.insert(rel);
			 				    }
			 				
			 				 	  
		 		 				 if(routeType.equals("中转")){
		 		 					 
		 		 					EntityWrapper<CCarInfoRel>  relwrapper3=new EntityWrapper<CCarInfoRel>();
		 		 					relwrapper3.eq("CAR_ID", carInfo.getCarId());
		 		 					relwrapper3.eq("ROUTE_TYPE", routeType.toString());
		 		 					relwrapper3.eq("PROVINCE_TRANS", provinceTrans);
		 		 					relwrapper3.eq("CITY_TRANS", cityTrans);
		 		 					relwrapper3.eq("COUNTRY_TRANS", countryTrans);
		 		 					
		 		 					relwrapper3.eq("PROVINCE_START", provinceStart);
		 		 					relwrapper3.eq("CITY_START", cityStart);
		 		 					relwrapper3.eq("COUNTRY_START", countryStart);
		 		 					relwrapper3.eq("PROVINCE_END", provinceEnd);
		 		 					relwrapper3.eq("CITY_END",cityEnd);
		 		 					relwrapper3.eq("COUNTRY_END", countryEnd);;
		 		 					CCarInfoRel carRel3=cCarInfoRelService.selectOne(relwrapper3);
				 				    if(carRel3==null){
				 				    	CCarInfoRel rel3=new CCarInfoRel();
			 		 					rel3.setCarId(carInfo.getCarId());
			 		 					rel3.setCarRelId(UuidUtil.get32UUID());
			 		 					rel3.setType(2);
			 		 					rel3.setUseFlag(1);
			 		 					rel3.setRouteType(routeType.toString());
			 		 					rel3.setCreateTime(new Date());
			 		 					rel3.setProvinceTrans(provinceTrans.toString());
			 		 					rel3.setCityTrans(cityTrans.toString());
			 		 					rel3.setCountryTrans(countryTrans.toString());
			 		 					rel3.setProvinceStart(provinceStart.toString() );
			 		 					rel3.setCityStart( cityStart.toString());
			 		 					rel3.setCountryStart(countryStart.toString());
			 		 					 
			 		 					rel3.setProvinceEnd(provinceEnd.toString());
			 		 					rel3.setCityEnd(cityEnd.toString());
			 		 					rel3.setCountryEnd(countryEnd.toString());
				 		 				cCarInfoRelService.insert(rel3);
				 		 				
				 				    } 
		 		 					 
		 		 					
		 		 		 
		 		 				 } else{
		 		 					 
		 		 					  //常跑路线   开始
					 				  	EntityWrapper<CCarInfoRel>  relwrapper1=new EntityWrapper<CCarInfoRel>();
					 				  	relwrapper1.eq("CAR_ID", carInfo.getCarId());
					 					relwrapper1.eq("ROUTE_TYPE", routeType.toString());
					 				  	relwrapper1.eq("PROVINCE_START", provinceStart);
					 				  	relwrapper1.eq("CITY_START", cityStart);
					 				  	relwrapper1.eq("COUNTRY_START", countryStart);
					 				  	relwrapper1.eq("PROVINCE_END", provinceEnd);
					 				  	relwrapper1.eq("CITY_END",cityEnd);
					 				  	relwrapper1.eq("COUNTRY_END", countryEnd);
					 				  	
					 				  	CCarInfoRel carRel1=cCarInfoRelService.selectOne(relwrapper1);
					 				    if(carRel1==null){
					 				    	 CCarInfoRel rel1=new CCarInfoRel();
				 		 					 rel1.setCarId(carInfo.getCarId());
				 		 					 rel1.setCarRelId(UuidUtil.get32UUID());
				 		 					 rel1.setType(2);
				 		 					 rel1.setUseFlag(1);
				 		 					 rel1.setCreateTime(new Date());
				 		 					 rel1.setRouteType(routeType.toString());
				 		 					 rel1.setProvinceStart(provinceStart.toString() );
				 		 					 rel1.setCityStart( cityStart.toString());
				 		 					 rel1.setCountryStart(countryStart.toString());
				 		 					 
				 		 					rel1.setProvinceEnd(provinceEnd.toString());
				 		 					rel1.setCityEnd(cityEnd.toString());
				 		 					rel1.setCountryEnd(countryEnd.toString());
				 		 					 cCarInfoRelService.insert(rel1);
					 				    } 
					 				   
		 		 					 
		 		 					 
		 		 					 
		 		 				 }
			 				    
			 				    
		 				    }
		 				}
		 			}
		 		}	
		 	return null;	 
		 }
	 
	 
	 @PostMapping("/queyCustomer")
	 public Customer queyCustomer(@RequestParam("customerId") String customerId){
	  Customer cus=customerService.selectById(customerId);
	  return cus;
	 }
	//通话记录同步
	 @PostMapping("/call_log_syn")
	 public ResultModel<Boolean>  callLogSyn(){
		 Boolean result=true;
		 try {
			 Integer num= cCallLogService.getMaxTid();
			 List<CCallLog> list=jdbcUtil.get_tbl_bchat(num.toString(),null,null) ;			
			 if(list.size()>0){
				 for(int i=0;i<list.size();i++){
					 CCallLog log=list.get(i);
					 cCallLogService.insert(log);
					
				 } 
			 } 
	    	//循环到最后一条 更新数据
			 EntityWrapper<CCallLog>  Wrapper=new EntityWrapper<CCallLog>();
			 Wrapper.isNull("CUSTOMER_ID");
			 List<CCallLog> upList= cCallLogService.selectList(Wrapper);
			 for(CCallLog up:upList){
			    EntityWrapper<Customer>  cswrapper=new EntityWrapper<Customer>();	
			    cswrapper.eq("is_del", "0");
				cswrapper.eq("tid",up.gettCustId());
				Customer cus=customerService.selectOne(cswrapper);
				if(cus!=null){
					 up.setCustomerId(cus.getId()); 
					 cCallLogService.updateById(up);
				 }
				
			 }
				 
		} catch (Exception e) {
			result=false;
		}
		
		    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
		} 
	@PostMapping("/save")
	public ResultModel<CCallLog>  save(@RequestBody  CCallLog  cCallLog) {
		 boolean result; 	
		if(StringUtils.isBlank(cCallLog.getCallId())){			 
			cCallLog.setCallId(UuidUtil.get32UUID());
			cCallLog.setUseFlag(1);
			//cCallLog.setCreateTime();
		    result=cCallLogService.insert(cCallLog);	
		}else{		
			result=cCallLogService.updateById(cCallLog)	;
		}
		 
	    return new ResultModel<CCallLog>(ResultStatus.SUCCESS, cCallLog);
	}
	
	@PostMapping("/del")
	public ResultModel<Boolean>  del(@RequestBody CCallLog  cCallLog) { 
 
	    boolean result=cCallLogService.deleteById(cCallLog.getCallId());
	    return new ResultModel<Boolean>(ResultStatus.SUCCESS, result);
	}
	
	@PostMapping("/query_by_entity")
	public ResultModel<List<CCallLog>>  queryByEntity(@RequestBody CCallLog  cCallLog) { 
		 EntityWrapper<CCallLog>  Wrapper=new EntityWrapper<CCallLog>();
		 if(cCallLog.getCustomerId()!=null){
			 Wrapper.eq("CUSTOMER_ID",cCallLog.getCustomerId());
		 }
		 if(cCallLog.getContactPhone()!=null){
			 Wrapper.eq("CONTACT_PHONE",cCallLog.getContactPhone());
		 }
		 List<CCallLog> result=cCallLogService.selectList(Wrapper);
	    return new ResultModel<List<CCallLog>>(ResultStatus.SUCCESS, result);
	}
	
}
