package com.mos.eboot.service.platform.service;

 
 
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CustomerLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-06-05
 */
public interface ICustomerLogService extends IService<CustomerLog> {
	
}
