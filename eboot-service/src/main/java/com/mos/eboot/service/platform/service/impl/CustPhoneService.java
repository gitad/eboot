package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.service.platform.mapper.CustPhoneMapper;
import com.mos.eboot.service.platform.service.ICustPhoneService;

/**
 * <p>
 * 客户联系方式表 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@Service
public class CustPhoneService extends ServiceImpl<CustPhoneMapper, CustPhone> implements ICustPhoneService {
	
}
