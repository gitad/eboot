package com.mos.eboot.service.platform.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.PhoneCallOutLog;
import com.mos.eboot.service.platform.service.IPhoneCallOutLogService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;

/**
 * <p>
 * 自动拨打电话记录 前端控制器
 * </p>
 *
 * @author zhang
 * @since 2021-08-17
 */
@RestController
@RequestMapping("/api/PhoneCallOutLog")
public class PhoneCallOutLogController {

	@Autowired
	private IPhoneCallOutLogService iPhoneCallOutLogService;

	@PostMapping("/save")
	public ResultModel<String> save(@RequestBody PhoneCallOutLog phoneCallOutLog) {

		boolean flag=iPhoneCallOutLogService.insert(phoneCallOutLog);
		
		if(!flag) {
			return ResultModel.defaultError("新增失败");
		}
		
		return ResultModel.defaultSuccess("成功");
	}

	@PostMapping("/update")
	public ResultModel<String> update(@RequestBody PhoneCallOutLog phoneCallOutLog) {
		EntityWrapper<PhoneCallOutLog> wrapper = new EntityWrapper<PhoneCallOutLog>();
		wrapper.eq("call_id", phoneCallOutLog.getCallId());
		wrapper.and().eq("id", phoneCallOutLog.getId());
		boolean flag = iPhoneCallOutLogService.update(phoneCallOutLog, wrapper);

		if (!flag) {
			return ResultModel.defaultError("失败");
		}

		return ResultModel.defaultSuccess("成功");
	}

	@PostMapping("/get")
	public ResultModel<PhoneCallOutLog> selectByCondition(@RequestBody PhoneCallOutLog phoneCallOutLog) {
		EntityWrapper<PhoneCallOutLog> wrapper = new EntityWrapper<PhoneCallOutLog>();

		if (StringUtils.isNotEmpty(phoneCallOutLog.getPhoneNum())) {
			wrapper.eq("phone_num", phoneCallOutLog.getPhoneNum());
		}
		if (phoneCallOutLog.getcId() != null) {
			wrapper.eq("c_id", phoneCallOutLog.getcId());
		}
		if (StringUtils.isNotEmpty(phoneCallOutLog.getCallId())) {
			wrapper.eq("call_id", phoneCallOutLog.getCallId());
		}
		if (StringUtils.isNotEmpty(phoneCallOutLog.getUserId())) {
			wrapper.eq("user_id", phoneCallOutLog.getUserId());
		}

		PhoneCallOutLog callOutLog = iPhoneCallOutLogService.selectOne(wrapper);
		return new ResultModel<PhoneCallOutLog>(ResultStatus.SUCCESS, callOutLog);
	}

	@PostMapping("/list")
	public ResultModel<List<PhoneCallOutLog>> selectList(@RequestBody PhoneCallOutLog phoneCallOutLog) {
		EntityWrapper<PhoneCallOutLog> wrapper = new EntityWrapper<PhoneCallOutLog>();

		if (StringUtils.isNotEmpty(phoneCallOutLog.getPhoneNum())) {
			wrapper.eq("phone_num", phoneCallOutLog.getPhoneNum());
		}
		if (phoneCallOutLog.getcId() != null) {
			wrapper.eq("c_id", phoneCallOutLog.getcId());
		}
		if (StringUtils.isNotEmpty(phoneCallOutLog.getCallId())) {
			wrapper.eq("call_id", phoneCallOutLog.getCallId());
		}
		if (StringUtils.isNotEmpty(phoneCallOutLog.getUserId())) {
			wrapper.eq("user_id", phoneCallOutLog.getUserId());
		}

		List<PhoneCallOutLog> list = iPhoneCallOutLogService.selectList(wrapper);
		return new ResultModel<List<PhoneCallOutLog>>(ResultStatus.SUCCESS, list);
	}
}
