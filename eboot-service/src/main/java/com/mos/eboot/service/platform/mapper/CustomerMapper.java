package com.mos.eboot.service.platform.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.Customer;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface CustomerMapper extends BaseMapper<Customer> {
	int  getMaxTid();
	int saveCustomer(Customer customer);
	int updateCustomer(Customer customer);
	int updateTemCustomer(Customer customer);
	 
	 List<Customer> queryNameAndPhone(@Param("commonName")   String  commonName,
			 @Param("commonPhone")   String  commonPhone, Page<Customer> page);
	 Integer queryNameAndPhoneTotal(@Param("commonName")   String  commonName, @Param("commonPhone")   String  commonPhone );
	 
	 List<Customer> queryNameCf();
	 List<Customer> queryPhoneCf();
}