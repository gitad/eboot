package com.mos.eboot.service.platform.mapper;

import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface CCargoEntryRelMapper extends BaseMapper<CCargoEntryRel> {

}