package com.mos.eboot.service.platform.service.impl;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerXnh;
import com.mos.eboot.service.platform.mapper.CustomerXnhMapper;
import com.mos.eboot.service.platform.service.ICustomerXnhService;

/**
 * <p>
 * 用户虚拟号表 服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-22
 */
@Service
public class CustomerXnhService extends ServiceImpl<CustomerXnhMapper, CustomerXnh> implements ICustomerXnhService {
	@Autowired
	private CustomerXnhMapper customerMapper;

	@Override
	public int saveCustomer(CustomerXnh customer) {
		return customerMapper.saveCustomer(customer);
	}
}
