package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.CustChat;
import com.mos.eboot.service.platform.mapper.CustChatMapper;
import com.mos.eboot.service.platform.service.ICustChatService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-08-30
 */
@Service
public class CustChatService extends ServiceImpl<CustChatMapper, CustChat> implements ICustChatService {
	
}
