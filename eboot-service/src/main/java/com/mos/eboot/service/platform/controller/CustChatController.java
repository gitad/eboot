package com.mos.eboot.service.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CustChat;
import com.mos.eboot.service.platform.service.ICustChatService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zhang
 * @since 2021-08-30
 */
@RestController
@RequestMapping("api/custchat")
public class CustChatController {

	@Autowired
	private ICustChatService custChatService;

	@GetMapping("/list/all")
	public ResultModel<Page<CustChat>> queryList(@RequestBody Page<CustChat> page) {
		return new ResultModel<Page<CustChat>>(ResultStatus.SUCCESS, custChatService.selectPage(page));
	}

	@GetMapping("/list/by-cid")
	public ResultModel<Page<CustChat>> queryList(@RequestBody Page<CustChat> page,
			@RequestParam(name = "cid") Integer cid) {
		EntityWrapper<CustChat> entityWrapper = new EntityWrapper<CustChat>();
		entityWrapper.eq("c_id", cid);
		return new ResultModel<Page<CustChat>>(ResultStatus.SUCCESS, custChatService.selectPage(page, entityWrapper));
	}

	@GetMapping("/get/by/condition")
	public ResultModel<CustChat> queryCustChatByCondition(@RequestBody CustChat condition) {
		EntityWrapper<CustChat> wrapper = new EntityWrapper<CustChat>(condition);
		return new ResultModel<CustChat>(ResultStatus.SUCCESS, custChatService.selectOne(wrapper));
	}

	@PostMapping("/save")
	public ResultModel<String> saveCustChat(@RequestBody CustChat custChat) {
		custChatService.insert(custChat);
		return ResultModel.defaultSuccess("成功");
	}
}
