package com.mos.eboot.service.platform.mapper;

 import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CQuickReply;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-17
 */
public interface CQuickReplyMapper extends BaseMapper<CQuickReply> {

}