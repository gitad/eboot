package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.WxFriendsEboot;

/**
 * <p>
  * 微信好友表 Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-11-22
 */
public interface WxFriendsMapper extends BaseMapper<WxFriendsEboot> {

}