package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CustImRole;

/**
 * <p>
 * IM弹框窗口操作菜单权限 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface ICustImRoleService extends IService<CustImRole> {
	
}
