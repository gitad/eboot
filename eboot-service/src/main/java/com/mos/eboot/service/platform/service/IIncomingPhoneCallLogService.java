package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.IncomingPhoneCallLog;

/**
 * <p>
 * 来电-通话记录 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-20
 */
public interface IIncomingPhoneCallLogService extends IService<IncomingPhoneCallLog> {

}
