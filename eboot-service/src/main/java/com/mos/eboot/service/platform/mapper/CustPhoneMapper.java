package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CustPhone;

/**
 * <p>
  * 客户联系方式表 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface CustPhoneMapper extends BaseMapper<CustPhone> {

}