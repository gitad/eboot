package com.mos.eboot.service.platform.service.impl;

import com.mos.eboot.platform.entity.SysDocFile;
import com.mos.eboot.service.platform.mapper.SysDocFileMapper;
import com.mos.eboot.service.platform.service.ISysDocFileService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Service
public class SysDocFileService extends ServiceImpl<SysDocFileMapper, SysDocFile> implements ISysDocFileService {
	
}
