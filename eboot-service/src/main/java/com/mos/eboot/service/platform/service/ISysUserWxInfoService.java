package com.mos.eboot.service.platform.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.SysUserWxInfo;

public interface ISysUserWxInfoService extends IService<SysUserWxInfo> {

	List<SysUserWxInfo> getWxByUserId(String userId);

	Boolean deleteByUserId(String id);

	Boolean updClientIdByTerminalId(String terminalId, String clientId);

}
