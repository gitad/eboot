package com.mos.eboot.service.platform.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CustImRole;

/**
 * <p>
  * IM弹框窗口操作菜单权限 Mapper 接口
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
public interface CustImRoleMapper extends BaseMapper<CustImRole> {

}