package com.mos.eboot.service.platform.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.impl.CCarInfoService;
import com.mos.eboot.service.platform.service.impl.CCargoEntryRelService;
import com.mos.eboot.service.platform.service.impl.CCargoEntryService;
import com.mos.eboot.service.platform.service.impl.CCargoInfoService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/cCargoInfo")
public class CCargoInfoController {
	
	@Autowired
	CCargoInfoService cCargoInfoService;
	
	@Autowired
	CCargoEntryService cCargoEntryService;
	
	@Autowired
	CCargoEntryRelService cCargoEntryRelService;
	
	 @Autowired
	 ICustomerService customerService;
	
	 @Autowired
	 CCarInfoService cCarInfoService;
	 
	 @PostMapping("/query")
	 public ResultModel<Page<CCargoInfo>> query(@RequestBody  Page<CCargoInfo> page) {
		Map<String,Object> condition= page.getCondition();
		EntityWrapper<CCargoInfo>  Wrapper=new EntityWrapper<CCargoInfo>();
	    Wrapper.eq("USE_FLAG", 1);
		   if(condition!=null){
			   if(condition.get("phone")!=null){//电话
			    	 Wrapper.like("PHONE", condition.get("phone").toString());
			    	 condition.remove("phone");
				}
			    if(condition.get("cargoId")!=null){ 
			    	 Wrapper.like("CARGO_ID", condition.get("cargoId").toString());
			    	 condition.remove("cargoId");
				} 
			    if(condition.get("customerId")!=null){ 
			    	 Wrapper.like("CUSTOMER_ID", condition.get("customerId").toString());
			    	 condition.remove("customerId");
				} 
		   }
		
	   
	   
		Page<CCargoInfo> dataPage=cCargoInfoService.selectPage(page, Wrapper);
		List<CCargoInfo> cargoList=dataPage.getRecords();
		for(CCargoInfo ca:cargoList){
		 	CCargoEntry li=cCargoEntryService.selectById(ca.getCargoId());
		 	if(li!=null){
		 		//System.out.println(li.getCarsGoodsAddress()+"----------");
				 ca.setCarsGoodsAddress(li.getCarsAndGoods());
		 	}
		
		
			
		} 
		return new ResultModel<Page<CCargoInfo>>(ResultStatus.SUCCESS, dataPage);
    }

	 
 
	 
	 @PostMapping("/queryCarGo")
	 public List<CCargoInfo> queryCarGo(@RequestBody  Map<String, Object> condition) {
		 
		 List<CCargoInfo> dataPage=null;
		
		 String type="";
		 if(condition!=null){
			 CCargoEntry CCargoEntry= JSONObject.parseObject(condition.get("CCargoEntry").toString(), CCargoEntry.class); 
			  type=condition.get("type")==null?"":condition.get("type").toString();
		      if(type.equals("2")){//匹配当天车辆即可
		    	  dataPage=cCargoInfoService.queryCarGo(CCargoEntry.getCargoId(),CCargoEntry.getCustomerId(),type); 
		      }   
		      if(type.equals("1")){//匹配历史货源信息
		    	   EntityWrapper<CCargoEntryRel>  stwrapper=new EntityWrapper<CCargoEntryRel>();//开始
		    	   stwrapper.eq("CARGO_ID",CCargoEntry.getCargoId()); 
		    	   stwrapper.eq("TRANSPORT_TYPE","0"); 
		    	   stwrapper.orderBy("ORDER_BY"); 
				   CCargoEntryRel st= cCargoEntryRelService.selectOne(stwrapper); 
				   
				   EntityWrapper<CCargoEntryRel>  endwrapper=new EntityWrapper<CCargoEntryRel>();//开始
				   endwrapper.eq("CARGO_ID",CCargoEntry.getCargoId()); 
				   endwrapper.eq("TRANSPORT_TYPE","1"); 
				   endwrapper.orderBy("ORDER_BY"); 
				   CCargoEntryRel end= cCargoEntryRelService.selectOne(endwrapper); 
				   
				   
				    
				   EntityWrapper<CCargoInfo>  cargowrapper=new EntityWrapper<CCargoInfo>();//货源匹配
				   cargowrapper.like("ADDRESS_DETAIL_STR", st.getCityCode());
				   cargowrapper.like("ADDRESS_DETAIL_END", end.getCityCode());
				   cargowrapper.like("CAR_LEN", CCargoEntry.getCarLen());
				   cargowrapper.like("CAR_TYPE", CCargoEntry.getCarType());
				   cargowrapper.lt("CREATE_TIME",  new SimpleDateFormat("yyyy-MM-dd").format(new Date()) );
				   dataPage=cCargoInfoService.selectList(cargowrapper);
				   
		      }
			 
		      
		 }
		 if(dataPage.size()>0){
			 for(CCargoInfo ca: dataPage){
				 if(ca.getCustomerId().equals("-1")){
					  ca.setWxStatus("无");
					  ca.setCustName("虚拟号");
					  ca.setVaildPhone(ca.getPhone()); 
				 }else{
					  EntityWrapper<Customer>  wrapper=new EntityWrapper<Customer>();
					  wrapper.eq("id", ca.getCustomerId());
					  wrapper.eq("is_del", "0");
					  Customer cust= customerService.selectOne(wrapper);
					  if(cust!=null){
						  ca.setWxStatus(cust.getWxStatus());
						  ca.setCustName(cust.getCustName());
						  ca.setVaildPhone(cust.getValidPhone()==null?"":cust.getValidPhone());
					  }
					   
				 }
				
			 }
			 
			 
		 }
		
		 
		return dataPage;
    }
	 
	 @PostMapping("/cCargoQuery")
	 public ResultModel<Page<CCargoEntry>> cCargoQuery (@RequestBody  Page<CCargoEntry> page) {	 
			EntityWrapper<CCargoEntry>  Wrapper=new EntityWrapper<CCargoEntry>();	 	
		//更具地址查询货源 
		Map<String,Object> condition= page.getCondition();
		if(condition.get("address")!=null){
			  Wrapper.like("ADDRESS_DETAIL_STR", condition.get("address").toString()).or().like("ADDRESS_DETAIL_END", condition.get("address").toString());
			  condition.remove("address");
		}			
	    Wrapper.eq("USE_FLAG", 1); 
	   // Wrapper.like(column, value)
		Page<CCargoEntry> dataPage=cCargoEntryService.selectPage(page, Wrapper);		 
		return new ResultModel<Page<CCargoEntry>>(ResultStatus.SUCCESS, dataPage);
    }
 
	 
	 
	 
	 @PostMapping("/save")
	 public CCargoInfo save(@RequestBody   CCargoInfo CCargoInfo) {
		 boolean bool;
		 
		   //保存时选查询 车辆
			EntityWrapper<CCarInfo>  wrapper=new EntityWrapper<CCarInfo>();	 	
			wrapper.like("CAR_LEN",CCargoInfo.getCarLen() );
			wrapper.like("CAR_TYPE", CCargoInfo.getCarType());
			CCarInfo carInfo=cCarInfoService.selectOne(wrapper);
		   if(carInfo==null){
			   carInfo=new CCarInfo(); 
			   carInfo.setCustomerId(CCargoInfo.getCustomerId());
			   carInfo.setCarType(CCargoInfo.getCarType());
			   carInfo.setCarLen(CCargoInfo.getCarLen());
			   carInfo.setUseFlag(1);
			   carInfo.setCarId(UuidUtil.get32UUID());
			   carInfo.setCreateTime(new Date());
			  cCarInfoService.insert(carInfo);	
		   }		
		 if(CCargoInfo.getCargoInfoId()!=null){
			  CCargoInfo.setCarId(carInfo.getCarId());
			  bool=cCargoInfoService.updateById(CCargoInfo);
		 }else{		
			 CCargoInfo.setUseCar(0);
			  CCargoInfo.setCarId(carInfo.getCarId());
			 CCargoInfo.setCargoInfoId(UuidUtil.get32UUID());
			 CCargoInfo.setUseFlag(1);
			 CCargoInfo.setCreateTime(new Date());
			 bool= cCargoInfoService.insert(CCargoInfo);
		 }
		
		return CCargoInfo;
    }
	 @PostMapping("/sourceSave")
	 public CCargoInfo sourceSave(@RequestBody   CCargoInfo CCargoInfo) {
		 boolean bool;
		   bool=cCargoInfoService.updateById(CCargoInfo);
		 
		return CCargoInfo;
    }

	 @PostMapping("/query_by_entity")
	 public ResultModel<List<CCargoInfo>> queryByEntity (@RequestBody  CCargoInfo  cCargoInfo) {	 
			EntityWrapper<CCargoInfo>  Wrapper=new EntityWrapper<CCargoInfo>();	 			 
			 if(cCargoInfo.getCustomerId()!=null){
				 Wrapper.eq("CUSTOMER_ID",cCargoInfo.getCustomerId());
			 }
			 if(cCargoInfo.getPhone()!=null){
				 Wrapper.eq("PHONE",cCargoInfo.getPhone());
			 }
			 List<CCargoInfo> list=cCargoInfoService.selectList(Wrapper);		 
		return new ResultModel<List<CCargoInfo>>(ResultStatus.SUCCESS, list);
    }
 
	
}
