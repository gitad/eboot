package com.mos.eboot.service.platform.service.impl;

 
import com.mos.eboot.platform.entity.CQuickReply;
import com.mos.eboot.service.platform.mapper.CQuickReplyMapper;
import com.mos.eboot.service.platform.service.ICQuickReplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-17
 */
@Service
public class CQuickReplyService extends ServiceImpl<CQuickReplyMapper, CQuickReply> implements ICQuickReplyService {
	
}
