package com.mos.eboot.service.platform.service;

 
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.WxLogin;

/**
 * <p>
 * 微信登录用户表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-11
 */
public interface IWxLoginService extends IService<WxLogin> {
	
}
