package com.mos.eboot.service.platform.service.impl;

import java.util.List;

import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.service.platform.mapper.CCargoEntryMapper;
import com.mos.eboot.service.platform.mapper.COrderMapper;
import com.mos.eboot.service.platform.service.ICOrderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@Service
public class COrderService extends ServiceImpl<COrderMapper, COrder> implements ICOrderService {
	@Autowired
	private COrderMapper cOrderMapper;

	@Override 
	public COrder queryCount(@RequestParam("createTimeStart")   String  createTimeStart,
							 @RequestParam("createTimeEnd")   String  createTimeEnd,
							 @RequestParam("payPathId")   String  payPathId,
							 @RequestParam("createId")   String  createId ) {
		return cOrderMapper.queryCount(createTimeStart,createTimeEnd,payPathId,createId);
	}
	@Override 
	public List<COrder> queryCountDetail(@RequestParam("createTimeStart")   String  createTimeStart,
							 @RequestParam("createTimeEnd")   String  createTimeEnd,
							 @RequestParam("payPathId")   String  payPathId,
							 @RequestParam("createId")   String  createId , @RequestParam("orderNum")   String  orderNum ) {
		return cOrderMapper.queryCountDetail(createTimeStart,createTimeEnd,payPathId,createId,orderNum);
	}

}
