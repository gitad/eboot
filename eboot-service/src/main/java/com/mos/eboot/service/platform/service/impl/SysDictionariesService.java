package com.mos.eboot.service.platform.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.service.platform.mapper.CustomerMapper;
import com.mos.eboot.service.platform.mapper.SysDictionariesMapper;
import com.mos.eboot.service.platform.service.ISysDictionariesService;

 
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
@Service
public class SysDictionariesService extends ServiceImpl<SysDictionariesMapper, SysDictionaries> implements ISysDictionariesService {
	@Autowired
	private SysDictionariesMapper sysDictionariesMapper;

	@Override
	public List<SysDictionaries> listSubDictByParentId(@RequestParam("parentId")   String  parentId) {
		return sysDictionariesMapper.listSubDictByParentId(parentId);
	}

	@Override
	public List<SysDictionaries> listSubDict() {
		return sysDictionariesMapper.listSubDict();
	}
}
