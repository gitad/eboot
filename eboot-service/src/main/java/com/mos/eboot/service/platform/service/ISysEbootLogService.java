package com.mos.eboot.service.platform.service;

 
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.SysEbootLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
public interface ISysEbootLogService extends IService<SysEbootLog>  {
	
}
