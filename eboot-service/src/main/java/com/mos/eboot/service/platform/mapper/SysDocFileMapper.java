package com.mos.eboot.service.platform.mapper;

import com.mos.eboot.platform.entity.SysDocFile;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface SysDocFileMapper extends BaseMapper<SysDocFile> {

}