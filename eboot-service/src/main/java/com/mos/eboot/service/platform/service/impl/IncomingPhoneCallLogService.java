package com.mos.eboot.service.platform.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.IncomingPhoneCallLog;
import com.mos.eboot.service.platform.mapper.IncomingPhoneCallLogMapper;
import com.mos.eboot.service.platform.service.IIncomingPhoneCallLogService;

/**
 * <p>
 * 来电-通话记录 服务实现类
 * </p>
 *
 * @author zhang
 * @since 2021-05-20
 */
@Service
public class IncomingPhoneCallLogService extends ServiceImpl<IncomingPhoneCallLogMapper, IncomingPhoneCallLog>
		implements IIncomingPhoneCallLogService {

}
