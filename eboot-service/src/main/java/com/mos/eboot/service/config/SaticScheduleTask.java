package com.mos.eboot.service.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;



















import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.HisWxMsg;
import com.mos.eboot.service.platform.service.ICustWxService;
import com.mos.eboot.service.platform.service.ICustomerService;
import com.mos.eboot.service.platform.service.impl.CCustomerMarketService;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.DateUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.ImMsgConversationListVO;
/**
 * 
 * 定时任务
 * */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling  
public class SaticScheduleTask {
	@Autowired
	ICustomerService customerService ;
	@Autowired
	CCustomerMarketService customerMarketService;
	@Autowired
	ICustWxService iCustWxService;
	@Autowired
	private RedisUtil redisUtil;
	  //3.添加定时任务
   // @Scheduled(cron = "00 * * * * ?")
    //或直接指定时间间隔，例如：5秒
	//将分配得人员进行处理
   // @Scheduled(fixedRate=60*1000)
    private void configureTasks() {
    	 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
    	EntityWrapper<Customer> entity=new EntityWrapper<Customer>();
    	entity.eq("is_del", "0");
    	entity.isNotNull("cust_admin_user_id_tem");
    	List<Customer> list=customerService.selectList(entity);
    	if(list.size()>0){
    		for(Customer li:list){
        		 Long  time=DateUtil.pastMinutes(li.getTemTime())/60;
        		 if(time>=24){//超过24小时的则将数据还给原先的人
        		   customerService.updateTemCustomer(li);
        			String[] str=new String[2];
        			str[0]=li.getId();
        			str[1]=li.getCustAdminUserId();
        		   map.put(li.getCustWxId(),str);
        		 }
         
        		
        	}
    		redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,map,-1); 
    	}
    	
     
    }
    
    //对超过两小时未回消息得人进行消息提醒
  //  @Scheduled(fixedRate=60*1000)
    private void SystemMess() {
    	 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_HISTORY);   
    	 for(Object key:map.keySet()){//keySet获取map集合key的集合  然后在遍历key即可
    		List<HisWxMsg> list= JSONObject.parseArray(map.get(key).toString(), HisWxMsg.class);
    		 if(list.size()>0){
    			 HisWxMsg his=list.get(list.size()-1);
    			 if(his.getSYSFLAG().equals("0")){//查看最后一条是否是对方发的消息
    				 try {
						Long  time=DateUtil.pastMinutes(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(his.getCTIME()))/60;
						if(time>=2){//超过两小时
							EntityWrapper<Customer> entity=new EntityWrapper<Customer>();
					    	entity.eq("id",key);
					    	Customer cust=customerService.selectOne(entity);
					    	if(cust!=null){
					    		
					    		String peoId="";
					    		if(StringUtils.isNotBlank(cust.getCustAdminUserIdTem())){//如何已分配别人 先按照分配的来
					    			 peoId= cust.getCustAdminUserIdTem();	
					    		}else if(StringUtils.isNotBlank(cust.getCustAdminUserId())){					    			 
					    			 peoId= cust.getCustAdminUserId();	
					    		}else{
					    			 peoId="1";
					    		}
					    		List<Object> chatList= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + peoId, 0, -1);
					    		if(chatList!=null){
					    			for (int i=0;i< chatList.size();i++) {
					    				ImMsgConversationListVO  conversationListVO = (ImMsgConversationListVO) chatList.get(i);
					    				if (conversationListVO.getcId().equals(key)) {
					    					conversationListVO.setWxWarm(true);
					    					redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + peoId, i, conversationListVO);//更新聊天消息	
					    					break;
					    				}
					    			}
					    		}
					    	}
						}
					 
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			 }
    		 }
      
         }
  
    }
    
    //更新微信和用户之间的关系 每天晚上三点定时执行
    @Scheduled(cron = "0 10 3 * * ?")
    private void updateWxAndCustomer() {
    	
    	 EntityWrapper<CustWx> entityWrapper = new EntityWrapper<CustWx>();
		  entityWrapper.isNull("c_id").or().eq("c_id","");	 
		  //entityWrapper.eq("wx_id","wxid_ai1lyes8p2x422");	
		 // entityWrapper.eq("sys_wx_id","wxid_wj9qo6gvftnk22");	
		  
		 // entityWrapper.like("wx_remark", "1"); 
		 List<CustWx>  wxFriendsList=iCustWxService.selectList(entityWrapper);
		// Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);  
		 if(wxFriendsList.size()>0){
			 for(CustWx cu:wxFriendsList){
				 String strPhone=StringUtil.str_Phone(cu.getWxRemark());
				 if(!StringUtil.isBlank(strPhone)){
					 EntityWrapper<Customer> curapper = new EntityWrapper<Customer>();
					 curapper.eq("is_del", "0");
					 curapper.like("valid_phone", strPhone).or().like("other_phone", strPhone);
					 Customer customer=customerService.selectOne(curapper);
						 if(customer!=null){
							 String[] custr=new String[2];
							 custr[0]=customer.getId();
							 custr[1]=customer.getCustAdminUserId();
							 //map.put(cu.getWxId(), JSON.toJSONString(custr));	
							 cu.setcId(customer.getId());	
							 cu.setUserId(customer.getCustAdminUserId());
							 iCustWxService.updateById(cu);
							 
							
							 
							}
						 }						 
				 }
			 }   
    }
 
 
}

