package com.mos.eboot.service.platform.service;


import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.SysDocFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface ISysDocFileService extends IService<SysDocFile> {
	
}
