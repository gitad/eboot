package com.mos.eboot.service.platform.service;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.SysDictionaries;

 
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
public interface ISysDictionariesService extends IService<SysDictionaries> {

	List<SysDictionaries> listSubDictByParentId(String parentId);
	List<SysDictionaries> listSubDict();
	
}
