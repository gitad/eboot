package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CCustomerMarket;

/**
 * <p>
  * 客户营销表 Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
public interface CCustomerMarketMapper extends BaseMapper<CCustomerMarket> {

}