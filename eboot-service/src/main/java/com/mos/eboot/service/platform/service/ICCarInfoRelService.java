package com.mos.eboot.service.platform.service;

import com.mos.eboot.platform.entity.CCarInfoRel;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
public interface ICCarInfoRelService extends IService<CCarInfoRel> {
	
}
