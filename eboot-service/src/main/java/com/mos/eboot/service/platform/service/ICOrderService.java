package com.mos.eboot.service.platform.service;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.COrder;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
public interface ICOrderService extends IService<COrder> {
	
	
	COrder queryCount(@RequestParam("createTimeStart")   String  createTimeStart,
			 @RequestParam("createTimeEnd")   String  createTimeEnd,
			 @RequestParam("payPathId")   String  payPathId,
			 @RequestParam("createId")   String  createId );
	List<COrder> queryCountDetail(@RequestParam("createTimeStart")   String  createTimeStart,
			 @RequestParam("createTimeEnd")   String  createTimeEnd,
			 @RequestParam("payPathId")   String  payPathId,
			 @RequestParam("createId")   String  createId, @RequestParam("orderNum")   String  orderNum);
	
}
