package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.CCompanyDetail;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
public interface CCompanyDetailMapper extends BaseMapper<CCompanyDetail> {

}