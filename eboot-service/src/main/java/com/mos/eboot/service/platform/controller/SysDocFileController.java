package com.mos.eboot.service.platform.controller;


import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCompanyDetail;
import com.mos.eboot.platform.entity.SysDocFile;
import com.mos.eboot.service.platform.service.ISysDocFileService;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@RestController
@RequestMapping("/sysDocFile")
public class SysDocFileController {
	
	@Autowired
	ISysDocFileService iSysDocFileService;
	
	@PostMapping("/query")	 
	public List<SysDocFile> query(  @RequestParam(value = "businessId", required = false)  String businessId,
			   @RequestParam(value = "businessTable", required = false) String businessTable,
			  @RequestParam(value = "businessType", required = false) String businessType) {
		EntityWrapper<SysDocFile>  Wrapper=new EntityWrapper<SysDocFile>();
	    Wrapper.eq("USE_FLAG", 1);
		if(StringUtil.isNotBlank(businessId)){
			  Wrapper.eq("business_id", businessId);
		}
		if(StringUtil.isNotBlank(businessTable)){
			  Wrapper.eq("business_table", businessTable);	
		}
		if(StringUtil.isNotBlank(businessType)){
			  Wrapper.eq("business_type", businessType);
		}		
		List<SysDocFile> list=iSysDocFileService.selectList(Wrapper);
		 return  list; 		 
    }
	
	
	@PostMapping("/save")	 
	public ResultModel<Boolean> save( @RequestBody SysDocFile sysDocFile) {
	 
	    Boolean bool= iSysDocFileService.insert(sysDocFile);
		 return  new ResultModel<Boolean>(ResultStatus.SUCCESS, bool);		 
    }
	
	@PostMapping("/del")	 
	public ResultModel<Boolean> del( @RequestBody SysDocFile sysDocFile) {
		sysDocFile.setUseFlag(0);
		iSysDocFileService.updateById(sysDocFile);
	    Boolean bool= iSysDocFileService.updateById(sysDocFile);//iSysDocFileService.deleteById(sysDocFile.getId());
		 return  new ResultModel<Boolean>(ResultStatus.SUCCESS, bool);		 
    }
	
}
