package com.mos.eboot.service.platform.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.service.platform.service.ISysUserWxInfoService;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 小尘哥
 * @since 2018-05-06
 */
@RestController
@RequestMapping("/user/wx")
public class SysUserWxInfoController extends BaseController {

    @Resource
    private ISysUserWxInfoService iSysUserWxInfoService;

    @ApiOperation(value = "获取字典详情", notes = "根据id获取字典")
    @RequestMapping(value = "get-by-id", method = RequestMethod.GET)
    public ResultModel<SysUserWxInfo> getById(@RequestParam("id") String id) {
        return new ResultModel<>(ResultStatus.SUCCESS, iSysUserWxInfoService.selectById(id));
    }

    @ApiOperation(value = "获取字典详情", notes = "根据id获取字典")
    @RequestMapping(value = "get-by-userId", method = RequestMethod.GET)
    public ResultModel<List<SysUserWxInfo>> getByUserId(@RequestParam("userId") String userId) {
        List<SysUserWxInfo> list = iSysUserWxInfoService.getWxByUserId(userId);
        return new ResultModel<>(ResultStatus.SUCCESS, list);
    }
    
    @RequestMapping(value = "del-by-id", method = RequestMethod.POST)
    public ResultModel<String> delById(@RequestParam("id") String id) {
        return this.basicResult(iSysUserWxInfoService.deleteByUserId(id));
    }

    
    @RequestMapping(value = "/del-by-userId-wxid", method = RequestMethod.POST)
    public ResultModel<String> delByUserIdAndWxid(@RequestParam("userId") String userId,@RequestParam("wxid") String wxid) {
    	EntityWrapper<SysUserWxInfo> wrapper= new EntityWrapper<>();
    	wrapper.eq("user_id", userId);
    	wrapper.eq("wx_id", wxid);
        return this.basicResult(iSysUserWxInfoService.delete(wrapper));
    }

    
    
    
    @ApiOperation(value = "新增|修改字典", notes = "新增|修改字典")
    @RequestMapping(value = "save-or-update", method = RequestMethod.POST)
    public ResultModel<String> saveOrUpdate(@RequestBody SysUserWxInfo sysUserWxInfo) {
        Boolean r;
        if (sysUserWxInfo.getId() == null) {
            r = iSysUserWxInfoService.insert(sysUserWxInfo);
        } else {
            r = iSysUserWxInfoService.updateById(sysUserWxInfo);
        }
        return this.basicResult(r);
    }
    
    @PostMapping("/update-cid-by-tid")
	public boolean updClientIdByTerminalId(@RequestParam("terminalId") String terminalId,
			@RequestParam("clientId") String clientId) {
		Boolean r = iSysUserWxInfoService.updClientIdByTerminalId(terminalId, clientId);
		return r;
    }

    @ApiOperation(value = "分页查询菜单", notes = "分页查询菜单")
    @RequestMapping(value = "query-page", method = RequestMethod.POST)
    public ResultModel<Page<SysUserWxInfo>> queryPage(@ApiParam @RequestBody Page<SysUserWxInfo> page) {
        EntityWrapper<SysUserWxInfo> wrapper = new EntityWrapper<SysUserWxInfo>();
         //wrapper.orderBy("sort_no", true);
        return new ResultModel<>(ResultStatus.SUCCESS, iSysUserWxInfoService.selectPage(page, wrapper));
    }
    

	@GetMapping("/get-by-userId-and-wxid")
	public SysUserWxInfo getWxInfoByUidAndWxid(@RequestParam("userId") String userId,@RequestParam("wxid") String wxid) {
 		EntityWrapper<SysUserWxInfo> wrapper= new EntityWrapper<>();
 		if(StringUtil.isNotBlank(userId)){
 			wrapper.eq("user_id", userId);
 		}
 		if(StringUtil.isNotBlank(wxid)){
 			wrapper.eq("wx_id", wxid);
 		}
		
		return iSysUserWxInfoService.selectOne(wrapper);
	}

}
