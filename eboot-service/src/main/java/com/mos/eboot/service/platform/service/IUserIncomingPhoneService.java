package com.mos.eboot.service.platform.service;

import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.UserIncomingPhone;

/**
 * <p>
 * 用户来电手机关联关系 服务类
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
public interface IUserIncomingPhoneService extends IService<UserIncomingPhone> {
	
}
