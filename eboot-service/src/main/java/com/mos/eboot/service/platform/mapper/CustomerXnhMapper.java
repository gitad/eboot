package com.mos.eboot.service.platform.mapper;

 
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerXnh;

/**
 * <p>
  * 用户虚拟号表 Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-22
 */
public interface CustomerXnhMapper extends BaseMapper<CustomerXnh> {
	int saveCustomer(CustomerXnh customer);
}