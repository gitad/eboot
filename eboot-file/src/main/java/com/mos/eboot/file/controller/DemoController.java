package com.mos.eboot.file.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

@RestController
@Controller
@RequestMapping("demo1")
public class DemoController {

	@RequestMapping("/im1")
	public String test1() {
		String string = "this is test111111111";
		String name = PrincipalUtils.getCurrentUser().getUsername();
		System.out.println(string+"------------"+name);
		return string+"------------"+name;
	}

}
