package com.mos.eboot.platform.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 客户营销表
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
@TableName("c_customer_market")
public class CCustomerMarket extends Model<CCustomerMarket> {

    private static final long serialVersionUID = 1L;

    @TableId("market_id")
	private String marketId;
    /**
     * CREATE_TIME	datetime	0	0	-1	0	0	0	0		0	通话时间				0	0
     */
	@TableField("customer_id")
	private String customerId;
	
	@TableField("market_phone")
	private String marketPhone;
	
 	
	
    /**
     * 通话时间
     */
	@TableField("create_time")
	private Date createTime;
	
	@TableField("create_user")
	private String createUser;
	
	@TableField("create_user_id")
	private String createUserId;
	
	
	@TableField("update_time")
	private Date updateTime;
	
	@TableField("update_user")
	private String updateUser;
    /**
     * 沟通状态 0未沟通1已沟通
     */
	@TableField("use_flag")
	private Integer useFlag;
    /**
     * 沟通方式  微信 /电话
     */
	@TableField("exchage_type")
	private String exchageType;
    /**
     * 通话状态  已接  未接
     */
	@TableField("wx_phone_states")
	private String wxPhoneStates;
    /**
     * 通话状态的原因
     */
	@TableField("phone_resion")
	private String phoneResion;
    /**
     * 通话内容/本次摘要
     */
	private String content;
    /**
     * 下次营销时间
     */
	@TableField("market_days")
	private String marketDays;
    /**
     * 营销方式 系统提示 微信
     */
	@TableField("market_type")
	private String marketType;
    /**
     * 营销内容 下次发送
     */
	@TableField("market_content")
	private String marketContent;


	public String getMarketId() {
		return marketId;
	}

	public void setMarketId(String marketId) {
		this.marketId = marketId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public String getExchageType() {
		return exchageType;
	}

	public void setExchageType(String exchageType) {
		this.exchageType = exchageType;
	}

 

	 
	public String getPhoneResion() {
		return phoneResion;
	}

	public void setPhoneResion(String phoneResion) {
		this.phoneResion = phoneResion;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMarketDays() {
		return marketDays;
	}

	public void setMarketDays(String marketDays) {
		this.marketDays = marketDays;
	}

	public String getMarketType() {
		return marketType;
	}

	public void setMarketType(String marketType) {
		this.marketType = marketType;
	}

	public String getMarketContent() {
		return marketContent;
	}

	public void setMarketContent(String marketContent) {
		this.marketContent = marketContent;
	}

	public String getWxPhoneStates() {
		return wxPhoneStates;
	}

	public void setWxPhoneStates(String wxPhoneStates) {
		this.wxPhoneStates = wxPhoneStates;
	}

	@Override
	protected Serializable pkVal() {
		return this.marketId;
	}

	public String getMarketPhone() {
		return marketPhone;
	}

	public void setMarketPhone(String marketPhone) {
		this.marketPhone = marketPhone;
	}

	public String getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(String createUserId) {
		this.createUserId = createUserId;
	}

	@Override
	public String toString() {
		return "CCustomerMarket{" +
			", marketId=" + marketId +
			", customerId=" + customerId +
			", createTime=" + createTime +
			", createUser=" + createUser +
			", updateTime=" + updateTime +
			", updateUser=" + updateUser +
			", useFlag=" + useFlag +
			", exchageType=" + exchageType +
			", wxPhoneStates=" + wxPhoneStates +
			", phoneResion=" + phoneResion +
			", content=" + content +
			", marketDays=" + marketDays +
			", marketType=" + marketType +
			", marketContent=" + marketContent +
			"}";
	}
}
