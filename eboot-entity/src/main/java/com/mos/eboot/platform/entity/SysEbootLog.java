package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 系统操作日志记录表(记录系统人员操作行为)
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-08
 */
@TableName("sys_eboot_log")
public class SysEbootLog extends Model<SysEbootLog> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 所关联的业务主键
     */
	@TableField("log_business_id")
	private String logBusinessId;
    /**
     * 日志类型 一般用表名即可 自定义
     */
	@TableField("log_business_type")
	private String logBusinessType;
    /**
     * 事件
     */
	@TableField("content")
	private String content;
    /**
     * 数据操作类型 add 添加 del 删除 edit修改 save保存
     */
	@TableField("log_staties")
	private String logStaties;
    /**
     * 创建时间
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 操作人
     */
	@TableField("create_peo")
	private String createPeo;
	
	@TableField("create_peo_id")
	private String createPeoId;
	
	@TableField("customer_id")
	private String customerId;
	
	
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCreatePeoId() {
		return createPeoId;
	}

	public void setCreatePeoId(String createPeoId) {
		this.createPeoId = createPeoId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

  

	public String getLogBusinessId() {
		return logBusinessId;
	}

	public void setLogBusinessId(String logBusinessId) {
		this.logBusinessId = logBusinessId;
	}

	public String getLogBusinessType() {
		return logBusinessType;
	}

	public void setLogBusinessType(String logBusinessType) {
		this.logBusinessType = logBusinessType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLogStaties() {
		return logStaties;
	}

	public void setLogStaties(String logStaties) {
		this.logStaties = logStaties;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreatePeo() {
		return createPeo;
	}

	public void setCreatePeo(String createPeo) {
		this.createPeo = createPeo;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

 
}
