package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import cn.afterturn.easypoi.excel.annotation.Excel;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mos.eboot.tools.shiro.entity.IUser;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.UserIncomingPhoneVO;


/**
 * <p>
 * <p>
 * </p>
 *
 * @author 小尘哥
 * @since 2018-01-13
 */
@TableName("sys_user")
public class SysUser extends Model<SysUser> implements IUser,UserDetails {

	public static final String DEFAULT_PWD = "123456";

	public static String SUPER_ID = "1";

	private static final long serialVersionUID = 1L;

	private String id;

	@Excel(name = "用户名",width = 40D)
	@NotBlank(message = "用户名不能为空")
	private String username;

	@Length(min = 6, max = 20, message = "密码长度不少于6个字符，不超过20个字符")
	private String password;

	@TableField("is_del")
	@TableLogic
	private String isDel;

	@Excel(name = "昵称", orderNum = "1",width = 20D)
	private String nickname;

	@Excel(name = "手机号", orderNum = "2", width = 30D)
	private String mobile;

	private String photo;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("create_time")
	private Date createTime;

	@TableField("create_user")
	private String createUser;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("last_login_time")
	private Date lastLoginTime;

	@Excel(name = "最后登录IP", orderNum = "5",width = 40D)
	private String ip;

	private String disabled;

	private String locked;

 
	private String token;
	
	@TableField(exist = false)
	private List<String> roleIds;
	
	@TableField(exist = false)
	private List<SysRole> roles;
	
	@TableField(exist = false)
	private List<SysUserWxInfo> userWxInfo;
	 
	@TableField(exist = false)
	private List<SysUserWxInfo> userWxInfoAll;
	
	@TableField(exist = false)
	private List<UserIncomingPhoneVO> userPhone;
	 
	@TableField(exist = false)
	private List<UserIncomingPhoneVO> userPhoneAll;
	
	
	
	public List<UserIncomingPhoneVO> getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(List<UserIncomingPhoneVO> userPhone) {
		this.userPhone = userPhone;
	}

	public List<UserIncomingPhoneVO> getUserPhoneAll() {
		return userPhoneAll;
	}

	public void setUserPhoneAll(List<UserIncomingPhoneVO> userPhoneAll) {
		this.userPhoneAll = userPhoneAll;
	}

	public List<SysUserWxInfo> getUserWxInfoAll() {
		return userWxInfoAll;
	}

	public void setUserWxInfoAll(List<SysUserWxInfo> userWxInfoAll) {
		this.userWxInfoAll = userWxInfoAll;
	}

	public List<SysUserWxInfo> getUserWxInfo() {
		return userWxInfo;
	}

	public void setUserWxInfo(List<SysUserWxInfo> userWxInfo) {
		this.userWxInfo = userWxInfo;
	}

	public List<SysRole> getRoles() {
		return roles;
	}

	public void setRoles(List<SysRole> roles) {
		this.roles = roles;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Boolean getDisabled() {
		return Constants.POSITIVE.equals(disabled);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public Integer getDeleted() {
		return StringUtil.isBlank(isDel) ? 0 : Integer.valueOf(isDel);
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}

	@Override
	public Boolean getLocked() {
		return Constants.POSITIVE.equals(locked);
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public List<String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "SysUser{" +
				", id=" + id +
				", username=" + username +
				", password=" + password +
				", isDel=" + isDel +
				", nickname=" + nickname +
				", mobile=" + mobile +
				", photo=" + photo +
				", createTime=" + createTime +
				", createUser=" + createUser +
				", lastLoginTime=" + lastLoginTime +
				", ip=" + ip +
				", disabled=" + disabled +
				", locked=" + locked +
				", token=" + token +
				"}";
	}
}
