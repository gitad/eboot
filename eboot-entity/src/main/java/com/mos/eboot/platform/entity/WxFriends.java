package com.mos.eboot.platform.entity;

import java.io.Serializable;

public class WxFriends implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5586770164963944463L;

	private Integer id;
	private String fWxId; // 微信好友ID
	private String fWxNumber; // 微信好友微信号
	private String fWxAvatar;// 微信好友头像
	private String fWxNickname; // 微信好友昵称
	private String fWxRemark; // 微信好友备注


	private String fWxV1; // 微信好友微信V1
	private String fromSocket; // 当前登陆人的socket概念
	private String fromWxId; // 当前登陆人的微信ID
	private Integer wxType; // 微信类型：1->个人微信号；2->群微信号
	private String cid; // 微信类型：1->个人微信号；2->群微信号

	public String getfWxId() {
		return fWxId;
	}

	public void setfWxId(String fWxId) {
		this.fWxId = fWxId;
	}

	public String getfWxNumber() {
		return fWxNumber;
	}

	public void setfWxNumber(String fWxNumber) {
		this.fWxNumber = fWxNumber;
	}

	public String getfWxAvatar() {
		return fWxAvatar;
	}

	public void setfWxAvatar(String fWxAvatar) {
		this.fWxAvatar = fWxAvatar;
	}

	public String getfWxNickname() {
		return fWxNickname;
	}

	public void setfWxNickname(String fWxNickname) {
		this.fWxNickname = fWxNickname;
	}

	public String getfWxRemark() {
		return fWxRemark;
	}

	public void setfWxRemark(String fWxRemark) {
		this.fWxRemark = fWxRemark;
	}

	public String getfWxV1() {
		return fWxV1;
	}

	public void setfWxV1(String fWxV1) {
		this.fWxV1 = fWxV1;
	}

	public String getFromSocket() {
		return fromSocket;
	}

	public void setFromSocket(String fromSocket) {
		this.fromSocket = fromSocket;
	}

	public String getFromWxId() {
		return fromWxId;
	}

	public void setFromWxId(String fromWxId) {
		this.fromWxId = fromWxId;
	}

	public Integer getWxType() {
		return wxType;
	}

	public void setWxType(Integer wxType) {
		this.wxType = wxType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

}
