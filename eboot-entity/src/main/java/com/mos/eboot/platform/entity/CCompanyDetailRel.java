package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@TableName("c_company_detail_rel")
public class CCompanyDetailRel extends Model<CCompanyDetailRel> {

    private static final long serialVersionUID = 1L;

    @TableId("company_rel_id")
	private String companyRelId;
	@TableField("company_id")
	private String companyId;
    /**
     * 姓名
     */
	private String name;
    /**
     * 电话
     */
	private String phone;
    /**
     * 备注
     */
	private String remark;
    /**
     * 平台名称
     */
	@TableField("platform_name")
	private String platformName;
    /**
     * 关联数据
     */
	private String relation;


	public String getCompanyRelId() {
		return companyRelId;
	}

	public void setCompanyRelId(String companyRelId) {
		this.companyRelId = companyRelId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPlatformName() {
		return platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	@Override
	protected Serializable pkVal() {
		return this.companyRelId;
	}

	@Override
	public String toString() {
		return "CCompanyDetailRel{" +
			", companyRelId=" + companyRelId +
			", companyId=" + companyId +
			", name=" + name +
			", phone=" + phone +
			", remark=" + remark +
			", platformName=" + platformName +
			", relation=" + relation +
			"}";
	}
}
