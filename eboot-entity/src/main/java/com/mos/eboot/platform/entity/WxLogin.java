package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 微信登录用户表
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-11
 */
@TableName("wx_login")
public class WxLogin extends Model<WxLogin> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 微信ID
     */
	@TableField("wx_id")
	private String wxId;
    /**
     * 微信号
     */
	@TableField("wx_number")
	private String wxNumber;
    /**
     * 微信头像
     */
	@TableField("wx_avatar")
	private String wxAvatar;
    /**
     * 微信昵称
     */
	@TableField("wx_nickname")
	private String wxNickname;
    /**
     * 微信客户端ID
     */
	@TableField("client_id")
	private String clientId;
    /**
     * 终端ID
     */
	@TableField("terminal_id")
	private String terminalId;
    /**
     * 在线状态：0离线，1在线
     */
	@TableField("online_stat")
	private String onlineStat;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNumber() {
		return wxNumber;
	}

	public void setWxNumber(String wxNumber) {
		this.wxNumber = wxNumber;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getOnlineStat() {
		return onlineStat;
	}

	public void setOnlineStat(String onlineStat) {
		this.onlineStat = onlineStat;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "WxLogin{" +
			", id=" + id +
			", wxId=" + wxId +
			", wxNumber=" + wxNumber +
			", wxAvatar=" + wxAvatar +
			", wxNickname=" + wxNickname +
			", clientId=" + clientId +
			", terminalId=" + terminalId +
			", onlineStat=" + onlineStat +
			"}";
	}
}
