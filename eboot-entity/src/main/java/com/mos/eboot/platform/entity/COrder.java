package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 订单表
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@TableName("c_order")
public class COrder extends Model<COrder> {

    private static final long serialVersionUID = 1L;

    @TableId("order_id")
	private String orderId;
    /**
     * 货源主键
     */
	@TableField("cargo_id")
	private String cargoId;
	
	@TableField("cargo_info_id")
	private String cargoInfoId;
	
	
	@TableField("order_num")
	private String orderNum;
    /**
     * 支付金额
     */
	@TableField("pay_num")
	private String payNum;
	
	@TableField("pay_mes_num")
	private String payMesNum;
 
    /**
     * 支付类型
     */
	@TableField("pay_states")
	private String payStates;
    /**
     * 支付方式
     */
	@TableField("pay_type")
	private String payType;
    /**
     * 支付路径
     */
	@TableField("pay_path")
	private String payPath;
	
	@TableField("pay_path_id")
	private String payPathId;
	
	
	
    /**
     * 订单状态
     */
	@TableField("order_states")
	private String orderStates;
	
	 /**
     * 运费
     */
	@TableField("pay_fre_num")
	private String payFreNum;
	 
	@TableField("remark")
	private String remark;
	
    /**
     * 支付人的姓名
     */
	@TableField("pay_name")
	private String payName;
   
	@TableField("pay_num_states")
	private String payNumStates;
	
	@TableField("pay_mes_num_states")
	private String payMesNumStates;
	
	@TableField("pay_fre_num_states")
	private String payFreNumStates;
 	/**
     * 支付人电话
     */
	@TableField("pay_phone")
	private String payPhone;
	
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("create_time")
	private Date createTime;
	
	//@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	@TableField("pay_time")
	private Date payTime;
	
 	@TableField("create_peo")
	private String createPeo;
	
	@JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
	@TableField("update_time")
	private Date updateTime;
	
	@TableField("update_peo")
	private String updatePeo;
	
	@TableField("use_flag")
	private String useFlag;

	@TableField("create_id")
	private String createId;
	
	@TableField("cust_name")
	private String custName;
	
	@TableField("return_num")
	private String returnNum;
	
	@TableField("return_num_states")
	private String returnNumStates;
	
	@TableField("return_num_channel")
	private String returnNumChannel;
	
	@TableField("return_resion")
	private String returnResion;
	
	@TableField("un_return_resion")
	private String unReturnResion;
	
	@TableField("vaild_phone")
	private String vaildPhone;
	
	@TableField("return_time")
	private Date returnTime;
	
	@TableField(exist=false)
	private String createTimeStart;
	
	@TableField(exist=false)
	private String createTimeEnd;
	
	 

	public String getCreateTimeStart() {
		return createTimeStart;
	}


	public void setCreateTimeStart(String createTimeStart) {
		this.createTimeStart = createTimeStart;
	}


	public String getCreateTimeEnd() {
		return createTimeEnd;
	}


	public void setCreateTimeEnd(String createTimeEnd) {
		this.createTimeEnd = createTimeEnd;
	}


	public String getReturnNum() {
		return returnNum;
	}

 
	public String getPayPathId() {
		return payPathId;
	}


	public void setPayPathId(String payPathId) {
		this.payPathId = payPathId;
	}


	public String getReturnNumChannel() {
		return returnNumChannel;
	}


	public void setReturnNumChannel(String returnNumChannel) {
		this.returnNumChannel = returnNumChannel;
	}


	public String getReturnNumStates() {
		return returnNumStates;
	}


	public void setReturnNumStates(String returnNumStates) {
		this.returnNumStates = returnNumStates;
	}


	public void setReturnNum(String returnNum) {
		this.returnNum = returnNum;
	}

	public String getReturnResion() {
		return returnResion;
	}

	public void setReturnResion(String returnResion) {
		this.returnResion = returnResion;
	}

	public String getUnReturnResion() {
		return unReturnResion;
	}


	public void setUnReturnResion(String unReturnResion) {
		this.unReturnResion = unReturnResion;
	}


	public Date getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(Date returnTime) {
		this.returnTime = returnTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Date getPayTime() {
		return payTime;
	}

	public void setPayTime(Date payTime) {
		this.payTime = payTime;
	}

 

 
	public String getPayNumStates() {
		return payNumStates;
	}

	public void setPayNumStates(String payNumStates) {
		this.payNumStates = payNumStates;
	}

	public String getPayMesNumStates() {
		return payMesNumStates;
	}

	public void setPayMesNumStates(String payMesNumStates) {
		this.payMesNumStates = payMesNumStates;
	}

	public String getPayFreNumStates() {
		return payFreNumStates;
	}

	public void setPayFreNumStates(String payFreNumStates) {
		this.payFreNumStates = payFreNumStates;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getPayMesNum() {
		return payMesNum;
	}

	public void setPayMesNum(String payMesNum) {
		this.payMesNum = payMesNum;
	}

	public String getVaildPhone() {
		return vaildPhone;
	}

	public void setVaildPhone(String vaildPhone) {
		this.vaildPhone = vaildPhone;
	}

	public String getPayFreNum() {
		return payFreNum;
	}

	public void setPayFreNum(String payFreNum) {
		this.payFreNum = payFreNum;
	}

	public String getOrderId() {
		return orderId;
	}

	public String getCargoInfoId() {
		return cargoInfoId;
	}

	public void setCargoInfoId(String cargoInfoId) {
		this.cargoInfoId = cargoInfoId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCargoId() {
		return cargoId;
	}

	public void setCargoId(String cargoId) {
		this.cargoId = cargoId;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getPayNum() {
		return payNum;
	}

	public void setPayNum(String payNum) {
		this.payNum = payNum;
	}

	public String getPayStates() {
		return payStates;
	}

	public void setPayStates(String payStates) {
		this.payStates = payStates;
	}

	public String getPayPath() {
		return payPath;
	}

	public void setPayPath(String payPath) {
		this.payPath = payPath;
	}

	public String getOrderStates() {
		return orderStates;
	}

	public void setOrderStates(String orderStates) {
		this.orderStates = orderStates;
	}

	public String getPayName() {
		return payName;
	}

	public void setPayName(String payName) {
		this.payName = payName;
	}

	public String getPayPhone() {
		return payPhone;
	}

	public void setPayPhone(String payPhone) {
		this.payPhone = payPhone;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreatePeo() {
		return createPeo;
	}

	public void setCreatePeo(String createPeo) {
		this.createPeo = createPeo;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdatePeo() {
		return updatePeo;
	}

	public void setUpdatePeo(String updatePeo) {
		this.updatePeo = updatePeo;
	}

	public String getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(String useFlag) {
		this.useFlag = useFlag;
	}

	@Override
	protected Serializable pkVal() {
		return this.orderId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "数据详情{" +
			", 主键=" + orderId +
			 
			", 订单编号=" + orderNum +
			", 运费=" + payFreNum +
			", 支付金额=" + payNum +
			", 信息费=" + payMesNum +
			", 运费状态=" + payFreNumStates +
			", 信息费状态=" + payMesNumStates +
			", 支付金额状态=" + payNumStates +
			", 退款金额=" + returnNum +
			", 退款时间=" + returnTime +
			", 退款理由=" + returnResion +
			", 退款金额状态=" + returnNumStates +
			", 退款渠道=" + returnNumChannel +
			", 取消退款理由=" + unReturnResion +
			 
			", 支付类型=" + payStates +
			", 支付方式=" + payType +
			", 支付路径=" + payPath +
			", 支付人姓名=" + payName +
			", 支付人电话=" + payPhone +
			", 支付路径=" + payPath +	
			", 订单状态=" + orderStates +
	
			", 支付时间=" + payTime +
			", 支付时间=" + payTime +
			", 司机姓名=" + custName +
			", 司机电话=" + vaildPhone +
			
			", payPhone=" + payPhone +
			", 创建时间=" + createTime +
			", 创建人=" + createPeo +
			", 修改时间=" + updateTime +
			", 修改人=" + updatePeo +		 
			", 摘要备注=" + remark +
			"}";
	}
}
