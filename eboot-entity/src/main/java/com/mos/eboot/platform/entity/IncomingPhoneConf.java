package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 来电弹屏手机号配置信息
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
@TableName("sys_incoming_phone_conf")
public class IncomingPhoneConf extends Model<IncomingPhoneConf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3503157088102053852L;
	
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 手机号
	 */
	@TableField("phone_num")
	private String phoneNum;
	/**
	 * 别名，简称
	 */
	@TableField("phone_alias")
	private String phoneAlias;
	/**
	 * 登录用户名
	 */
	@TableField("login_user_name")
	private String loginUserName;
	/**
	 * 登录密码
	 */
	@TableField("login_pwd")
	private String loginPwd;
	/**
	 * 外呼 主叫号码
	 */
	@TableField("is_out_bound")
	private String isOutBound;
	/**
	 * 号码归属地
	 */
	@TableField("phone_qcellcore")
	private String phoneQcellcore;
	
	
	
	
	/**
	 * 号码归属地 区号
	 */
	@TableField("phone_attr")
	private String phoneAttr;


	public String getPhoneAttr() {
		return phoneAttr;
	}

	public void setPhoneAttr(String phoneAttr) {
		this.phoneAttr = phoneAttr;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPhoneAlias() {
		return phoneAlias;
	}

	public void setPhoneAlias(String phoneAlias) {
		this.phoneAlias = phoneAlias;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getIsOutBound() {
		return isOutBound;
	}

	public void setIsOutBound(String isOutBound) {
		this.isOutBound = isOutBound;
	}

	public String getPhoneQcellcore() {
		return phoneQcellcore;
	}

	public void setPhoneQcellcore(String phoneQcellcore) {
		this.phoneQcellcore = phoneQcellcore;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "IncomingPhoneConf{" + ", id=" + id + ", phoneNum=" + phoneNum + ", phoneAlias=" + phoneAlias
				+ ", loginUserName=" + loginUserName + ", loginPwd=" + loginPwd + ", isOutBound=" + isOutBound
				+ ", phoneQcellcore=" + phoneQcellcore + "}";
	}
}
