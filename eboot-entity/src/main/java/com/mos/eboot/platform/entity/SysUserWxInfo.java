package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * 登录用户与微信号绑定关系
 * 
 * @author EDZ
 *
 */
@TableName("sys_user_wx_info")
public class SysUserWxInfo extends Model<SysUserWxInfo> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1053091482853835842L;

	@TableId(type = IdType.AUTO)
	private Integer id;
	@TableField(exist=false)
	private String name;

	@TableField("user_id")
	private String userId;

	@TableField("wx_login_id")
	private Long wxLoginId; // 主键
	@TableField("wx_id")
	private String wxId; // 登录人微信ID

	@TableField("wx_avatar")
	private String wxAvatar; // 登录人微信头像

	@TableField("client_id")
	private String clientId; // 微信客户端ID

	@TableField("terminal_id")
	private String terminalId; // 终端ID

	@TableField("wx_nickname")
	private String wxNickname;// 微信昵称
	@TableField(exist=false)
	private String onlineStat;// 在线状态：0离线，1在线

	@TableField(exist=false)
	private boolean chceked;//标记是否选中

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Boolean getChceked() {
		return chceked;
	}

	public void setChceked(boolean chceked) {
		this.chceked = chceked;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getWxLoginId() {
		return wxLoginId;
	}

	public void setWxLoginId(Long wxLoginId) {
		this.wxLoginId = wxLoginId;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getOnlineStat() {
		return onlineStat;
	}

	public void setOnlineStat(String onlineStat) {
		this.onlineStat = onlineStat;
	}

}
