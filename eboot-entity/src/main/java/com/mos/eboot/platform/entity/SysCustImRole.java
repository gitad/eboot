package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("sys_cust_im_role")
public class SysCustImRole extends Model<SysAccessory> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7719235696283915436L;

	private Integer id;

	@TableField("user_id")
	private Integer userId;

	@TableField("cust_type")
	private String custType;

	@TableField("button_name")
	private String buttonName;

	@TableField("url")
	private String url;

	@TableField("sort")
	private Integer sort;

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
