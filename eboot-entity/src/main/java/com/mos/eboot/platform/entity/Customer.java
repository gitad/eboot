package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

@TableName("t_customer")
public class Customer extends Model<Customer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7649136861882384805L;
	
	//@TableId(value = "id", type = IdType.AUTO)
	@TableId(type = IdType.UUID)
	private String id;
	
	@TableField("tid")
	private String tid;
	
	@TableField("cust_name")
	private String custName;

	@TableField("valid_phone")
	private String validPhone;

	@TableField("other_phone")
	private String otherPhone;

	@TableField("qq")
	private String qq;

	@TableField("cust_wx_id")
	private String custWxId;

	@TableField("sys_wx_id")
	private String sysWxId;

	@TableField("wx_status")
	private String wxStatus;

	@TableField("province_name")
	private String provinceName;

	@TableField("city_name")
	private String cityName;

	@TableField("area_name")
	private String areaName;

	@TableField("cust_type")
	private String custType;

	@TableField("address_info")
	private String addressInfo;

	@TableField("is_del")
	private String isDel;

	@TableField("is_upload")
	private String isUpload;

	@TableField("raw_add_user")
	private String rawAddUser;

	@TableField("raw_add_time")
	private String rawAddTime;

	@TableField("raw_up_user")
	private String rawUpUser;

	@TableField("raw_up_time")
	private String rawUpTime;


	@TableField("cust_admin_user_id")
	private String custAdminUserId;
	
	@TableField("cust_admin_nickname")
	private String custAdminNickname;
	
	@TableField("if_normal")
	private String ifNormal;
	
	@TableField("cust_admin_user_id_tem")
	private String custAdminUserIdTem;
	
	@TableField("cust_admin_nickname_tem")
	private String custAdminNicknameTem;
	
	@TableField("tem_time")
	private Date  temTime;
	
	@TableField("remark")
	private String remark;
 
	
	@TableField("follow_type")
	private String followType;
	
	@TableField("last_call_status")
	private String  lastCallStatus;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("last_call_time")
	private Date lastCallTime;
 
	
	//是否添加到
	@Value("false")
	@TableField(exist=false)
	private Boolean states;
	 
	//是否主数据
	@Value("false")
	@TableField(exist=false)
	private Boolean mainData;
	 
	
	
	public Boolean getMainData() {
		return mainData;
	}

	public void setMainData(Boolean mainData) {
		this.mainData = mainData;
	}

	public Boolean getStates() {
		return states;
	}

	public void setStates(Boolean states) {
		this.states = states;
	}

	public String getLastCallStatus() {
		return lastCallStatus;
	}

	public String getFollowType() {
		return followType;
	}

	public void setFollowType(String followType) {
		this.followType = followType;
	}

	public void setLastCallStatus(String lastCallStatus) {
		this.lastCallStatus = lastCallStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCustAdminUserIdTem() {
		return custAdminUserIdTem;
	}

	public void setCustAdminUserIdTem(String custAdminUserIdTem) {
		this.custAdminUserIdTem = custAdminUserIdTem;
	}

	public String getCustAdminNicknameTem() {
		return custAdminNicknameTem;
	}

	public void setCustAdminNicknameTem(String custAdminNicknameTem) {
		this.custAdminNicknameTem = custAdminNicknameTem;
	}

	 

	public Date getTemTime() {
		return temTime;
	}

	public void setTemTime(Date temTime) {
		this.temTime = temTime;
	}

	public String getIfNormal() {
		return ifNormal;
	}

	public void setIfNormal(String ifNormal) {
		this.ifNormal = ifNormal;
	}



	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}

 

	public String getCustAdminUserId() {
		return custAdminUserId;
	}



	public void setCustAdminUserId(String custAdminUserId) {
		this.custAdminUserId = custAdminUserId;
	}



	public String getCustAdminNickname() {
		return custAdminNickname;
	}

	public void setCustAdminNickname(String custAdminNickname) {
		this.custAdminNickname = custAdminNickname;
	}

 

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getValidPhone() {
		return validPhone;
	}

	public void setValidPhone(String validPhone) {
		this.validPhone = validPhone;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getCustWxId() {
		return custWxId;
	}

	public void setCustWxId(String custWxId) {
		this.custWxId = custWxId;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public String getWxStatus() {
		return wxStatus;
	}

	public void setWxStatus(String wxStatus) {
		this.wxStatus = wxStatus;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(String addressInfo) {
		this.addressInfo = addressInfo;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsUpload() {
		return isUpload;
	}

	public void setIsUpload(String isUpload) {
		this.isUpload = isUpload;
	}

	public String getRawAddUser() {
		return rawAddUser;
	}

	public void setRawAddUser(String rawAddUser) {
		this.rawAddUser = rawAddUser;
	}

	public String getRawAddTime() {
		return rawAddTime;
	}

	public void setRawAddTime(String rawAddTime) {
		this.rawAddTime = rawAddTime;
	}

	public String getRawUpUser() {
		return rawUpUser;
	}

	public void setRawUpUser(String rawUpUser) {
		this.rawUpUser = rawUpUser;
	}

	public String getRawUpTime() {
		return rawUpTime;
	}

	public void setRawUpTime(String rawUpTime) {
		this.rawUpTime = rawUpTime;
	}

	public Date getLastCallTime() {
		return lastCallTime;
	}

	public void setLastCallTime(Date lastCallTime) {
		this.lastCallTime = lastCallTime;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

}
