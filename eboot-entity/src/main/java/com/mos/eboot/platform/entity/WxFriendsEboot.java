package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 微信好友表
 * </p>
 *
 * @author jiangweijie
 * @since 2021-11-22
 */
@TableName("wx_friends")
public class WxFriendsEboot extends Model<WxFriendsEboot> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 微信ID
     */
	@TableField("f_wx_id")
	private String fWxId;
    /**
     * 微信号
     */
	@TableField("f_wx_number")
	private String fWxNumber;
    /**
     * 微信头像
     */
	@TableField("f_wx_avatar")
	private String fWxAvatar;
    /**
     * 微信昵称
     */
	@TableField("f_wx_nickname")
	private String fWxNickname;
    /**
     * 微信备注
     */
	@TableField("f_wx_remark")
	private String fWxRemark;
    /**
     * 微信V1
     */
	@TableField("f_wx_v1")
	private String fWxV1;
    /**
     * 登录人微信ID
     */
	@TableField("from_wx_id")
	private String fromWxId;
    /**
     * 微信类型：1->个人微信号；2->群微信号
     */
	@TableField("wx_type")
	private String wxType;
    /**
     * 好友微信状态，1:正常，2被删除,3，拉黑
     */
	@TableField("wx_stat")
	private String wxStat;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getfWxId() {
		return fWxId;
	}

	public void setfWxId(String fWxId) {
		this.fWxId = fWxId;
	}

	public String getfWxNumber() {
		return fWxNumber;
	}

	public void setfWxNumber(String fWxNumber) {
		this.fWxNumber = fWxNumber;
	}

	public String getfWxAvatar() {
		return fWxAvatar;
	}

	public void setfWxAvatar(String fWxAvatar) {
		this.fWxAvatar = fWxAvatar;
	}

	public String getfWxNickname() {
		return fWxNickname;
	}

	public void setfWxNickname(String fWxNickname) {
		this.fWxNickname = fWxNickname;
	}

	public String getfWxRemark() {
		return fWxRemark;
	}

	public void setfWxRemark(String fWxRemark) {
		this.fWxRemark = fWxRemark;
	}

	public String getfWxV1() {
		return fWxV1;
	}

	public void setfWxV1(String fWxV1) {
		this.fWxV1 = fWxV1;
	}

	public String getFromWxId() {
		return fromWxId;
	}

	public void setFromWxId(String fromWxId) {
		this.fromWxId = fromWxId;
	}

	public String getWxType() {
		return wxType;
	}

	public void setWxType(String wxType) {
		this.wxType = wxType;
	}

	public String getWxStat() {
		return wxStat;
	}

	public void setWxStat(String wxStat) {
		this.wxStat = wxStat;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "WxFriends{" +
			", id=" + id +
			", fWxId=" + fWxId +
			", fWxNumber=" + fWxNumber +
			", fWxAvatar=" + fWxAvatar +
			", fWxNickname=" + fWxNickname +
			", fWxRemark=" + fWxRemark +
			", fWxV1=" + fWxV1 +
			", fromWxId=" + fromWxId +
			", wxType=" + wxType +
			", wxStat=" + wxStat +
			"}";
	}
}
