package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 客户联系方式表
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@TableName("t_cust_phone")
public class CustPhone extends Model<CustPhone> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	@TableField("c_id")
	private String cId;
	@TableField("cust_phone")
	private String custPhone;
    /**
     * 默认：0否，1是
     */
	@TableField("is_default")
	private String isDefault;
    /**
     * 类型：1个人；2企业
     */
	@TableField("phone_type")
	private String phoneType;
    /**
     * 状态：1正常，2分离
     */
	private String status;
    /**
     * 备注
     */
	private String remark;
    /**
     * 0正常，1删除
     */
	@TableField("is_delete")
	private String isDelete;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getCustPhone() {
		return custPhone;
	}

	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CustPhone{" +
			", id=" + id +
			", cId=" + cId +
			", custPhone=" + custPhone +
			", isDefault=" + isDefault +
			", phoneType=" + phoneType +
			", status=" + status +
			", remark=" + remark +
			", isDelete=" + isDelete +
			"}";
	}
}
