package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_car_info")
public class CCarInfo extends Model<CCarInfo> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String carId;
	private Integer useFlag;
	private Date createTime;
	private String createUser;
	private Date updateTime;
	private String updateUser;
	private String remark;
	private String carType;
	private String carLen;
	private String weight;
	private String carLincese;	
	private String carUseType;	
	private String carTravelNum;	
	private String customerId;
	
	private String adress;
	
	private String engNum;
	
	private String company;
	
 	@TableField(exist=false)
	private String custName;
	
	@TableField(exist=false)
	private String vaildPhone;
	
	@TableField(exist=false)
	private List<CCarInfoRel> relList;	
	
	@TableField(exist=false)
	private List<SysDocFile> docFileList;	
	
	public List<SysDocFile> getDocFileList() {
		return docFileList;
	}

	public void setDocFileList(List<SysDocFile> docFileList) {
		this.docFileList = docFileList;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getEngNum() {
		return engNum;
	}

	public void setEngNum(String engNum) {
		this.engNum = engNum;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCarTravelNum() {
		return carTravelNum;
	}

	public void setCarTravelNum(String carTravelNum) {
		this.carTravelNum = carTravelNum;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getVaildPhone() {
		return vaildPhone;
	}

	public void setVaildPhone(String vaildPhone) {
		this.vaildPhone = vaildPhone;
	}

	public String getCarUseType() {
		return carUseType;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public void setCarUseType(String carUseType) {
		this.carUseType = carUseType;
	}

	public List<CCarInfoRel> getRelList() {
		return relList;
	}

	public void setRelList(List<CCarInfoRel> relList) {
		this.relList = relList;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarLen() {
		return carLen;
	}

	public void setCarLen(String carLen) {
		this.carLen = carLen;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getCarLincese() {
		return carLincese;
	}

	public void setCarLincese(String carLincese) {
		this.carLincese = carLincese;
	}

 
	@Override
	protected Serializable pkVal() {
		return this.carId;
	}
	@Override
	public String toString() {
		return "数据详情{" +
		 
			" 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +
		 
			", 车型=" + carType +
			", 车长=" + carLen +
			", 吨位=" + weight +
			", 车牌号=" + carLincese +
			"}";
	}
}
