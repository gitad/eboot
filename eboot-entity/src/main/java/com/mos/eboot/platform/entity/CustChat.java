package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhang
 * @since 2021-08-30
 */
@TableName("t_cust_chat")
public class CustChat extends Model<CustChat> {

    private static final long serialVersionUID = 1L;

	private Long id;
	private String color;
    /**
     * 客户ID
     */
	@TableField("c_id")
	private Integer cId;
    /**
     * 添加人
     */
	@TableField("raw_add_user")
	private String rawAddUser;
    /**
     * 添加时间
     */
	@TableField("raw_add_time")
	private String rawAddTime;
    /**
     * 沟通时间
     */
	@TableField("chat_date")
	private String chatDate;
    /**
     * 回复时间
     */
	@TableField("reply_chat_date")
	private String replyChatDate;
    /**
     * 内容
     */
	@TableField("chat_contents")
	private String chatContents;
    /**
     * 类型
     */
	@TableField("chat_type")
	private String chatType;
    /**
     * 备注
     */
	private String memo;
    /**
     * 是否删除
     */
	@TableField("is_del")
	private String isDel;
    /**
     * 是否上传
     */
	@TableField("is_upload")
	private String isUpload;
    /**
     * 更新日期
     */
	@TableField("raw_up_time")
	private String rawUpTime;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getcId() {
		return cId;
	}

	public void setcId(Integer cId) {
		this.cId = cId;
	}

	public String getRawAddUser() {
		return rawAddUser;
	}

	public void setRawAddUser(String rawAddUser) {
		this.rawAddUser = rawAddUser;
	}

	public String getRawAddTime() {
		return rawAddTime;
	}

	public void setRawAddTime(String rawAddTime) {
		this.rawAddTime = rawAddTime;
	}

	public String getChatDate() {
		return chatDate;
	}

	public void setChatDate(String chatDate) {
		this.chatDate = chatDate;
	}

	public String getReplyChatDate() {
		return replyChatDate;
	}

	public void setReplyChatDate(String replyChatDate) {
		this.replyChatDate = replyChatDate;
	}

	public String getChatContents() {
		return chatContents;
	}

	public void setChatContents(String chatContents) {
		this.chatContents = chatContents;
	}

	public String getChatType() {
		return chatType;
	}

	public void setChatType(String chatType) {
		this.chatType = chatType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsUpload() {
		return isUpload;
	}

	public void setIsUpload(String isUpload) {
		this.isUpload = isUpload;
	}

	public String getRawUpTime() {
		return rawUpTime;
	}

	public void setRawUpTime(String rawUpTime) {
		this.rawUpTime = rawUpTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CustChat{" +
			", id=" + id +
			", color=" + color +
			", cId=" + cId +
			", rawAddUser=" + rawAddUser +
			", rawAddTime=" + rawAddTime +
			", chatDate=" + chatDate +
			", replyChatDate=" + replyChatDate +
			", chatContents=" + chatContents +
			", chatType=" + chatType +
			", memo=" + memo +
			", isDel=" + isDel +
			", isUpload=" + isUpload +
			", rawUpTime=" + rawUpTime +
			"}";
	}
}
