package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_cargo_info")
public class CCargoInfo extends Model<CCargoInfo> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String cargoInfoId;
	private Integer useFlag;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;
	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;
	private String updateUser;
	private String remark;
	private String cargoId;
	private String price;
	private String budgetPrice;
	private String phone;
	private String cargoPeojess;
	private String customerId;
	private String carLen;
	private String carType;
	private String tag;
	private String contactRelId;
	private String contactId;
	private Integer useCar;
	//地址详情
	private String addressDetailStr;	
	private String addressDetailEnd; 
	
	
	private String carId;
	@TableField(exist=false)
	private String  carsGoodsAddress;
	
	@TableField(exist=false)
	private String  vaildPhone;//司机电话
	
	@TableField(exist=false)
	private String  custName;//司机姓名
	
	@TableField(exist=false)
	private String  wxStatus;//微信状态
	
	

	public String getAddressDetailStr() {
		return addressDetailStr;
	}

	public void setAddressDetailStr(String addressDetailStr) {
		this.addressDetailStr = addressDetailStr;
	}

	public String getAddressDetailEnd() {
		return addressDetailEnd;
	}

	public void setAddressDetailEnd(String addressDetailEnd) {
		this.addressDetailEnd = addressDetailEnd;
	}


	public String getTag() {
		return tag;
	}

	public String getVaildPhone() {
		return vaildPhone;
	}

	public Integer getUseCar() {
		return useCar;
	}

	public void setUseCar(Integer useCar) {
		this.useCar = useCar;
	}

	public void setVaildPhone(String vaildPhone) {
		this.vaildPhone = vaildPhone;
	}

	public String getContactRelId() {
		return contactRelId;
	}

	public void setContactRelId(String contactRelId) {
		this.contactRelId = contactRelId;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getWxStatus() {
		return wxStatus;
	}

	public void setWxStatus(String wxStatus) {
		this.wxStatus = wxStatus;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getCarsGoodsAddress() {
		return carsGoodsAddress;
	}

	public void setCarsGoodsAddress(String carsGoodsAddress) {
		this.carsGoodsAddress = carsGoodsAddress;
	}

	public String getCarLen() {
		return carLen;
	}

	public void setCarLen(String carLen) {
		this.carLen = carLen;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCargoInfoId() {
		return cargoInfoId;
	}

	public void setCargoInfoId(String cargoInfoId) {
		this.cargoInfoId = cargoInfoId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCargoId() {
		return cargoId;
	}

	public void setCargoId(String cargoId) {
		this.cargoId = cargoId;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getBudgetPrice() {
		return budgetPrice;
	}

	public void setBudgetPrice(String budgetPrice) {
		this.budgetPrice = budgetPrice;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCargoPeojess() {
		return cargoPeojess;
	}

	public void setCargoPeojess(String cargoPeojess) {
		this.cargoPeojess = cargoPeojess;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

 
	@Override
	protected Serializable pkVal() {
		return this.cargoInfoId;
	}
	@Override
	public String toString() {
		return "数据详情{" +
			", 主键=" + cargoInfoId +
			", 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +	
			", 询价备注=" + remark +
			", 货源主键=" + cargoId +
			", 司机报价=" + price +
			 
			", 电话=" + phone +
			", 进度=" + cargoPeojess +
		 
			"}";
	}
}
