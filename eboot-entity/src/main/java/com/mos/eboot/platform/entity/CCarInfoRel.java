package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_car_info_rel")
public class CCarInfoRel extends Model<CCarInfoRel> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String carRelId;
	private String carId;
	private Integer useFlag;
	private Date createTime;
	private String createUser;
	private Date updateTime;
	private String updateUser;
	private String remark;
	private Integer type;
	private String carryType;
	private String routeType;
	private String provinceStart;
	private String cityStart;
	private String countryStart;
	private String provinceEnd;
	private String cityEnd;
	private String countryEnd;
	private String provinceTrans;
	private String cityTrans;
	private String countryTrans;
	
	
 	@TableField(exist=false)
	private String provinceCode;
 	@TableField(exist=false)
	private String cityCode;
 	@TableField(exist=false)
	private String areaCode;
 	
 	
	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getCarRelId() {
		return carRelId;
	}

	public void setCarRelId(String carRelId) {
		this.carRelId = carRelId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCarryType() {
		return carryType;
	}

	public void setCarryType(String carryType) {
		this.carryType = carryType;
	}

	public String getRouteType() {
		return routeType;
	}

	public void setRouteType(String routeType) {
		this.routeType = routeType;
	}

	public String getProvinceStart() {
		return provinceStart;
	}

	public void setProvinceStart(String provinceStart) {
		this.provinceStart = provinceStart;
	}

	public String getCityStart() {
		return cityStart;
	}

	public void setCityStart(String cityStart) {
		this.cityStart = cityStart;
	}

	public String getCountryStart() {
		return countryStart;
	}

	public void setCountryStart(String countryStart) {
		this.countryStart = countryStart;
	}

	public String getProvinceEnd() {
		return provinceEnd;
	}

	public void setProvinceEnd(String provinceEnd) {
		this.provinceEnd = provinceEnd;
	}

	public String getCityEnd() {
		return cityEnd;
	}

	public void setCityEnd(String cityEnd) {
		this.cityEnd = cityEnd;
	}

	public String getCountryEnd() {
		return countryEnd;
	}

	public void setCountryEnd(String countryEnd) {
		this.countryEnd = countryEnd;
	}

	public String getProvinceTrans() {
		return provinceTrans;
	}

	public void setProvinceTrans(String provinceTrans) {
		this.provinceTrans = provinceTrans;
	}

	public String getCityTrans() {
		return cityTrans;
	}

	public void setCityTrans(String cityTrans) {
		this.cityTrans = cityTrans;
	}

	public String getCountryTrans() {
		return countryTrans;
	}

	public void setCountryTrans(String countryTrans) {
		this.countryTrans = countryTrans;
	}

	@Override
	protected Serializable pkVal() {
		return this.carRelId;
	}

	@Override
	public String toString() {
		return "数据详情{" +
			", 主键=" + carRelId +
		 
			" 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +
		 
			", 类型=" + type +
		 
			", 路线类型=" + routeType +
			", 出发地省=" + provinceStart +
			", 出发地市=" + cityStart +
			", 出发地区=" + countryStart +
			", 目的地省=" + provinceEnd +
			", 目的地市=" + cityEnd +
			", 目的地区=" + countryEnd +
			", 中转地省级代码=" + provinceTrans +
			", 中转地地市代码=" + cityTrans +
			", 中转地区县代码=" + countryTrans +
			"}";
	}
}
