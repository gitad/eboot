package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 
 * </p>
 *货源录入
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_cargo_entry")
public class CCargoEntry extends Model<CCargoEntry> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String cargoId;
	private Integer useFlag;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;
	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;
	private String updateUser;
	private String customerPrice;
	private String budgetPrice;
	private String matchType;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date planTime;
	private String customerId;
 
	private String cargoType;
	private String customerRemark;
	private String remark;
	private String pack;
	private String weightStr;
	private String weightEnd;
	private String volumeStr;
	private String volumeEnd;
	private String carUseType;
	private String carLen;
	private String carType;
	private String cargoCatagory;
	private String cargoSubCatagory;
	//地址详情
	private String addressDetailStr;	
	private String addressDetailEnd;	
	//车及货物详
	private String carsAndGoods;
	
	private String tag;
 
	//手机号
	 
	private String phoneNum;
	
	//用户姓名	
 
	private String custName;
	 
	//匹配车辆	
	@TableField(exist=false)
	private Integer pCar;
	
	//报价	
	@TableField(exist=false)
	private Integer pPriceCar;
	
	
	
 	public Integer getpCar() {
		return pCar;
	}

	public void setpCar(Integer pCar) {
		this.pCar = pCar;
	}

	public Integer getpPriceCar() {
		return pPriceCar;
	}

	public void setpPriceCar(Integer pPriceCar) {
		this.pPriceCar = pPriceCar;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

 

	public String getAddressDetailStr() {
		return addressDetailStr;
	}

	public void setAddressDetailStr(String addressDetailStr) {
		this.addressDetailStr = addressDetailStr;
	}

	public String getAddressDetailEnd() {
		return addressDetailEnd;
	}

	public void setAddressDetailEnd(String addressDetailEnd) {
		this.addressDetailEnd = addressDetailEnd;
	}

	public String getCarsAndGoods() {
		return carsAndGoods;
	}

	public void setCarsAndGoods(String carsAndGoods) {
		this.carsAndGoods = carsAndGoods;
	}

	public String getCargoId() {
		return cargoId;
	}

	public void setCargoId(String cargoId) {
		this.cargoId = cargoId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCustomerPrice() {
		return customerPrice;
	}

	public void setCustomerPrice(String customerPrice) {
		this.customerPrice = customerPrice;
	}

	public String getBudgetPrice() {
		return budgetPrice;
	}

	public void setBudgetPrice(String budgetPrice) {
		this.budgetPrice = budgetPrice;
	}

	public String getMatchType() {
		return matchType;
	}

	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlantTime(Date planTime) {
		this.planTime = planTime;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCargoType() {
		return cargoType;
	}

	public void setCargoType(String cargoType) {
		this.cargoType = cargoType;
	}

	public String getCustomerRemark() {
		return customerRemark;
	}

	public void setCustomerRemark(String customerRemark) {
		this.customerRemark = customerRemark;
	}

	public String getRemark() {
		return remark;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getWeightStr() {
		return weightStr;
	}

	public void setWeightStr(String weightStr) {
		this.weightStr = weightStr;
	}

	public String getWeightEnd() {
		return weightEnd;
	}

	public void setWeightEnd(String weightEnd) {
		this.weightEnd = weightEnd;
	}

	public String getVolumeStr() {
		return volumeStr;
	}

	public void setVolumeStr(String volumeStr) {
		this.volumeStr = volumeStr;
	}

	public String getVolumeEnd() {
		return volumeEnd;
	}

	public void setVolumeEnd(String volumeEnd) {
		this.volumeEnd = volumeEnd;
	}

	public String getCarUseType() {
		return carUseType;
	}

	public void setCarUseType(String carUseType) {
		this.carUseType = carUseType;
	}

	public String getCarLen() {
		return carLen;
	}

	public void setCarLen(String carLen) {
		this.carLen = carLen;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCargoCatagory() {
		return cargoCatagory;
	}

	public void setCargoCatagory(String cargoCatagory) {
		this.cargoCatagory = cargoCatagory;
	}

	public String getCargoSubCatagory() {
		return cargoSubCatagory;
	}

	public void setCargoSubCatagory(String cargoSubCatagory) {
		this.cargoSubCatagory = cargoSubCatagory;
	}

	@Override
	protected Serializable pkVal() {
		return this.cargoId;
	}

	@Override
	public String toString() {
		return "详情数据{" +
			", 主键=" + cargoId +
		 
			", 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +
			", 客户报价=" + customerPrice +
			", 预算=" + budgetPrice +
			", 类型=" + matchType +
			", 计划处理时间=" + planTime +
		 	", 货物类型=" + cargoType +
			", 客户备注=" + customerRemark +
			", 我方备注=" + remark +
			", 包装方式=" + pack +
			", 吨位=" + weightStr +
			", 吨位=" + weightEnd +
			", 体积=" + volumeStr +
			", 体积=" + volumeEnd +
			", 用车类型=" + carUseType +
			", 长度=" + carLen +
			", 车型=" + carType +
			", 货物大类=" + cargoCatagory +
			", 货物小类=" + cargoSubCatagory +
			"}";
	}
}
