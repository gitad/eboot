package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * IM弹框窗口操作菜单权限
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */
@TableName("sys_cust_im_role")
public class CustImRole extends Model<CustImRole> {

	private static final long serialVersionUID = 1L;

	private Integer id;
	@TableField("user_id")
	private String userId;
	@TableField("cust_type")
	private String custType;
	@TableField("button_name")
	private String buttonName;

	@TableField("url")
	private String url;

	@TableField("sort")
	private String sort;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CustImRole{" + ", id=" + id + ", userId=" + userId + ", custType=" + custType + ", buttonName="
				+ buttonName + ", url=" + url + ", sort=" + sort + "}";
	}
}
