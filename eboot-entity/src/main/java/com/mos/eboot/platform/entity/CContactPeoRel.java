package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_contact_peo_rel")
public class CContactPeoRel extends Model<CContactPeoRel> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String contactRelId;
	private String contactId;
	private Integer useFlag;
	private Date createTime;
	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;
	private String updateUser;
	private String remark;
	private String contactPhone;
	private String wx;
	private String wxType;
	private String contactState;
	private String phoneDefault;
	private String customerId;
	private String phoneArea;
	private String phoneAtt;
	
	public String getPhoneArea() {
		return phoneArea;
	}

	public void setPhoneArea(String phoneArea) {
		this.phoneArea = phoneArea;
	}

	public String getPhoneAtt() {
		return phoneAtt;
	}

	public void setPhoneAtt(String phoneAtt) {
		this.phoneAtt = phoneAtt;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getContactRelId() {
		return contactRelId;
	}

	public void setContactRelId(String contactRelId) {
		this.contactRelId = contactRelId;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

 
	public String getWx() {
		return wx;
	}

	public void setWx(String wx) {
		this.wx = wx;
	}

	public String getWxType() {
		return wxType;
	}

	public void setWxType(String wxType) {
		this.wxType = wxType;
	}

	public String getContactState() {
		return contactState;
	}

	public void setContactState(String contactState) {
		this.contactState = contactState;
	}

	public String getPhoneDefault() {
		return phoneDefault;
	}

	public void setPhoneDefault(String phoneDefault) {
		this.phoneDefault = phoneDefault;
	}

	@Override
	protected Serializable pkVal() {
		return this.contactRelId;
	}

	@Override
	public String toString() {
		return "数据详情{" +
			", 主键=" + contactRelId +
			", 关联主键=" + contactId +	 
			", 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +	 
			", 电话号码=" + contactPhone +
			", 添加我方微信号=" + wx +
			", 类型=" + wxType +
			", 状态=" + contactState +
			", 默认手机号=" + phoneDefault +
			"}";
	}
}
