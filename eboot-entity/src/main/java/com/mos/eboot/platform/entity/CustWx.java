package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldStrategy;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 客户微信对应关系表
 * </p>
 *
 * @author zhang
 * @since 2021-05-12
 */ 
@TableName("t_cust_wx")
public class CustWx extends Model<CustWx> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	
	@TableField(strategy = FieldStrategy.IGNORED)
	private String cId;
	
	@TableField("wx_id")	
	private String wxId;
	
	@TableField("wx_number")
	private String wxNumber;
	@TableField("wx_remark")
	private String wxRemark;
	@TableField("wx_nickname")
	private String wxNickname;
	@TableField("sys_wx_id")
	private String sysWxId;
	@TableField("wx_type")
	private String wxType;
	@TableField("wx_stat")
	private String wxStat;

	@TableField("wx_avatar")
	private String wxAvatar;
	@TableField("create_time")
	private Date createTime;
	@TableField(strategy = FieldStrategy.IGNORED)
	private String userId;
	
	
	@TableField(exist=false)
	private String custName;
	
	@TableField(exist=false)
	private String type;
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxStat() {
		return wxStat;
	}

	public void setWxStat(String wxStat) {
		this.wxStat = wxStat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNumber() {
		return wxNumber;
	}

	public void setWxNumber(String wxNumber) {
		this.wxNumber = wxNumber;
	}

	public String getWxRemark() {
		return wxRemark;
	}

	public void setWxRemark(String wxRemark) {
		this.wxRemark = wxRemark;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public String getWxType() {
		return wxType;
	}

	public void setWxType(String wxType) {
		this.wxType = wxType;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CustWx{" +
			", id=" + id +
			", cId=" + cId +
			", wxId=" + wxId +
			", wxNumber=" + wxNumber +
			", wxRemark=" + wxRemark +
			", wxNickname=" + wxNickname +
			", sysWxId=" + sysWxId +
			", wxType=" + wxType +
			"}";
	}
}
