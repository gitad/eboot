package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiang
 * @since 2021-10-07
 */
@TableName("sys_dictionaries")
public class SysDictionaries extends Model<SysDictionaries> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String dictionariesId;
	
	//是否有子
	@TableField(exist = false)
	private List<SysDictionaries> children;
    /**
     * 名称
     */
	private String name;
    /**
     * 英文
     */
	private String nameEn;
    /**
     * 编码
     */
	private String bianma;
    /**
     * 排序
     */
	private Integer orderBy;
    /**
     * 上级ID
     */
	private String parentId;
    /**
     * 备注
     */
	private String bz;
    /**
     * 排查表
     */
	private String tbsname;
	private String tbfield;
	private String yndel;


	public String getDictionariesId() {
		return dictionariesId;
	}

	public void setDictionariesId(String dictionariesId) {
		this.dictionariesId = dictionariesId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

 
 
	public List<SysDictionaries> getChildren() {
		return children;
	}

	public void setChildren(List<SysDictionaries> children) {
		this.children = children;
	}

	public String getBianma() {
		return bianma;
	}

	public void setBianma(String bianma) {
		this.bianma = bianma;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getBz() {
		return bz;
	}

	public void setBz(String bz) {
		this.bz = bz;
	}

	public String getTbsname() {
		return tbsname;
	}

	public void setTbsname(String tbsname) {
		this.tbsname = tbsname;
	}

	public String getTbfield() {
		return tbfield;
	}

	public void setTbfield(String tbfield) {
		this.tbfield = tbfield;
	}

	public String getYndel() {
		return yndel;
	}

	public void setYndel(String yndel) {
		this.yndel = yndel;
	}

	@Override
	protected Serializable pkVal() {
		return this.dictionariesId;
	}

	@Override
	public String toString() {
		return "SysDictionaries{" +
			", dictionariesId=" + dictionariesId +
			", name=" + name +
			", nameEn=" + nameEn +
			", bianma=" + bianma +
			", orderBy=" + orderBy +
			", parentId=" + parentId +
			", bz=" + bz +
			", tbsname=" + tbsname +
			", tbfield=" + tbfield +
			", yndel=" + yndel +
			"}";
	}
}
