package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_contact_peo")
public class CContactPeo extends Model<CContactPeo> {

    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.UUID)
	private String contactId;
	private Integer useFlag;
	private Date createTime;
	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;
	private String updateUser;
	private String customerId;
	private String remark;
	private String contactName;
	private String contactSex;
	private String contactCard;
	private String contactType;
	private String contactAut;
	private String contactSort;
 
	private String adress;
	
 
	private String carType;
	
 
	private String carUseStart;
	
	 
	private String carUseEnd;
	
	@TableField(exist=false)
	private List<CContactPeoRel> contactPeoList;
	
	
	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarUseStart() {
		return carUseStart;
	}

	public void setCarUseStart(String carUseStart) {
		this.carUseStart = carUseStart;
	}

	public String getCarUseEnd() {
		return carUseEnd;
	}

	public void setCarUseEnd(String carUseEnd) {
		this.carUseEnd = carUseEnd;
	}

	public List<CContactPeoRel> getContactPeoList() {
		return contactPeoList;
	}

	public void setContactPeoList(List<CContactPeoRel> contactPeoList) {
		this.contactPeoList = contactPeoList;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactSex() {
		return contactSex;
	}

	public void setContactSex(String contactSex) {
		this.contactSex = contactSex;
	}

	public String getContactCard() {
		return contactCard;
	}

	public void setContactCard(String contactCard) {
		this.contactCard = contactCard;
	}
 
	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactAut() {
		return contactAut;
	}

	public void setContactAut(String contactAut) {
		this.contactAut = contactAut;
	}

	public String getContactSort() {
		return contactSort;
	}

	public void setContactSort(String contactSort) {
		this.contactSort = contactSort;
	}

	@Override
	protected Serializable pkVal() {
		return this.contactId;
	}

	@Override
	public String toString() {
		return "数据详情{" +
	       ", 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +
		 	", 姓名=" + contactName +
			", 性别=" + contactSex +
			", 身份证=" + contactCard +
			", 职务=" + contactType +
			", 认证情况=" + contactAut +
			", 是否主要联系人=" + contactSort +
			"}";
	}
}
