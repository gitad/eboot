package com.mos.eboot.platform.entity;

import java.io.Serializable;

/**
 * 历史消息记录
 * 
 * @author EDZ
 *
 */
public class HisWxMsg implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3123216830281200818L;

	private String HISMSG_ID;
	private String USERNAME;
	private String TOID;
	private String TYPE;
	private String NAME;
	private String PHOTO;
	private String CTIME;
	private String CONTENT;
	private String DREAD;
	private String OWNER;

	private String SYSFLAG;// 标识是否是管理员发送的聊天记录
	private String WXNICKNAME;
	private String SENDNAME;//发送人的姓名
	
	public String getSENDNAME() {
		return SENDNAME;
	}

	public void setSENDNAME(String sENDNAME) {
		SENDNAME = sENDNAME;
	}

	public String getHISMSG_ID() {
		return HISMSG_ID;
	}

	public void setHISMSG_ID(String hISMSG_ID) {
		HISMSG_ID = hISMSG_ID;
	}

	public String getUSERNAME() {
		return USERNAME;
	}

	public void setUSERNAME(String uSERNAME) {
		USERNAME = uSERNAME;
	}

	public String getTOID() {
		return TOID;
	}

	public void setTOID(String tOID) {
		TOID = tOID;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public String getNAME() {
		return NAME;
	}

	public void setNAME(String nAME) {
		NAME = nAME;
	}

	public String getPHOTO() {
		return PHOTO;
	}

	public void setPHOTO(String pHOTO) {
		PHOTO = pHOTO;
	}

	public String getCTIME() {
		return CTIME;
	}

	public void setCTIME(String cTIME) {
		CTIME = cTIME;
	}

	public String getCONTENT() {
		return CONTENT;
	}

	public void setCONTENT(String cONTENT) {
		CONTENT = cONTENT;
	}

	public String getDREAD() {
		return DREAD;
	}

	public void setDREAD(String dREAD) {
		DREAD = dREAD;
	}

	public String getOWNER() {
		return OWNER;
	}

	public void setOWNER(String oWNER) {
		OWNER = oWNER;
	}

	public String getSYSFLAG() {
		return SYSFLAG;
	}

	public void setSYSFLAG(String sYSFLAG) {
		SYSFLAG = sYSFLAG;
	}

	public String getWXNICKNAME() {
		return WXNICKNAME;
	}

	public void setWXNICKNAME(String wXNICKNAME) {
		WXNICKNAME = wXNICKNAME;
	}
}
