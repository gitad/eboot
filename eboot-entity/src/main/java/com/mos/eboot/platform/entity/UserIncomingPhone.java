package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户来电手机关联关系
 * </p>
 *
 * @author zhang
 * @since 2021-05-17
 */
@TableName("sys_user_incoming_phone")
public class UserIncomingPhone extends Model<UserIncomingPhone> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7680656255738444927L;
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	@TableField("user_id")
	private String userId;
	/**
	 * 关联sys_incoming_phone_conf来电弹屏手机号配置ID
	 */
	@TableField("conf_id")
	private Integer confId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getConfId() {
		return confId;
	}

	public void setConfId(Integer confId) {
		this.confId = confId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "UserIncomingPhone{" + ", id=" + id + ", userId=" + userId + ", confId=" + confId + "}";
	}
}
