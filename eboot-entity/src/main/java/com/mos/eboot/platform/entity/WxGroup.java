package com.mos.eboot.platform.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-17
 */
@TableName("wx_group")
public class WxGroup extends Model<WxGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * 微信群id
     */
    @TableId
	private String groupId;
    /**
     * 备注
     */
	private String remark;
    /**
     * 承运车长
     */
 
	private String carLen;
    /**
     * 承运车型
     */
 
	private String carType;
    /**
     * 获利方式
     */
	private String profit;
	
 
	private String owerWx;
	
	private String messWxid;

	private String wxNums;
	
	private String  groupTag;
	@TableField(exist=false)
	private String messName;
	@TableField(exist=false)
	private String groupName;
	
	@TableField(exist=false)
	private String wxId;

	@TableField(exist=false)
	private String owerName;
 	
 
 
	public String getMessName() {
		return messName;
	}

	public void setMessName(String messName) {
		this.messName = messName;
	}

	public String getMessWxid() {
		return messWxid;
	}

	public void setMessWxid(String messWxid) {
		this.messWxid = messWxid;
	}

	public String getGroupTag() {
		return groupTag;
	}

	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNums() {
		return wxNums;
	}

	public void setWxNums(String wxNums) {
		this.wxNums = wxNums;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getOwerName() {
		return owerName;
	}

	public void setOwerName(String owerName) {
		this.owerName = owerName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCarLen() {
		return carLen;
	}

	public void setCarLen(String carLen) {
		this.carLen = carLen;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getProfit() {
		return profit;
	}

	public void setProfit(String profit) {
		this.profit = profit;
	}

	public String getOwerWx() {
		return owerWx;
	}

	public void setOwerWx(String owerWx) {
		this.owerWx = owerWx;
	}

	@Override
	protected Serializable pkVal() {
		return this.groupId;
	}

	@Override
	public String toString() {
		return "WxGroup{" +
			", groupId=" + groupId +
			", remark=" + remark +
			", carLen=" + carLen +
			", carType=" + carType +
			", profit=" + profit +
			", owerWx=" + owerWx +
			"}";
	}
}
