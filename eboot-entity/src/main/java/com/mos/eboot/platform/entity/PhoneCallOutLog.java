package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 自动拨打电话记录
 * </p>
 *
 * @author zhang
 * @since 2021-08-17
 */
@TableName("sys_phone_call_out_log")
public class PhoneCallOutLog extends Model<PhoneCallOutLog> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6722955955450747111L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	@TableField("user_id")
	private String userId;
	/**
	 * 手机号
	 */
	@TableField("phone_num")
	private String phoneNum;
	/**
	 * 号码归属地
	 */
	@TableField("callerloc")
	private String callerloc;
	/**
	 * 拨打时间
	 */
	@TableField("call_time")
	private Date callTime;
	/**
	 * 呼叫状态，1已拨打,2未拨打，3未接，4拨打中
	 */
	@TableField("call_stat")
	private String callStat;
	/**
	 * 用户ID;customerId
	 */
	@TableField("c_id")
	private Integer cId;

	@TableField("virtual_phone")
	private String virtualPhone;
	
	@TableField("call_id")
	private String callId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getCallerloc() {
		return callerloc;
	}

	public void setCallerloc(String callerloc) {
		this.callerloc = callerloc;
	}

	public Date getCallTime() {
		return callTime;
	}

	public void setCallTime(Date callTime) {
		this.callTime = callTime;
	}

	public String getCallStat() {
		return callStat;
	}

	public void setCallStat(String callStat) {
		this.callStat = callStat;
	}

	public Integer getcId() {
		return cId;
	}

	public void setcId(Integer cId) {
		this.cId = cId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getVirtualPhone() {
		return virtualPhone;
	}

	public void setVirtualPhone(String virtualPhone) {
		this.virtualPhone = virtualPhone;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	@Override
	public String toString() {
		return "PhoneCallOutLog{" + ", id=" + id + ", userId=" + userId + ", phoneNum=" + phoneNum + ", callerloc="
				+ callerloc + ", callTime=" + callTime + ", callStat=" + callStat + ", cId=" + cId + "}";
	}
}
