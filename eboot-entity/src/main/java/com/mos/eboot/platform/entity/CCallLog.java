package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p>
 * 
 * </p>
 *  
 * @author jiangweijie
 * @since 2021-10-01
 */
@TableName("c_call_log")
public class CCallLog extends Model<CCallLog> {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.UUID)
	private String callId;
    
	private Integer useFlag;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;
	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date updateTime;
	private String updateUser;
	private String customerId;
	private String callType;
	private String remark;
	private String callRemark;
	 
	private Date planTime;	 
	private String callContent;
	
	private String userId;
	private String Tid;
	private String tCustId;
	private String userName;
	 
	private String contactPhone;
	 
	@TableField(exist=false)
	private String addressInfo;

	@TableField(exist=false)
	private String custName;//数据筛选用

	
	@TableField(exist=false)
	private String validPhone;

	@TableField(exist=false)
	private String otherPhone;
	
	
	
	public String getContactPhone() {
		return contactPhone;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getTid() {
		return Tid;
	}

	public void setTid(String tid) {
		Tid = tid;
	}

 

 

	public String gettCustId() {
		return tCustId;
	}

	public void settCustId(String tCustId) {
		this.tCustId = tCustId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCallId() {
		return callId;
	}

 
	public String getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(String addressInfo) {
		this.addressInfo = addressInfo;
	}

	public String getValidPhone() {
		return validPhone;
	}

	public void setValidPhone(String validPhone) {
		this.validPhone = validPhone;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public Integer getUseFlag() {
		return useFlag;
	}

	public void setUseFlag(Integer useFlag) {
		this.useFlag = useFlag;
	}

 
	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	 
	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCallRemark() {
		return callRemark;
	}

	public void setCallRemark(String callRemark) {
		this.callRemark = callRemark;
	}
 

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

 
	public String getCallContent() {
		return callContent;
	}

	public void setCallContent(String callContent) {
		this.callContent = callContent;
	}

	@Override
	protected Serializable pkVal() {
		return this.callId;
	}
 
	@Override
	public String toString() {
		return "详情数据{" +
			", 通话主键=" + callId +		 
			", 创建时间=" + createTime +
			", 创建人=" + createUser +
			", 修改时间=" + updateTime +
			", 修改人=" + updateUser +		 
			", 通话类别=" + callType +
			", 备注=" + remark +
			", 通话备注=" + callRemark +
			", 计划处理时间=" + planTime +
			"}";
	}
}
