package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@TableName("c_company_detail")
public class CCompanyDetail extends Model<CCompanyDetail> {

    private static final long serialVersionUID = 1L;

    @TableId("company_id")
	private String companyId;
	@TableField("customer_id")
	private String customerId;
	
	 /**
     * 实际方式
     */
	@TableField("vaild_way")
	private String vaildWay;
    /**
     * 常用货物
     */
	@TableField("common_goods")
	private String commonGoods;
    /**
     * 产品或服务
     */
	@TableField("products_or_services")
	private String productsOrServices;
    /**
     * 备注
     */
	private String remark;
    /**
     * 公司名称
     */
	@TableField("company_name")
	private String companyName;
    /**
     * 其他名称
     */
	@TableField("company_name_other")
	private String companyNameOther;
    /**
     * 公司经营状态
     */
	@TableField("company_status")
	private String companyStatus;
    /**
     * 公司法人
     */
	@TableField("company_legal")
	private String companyLegal;
    /**
     * 成立日期
     */
	@TableField("create_time")
	private Date createTime;
    /**
     * 注册资本
     */
	@TableField("reg_capital")
	private Long regCapital;
    /**
     * 省
     */
	private String province;
    /**
     * 市
     */
	private String city;
    /**
     * 其他电话
     */
	@TableField("other_phone")
	private String otherPhone;
    /**
     * 邮箱
     */
	private String email;
    /**
     * 统一社会认证码
     */
	@TableField("unify_socil_code")
	private String unifySocilCode;
    /**
     * 纳税人识别号
     */
	@TableField("tax_no")
	private String taxNo;
    /**
     * 注册识别号
     */
	@TableField("reg_no")
	private String regNo;
    /**
     * 组织机构
     */
	@TableField("hs_unit")
	private String hsUnit;
    /**
     * 参保人数
     */
	@TableField("insured_num")
	private String insuredNum;
    /**
     * 企业类型
     */
	@TableField("unit_type")
	private String unitType;
    /**
     * 所属行业
     */
	private String Industry;
    /**
     * 网址
     */
	private String website;
    /**
     * 企业地址
     */
	private String address;
    /**
     * 经营范围
     */
	@TableField("business_scope")
	private String businessScope;


	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCommonGoods() {
		return commonGoods;
	}

	public void setCommonGoods(String commonGoods) {
		this.commonGoods = commonGoods;
	}

	public String getProductsOrServices() {
		return productsOrServices;
	}

	public void setProductsOrServices(String productsOrServices) {
		this.productsOrServices = productsOrServices;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyNameOther() {
		return companyNameOther;
	}

	public void setCompanyNameOther(String companyNameOther) {
		this.companyNameOther = companyNameOther;
	}

	public String getCompanyStatus() {
		return companyStatus;
	}

	public void setCompanyStatus(String companyStatus) {
		this.companyStatus = companyStatus;
	}

	public String getCompanyLegal() {
		return companyLegal;
	}

	public void setCompanyLegal(String companyLegal) {
		this.companyLegal = companyLegal;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getRegCapital() {
		return regCapital;
	}

	public void setRegCapital(Long regCapital) {
		this.regCapital = regCapital;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUnifySocilCode() {
		return unifySocilCode;
	}

	public void setUnifySocilCode(String unifySocilCode) {
		this.unifySocilCode = unifySocilCode;
	}

	public String getTaxNo() {
		return taxNo;
	}

	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}

	public String getRegNo() {
		return regNo;
	}

	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}

	public String getHsUnit() {
		return hsUnit;
	}

	public void setHsUnit(String hsUnit) {
		this.hsUnit = hsUnit;
	}

	public String getInsuredNum() {
		return insuredNum;
	}

	public void setInsuredNum(String insuredNum) {
		this.insuredNum = insuredNum;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getIndustry() {
		return Industry;
	}

	public void setIndustry(String Industry) {
		this.Industry = Industry;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getVaildWay() {
		return vaildWay;
	}

	public void setVaildWay(String vaildWay) {
		this.vaildWay = vaildWay;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusinessScope() {
		return businessScope;
	}

	public void setBusinessScope(String businessScope) {
		this.businessScope = businessScope;
	}

	@Override
	protected Serializable pkVal() {
		return this.companyId;
	}

	@Override
	public String toString() {
		return "数据详情{" +
			", 主键=" + companyId +
		 
			", 常用货物名称=" + commonGoods +
			", 实际经营产品或服务=" + productsOrServices +
			", 备注=" + remark +
			", 公司名称=" + companyName +
			", 公司别称=" + companyNameOther +
			", 经营状态=" + companyStatus +
			", 法定代表人=" + companyLegal +
			", 创建时间=" + createTime +
			", 注册资本=" + regCapital +
			", 省份=" + province +
			", 城市=" + city +
			", 更多电话号码=" + otherPhone +
			", 邮箱=" + email +
			", 统一社会信用代码=" + unifySocilCode +
			", 纳税人识别号=" + taxNo +
			", 注册号=" + regNo +
			", 组织机构代码=" + hsUnit +
			", 参保人数=" + insuredNum +
			", 企业类型=" + unitType +
			", 所属行业=" + Industry +
			", 网址=" + website +
			", 企业地址=" + address +
			", 经营范围=" + businessScope +
			"}";
	}
}
