package com.mos.eboot.platform.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 来电-通话记录
 * </p>
 *
 * @author zhang
 * @since 2021-05-20
 */
@TableName("sys_incoming_phone_call_log")
public class IncomingPhoneCallLog extends Model<IncomingPhoneCallLog> {

	private static final long serialVersionUID = 1L;

	/**
	 * PK
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 用户ID
	 */
	@TableField("user_id")
	private String userId;
	/**
	 * 记录ID
	 */
	@TableField("call_id")
	private String callId;
	/**
	 * 账户ID
	 */
	@TableField("call_user_id")
	private String callUserId;
	/**
	 * 电话
	 */
	@TableField("phone_num")
	private String phoneNum;
	/**
	 * 通话类型：1来电 2外拨 3未接
	 */
	@TableField("call_type")
	private String callType;
	/**
	 * 状态：1成功 2.外拨失败 3.对方未接 4.号码错误 5.拒接 6.通话中 8.来电未接
	 */
	private String status;
	/**
	 * 通话时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 通话分钟
	 */
	private String duration;
	/**
	 * 通话录音保存地址
	 */
	@TableField("recording_path")
	private String recordingPath;
	/**
	 * 添加时间
	 */
	@TableField("raw_add_time")
	private Date rawAddTime;
	/**
	 * raw
	 */
	@TableField("raw_add_user")
	private String rawAddUser;
	@TableField("raw_update_time")
	private Date rawUpdateTime;
	@TableField("raw_update_user")
	private String rawUpdateUser;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getCallUserId() {
		return callUserId;
	}

	public void setCallUserId(String callUserId) {
		this.callUserId = callUserId;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRecordingPath() {
		return recordingPath;
	}

	public void setRecordingPath(String recordingPath) {
		this.recordingPath = recordingPath;
	}

	public Date getRawAddTime() {
		return rawAddTime;
	}

	public void setRawAddTime(Date rawAddTime) {
		this.rawAddTime = rawAddTime;
	}

	public String getRawAddUser() {
		return rawAddUser;
	}

	public void setRawAddUser(String rawAddUser) {
		this.rawAddUser = rawAddUser;
	}

	public Date getRawUpdateTime() {
		return rawUpdateTime;
	}

	public void setRawUpdateTime(Date rawUpdateTime) {
		this.rawUpdateTime = rawUpdateTime;
	}

	public String getRawUpdateUser() {
		return rawUpdateUser;
	}

	public void setRawUpdateUser(String rawUpdateUser) {
		this.rawUpdateUser = rawUpdateUser;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "IncomingPhoneCallLog{" + ", id=" + id + ", userId=" + userId + ", callId=" + callId + ", callUserId="
				+ callUserId + ", phoneNum=" + phoneNum + ", callType=" + callType + ", status=" + status
				+ ", createTime=" + createTime + ", duration=" + duration + ", recordingPath=" + recordingPath
				+ ", rawAddTime=" + rawAddTime + ", rawAddUser=" + rawAddUser + ", rawUpdateTime=" + rawUpdateTime
				+ ", rawUpdateUser=" + rawUpdateUser + "}";
	}
}
