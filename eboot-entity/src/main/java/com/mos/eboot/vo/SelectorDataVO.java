package com.mos.eboot.vo;

import java.io.Serializable;

public class SelectorDataVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -862966504432541722L;
	private String userId;//sys_user_wx_info 主键
	private String name;
	private String value;
	private boolean selected;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	 
 
}
