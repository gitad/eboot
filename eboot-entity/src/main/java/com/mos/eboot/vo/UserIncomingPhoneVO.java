package com.mos.eboot.vo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;

public class UserIncomingPhoneVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6712277505127719273L;

	private Integer id;

	/**
	 * 用户ID
	 */
	private String userId;

	/**
	 * 手机号
	 */
	private String phoneNum;
	/**
	 * 别名，简称
	 */
	private String phoneAlias;
	/**
	 * 登录用户名
	 */
	private String loginUserName;
	/**
	 * 登录密码
	 */
	private String loginPwd;
	/**
	 * 外呼 主叫号码
	 */
	private String isOutBound;
	/**
	 * 号码归属地
	 */
	private String phoneQcellcore;
	private String phoneAttr;


	/**
	 * 是否选中
	 */
	private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public String getPhoneAttr() {
		return phoneAttr;
	}

	public void setPhoneAttr(String phoneAttr) {
		this.phoneAttr = phoneAttr;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPhoneAlias() {
		return phoneAlias;
	}

	public void setPhoneAlias(String phoneAlias) {
		this.phoneAlias = phoneAlias;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	public String getLoginPwd() {
		return loginPwd;
	}

	public void setLoginPwd(String loginPwd) {
		this.loginPwd = loginPwd;
	}

	public String getIsOutBound() {
		return isOutBound;
	}

	public void setIsOutBound(String isOutBound) {
		this.isOutBound = isOutBound;
	}

	public String getPhoneQcellcore() {
		return phoneQcellcore;
	}

	public void setPhoneQcellcore(String phoneQcellcore) {
		this.phoneQcellcore = phoneQcellcore;
	}

}
