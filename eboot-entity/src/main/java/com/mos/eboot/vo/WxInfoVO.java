package com.mos.eboot.vo;

import java.io.Serializable;
import java.util.List;

public class WxInfoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -814797095992942838L;

	private String userId;

	private List<String> wxIds; // 登录人微信ID

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getWxIds() {
		return wxIds;
	}

	public void setWxIds(List<String> wxIds) {
		this.wxIds = wxIds;
	}
}
