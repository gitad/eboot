package com.mos.eboot.vo;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.Customer;
 

/**
 * 聊天窗口，左侧弹出列表-对应VO
 * 
 * @author EDZ
 *
 */
public class ImMsgConversationListVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9095015455442558580L;

	/**
	 * ID
	 */
	private String sessionId;

	/**
	 * 客户ID
	 */
	private String cId;

	/**
	 * 客户姓名
	 */
	private String custName;

	/**
	 * 客户类型
	 */
	private String custType;

	/**
	 * 联系电话
	 */
	private String custPhone;
	/**
	 * 联系电话虚拟号
	 */
	private String custPhoneXnh;
 
	/**
	 * 对应微信
	 */
	private String custWxId;
	/**
	 * 微信t_cust_wx表主键
	 */
	private String tCustWxId;

	/**
	 * 系统管理员-微信
	 */
	private String sysWxId;

	/**
	 * 未读消息数量
	 */
	private Integer noReadMsgCount;

	/**
	 * 公司
	 */
	private String company;

	/**
	 * 地址
	 */
	private String address;

	/**
	 * 用户权限
	 */
	private List<CustImRole> custRoleList;

	/**
	 * 是否有微信权限,0没有，1有
	 */
	private String hasWxRole;

	/**
	 * 客户--微信对应关系list
	 */
	//private List<WxFriendsEbootVO> custWxvoList;

	private List<CustWxInfoVO> custWxvoList;
	

	/**
	 *群聊好友  主要用于存储群发消息  用这个原因是不需要存储过多得用户信息，只需要id和name即可 
	 */
	private List<JSONObject> custJsonList;
	
	/**
	 *群聊好友   用这个太过于繁琐
	 */
	private List<Customer> custList;
	
	
	
	private Long addTimestamp;


	/**
	 * 微信未读消息数量
	 */
	private Integer wxNoReadNum;

	/**
	 * 来电状态 
	 * type:1来电 2外拨 3未接
		status:	1成功 2.外拨失败 3.对方未接 4.号码错误 5.拒接 6.通话中 8.来电未接
	 */
	private String phoneStatus;
	/**
	 * 来电状态 
	 * type:1来电 2外拨 3未接
		status:	1成功 2.外拨失败 3.对方未接 4.号码错误 5.拒接 6.通话中 8.来电未接
	 */
	private String phonetype;

	/**
	 * 来电数量
	 */
	private Integer phoneNoReadNum;

	/**
	 * 最后沟通内容和留言
	 */
	private String lastMsg;
	
	/**
	 * 会话时间
	 */
	private String addTime;

	
	/**
	 * 判断是否转交
	 */
	private boolean trans;
	/**
	 * 判断是否跟进  客户营销
	 */
	private boolean  genjin;
	/**
	 * 判断当前页是否进行显示
	 */
	private boolean show;

	
	/**
	 * 消息类型   0微信  /1群/2拨打/3虚拟号/来电
	 */
	private Integer mesType;

	
	/**
	 * 是否置顶
	 */
	private boolean ifTop;
 
	/**
	 * 是否消息提醒
	 */
	private boolean wxWarm;
 
	/**
	 * 电话未接
	 */
	private boolean unPhone;
 
	
	/**
	 * 微信状态,1正常，2没有微信，3微信被删
	 */
	private String wxStatus;

	/**
	 * 发货次数
	 */
	private Integer fhNum;

	/**
	 * 成交次数
	 */
	private Integer cjNum;

	/**
	 * 信用
	 */
	private Long xyNum;

	/**
	 * 是否进行了处理 回复了消息即代表已处理
	 */
	private Boolean ifDealWith ;

	
	public Boolean getIfDealWith() {
		return ifDealWith;
	}

	public void setIfDealWith(Boolean ifDealWith) {
		this.ifDealWith = ifDealWith;
	}

	public Integer getFhNum() {
		return fhNum;
	}

	public void setFhNum(Integer fhNum) {
		this.fhNum = fhNum;
	}

	public Integer getCjNum() {
		return cjNum;
	}

	public void setCjNum(Integer cjNum) {
		this.cjNum = cjNum;
	}

	public Long getXyNum() {
		return xyNum;
	}

	public void setXyNum(Long xyNum) {
		this.xyNum = xyNum;
	}

	public boolean isUnPhone() {
		return unPhone;
	}

	public void setUnPhone(boolean unPhone) {
		this.unPhone = unPhone;
	}

	public String getPhonetype() {
		return phonetype;
	}

	public void setPhonetype(String phonetype) {
		this.phonetype = phonetype;
	}

	public boolean isWxWarm() {
		return wxWarm;
	}

	public void setWxWarm(boolean wxWarm) {
		this.wxWarm = wxWarm;
	}

	public boolean isIfTop() {
		return ifTop;
	}

	public void setIfTop(boolean ifTop) {
		this.ifTop = ifTop;
	}

	public Integer getMesType() {
		return mesType;
	}

	public boolean isTrans() {
		return trans;
	}

	public void setTrans(boolean trans) {
		this.trans = trans;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}

	public void setMesType(Integer mesType) {
		this.mesType = mesType;
	}

	public String gettCustWxId() {
		return tCustWxId;
	}

	public void settCustWxId(String tCustWxId) {
		this.tCustWxId = tCustWxId;
	}

	public List<CustWxInfoVO> getCustWxvoList() {
		return custWxvoList;
	}

	public void setCustWxvoList(List<CustWxInfoVO> custWxvoList) {
		this.custWxvoList = custWxvoList;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getCustPhone() {
		return custPhone;
	}

	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}

	public String getCustWxId() {
		return custWxId;
	}

	public void setCustWxId(String custWxId) {
		this.custWxId = custWxId;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public Integer getNoReadMsgCount() {
		return noReadMsgCount;
	}

	public void setNoReadMsgCount(Integer noReadMsgCount) {
		this.noReadMsgCount = noReadMsgCount;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<CustImRole> getCustRoleList() {
		return custRoleList;
	}

	public void setCustRoleList(List<CustImRole> custRoleList) {
		this.custRoleList = custRoleList;
	}

	public String getHasWxRole() {
		return hasWxRole;
	}

	public void setHasWxRole(String hasWxRole) {
		this.hasWxRole = hasWxRole;
	}

 

 
	public Long getAddTimestamp() {
		return addTimestamp;
	}

	public void setAddTimestamp(Long addTimestamp) {
		this.addTimestamp = addTimestamp;
	}

	public String getWxStatus() {
		return wxStatus;
	}

	public void setWxStatus(String wxStatus) {
		this.wxStatus = wxStatus;
	}

	public Integer getWxNoReadNum() {
		return wxNoReadNum;
	}

	public void setWxNoReadNum(Integer wxNoReadNum) {
		this.wxNoReadNum = wxNoReadNum;
	}

	public String getPhoneStatus() {
		return phoneStatus;
	}

	public void setPhoneStatus(String phoneStatus) {
		this.phoneStatus = phoneStatus;
	}

	public Integer getPhoneNoReadNum() {
		return phoneNoReadNum;
	}

	public void setPhoneNoReadNum(Integer phoneNoReadNum) {
		this.phoneNoReadNum = phoneNoReadNum;
	}

	public String getLastMsg() {
		return lastMsg;
	}

	public void setLastMsg(String lastMsg) {
		this.lastMsg = lastMsg;
	}

	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public List<JSONObject> getCustJsonList() {
		return custJsonList;
	}

	public void setCustJsonList(List<JSONObject> custJsonList) {
		this.custJsonList = custJsonList;
	}

	public List<Customer> getCustList() {
		return custList;
	}

	public void setCustList(List<Customer> custList) {
		this.custList = custList;
	}

	public boolean isGenjin() {
		return genjin;
	}

	public void setGenjin(boolean genjin) {
		this.genjin = genjin;
	}

	public String getCustPhoneXnh() {
		return custPhoneXnh;
	}

	public void setCustPhoneXnh(String custPhoneXnh) {
		this.custPhoneXnh = custPhoneXnh;
	}
}
