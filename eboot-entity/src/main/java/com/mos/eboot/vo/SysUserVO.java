package com.mos.eboot.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.util.Constants;

/**
 * <p>
 * <p>
 * </p>
 *
 * @author 小尘哥
 * @since 2018-01-13
 */
public class SysUserVO implements Serializable {

	public static String SUPER_ID = "1";

	private String id;

	private String username;

	private String password;

	private String isDel;

	private String nickname;

	private String mobile;

	private String photo;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createTime;

	private String createUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField("last_login_time")
	private Date lastLoginTime;

	private String ip;

	private String disabled;

	private String locked;

	private List<String> roleIds;

	private List<SysUserWxInfo> userWxInfo;
	
	private List<SysUserWxInfo> userWxInfoAll;

	public List<SysUserWxInfo> getUserWxInfoAll() {
		return userWxInfoAll;
	}

	public void setUserWxInfoAll(List<SysUserWxInfo> userWxInfoAll) {
		this.userWxInfoAll = userWxInfoAll;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUsername() {
		return username;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Boolean getDisabled() {
		return Constants.POSITIVE.equals(disabled);
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public List<String> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<String> roleIds) {
		this.roleIds = roleIds;
	}

	public List<SysUserWxInfo> getUserWxInfo() {
		return userWxInfo;
	}

	public void setUserWxInfo(List<SysUserWxInfo> userWxInfo) {
		this.userWxInfo = userWxInfo;
	}

	@Override
	public String toString() {
		return "SysUser{" + ", id=" + id + ", username=" + username + ", password=" + password + ", isDel=" + isDel
				+ ", nickname=" + nickname + ", mobile=" + mobile + ", photo=" + photo + ", createTime=" + createTime
				+ ", createUser=" + createUser + ", lastLoginTime=" + lastLoginTime + ", ip=" + ip + ", disabled="
				+ disabled + ", locked=" + locked + "}";
	}
}
