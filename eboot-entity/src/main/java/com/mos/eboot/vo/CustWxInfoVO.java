package com.mos.eboot.vo;

import java.io.Serializable;

import com.mos.eboot.platform.entity.SysUserWxInfo;

public class CustWxInfoVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8175170536400512347L;

	private Integer id;

	private String cId;
	private String userId;

	private String wxId;

	private String wxNumber;

	private String wxRemark;

	private String wxNickname;

	private String sysWxId;

	private String wxType;

	private SysUserWxInfo sysUserWxInfo;
	
	private String wxStat;

	private String wxAvatar;
	
	
	public String getWxAvatar() {
		return wxAvatar;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxStat() {
		return wxStat;
	}

	public void setWxStat(String wxStat) {
		this.wxStat = wxStat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNumber() {
		return wxNumber;
	}

	public void setWxNumber(String wxNumber) {
		this.wxNumber = wxNumber;
	}

	public String getWxRemark() {
		return wxRemark;
	}

	public void setWxRemark(String wxRemark) {
		this.wxRemark = wxRemark;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public String getWxType() {
		return wxType;
	}

	public void setWxType(String wxType) {
		this.wxType = wxType;
	}

	public SysUserWxInfo getSysUserWxInfo() {
		return sysUserWxInfo;
	}

	public void setSysUserWxInfo(SysUserWxInfo sysUserWxInfo) {
		this.sysUserWxInfo = sysUserWxInfo;
	}
	@Override
	public String toString() {
		return "CustWx{" +
			", id=" + id +
			", cId=" + cId +
			", wxId=" + wxId +
			", wxNumber=" + wxNumber +
			", wxRemark=" + wxRemark +
			", wxNickname=" + wxNickname +
			", sysWxId=" + sysWxId +
			", wxType=" + wxType +
			"}";
	}
}
