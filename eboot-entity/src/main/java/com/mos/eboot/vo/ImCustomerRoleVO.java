package com.mos.eboot.vo;

import java.io.Serializable;

public class ImCustomerRoleVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -933265662180880448L;
	private Integer roleId;
	private String custType;
	private String buttonName;
	private String url;
	private Integer sort;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}

	public String getButtonName() {
		return buttonName;
	}

	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
}
