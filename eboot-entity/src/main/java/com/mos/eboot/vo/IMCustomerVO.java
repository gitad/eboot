package com.mos.eboot.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;

public class IMCustomerVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5027557584442040436L;

	private Integer id;

	private String custName;

	private String custType;

	private String company;

	private String custPhone;

	private List<ImCustomerRoleVO> imRole;

 
	private String tid;
 
	private String validPhone;
 
	private String otherPhone;

 
	private String qq;

 
	private String custWxId;

 
	private String sysWxId;
 
	private String wxStatus;

 
	private String provinceName;

 
	private String cityName;

 
	private String areaName;

 
	 
 
	private String addressInfo;

 
	private String isDel;

 
	private String isUpload;

 
	private String rawAddUser;

 
	private String rawAddTime;

 
	private String rawUpUser;

 
	private String rawUpTime;

 
	private Date lastCallTime;
	
	private String custAdminUserId;
	
	private String custAdminNickname;
 
	private String ifNormal;
	 
 
	private String custAdminUserIdTem;
	
 
	private String custAdminNicknameTem;
	
 
	private Date temTime;
	
	private String remark;
	
	 
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getTemTime() {
		return temTime;
	}

	public void setTemTime(Date temTime) {
		this.temTime = temTime;
	}

	public String getCustAdminUserIdTem() {
		return custAdminUserIdTem;
	}

	public void setCustAdminUserIdTem(String custAdminUserIdTem) {
		this.custAdminUserIdTem = custAdminUserIdTem;
	}

	public String getCustAdminNicknameTem() {
		return custAdminNicknameTem;
	}

	public void setCustAdminNicknameTem(String custAdminNicknameTem) {
		this.custAdminNicknameTem = custAdminNicknameTem;
	}
 

 
 
	public String getIfNormal() {
		return ifNormal;
	}

	public void setIfNormal(String ifNormal) {
		this.ifNormal = ifNormal;
	}

	public String getCustAdminUserId() {
		return custAdminUserId;
	}

	public void setCustAdminUserId(String custAdminUserId) {
		this.custAdminUserId = custAdminUserId;
	}

	public String getCustAdminNickname() {
		return custAdminNickname;
	}

	public void setCustAdminNickname(String custAdminNickname) {
		this.custAdminNickname = custAdminNickname;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getValidPhone() {
		return validPhone;
	}

	public void setValidPhone(String validPhone) {
		this.validPhone = validPhone;
	}

	public String getOtherPhone() {
		return otherPhone;
	}

	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getCustWxId() {
		return custWxId;
	}

	public void setCustWxId(String custWxId) {
		this.custWxId = custWxId;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public String getWxStatus() {
		return wxStatus;
	}

	public void setWxStatus(String wxStatus) {
		this.wxStatus = wxStatus;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAddressInfo() {
		return addressInfo;
	}

	public void setAddressInfo(String addressInfo) {
		this.addressInfo = addressInfo;
	}

	public String getIsDel() {
		return isDel;
	}

	public void setIsDel(String isDel) {
		this.isDel = isDel;
	}

	public String getIsUpload() {
		return isUpload;
	}

	public void setIsUpload(String isUpload) {
		this.isUpload = isUpload;
	}

	public String getRawAddUser() {
		return rawAddUser;
	}

	public void setRawAddUser(String rawAddUser) {
		this.rawAddUser = rawAddUser;
	}

	public String getRawAddTime() {
		return rawAddTime;
	}

	public void setRawAddTime(String rawAddTime) {
		this.rawAddTime = rawAddTime;
	}

	public String getRawUpUser() {
		return rawUpUser;
	}

	public void setRawUpUser(String rawUpUser) {
		this.rawUpUser = rawUpUser;
	}

	public String getRawUpTime() {
		return rawUpTime;
	}

	public void setRawUpTime(String rawUpTime) {
		this.rawUpTime = rawUpTime;
	}

	public Date getLastCallTime() {
		return lastCallTime;
	}

	public void setLastCallTime(Date lastCallTime) {
		this.lastCallTime = lastCallTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustType() {
		return custType;
	}

	public void setCustType(String custType) {
		this.custType = custType;
	}


	public List<ImCustomerRoleVO> getImRole() {
		return imRole;
	}

	public void setImRole(List<ImCustomerRoleVO> imRole) {
		this.imRole = imRole;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCustPhone() {
		return custPhone;
	}

	public void setCustPhone(String custPhone) {
		this.custPhone = custPhone;
	}

}
