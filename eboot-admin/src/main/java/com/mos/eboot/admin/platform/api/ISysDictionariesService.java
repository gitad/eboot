package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.tools.result.ResultModel;

 
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
@FeignClient("boot-service")
public interface ISysDictionariesService  {

	/**字典树
	 * @param page
	 * @throws Exception
	 */
	@RequestMapping(value = "sysDictionaries/queryByparentId",method = RequestMethod.POST)
	 List<SysDictionaries> queryByparentId(@RequestParam("parentId")   String  parentId);
	 
	/**通用字典查询  根据bianma
	 * @param page
	 * @throws Exception
	 */
	@RequestMapping(value = "sysDictionaries/query",method = RequestMethod.POST)
	 List<SysDictionaries> query(@RequestParam("parentId")   String  parentId);
	
	
	@PostMapping("sysDictionaries/save")
	ResultModel<Boolean> save(@RequestBody   List<SysDictionaries> list);
	
	@PostMapping("sysDictionaries/del")
	ResultModel<Boolean> del(@RequestBody SysDictionaries  sSysDictionaries);
	
}
