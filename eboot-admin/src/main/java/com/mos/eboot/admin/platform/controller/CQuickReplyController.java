package com.mos.eboot.admin.platform.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICCallLogService;
import com.mos.eboot.admin.platform.api.ICQuickReplyService;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CQuickReply;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cQuickReply")
public class CQuickReplyController  extends BaseController {
	
	@Autowired
	ICQuickReplyService iCQuickReplyService;
	
	@RequestMapping("to-page")
	public String toPage() {
	   return "platform/quickReply/list";
	}
	
	
	
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CQuickReply> query(Page<CQuickReply> page) {
		ResultModel<Page<CQuickReply>> dataPage=iCQuickReplyService.query(page);		
		return this.getLayPage(dataPage);
    }
 
	
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<Boolean> save(@RequestBody CQuickReply  cQuickReply) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 cQuickReply.setCreateUser(user.getNickname());
		 cQuickReply.setCreateTime(new Date()); 
	    ResultModel<Boolean> result=iCQuickReplyService.save(cQuickReply);	
	    return result;
	}
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestBody CQuickReply  cQuickReply) {
	    ResultModel<Boolean> result=iCQuickReplyService.del(cQuickReply);
	    return result;
	}
 
	
}
