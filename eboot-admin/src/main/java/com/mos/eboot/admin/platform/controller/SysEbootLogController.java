package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
@Controller
@RequestMapping("/sysEbootLog")
public    class SysEbootLogController extends BaseController{
	
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	enum Staties{
	 
	   add("增加",1),del("删除",2),update("修改",3),save("保存",4),login("登陆",5),newAdd("新增",6);
	    private String name;
	    private int index;
         //构造函数
	    private Staties(String name,int index){
	        this.name=name;
	        this.index=index;
	    }
	    //枚举方法
	    public static String getColorName(String name){
	        for(Staties c : Staties.values()){
	           if(c.toString().equals(name)){
	                return c.getName();
	            }
	        }
	        return null;
	    }

	   

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public int getIndex() {
	        return index;
	    }

	    public void setIndex(int index) {
	        this.index = index;
	    }
	}
	
	@PostMapping("/query")
	@ResponseBody
	public List<SysEbootLog> query(String logBusinessId,  String logBusinessType,  String logStaties,  String customerId) {		 
		 List<SysEbootLog> list=iSysEbootLogService.queryPage(queryPage(logBusinessId,  logBusinessType,    logStaties,   customerId));
		return list;
    }
	//数据插入
 	public static  SysEbootLog queryPage( String logBusinessId,  String logBusinessType, 	 String logStaties,  String customerId) {	
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		 if(currentUser==null){
		    currentUser= WeChatController.sysUser;
	     }
	    SysEbootLog log=new SysEbootLog();
	    log.setCreatePeo(currentUser.getNickname());
	    log.setCreatePeoId(currentUser.getId());
	    log.setCreateTime(new Date());	   
	    log.setLogBusinessId(logBusinessId); 
	    log.setLogBusinessType(logBusinessType);	   
	    log.setLogStaties(Staties.getColorName(logStaties));	  
	    log.setCustomerId(customerId);
		return log;
	}
	
	//数据插入
 	public static  SysEbootLog insert( String logBusinessId,  String logBusinessType, String logStaties,Object content,  String customerId) {	
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		 if(currentUser==null){
			  currentUser= WeChatController.sysUser;
	    }
	    SysEbootLog log=new SysEbootLog();
	    log.setCreatePeo(currentUser.getNickname());
	    log.setCreatePeoId(currentUser.getId());
	    log.setCreateTime(new Date());
	    log.setContent(JSONObject.toJSONString(content));
	    log.setLogBusinessId(logBusinessId); 
	    log.setLogBusinessType(logBusinessType);	   
	    log.setLogStaties(Staties.getColorName(logStaties));	  
	    log.setCustomerId(customerId);
		return log;
	}

 
	
	
	
}
