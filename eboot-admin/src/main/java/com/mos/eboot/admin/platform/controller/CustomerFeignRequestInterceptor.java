package com.mos.eboot.admin.platform.controller;
 
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
/**
 * 监听请求
 * jiang
 * 2021/10/02
 * */
//@Slf4j
//@Component
public class CustomerFeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        if (attributes != null) {
            HttpServletRequest request = attributes.getRequest();
            template.header("sessionId", request.getHeader("sessionId"));
        }
       
           System.out.print(System.lineSeparator()+ template);
         
         
    }
}