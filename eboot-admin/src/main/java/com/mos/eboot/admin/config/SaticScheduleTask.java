package com.mos.eboot.admin.config;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICCargoEntryService;
import com.mos.eboot.admin.platform.api.ICCustomerMarketService;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.IWxGroupService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.platform.entity.WxGroup;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.BeanCopyUtilCallBack;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.CustWxInfoVO;
import com.mos.eboot.vo.ImMsgConversationListVO;
/**
 * 
 * 定时任务
 * */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling  
public class SaticScheduleTask {
 
	@Autowired
	private RedisUtil redisUtil;
	
	@Autowired
	ISysUserService sysUserService;
	
	@Autowired
	ICCustomerMarketService customerMarketService;  
	
	@Autowired
	private ICustomerService iCustomerService;
	
	@Autowired
	private ImApiService imApiService;
	
    @Autowired
    IWxGroupService wxGroupService;
    
    @Autowired
    ICCargoEntryService iCCargoEntryService;  
	//将客户营销数据进行处理
  @Scheduled(fixedRate=60*1000*60)
  private void CCustomerMarketTasks() {
		/*Date date = new Date(newEnd);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		System.out.println(df.format(date));*/
		   Page<CCustomerMarket> page=new Page<CCustomerMarket>();
			Map<String,Object> condition=new HashMap<String, Object>(); 	
			condition.put("useFlag", 0);
			 page.setCondition(condition);
			ResultModel<Page<CCustomerMarket>> dataPage=customerMarketService.query(page);	
         	List<CCustomerMarket> list=dataPage.getData().getRecords();
			if(list.size()>0){
				for(CCustomerMarket li:list){
					Integer dayNum=Integer.valueOf(li.getMarketDays());//获取下次要发送的时间
				    Long  st= new Date().getTime(); //当前时间
					Long  end=(long) (dayNum*60*1000*60*24+li.getCreateTime().getTime());//创建时间
					if(st>end){//当前时间大于结束时间
						if(StringUtil.isNotBlank(li.getMarketType())){
							if(li.getMarketType().equals("微信")){
								 ResultModel<List<CustWx>>  result=iCustomerService.getCustomerWxByCustId(li.getCustomerId(),"");
								 List<CustWx> custWxList =result.getData();
								 if(custWxList!=null&&custWxList.size()>0){
									 for(CustWx custWx:custWxList){
										 if(custWx.getWxStat().equals("1")){
											 imApiService.send_v1(li.getMarketContent(),custWx.getWxId(),custWx.getSysWxId(),li.getCreateUserId(),"");
											 li.setUseFlag(1);
											 customerMarketService.save(li);
											 
											 break;
										 }
										
									 }
								 }
								 
							}
							if(li.getMarketType().equals("系统提示")){
								  List<Object> objList= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + li.getCreateUserId(), 0, -1);//构建人员聊天关系
								 boolean create=false;
								 ImMsgConversationListVO conversationListVO=null;
								  for(int i=0;i<objList.size();i++){
									    conversationListVO =(ImMsgConversationListVO)objList.get(i); 
									   if(conversationListVO.getcId().equals(li.getCustomerId())){
										  create=true;
										  conversationListVO.setGenjin(true);
										  conversationListVO.setIfTop(true);
										  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + li.getCreateUserId(),i, conversationListVO);	
										  li.setUseFlag(1);
										  customerMarketService.save(li);
									   }
									   
								   }
								  if(create==false){//代表没有查询到任何人  进行创建
									  Customer customer=iCustomerService.getCustomerById(li.getCustomerId()).getData();
									  List<CustImRole> custRoleList = iCustomerService.getCustImRole(li.getCreateUserId(), customer.getCustType()).getData();
									  conversationListVO=genConversationVO(li.getMarketPhone(), customer, custRoleList, 0);
									  conversationListVO.setGenjin(true);
									  conversationListVO.setIfTop(true);
									  objList.add(conversationListVO);	
									  redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + li.getCreateUserId(), objList);  
									  li.setUseFlag(1);
										 customerMarketService.save(li);
								  }
									
							}
						}
						
					}
				}	
			}
  }
   
	//定时任务 定时往群里发送消息  
 //@Scheduled(fixedRate=60*1000*60*2)
 //@Scheduled(fixedRate=30*1000)
  private void wxGroupTasks() {
	    Integer dayNum=Integer.valueOf(new SimpleDateFormat("HH").format(new Date()));
	    Integer sendNum=0;
		  if(dayNum>=7||dayNum<=20){
			  Page<CCargoEntry> carGopage=new Page<CCargoEntry>();
			   carGopage.setSize(500);
		    	 Map<String, Object> cond=new HashMap<String, Object>();
		    	 Date date = new Date();
	    	     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    	     String format = sdf.format(date);		      
				 cond.put("planTime",format);
				 carGopage.setCondition(cond);
				 ResultModel<Page<CCargoEntry>> carGoPage=iCCargoEntryService.query(carGopage);	
				 
				 List<CCargoEntry>  carGoList=carGoPage.getData().getRecords();
				 for (int i = 0; i < carGoList.size(); i++) {
					 
					 CCargoEntry carGo=carGoList.get(i);
						 if(carGo.getCargoType().equals("急发")||carGo.getCargoType().equals("普通")){
							 sendNum++;
							 Page<WxGroup> page=new Page<WxGroup>();
							 page.setSize(5000);
							  ResultModel<Page<WxGroup>> dataPage=wxGroupService.queryGroup(page);
							  List<WxGroup> list=dataPage.getData().getRecords();
								  if(list.size()>0){
										  for(int k=0;k<list.size();k++){
												    WxGroup  li=list.get(k);
												  	String tags=li.getGroupTag();
												  	String  sendMess=li.getMessWxid();//被制定的微信消息发送人
												  	if(StringUtil.isBlank(sendMess)){
												  		List<CustWx> cust=iCustomerService.getCustomerListWxByWxId(li.getGroupId()).getData();
												  		sendMess=cust.get(0).getSysWxId();
												  	}
													if( StringUtil.isNotBlank(tags)&&StringUtil.isNotBlank(sendMess)){
													    	  if(tags.contains("货运")){
													    		   String content=carGo.getAddressDetailStr()+"   到   "
																			 +carGo.getAddressDetailEnd()+"      " +
																			 carGo.getWeightStr()+"-"+carGo.getWeightEnd() + carGo.getVolumeStr()+"-"+carGo.getVolumeEnd() +"   求  "
																			 +carGo.getCarLen()+"   "+carGo.getCarType()+"   "+carGo.getCarUseType()+"  <运费高,急发>";
																	 SysUser  user= sysUserService.getByUserNickname(carGo.getCreateUser());
																	 if(user!=null){
																		 content+="，联系电话 "+user.getMobile();
																	 }
																	  try {
																		TimeUnit.SECONDS.sleep(1);
																	} catch (InterruptedException e) {
																		// TODO Auto-generated catch block
																		e.printStackTrace();
																	}//秒
																	 imApiService.send_v1(content,li.getGroupId(),sendMess,"","");
													    		     
													    		    //  System.out.println(content+li.getGroupId());
													    		       
													    	   }
													    	 
												    }
											 
										}
								  }
							 
								  if(sendNum!=0){
								    	if(sendNum%6==0){
								    		 
								    		  try {
												  TimeUnit.MINUTES.sleep(10);//等待10分钟下次发送yao
												 //TimeUnit.SECONDS.sleep(10);//秒
												
											} catch (InterruptedException e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											} 
									    }
								    }
								
								
						 }
					
				 }
		  }
	
	  }
	private ImMsgConversationListVO genConversationVO(String custPhone, Customer customer,
			List<CustImRole> custRoleList,Integer  mesType) {
		ImMsgConversationListVO conversationListVO =new ImMsgConversationListVO();
		conversationListVO.setCustName(customer.getCustName());
		conversationListVO.setSessionId(UuidUtil.get32UUID());
		conversationListVO.setCustType(customer.getCustType());
		conversationListVO.setCustPhone(custPhone);//拨打电话设置电话号码
		conversationListVO.setcId(customer.getId());
		conversationListVO.setCustRoleList(custRoleList);
		conversationListVO.setCompany(customer.getAddressInfo());
		conversationListVO.setAddress(customer.getAddressInfo());
		conversationListVO.setHasWxRole("0");
		conversationListVO.setPhoneNoReadNum(0);
		conversationListVO.setWxNoReadNum(1);
		conversationListVO.setMesType(mesType); 
		conversationListVO.setShow(false);
		conversationListVO.setIfTop(false);
		conversationListVO.setWxWarm(false);
		conversationListVO.setWxStatus(customer.getWxStatus());
		conversationListVO.setCjNum(0);//成交
		conversationListVO.setFhNum(0);//发货
		conversationListVO.setGenjin(false);
		conversationListVO.setXyNum(new Long(0));//信用
		if(mesType==0){//消息类型   0微信  /1群/2拨打/3来电
			conversationListVO.setIfDealWith(false);//处理状态
		}else{
			conversationListVO.setIfDealWith(true);//虚拟号 群聊 来电 默认已处理状态
		}
	
		if(StringUtil.isNotBlank(customer.getCustAdminUserIdTem())){
			conversationListVO.setTrans(true);//是否转交
		}else{
			conversationListVO.setTrans(false);//是否转交	
		}
	
		ResultModel<List<CustWx>> result= iCustomerService.getCustListWxByCid(customer.getId().toString());
		List<CustWx>  custWxList= result.getData();
				 
		
		if(custWxList != null && custWxList.size() >0) {
			conversationListVO.setHasWxRole("1");
			if(StringUtil.isNotBlank(custWxList.get(0).getWxRemark())){
				conversationListVO.setCustPhone(custWxList.get(0).getWxRemark());//聊天设置此次聊天的号码
			}
			 
			conversationListVO.settCustWxId(custWxList.get(0).getId().toString());
			/**
			 * 构建左侧弹框列表
			 */
			List<CustWxInfoVO> custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
				@Override
				public void callBack(CustWx s, CustWxInfoVO t) {
					// TODO Auto-generated method stub
					
					SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
					t.setSysUserWxInfo(sysUserWxInfo);
				}
			});
			String lastMsg= imApiService.getLastHisMsgList(custWxvoList.get(0).getWxId(), custWxvoList.get(0).getSysWxId());
			conversationListVO.setLastMsg(lastMsg);

			conversationListVO.setCustWxvoList(custWxvoList);
		}
		
		
		
		conversationListVO.setAddTimestamp(System.currentTimeMillis());
		
		conversationListVO.setAddTime(LocalTime.now().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		return conversationListVO;
	}
}

