package com.mos.eboot.admin.config;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.Filter;

import org.apache.commons.io.IOUtils;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.io.ResourceUtils;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.MethodInvokingFactoryBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.support.MessageSourceAccessor;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;

import com.google.common.collect.Maps;
import com.mos.eboot.admin.config.service.impl.PermissionService;
import com.mos.eboot.admin.config.service.impl.UserService;
import com.mos.eboot.tools.shiro.KaptchaValidateFilter;

/**
 * @author 小尘哥
 */
@Configuration
public class ShiroConfig {

	/**
	 * 缓存管理器 使用Ehcache实现
	 */
	@Bean
	public EhCacheManager getEhCacheManager() {
		net.sf.ehcache.CacheManager cacheManager = net.sf.ehcache.CacheManager.getCacheManager("ruoyi");
		EhCacheManager em = new EhCacheManager();
		if (null == cacheManager) {
			em.setCacheManager(new net.sf.ehcache.CacheManager(getCacheManagerConfigFileInputStream()));
			return em;
		} else {
			em.setCacheManager(cacheManager);
			return em;
		}
	}

	/**
	 * 返回配置文件流 避免ehcache配置文件一直被占用，无法完全销毁项目重新部署
	 */
	protected InputStream getCacheManagerConfigFileInputStream() {
		String configFile = "classpath:ehcache/ehcache-shiro.xml";
		InputStream inputStream = null;
		try {
			inputStream = ResourceUtils.getInputStreamForPath(configFile);
			byte[] b = IOUtils.toByteArray(inputStream);
			InputStream in = new ByteArrayInputStream(b);
			return in;
		} catch (IOException e) {
			throw new ConfigurationException(
					"Unable to obtain input stream for cacheManagerConfigFile [" + configFile + "]", e);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

	@Bean(name = "securityManager")
	public DefaultWebSecurityManager securityManager(PermissionService permissionService) {
		DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
		manager.setRealm(userRealm(permissionService));
		// 注入缓存管理器;
		manager.setCacheManager(getEhCacheManager());
		return manager;
	}

	@Bean
	public MethodInvokingFactoryBean setSecurityManager(PermissionService permissionService) {
		MethodInvokingFactoryBean methodInvokingFactoryBean = new MethodInvokingFactoryBean();
		methodInvokingFactoryBean.setStaticMethod("org.apache.shiro.SecurityUtils.setSecurityManager");
		methodInvokingFactoryBean.setArguments(securityManager(permissionService));
		return methodInvokingFactoryBean;
	}

	@Bean
	@DependsOn(value = "lifecycleBeanPostProcessor")
	public UserRealm userRealm(PermissionService permissionService) {
		UserRealm userRealm = new UserRealm(permissionService);
		userRealm.setCredentialsMatcher(credentialsMatcher());
		return userRealm;
	}

	@Bean
	public HashedCredentialsMatcher credentialsMatcher() {
		HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
		credentialsMatcher.setHashAlgorithmName("MD5");
//        credentialsMatcher.setHashIterations(2);
		return credentialsMatcher;
	}

	@Bean
	public FormAuthenticationFilter authcFilter(
			@Qualifier("messageSourceAccessor") MessageSourceAccessor messageSourceAccessor,
			@Qualifier("userService") UserService userService) {
		CustomFormAuthenticationFilter authenticationFilter = new CustomFormAuthenticationFilter(messageSourceAccessor,
				userService);
		authenticationFilter.setLoginUrl("/login");
		return authenticationFilter;
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(
			@Qualifier("permissionService") PermissionService permissionService) {
		AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
		authorizationAttributeSourceAdvisor.setSecurityManager(securityManager(permissionService));
		return authorizationAttributeSourceAdvisor;
	}

	public LogoutFilter logoutFilter() {
		LogoutFilter logout = new LogoutFilter();
		logout.setRedirectUrl("/toLogin");
		return logout;
	}

	@Bean
	public KaptchaValidateFilter kaptchaValidate() {
		return new KaptchaValidateFilter();
	}

	@Bean(name = "shiroFilter")
	public ShiroFilterFactoryBean shiroFilter(
			@Qualifier("messageSourceAccessor") MessageSourceAccessor messageSourceAccessor,
			@Qualifier("userService") UserService userService,
			@Qualifier("permissionService") PermissionService permissionService) {
		ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
		bean.setSecurityManager(securityManager(permissionService));
		bean.setLoginUrl("/toLogin");
		bean.setUnauthorizedUrl("/unauthor");
		bean.setSuccessUrl("/index");
		
		Map<String, Filter> filters = Maps.newHashMap();
	//	filters.put("kaptchaValidate", kaptchaValidate());
		filters.put("authc", authcFilter(messageSourceAccessor, userService));
		filters.put("logout", logoutFilter());
		bean.setFilters(filters);

		Map<String, String> filterChainDefinitionMap = Maps.newLinkedHashMap();		
		filterChainDefinitionMap.put("/weChat/openChat", "anon");
		filterChainDefinitionMap.put("/cQuickReply/**", "anon");	//快捷回复
		filterChainDefinitionMap.put("/cCargoEntry/**", "anon");	//货源录入
		filterChainDefinitionMap.put("/cCallLog/**", "anon");	//通话记录
		filterChainDefinitionMap.put("/cContactPeo/**", "anon");	//联系人及备注
		filterChainDefinitionMap.put("/cCargoInfo/**", "anon");	//车讯货
		filterChainDefinitionMap.put("/sys/cust/**", "anon");	//车讯货
		filterChainDefinitionMap.put("/cCompanyDetail/**", "anon");	//企业详情
		filterChainDefinitionMap.put("/cCarInfo/**", "anon");	//车辆资料
		
		
		
		filterChainDefinitionMap.put("/imapi/**", "anon");	//  
		filterChainDefinitionMap.put("/demo/**", "anon");
		filterChainDefinitionMap.put("/kaptcha.jpg", "anon");
		filterChainDefinitionMap.put("/app/**", "anon");
		filterChainDefinitionMap.put("/images/**", "anon");
		filterChainDefinitionMap.put("/css/**", "anon");
		filterChainDefinitionMap.put("/js/**", "anon");
		filterChainDefinitionMap.put("/toLogin", "anon");	
		filterChainDefinitionMap.put("/plugins/**", "anon");
		filterChainDefinitionMap.put("/actuator/**", "anon");
	 	filterChainDefinitionMap.put("/login", "kaptchaValidate,authc");
	 	filterChainDefinitionMap.put("/logout", "logout");
	 	filterChainDefinitionMap.put("/**", "user"); 
 		
		bean.setFilterChainDefinitionMap(filterChainDefinitionMap);

		return bean;
	}

	@Bean
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	@Bean
	public MessageSourceAccessor messageSourceAccessor(@Qualifier("messageSource") MessageSource messageSource) {
		return new MessageSourceAccessor(messageSource);
	}

	@Bean
	public ShiroDialect shiroDialect() {
		return new ShiroDialect();
	}
}
