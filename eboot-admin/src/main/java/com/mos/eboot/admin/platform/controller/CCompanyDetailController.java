package com.mos.eboot.admin.platform.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mos.eboot.admin.platform.api.ICCompanyDetailService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCompanyDetail;
import com.mos.eboot.platform.entity.CCompanyDetailRel;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@Controller
@RequestMapping("/cCompanyDetail")
public class CCompanyDetailController {
	
	@Autowired
	ICCompanyDetailService cCompanyDetailService;
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	private static String customerId="";
	private static String phone="";
	
    @GetMapping("/to-page")
    public String toPage( String cid,String sid,Model model) {
    	if(cid!=null){
			this.customerId=cid; 
    	}
    	 return "platform/cCompany/list";
    }
    
    
    @PostMapping("/query")
	@ResponseBody
	public CCompanyDetail query() {	
    	CCompanyDetail cCompanyDetail=new CCompanyDetail();
    	cCompanyDetail.setCustomerId(customerId);
    	return cCompanyDetailService.query(cCompanyDetail).getData();
    }
 
    @PostMapping("/relQuery")
 	@ResponseBody
 	public List<CCompanyDetailRel> relQuery(String companyId) {	   	 
     	return cCompanyDetailService.relQuery(companyId).getData();
     }
    
    
    @PostMapping("/relDel")
 	@ResponseBody
 	public Boolean relDel(@RequestBody CCompanyDetailRel  cCompanyDetailRel) {	
    	 return cCompanyDetailService.relDel(cCompanyDetailRel).getData();
     }
    
    @PostMapping("/relAdd")
   	@ResponseBody
   	public CCompanyDetailRel relAdd(String companyId) {	
    	CCompanyDetailRel detailRel=new CCompanyDetailRel();
    	detailRel.setCompanyId(companyId);
      	 return detailRel;
       }
      
    @PostMapping("/save")
	@ResponseBody
	public CCompanyDetail save(@RequestBody CCompanyDetail  cCompanyDetail) {	
    	cCompanyDetail.setCustomerId(customerId);
    	  if(StringUtil.isNotBlank(cCompanyDetail.getCompanyId())){
 			 
			  iSysEbootLogService.save( SysEbootLogController.insert(cCompanyDetail.getCompanyId(), "c_company_detail", "update", cCompanyDetail.toString(),cCompanyDetail.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cCompanyDetail.getCompanyId(), "c_company_detail", "add", cCompanyDetail.toString(),cCompanyDetail.getCustomerId())) ;
		  }
    	return  cCompanyDetailService.save(cCompanyDetail).getData();
    }
 
    @PostMapping("/relSave")
	@ResponseBody
	public List<CCompanyDetailRel> save(@RequestBody List<CCompanyDetailRel>  list) {	
    	 for(CCompanyDetailRel li:list){
    		li= cCompanyDetailService.relSave(li).getData();
    	 }
    	return  list;
    }
  
    
    
}
