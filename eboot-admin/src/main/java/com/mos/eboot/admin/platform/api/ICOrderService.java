package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@FeignClient("boot-service")
public interface ICOrderService  {

	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cOrder/query",method = RequestMethod.POST)
	 ResultModel<Page<COrder>> query(@RequestBody Page<COrder> page);


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cOrder/queryCount",method = RequestMethod.POST)
	  List<JSONObject>  queryCount(@RequestBody Page<COrder> page);


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cOrder/queryCountDetail",method = RequestMethod.POST)
	  List<COrder>  queryCountDetail(@RequestBody Page<COrder> page);

	 
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */ 
	@RequestMapping(value = "cOrder/queryDetail",method = RequestMethod.POST)
	JSONObject queryDetail(@RequestParam("orderId") String orderId);
	 
	@RequestMapping(value = "cOrder/queryByCargoInfoId",method = RequestMethod.POST)
	COrder queryByCargoId(@RequestParam("cargoInfoId") String cargoInfoId);
	
	
 
	 @RequestMapping(value = "cOrder/save",method = RequestMethod.POST)
	 COrder  save(@RequestBody  COrder cOrder);

}
