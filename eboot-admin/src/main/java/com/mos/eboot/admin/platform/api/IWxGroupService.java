package com.mos.eboot.admin.platform.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.WxGroup;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务类 
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-03
 */
@FeignClient("boot-service")
public interface IWxGroupService  { 


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "wxGroup/queryGroup",method = RequestMethod.POST)
	ResultModel<Page<WxGroup>> queryGroup(@RequestBody Page<WxGroup> page);
	 
	  
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("wxGroup/save")
	ResultModel<Boolean> save( @RequestBody WxGroup wxGroup);
	 
}
