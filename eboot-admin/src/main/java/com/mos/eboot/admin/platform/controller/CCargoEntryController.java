package com.mos.eboot.admin.platform.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICCargoEntryService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  货源录入
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-04
 */
@Controller
@RequestMapping("/cCargoEntry")
public class CCargoEntryController  extends BaseController{
	

	@Autowired
	ICCargoEntryService iCCargoEntryService;
	
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	
	private static String customerId="";
	private static String phone="";
	@RequestMapping("to-page")
	public String toPage( String cid,String phoneNum) {
		if(cid!=null){
			this.customerId=cid; 
			this.phone=StringUtil.str_Phone(phoneNum);; 
		 }
	   return "platform/cargoEntry/cargoEntry";
	}
	
	@RequestMapping("to-page1")
	public String toPage1( String cid) {
		if(cid!=null){
			this.customerId=cid; 
		 
		 }
	   return "platform/cargoEntry/list1";
	}
	@RequestMapping("region-up")
	public String regionUp() {
	   return "platform/cargoEntry/region-up";
	}
	
	@RequestMapping("address-up")
	public String addressUp() {
	   return "platform/cargoEntry/address-up";
	}
	
	@RequestMapping("goods-up")
	public String goodsUp() {
	   return "platform/cargoEntry/goods-up";
	}
	
	@RequestMapping("cars-up")
	public String carsUp() {
	   
		return "platform/cargoEntry/cars-up";
	}
	
	@RequestMapping("pricePop")
	public String pricePop() {
	   return "platform/cargoEntry/pricePop";
	}
	
	//查询全部
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CCargoEntry> query(Page<CCargoEntry> page) {		
		if(page.getCondition()==null){		
			 page.setCondition(new HashMap<String, Object>());
		}
		 Map<String, Object> cond=page.getCondition();
		 cond.put("customerId", customerId);
		ResultModel<Page<CCargoEntry>> dataPage=iCCargoEntryService.query(page);		
		return this.getLayPage(dataPage);
    }
	
	//主键查询
	@PostMapping("/queryById")
	@ResponseBody
	public  CCargoEntry  queryById(String cargoId) {		
		CCargoEntry  dataPage=iCCargoEntryService.queryById(cargoId);		
		return dataPage;
    }
	
	
	@PostMapping("/queryForm")
	@ResponseBody
	public List<CCargoEntryRel> queryForm(String cargoId,String type) {
	  	 ResultModel<List<CCargoEntryRel>> dataPage=iCCargoEntryService.queryForm(cargoId,type);	
	  	List<CCargoEntryRel> list=dataPage.getData();
	  	 
		return  list;
    }
 
	
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<Boolean> save(
						@RequestParam("dataList") String dataList,
						@RequestParam("goodsInfo") String goodsInfo,
						@RequestParam("carsInfo") String carsInfo,
						@RequestParam("cCargoEntry") String cCargoEntry) { 
		 Map<String, String> map=new HashMap<String, String>();
		   SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 map.put("SysUser", JSON.toJSONString(user));  
		 map.put("dataList", dataList);
		 map.put("goodsInfo", goodsInfo);
		 map.put("carsInfo", carsInfo);
		 map.put("cCargoEntry",cCargoEntry );
		 map.put("customerId",customerId );
		 
		 ResultModel<Boolean> result=iCCargoEntryService.save(map);
	    return result;
	}
	@PostMapping("/saveAll")
	@ResponseBody
	public   ResultModel<CCargoEntry> saveAll(@RequestParam("mainData") String mainData,
										@RequestParam("startForm") String startForm,
										@RequestParam("endForm") String endForm ) { 
		 Map<String, String> map=new HashMap<String, String>();
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 map.put("SysUser", JSON.toJSONString(user));  
		 map.put("mainData",mainData); 
		 map.put("startForm",startForm); 
		 map.put("endForm",endForm); 
		CCargoEntry cargo1=JSONObject.parseObject(map.get("mainData"), CCargoEntry.class);
		if(StringUtil.isBlank(cargo1.getCustomerId())){
			 map.put("customerId",customerId );
		}else{
			 map.put("customerId",cargo1.getCustomerId() );
		}
		
		 map.put("phone",phone );
		 ResultModel<CCargoEntry> result=iCCargoEntryService.saveAll(map);
		 CCargoEntry cargo=result.getData();
	    CCargoEntry cg=JSONObject.parseObject(mainData, CCargoEntry.class);
		 if(StringUtil.isNotBlank(cg.getCargoId())){
			 
			  iSysEbootLogService.save( SysEbootLogController.insert(cargo.getCargoId(), "c_cargo_entry", "update", cargo.toString(),cargo.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cargo.getCargoId(), "c_cargo_entry", "add", cargo.toString(),cargo.getCustomerId())) ;
		  }
	    return result;
	}
 
	 
	@PostMapping("/queryPirce")
	@ResponseBody
	public  ResultModel<List<CCargoInfo>> queryPirce(@RequestParam("start") String start,
									   @RequestParam("end") String end,
									   @RequestParam("carLens") String carLens,
									   @RequestParam("type") String type) {
		 Map<String, String> map=new HashMap<String, String>();
		 map.put("start", start);  
		 map.put("end", end);
		 map.put("carLens", carLens);
		 map.put("type", type);
		 ResultModel<List<CCargoInfo>> dataPage=iCCargoEntryService.queryPirce(map);
	 
	    return dataPage;
	}
	
	@PostMapping("/saveCCargoEntry")
	@ResponseBody
	public  CCargoEntry saveCCargoEntry(@RequestBody CCargoEntry cargoEntry ) {
		 
		  CCargoEntry  dataPage=iCCargoEntryService.saveCCargoEntry(cargoEntry);
	 
	    return dataPage;
	}
	
}
