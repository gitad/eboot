package com.mos.eboot.admin.platform.controller;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCallLogService;
import com.mos.eboot.admin.platform.api.ICCargoInfoService;
import com.mos.eboot.admin.platform.api.ICContactPeoRelService;
import com.mos.eboot.admin.platform.api.ICContactPeoService;
import com.mos.eboot.admin.platform.api.ICustomerXnhService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.admin.platform.api.ISysUserCustMgrService;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.IUserIncomingPhoneService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.CustomerXnh;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.BeanCopyUtilCallBack;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.CustWxInfoVO;
import com.mos.eboot.vo.IMCustomerVO;
import com.mos.eboot.vo.ImMsgConversationListVO;

/**
 * <p>
 * 用户虚拟号表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-22
 */
@Controller
@RequestMapping("/customerXnh")
public class CustomerXnhController extends BaseController { 
 


	@Value("${sys.stock.url}")
	private String sotckUrl;
	
	@Value("${sys.udp.url}")
	private String udpUrl;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private ICustomerXnhService iCustomerService;

	@Autowired
	private IUserIncomingPhoneService iUserIncomingPhoneService;
	
	@Autowired
	private ImApiService imApiService;
	
	@Autowired
	private ISysUserCustMgrService iSysUserCustMgrService;
	
	@Autowired
	private RedisUtil redisUtil;
	
	@Autowired
	ISysEbootLogService iSysEbootLogService;

	@Autowired
	ICCallLogService iCCallLogService;
	
	@Autowired
	ICCargoInfoService cCargoInfoService;
	 
	@Autowired
	ICContactPeoRelService cContactPeoRelService;
	
	@Autowired
	ICContactPeoService cContactPeoService;
	
	List<CustImRole> custRoleList;
	List<Object> conversationList;
	
	List<?> msgList=null;
	IMCustomerVO vo=null;
	CustWx custWx=null;
	private boolean dataType=true;
	private boolean  sortType=false;

    @RequestMapping("/to-page")
    public String toPage() {
        return "platform/customerXnh";
    }
 
 
  
    
    /**
     * 查询系统所有业务员
     * */
    
    @ResponseBody
    @RequestMapping("/query-all-user")
    public List<SysUser> queryAllUser() {
    	List<SysUser> list =sysUserService.getAllUser();   	  
        return list;
    }

    
 
      
    @ResponseBody
    @PostMapping("/query-page")
    public LayPage<CustomerXnh> queryPage(Page<CustomerXnh> page) {   	
    	SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
    	 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		ResultModel<Page<CustomerXnh>> resultModel = iCustomerService.queryPage(page,currentUser.getId());
		return getLayPage(resultModel);
    } 
    
    /**
     * 虚拟号转换-转换为真实号
     * */
    @ResponseBody
    @RequestMapping("/customerZh")
    public ResultModel<Boolean> customerZh(@RequestBody List<CustomerXnh> culist){
    	return iCustomerService.customerZh(culist);
    } 
    /***
     * 查询当前聊天的人员  用于展示弹窗等一系列操作
     * TOID wxid发送人id
     * USERNAME  syswxid获取人id
     * 在当前聊天框中  聊天人和被聊天人与传过来的值是相反的
     * 如果弹窗在开启过程中来了新人员消息 则进行创建
     * */
	@PostMapping("im/getChatPeo")
	@ResponseBody
	public List<Object> getChatPeo(@RequestParam(value = "wxid",required = false) String wxid,
			@RequestParam(value = "syswxid",required = false) String syswxid,
			@RequestParam(value = "content",required = false) String content) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
 		//查询与当前人的聊天记录
	 	List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
		ImMsgConversationListVO conversationListVO = null;
		for (int k=0;k<list.size();k++) {
			ImMsgConversationListVO coV = (ImMsgConversationListVO) list.get(k);
			List<CustWxInfoVO> custList= coV.getCustWxvoList();
			//String wxid=cust.getWxId();//当前聊天人id 对方
			//String sysWxid=cust.getSysWxId();//自己id
			if(custList!=null){				 
				for(int i=0;i<custList.size();i++){
						CustWxInfoVO cust=custList.get(i);				 
						if(wxid.equals(cust.getWxId())&&syswxid.equals(cust.getSysWxId())){//如果发消息能找到这个人 则说明这个人存在直接跳出反回
							 conversationListVO=coV;
							coV.setAddTimestamp(System.currentTimeMillis());
							coV.setLastMsg(content);
							coV.setWxNoReadNum(coV.getWxNoReadNum()+1);
							// redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), k, coV);//更新聊天消息
							break;				
						}
				}
			}
		
			
		}  
		if(conversationListVO==null){			
			list= buildChatData(wxid,syswxid);	
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				List<CustWxInfoVO> custList= coV.getCustWxvoList();
				//String wxid=cust.getWxId();//当前聊天人id 对方
				//String sysWxid=cust.getSysWxId();//自己id
				if(custList!=null){
					for(CustWxInfoVO   cust:custList){
						if(wxid.equals(cust.getWxId())&&syswxid.equals(cust.getSysWxId())){//如果发消息能找到这个人 则说明这个人存在直接跳出反回
							conversationListVO=coV;
							break;				
						}
					}
				}
				
				
			} 
			
		}
		sortList(list);//数据排序
		senMessToPHZ(conversationListVO,wxid,syswxid);	
    	return list;
    }
	public void sortList (List<Object> list){
		if(this.sortType==false){
			this.sortType=true;
			if(list.size()>100){
				list=list.subList(0, 100);
			}
			
			SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
			 if(currentUser==null){
				 currentUser= WeChatController.sysUser;
			  }
			Collections.sort(list,new Comparator<Object>() {//更具时间进行排序
	
				@Override
				public int compare(Object o1, Object o2) {
					// TODO Auto-generated method stub
					ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
					ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
					return vo2.getAddTimestamp().compareTo(vo1.getAddTimestamp());
				}
			});
			
			Collections.sort(list,new Comparator<Object>() {//置顶排序
	
				@Override
				public int compare(Object o1, Object o2) {
					// TODO Auto-generated method stub
					ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
					ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
					int i=0;
					 boolean st1=vo1.isIfTop();
					 boolean st2=vo2.isIfTop();
					 if(st1==true&&st2==false){
						 i=-1;
					 }
					 if(st1==false&&st2==true){
						 i=1;
					 }
					 
					return i;
				}
			});
			
		
			redisUtil.del(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId());
			redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), list, -1);	
			this.sortType=false;
		}
	}
	 
	public void senMessToPHZ(ImMsgConversationListVO conversationListVO,String wxid,String syswxid){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 try {
			 
	//      1， 建立一个 包  ，或者socket ; 数据包
	      DatagramSocket socket = new DatagramSocket();
	
	//      2,建个包；
	     // String msg=currentUser.getUsername();
	      
	     //从备注取手机号 
	      String phone=conversationListVO.getCustWxvoList().get(0).getWxRemark();
	      String phoneNum=StringUtil.str_Phone(phone);
	      //接收人微信ID：我方微信ID  {"action":"newmsg","user":"_登录名","tel":"_电话","fromwxid":"_发起人微信ID","towxid":"_接收人微信ID"}
	     String msg="{'action':'newmsg','user':"+currentUser.getUsername()+","
	     		+ "'tel':"+phoneNum==null?"":phoneNum+",'fromwxid':'"+wxid+"','towxid':'"+syswxid+"'}";
	
	//      发送的人
	      InetAddress localhost = InetAddress.getByName(udpUrl);
	      
	      int prot =30020;
	
	      //      数据 ，数据的长度 起始，要发送的人 ，端口号；
	      DatagramPacket packet = new DatagramPacket(msg.getBytes(StandardCharsets.UTF_8),0,msg.getBytes(StandardCharsets.UTF_8).length,localhost,prot);     
	    	  //      3，发送包；
			socket.send(packet);
			
			// 4，关闭流
			  socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/***
	 * 对人员分类进行更新
	 * */
	@PostMapping("im/updateChatType")
	@ResponseBody
	public List<Object> updateChatType(@RequestParam(name = "sessionId", required = false) String sessionId){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		for(int i=0;i<list.size();i++){		
			ImMsgConversationListVO imListVo=(ImMsgConversationListVO)list.get(i);
			if(imListVo.getSessionId().equals(sessionId)){
				imListVo.setShow(true);
			}else{
				imListVo.setShow(false);
			}
			 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, imListVo);
		}	
	 
		return list;
	}
	
	
	/***
	 * 对微信号状态进行更改
	 * */
	@PostMapping("im/updateWxonlineStat")
	@ResponseBody
	public ImMsgConversationListVO updateWxonlineStat(@RequestParam(name = "sessionId", required = false) String sessionId){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 ImMsgConversationListVO imListVo=null;
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		for(int i=0;i<list.size();i++){		
			 imListVo=(ImMsgConversationListVO)list.get(i);
			 List<CustWxInfoVO> custList= imListVo.getCustWxvoList();
			 if(imListVo.getSessionId().equals(sessionId)){
				 for(CustWxInfoVO cu:custList){
					  SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(cu.getSysWxId());
					 cu.setSysUserWxInfo(sysUserWxInfo);
				 }
				 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, imListVo);
			 }
		}	
	 
		return imListVo;
	}
	/***
	 * 人员置顶/取消
	 * */
	@PostMapping("im/updateTop")
	@ResponseBody
	public List<Object> updateTop(@RequestParam(name = "sessionId", required = false) String sessionId,
			@RequestParam(name = "ifTop", required = false) Boolean ifTop){
		//type  0置顶 1取消置顶
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);	
	 
		for(int i=0;i<list.size();i++ ){	
			ImMsgConversationListVO imListVo=(ImMsgConversationListVO)list.get(i);
		
			if(imListVo.getSessionId().equals(sessionId)){
				imListVo.setIfTop(ifTop);	
				redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, imListVo);
			}
			 
		}	
 
		Collections.sort(list,new Comparator<Object>() {

			@Override
			public int compare(Object o1, Object o2) {
				// TODO Auto-generated method stub
				ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
				ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
				int i=0;
				 boolean st1=vo1.isIfTop();
				 boolean st2=vo2.isIfTop();
				 if(st1==true&&st2==false){
					 i=-1;
				 }
				 if(st1==false&&st2==true){
					 i=1;
				 }
				 
				return i;
			}
		});
	 
		return list;
	}
	
	
	/***
	 * 对人员分类进行更新
	 * */
	@PostMapping("im/update-custType")
	@ResponseBody
	public ImMsgConversationListVO updataType(@RequestParam(name = "custType", required = false) String custType,
			@RequestParam(name = "sessionId", required = false) String sessionId){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<CustImRole> custRoleList=null;
		
		//先从redis中查询所有当前的聊天信息
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		ImMsgConversationListVO imMsgConversationListVO=null;
		for(int i=0;i<list.size(); i++){		
			ImMsgConversationListVO imListVo=(ImMsgConversationListVO)list.get(i);
			//找到该条数据   1.更新类型  2.更新货主信息
			if(imListVo.getSessionId().equals(sessionId)){
				imMsgConversationListVO=imListVo;
				imListVo.setCustType(custType);						
				//更新t_customer表人员的类型
				ResultModel<CustomerXnh> cust=iCustomerService.getCustomerById(imListVo.getcId().toString());
				cust.getData().setCustType(custType);
				iCustomerService.updateCustomerById(cust.getData());	
				
				//去查询人员的相关按钮权限
				 custRoleList= iCustomerService.getCustImRole(currentUser.getId(), custType).getData();
				  
				//对redis的数据重新赋值
				imListVo.setCustRoleList(custRoleList);
				 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, imListVo);
			}
		}
		 
		
		 
	 
		return imMsgConversationListVO;
	}
	

	
	/**
	 * 修改人员备注和电话
	 * */
	@PostMapping(value = "/modify/wx/remark")
	@ResponseBody
	public void modifyRemark(@RequestParam(name = "sessionId") String sessionId, @RequestParam(name = "value") String value,@RequestParam(name = "states") String states) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		
		ImMsgConversationListVO conversationListVO=null;
		for (int i=0;i<list.size(); i++) {
			conversationListVO = (ImMsgConversationListVO) list.get(i);
			if (conversationListVO.getSessionId().equals(sessionId)) {
				if(states.equals("0")){//修改名字
					conversationListVO.setCustName(value);
					if(conversationListVO.getcId()!=null){
						iCustomerService.modifyRemarkPhone(conversationListVO.getcId().toString(),value,"0");
					}
				}
				if(states.equals("1")){//修改电话
					//查询一下电话，要是电话不存在则插入
					 List<CContactPeo> peoList=cContactPeoService.query(conversationListVO.getcId());
					 if(peoList.size()==0){
						 CustomerXnh  customer= iCustomerService.getCustomerById(conversationListVO.getcId()).getData();
						 if(customer!=null){
							 customer.setValidPhone(value); 
							 iCustomerService.createPeoRel(customer);//创建联系人及备注
						 }						
					 }else{
						 CContactPeoRel  peoRel = cContactPeoRelService.queryPhone(value).getData();
						 if(peoRel==null){
							 peoRel=new CContactPeoRel();
							 peoRel.setContactId(peoList.get(0).getContactId());
							 peoRel.setCustomerId(conversationListVO.getcId());
							 peoRel.setContactId(value);
							 peoRel.setUseFlag(1);
							 peoRel.setPhoneDefault("默认");//是否默认手机号 
							 peoRel.setContactState("正常");//状态0分离1正常
							 peoRel.setWxType("企业号");	
							 
							 cContactPeoRelService.save(peoRel); 
						 }
						  
						 
					 }
					
					conversationListVO.setCustPhone(value);				 
					List<CustWxInfoVO> custList=conversationListVO.getCustWxvoList();
					if(custList!=null&&custList.size()>0){
						for(CustWxInfoVO cu:custList){
							if(StringUtil.str_Phone(cu.getWxRemark())==null){
								//代表新添加得账号
								  iSysEbootLogService.save( SysEbootLogController.insert(cu.getSysWxId(), "t_cust_wx", "add", cu.toString(),cu.getUserId())) ;
							}else{
								  iSysEbootLogService.save( SysEbootLogController.insert(cu.getSysWxId(), "t_cust_wx", "update", cu.toString(),cu.getUserId())) ;
							}
							 cu.setWxRemark(value);
							  //根据主键  更新表里微信备注
							 imApiService.modifyRemark(cu.getWxId(),cu.getSysWxId(),value);		
							 //同时修改用户表里得vaild_phone
							 CustomerXnh csut= iCustomerService.getCustomerById(cu.getcId().toString()).getData();
							 csut.setValidPhone(value);
							 iCustomerService.updateCustomerById(csut) ;
						}
						
					}
					 
				}
				 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, conversationListVO);
				break;
			}
		}
		
  
	 
	}
	 	
	/**
	 * 电话未接  更新来电信息
	 * 
	 * @param phone
	 * @param model
	 * @return
	 */
	@GetMapping("/update/phone")
	public void updateByPhone(@RequestParam(name = "phone", required = false) String phone) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 List<Object> redisList= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);//构建人员聊天关系
		 
		 for (int i=0;i<redisList.size(); i++) {
			   ImMsgConversationListVO	re = (ImMsgConversationListVO) redisList.get(i);
			   if(re.getCustPhone()!=null){
				   if(re.getCustPhone().equals(phone)){
					   re.setPhoneStatus("8");
					   re.setPhoneNoReadNum(re.getPhoneNoReadNum()+1);
					   redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
				   }
			 }
		 }
	}
	/**
	 * 来电弹屏，根据手机号，查询用户信息
	 * 
	 * @param phone
	 * @param model 
	 * @return
	 */
	@GetMapping("/by/phone")
	public String getCustomerByPhone(@RequestParam(name = "phone", required = false) String phone,@RequestParam(name = "cid", required = false) String cid) {		
				SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
				 if(currentUser==null){
					 currentUser= WeChatController.sysUser;
				  }
				 List<Object> redisList= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);//构建人员聊天关系
				 Boolean type=false;
				//存在cid代表去电  cid为空代表来电
				 if(StringUtil.isNotBlank(cid)){
					 for (int i=0;i<redisList.size(); i++) {
						   ImMsgConversationListVO	re = (ImMsgConversationListVO) redisList.get(i);				 
							  if(re.getcId().equals(cid)){
								  type=true;
								  re.setShow(true);
								  re.setPhoneStatus("0");
								  re.setPhoneNoReadNum(0);
								   re.setPhonetype("2");
								  re.setCustPhone(phone);
								  re.setAddTimestamp(System.currentTimeMillis());
								  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
								 
							  } else{
								  re.setShow(false);
								  re.setPhonetype("0");							  
								  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
							  } 
		 	
					  }
				 }else{
					 for (int i=0;i<redisList.size(); i++) {
						   ImMsgConversationListVO	re = (ImMsgConversationListVO) redisList.get(i);				 
							  if(re.getCustPhone()!=null&&re.getCustPhone().equals(phone)){
								  type=true;
								  re.setShow(true);								  
								  re.setPhonetype("1");
								  re.setCustPhone(phone);
								  re.setAddTimestamp(System.currentTimeMillis());
								  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
								 
							  } else{
								  re.setPhonetype("0");
								  re.setShow(false);	
								  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
							  } 
			
					  } 
				 }
				 if(type==false){			
					    IMCustomerVO vo = new IMCustomerVO();
						List<CustWxInfoVO> custWxvoList = new ArrayList<CustWxInfoVO>();
						vo.setCustPhone(phone);
						CustomerXnh  customer=null;
						if(StringUtil.isNotBlank(cid)){//如果cid不为空 用cid去查询
							customer=iCustomerService.getCustomerById(cid).getData();
						}else{
							customer=iCustomerService.getCustomerByPhone(StringUtil.str_Phone(phone)).getData();//来电后先根据电话号码进行查询一下
							if(customer==null){
								customer=iCustomerService.getCustomerByvailOrOherPhone(StringUtil.str_Phone(phone)).getData();
							}
						}
							
						/**
						 * 给别人打电话时，号码都存在，如果打进来的电话不存在号码，标注为虚拟号
						 * */
						Integer  mesType=2;
						if(null != customer) {
							//查询聊天微信 先用cid去查  当第一次打电话得时候 可能cid会为空
							ResultModel<List<CustWx>> resultCustWxList= iCustomerService.getCustomerWxByCustId(customer.getId().toString(),"");
							List<CustWx> custWxList = resultCustWxList.getData();
							if(custWxList==null||custWxList.size()==0){//用cid查不到  改用电话去查
								 custWxList=iCustomerService.getCustListWxByPhone(phone).getData() ;
								 custWxList=iCustomerService.updateCustListCid(customer.getId(),custWxList).getData();
							}
							custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
								@Override
								public void callBack(CustWx s, CustWxInfoVO t) {
									// TODO Auto-generated method stub
									SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
									t.setSysUserWxInfo(sysUserWxInfo);
								}
							});
							
							vo.setCompany(customer.getAddressInfo());
						}else {
							customer=new CustomerXnh();
							customer.setCustName("未备注");
							customer.setCustType("虚拟号");
							customer.setId("-1");
							mesType=3;
						}		
						List<CustImRole> custRoleList = iCustomerService.getCustImRole(currentUser.getId(), customer.getCustType()).getData();
						
						redisList = buildConversationByCustomerNotnull(currentUser.getId(), phone, customer,custRoleList,mesType);
						 for (int i=0;i<redisList.size(); i++) {
							   ImMsgConversationListVO	re = (ImMsgConversationListVO) redisList.get(i);
							   if(re.getCustPhone()!=null){
								   if(re.getCustPhone().equals(phone)){							 
									  re.setShow(true);
									  if(mesType==2){
										  re.setPhonetype("2");
									  }
									  if(mesType==3){
										  re.setPhonetype("1");
									  }
									  re.setAddTimestamp(System.currentTimeMillis());
									  redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);							 
								  } else{
									  re.setShow(false);
									  re.setPhoneStatus("0");
									   redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, re);
								  } 
							   }
								
						  }
				 }
					 sortList(redisList);	//数据排序	
		     return "im/im_window";
	}
	
	/**
	 * 虚拟号保存电话
	 * */
	@PostMapping(value = "/modify/wx/saveRelPhone")
	@ResponseBody
	public List<Object> saveRelPhone(@RequestParam(name = "sessionId") String sessionId, @RequestParam(name = "value") String value) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		
		ImMsgConversationListVO conversationListVO=null;
		for (int i=0;i< list.size();i++) {
			conversationListVO = (ImMsgConversationListVO) list.get(i);
			if (conversationListVO.getSessionId().equals(sessionId)) {
				CustomerXnh  customer=null;
				CContactPeoRel  custPhone=iCustomerService.getCustByRelPhone(value).getData();
				
				 if(custPhone!=null){
					 ResultModel<CustomerXnh> customerMode = iCustomerService.getCustomerById(custPhone.getCustomerId()+"");
					 if(customerMode!=null){
						 customer=customerMode.getData();
					 }
					
				 }else{
					 ResultModel<CustomerXnh> customerMd=iCustomerService.getCustomerByvailOrOherPhone(value);
					 if(customerMd!=null){
						 customer=customerMd.getData();
					 }
				 }
				if(null != customer) {//搜不到用户
					
					 List<CustImRole> custRoleList = iCustomerService.getCustImRole(currentUser.getId(), customer.getCustType()).getData();
				     conversationListVO = genConversationVO(value, customer, custRoleList,0);
				     conversationListVO.setShow(true);
					 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, conversationListVO);
				}else{//创建新用户
					
					IMCustomerVO customerVO =new IMCustomerVO();
					if(custWx!=null){//微信
						customerVO.setCustPhone(custWx.getWxRemark());	
					}else{//电话
						customerVO.setCustPhone(value);		
					}
				
					customerVO.setCustName("未备注-虚拟号");
					customerVO.setCustType("其他");
					customerVO.setValidPhone(value);
					CustomerXnh  customerModel = iCustomerService.saveSampleCustomer(customerVO).getData();
					List<CustImRole> custRoleList = iCustomerService.getCustImRole(currentUser.getId(), customerModel.getCustType()).getData();
				     conversationListVO = genConversationVO(value, customerModel, custRoleList,0);
				     conversationListVO.setShow(true);
					 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, conversationListVO);
				}
			
				break;
			}
		}
		
  
		return redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
	}
	
	 
    /**
	 * 微信消息弹屏
	 * 
	 * @param phone
	 * @param model
	 * @return
	 */
	@GetMapping("im/open")
	public String defaultImWindow(Model model) {	
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		
		model.addAttribute("conversation",list);
		model.addAttribute("userName", currentUser.getId());
		model.addAttribute("sotckUrl",sotckUrl);	
		
    	return "im/im_window";
    }
	@GetMapping("im/open1")
	public String defaultImWindow1(Model model) {	
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		
		model.addAttribute("conversation",list);
		model.addAttribute("userName", currentUser.getId());
		model.addAttribute("sotckUrl",sotckUrl);			
    	return "im/im_window1";
    }
	@ResponseBody
	@GetMapping("/get/chat/data")
	public List<Object>  getChatData(){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);	
		
		return list;
	}
	@ResponseBody
	@GetMapping("/get/chat/data/filter")
	public List<Object>  getChatDataFilter(@RequestParam(name = "chatStatus", required = false) String chatStatus,
			@RequestParam(name = "wxStatus", required = false) String wxStatus,
			@RequestParam(name = "workStatus", required = false) String workStatus){
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		
		Map<String,Object> map=new HashMap<String, Object>();
		//@RequestParam Map<String, String> map
		String[] chats= (String[]) JSONObject.parseObject(chatStatus, String[].class);
		String[] wxs= (String[]) JSONObject.parseObject(wxStatus, String[].class);
		String[] works= (String[]) JSONObject.parseObject(workStatus, String[].class);
		if(chats.length>0){
			for(int i=0;i<chats.length;i++){
				if(chats[i].equals("0")){//来电未接
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 if(vo.isUnPhone()==true){
							 if(map.get(vo.getcId())==null){
								 map.put(vo.getcId(), vo);
							 }
							 
						 }
					 }
				}
				if(chats[i].equals("1")){//消息未回
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 if(vo.isWxWarm()==true){
							 if(map.get(vo.getcId())==null){
								 map.put(vo.getcId(), vo);
							 }
						 }
					 } 
				}
				if(chats[i].equals("2")){//系统提示   
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 if(vo.isTrans()==true){
							 if(map.get(vo.getcId())==null){
								 map.put(vo.getcId(), vo);
							 }
						 }
					 } 
				}
			}
		}
		if(wxs.length>0){
			for(int i=0;i<wxs.length;i++){
				if(wxs[i].equals("0")){//未添加
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 	if(StringUtils.isBlank(vo.getWxStatus())){//为null或者空
						 		if(map.get(vo.getcId())==null){
						 			 map.put(vo.getcId(), vo);	
						 		}	 
						 	 
						 }
					 } 
				}
				if(wxs[i].equals("1")){//正常
					for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 	if(vo.getWxStatus().equals("正常")){ 
						 		if(map.get(vo.getcId())==null){
						 			 map.put(vo.getcId(), vo);								
						 		}
						 	}
					 }  
				}
				if(wxs[i].equals("2")){//删除 
					for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);						
						 	if(vo.getWxStatus().equals("被删除")){ 
						 		if(map.get(vo.getcId())==null){
						 			 map.put(vo.getcId(), vo);	
						 		}		 
							 }					
					 }  
				}
			}
		}
		/**if(wxs.length>0){
			for(int i=0;i<wxs.length;i++){
				if(wxs[i].equals("0")){//未添加
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
							 if(map.get(vo.getcId())==null){
								 Customer cust=iCustomerService.getCustomerById(vo.getcId()).getData();
								 if(StringUtils.isBlank(cust.getWxStatus())){
									 map.put(vo.getcId(), vo);
								 }
								
							
						 }
					 } 
				}
				if(wxs[i].equals("1")){//正常
					for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						
							 if(map.get(vo.getcId())==null){
								 Customer cust=iCustomerService.getCustomerById(vo.getcId()).getData();
								 if(cust.getWxStatus().equals("正常")){
									 map.put(vo.getcId(), vo);
								 }
								
							 }
						
					 }  
				}
				if(wxs[i].equals("2")){//删除 
					for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						
							 if(map.get(vo.getcId())==null){
								 Customer cust=iCustomerService.getCustomerById(vo.getcId()).getData();
								 if(cust.getWxStatus().equals("被删除")){
									 map.put(vo.getcId(), vo);
								 }
								
							 }
						
					 }  
				}
			}
		}**/
		if(works.length>0){
			for(int i=0;i<works.length;i++){
				if(works[i].equals("0")){//未处理
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 if(vo.getIfDealWith()==false){
							 if(map.get(vo.getcId())==null){
								 map.put(vo.getcId(), vo);
							 }
							 
						 }
					 }
				}
				if(works[i].equals("1")){//已处理
					 for(int k=0;k<list.size();k++){
						 ImMsgConversationListVO vo=(ImMsgConversationListVO) list.get(k);
						 if(vo.getIfDealWith()==true){
							 if(map.get(vo.getcId())==null){
								 map.put(vo.getcId(), vo);
							 }
							 
						 }
					 }
				}

			}
			 
		}
 
		List<Object> newList=new ArrayList<Object>();   		
    	for(Object key:map.keySet()){//keySet获取map集合key的集合  然后在遍历key即可
    	   newList.add(map.get(key));
    	}
    	if( works.length==0&&chats.length==0&&wxs.length==0){
    		newList=list;
    	}
		return newList;
	}
	
	 /**
		 * 根据客户ID，查询用户信息
		 * 
		 * @param phone
		 * @param model
		 * @return
		 */
		@GetMapping("/by/im/id")
		@ResponseBody
		public Boolean getCustomerById(@RequestParam(name = "id", required = false) String id) {
				SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
				 if(currentUser==null){
					 currentUser= WeChatController.sysUser;
				  }
				ResultModel<List<CustPhone>> resultModel= iCustomerService.getCustPhoneByCustId(id);
				
				List<CustPhone> custPhonList =resultModel.getData();
				
				IMCustomerVO vo = new IMCustomerVO();
 			 
				ResultModel<CustomerXnh> customerModel= iCustomerService.getCustomerById(id);
				
				CustomerXnh  customer = customerModel.getData();
				
				BeanCopyUtil.copyProperties(customer, vo);

				vo.setCompany(customer.getAddressInfo());
				vo.setCustPhone(customer.getValidPhone());
				List<CustImRole> custRoleList = iCustomerService.getCustImRole(currentUser.getId(), customer.getCustType()).getData();
				
//				List<SysUserWxInfo> allWxList = imApiService.getAllWxLogin();
//				model.addAttribute("allWxList", allWxList);
				
				List<Object> list = buildConversationByCustomerNotnull(currentUser.getId(), vo.getCustPhone(), customer,custRoleList,0);
			 
				return true;
		}

		
	@GetMapping("im/closeCurrSession")
	@ResponseBody
	public List<Object> closeCurrSession(@RequestParam(name = "sid", required = false) String sid,Model model) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		
		for (Object object : list) {
			ImMsgConversationListVO conversationListVO = (ImMsgConversationListVO) object;
			if(conversationListVO.getSessionId().equals(sid)) {
			//	System.out.println(conversationListVO.getSessionId());
				redisUtil.lRemove(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 1, conversationListVO);
				break;
			}
		}
		return redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
	}

	@GetMapping("im/closePeo")
	@ResponseBody
	public List<Object> closePeo(@RequestParam(name = "sid", required = false) String sid,
			@RequestParam(name = "peoId", required = false) String peoId) {
		
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		 List<JSONObject> newCuList=new ArrayList<JSONObject>();	 	 
			for (int i=0;i<list.size();i++) {	
			ImMsgConversationListVO conversationListVO = (ImMsgConversationListVO) list.get(i);
			if(conversationListVO.getSessionId().equals(sid)) {				
				 List<JSONObject> cuList=conversationListVO.getCustJsonList();
				 for(JSONObject cu:cuList){
					  if(!cu.getString("id").equals(peoId)){
						  newCuList.add(cu);						 
					 }					 
				 }
				 conversationListVO.setCustJsonList(newCuList);
				 redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(),i, conversationListVO);
				 
				break;
			}
		 
		}
	 
			
			//redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), list, -1);
	 
	
		return list;
	}
    /**
	 * 微信消息弹屏
	 * 
	 * @param phone
	 * @param model
	 * @return
	 */
	@GetMapping("/by/im/wxid")
	public String getCustomerByWxId(@RequestParam(name = "wxid", required = false) String wxid,
			@RequestParam(name = "syswxid") String syswxid ,Model model) {	
		
		//默认查询走系统登陆
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		List<Object> list=buildChatData(wxid,syswxid);
		
		model.addAttribute("role",custRoleList);
		model.addAttribute("data", vo);
		model.addAttribute("conversation",conversationList);
		model.addAttribute("currentMsgList", msgList);
		//model.addAttribute("custWx",custWx);
		model.addAttribute("userName", currentUser.getId());
		model.addAttribute("sotckUrl",sotckUrl);
//		if(custWxModel.getData() == null){
//			return "im/im_virtual_phone_view";
//		}
		Collections.sort(list,new Comparator<Object>() {//更具时间进行排序

			@Override
			public int compare(Object o1, Object o2) {
				// TODO Auto-generated method stub
				ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
				ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
				return vo2.getAddTimestamp().compareTo(vo1.getAddTimestamp());
			}
		});
		
	 
		return "im/im_window";
	}
   
	
	
    /**
	 *  构建聊天消息
	 * 
	 * @param phone
	 * @param model
	 * @return
	 */
	public List<Object>  buildChatData(@RequestParam(name = "wxid", required = false) String wxid,
			@RequestParam(name = "syswxid") String syswxid) {		 
		//默认查询走系统登陆
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 vo = new IMCustomerVO();		 
		/**
		 * 根据微信ID，查询微信用户
		 */
		ResultModel<CustWx> custWxModel= iCustomerService.getCustomerWxByWxIdAndSysWxId(wxid,syswxid);			
		custWx = custWxModel.getData();	
		/**
		 * 根据客户ID，查询客户信息
		 */
		ResultModel<CustomerXnh> customerModel = null;
		CustomerXnh  customer = null;	
		if(StringUtil.isBlank(custWx.getcId()) ) {			 
			//判断微信id，是否存在已有用户信息。
			//custWxList=iCustomerService.getCustomerWxListByWxId(wxid,syswxid).getData();	 
			//对于已存在微信与用户关联关系，不用新增用户信息，不存在，则新增。
			IMCustomerVO customerVO =new IMCustomerVO();
			 
			if(StringUtils.isNotBlank(custWx.getWxRemark())){
				customerVO.setCustName(custWx.getWxRemark());//x先将备注加上  防止数据查不出来无法覆盖
			}else{
				customerVO.setCustName(custWx.getWxNickname());//x先将备注加上  防止数据查不出来无法覆盖
			}
		
			customerVO.setCustWxId(wxid);
			customerVO.setSysWxId(syswxid);
			//customerVO.setWxStatus("1");
			customerVO.setIsDel("0");
			customerVO.setIsUpload("1");
			customerVO.setIfNormal("0");
			customerVO.setCustAdminNickname(currentUser.getUsername());
			customerVO.setCustAdminUserId(currentUser.getId());
			//从备注中匹配出手机号
			customerVO.setCustPhone(StringUtil.str_Phone(custWx.getWxRemark()));	
			customerVO.setValidPhone(StringUtil.str_Phone(custWx.getWxRemark()));
			customerVO.setCustType("其他");
			customerModel = iCustomerService.saveSampleCustomer(customerVO);
			customer = customerModel.getData();
			custWxModel = iCustomerService.saveCustWx(custWx);//更新cid
			iCustomerService.createPeoRel(customer);//创建联系人及备注
		}else {
		 
			customerModel= iCustomerService.getCustomerById(custWxModel.getData().getcId()+"");
			if(customerModel==null){//先根据cid去查询这个人 如果查不到存在两种情况  1是这个人不存在  2是以前建立过关联关系 但是后来关系丢失了 需要进行二次处理
				//首先先根据电话去查询一下
				customerModel=iCustomerService.getCustomerByPhone(StringUtil.str_Phone(custWxModel.getData().getWxRemark()));
			}
			customer = customerModel.getData();
		}
	 
		
		/**
		 * 根据微信ID，查询历史消息
		 */
		  msgList = new ArrayList();
		 Map<String,Object> map =(Map<String, Object>) imApiService.himsgList(wxid, syswxid);

		 msgList = (List) map.get("varList");
	
		BeanCopyUtil.copyProperties(customer, vo);
		vo.setCompany(customer.getAddressInfo());
		if(StringUtil.isBlank(customer.getValidPhone())){
			vo.setCustPhone("号码X");
		}else{
			vo.setCustPhone(customer.getValidPhone());
		}
	
		/**
		 * 查询权限
		 */
	 
		 custRoleList= iCustomerService.getCustImRole(currentUser.getId(), customer.getCustType()).getData();
		 conversationList = buildConversationByCustomerNotnull(currentUser.getId(), vo.getCustPhone(), customer,custRoleList,0);	
		
		 /**
		   构建微信人员聊天关系并实时进行更行
		 * */
		 
			 Map<Object, Object> chatPeoMap=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
			 if(chatPeoMap.get(wxid+"-"+syswxid)==null){
				String[] str=new String[2];
				str[0]=customer.getId();
				str[1]=customer.getCustAdminUserId();
				chatPeoMap.put(wxid+"-"+syswxid, JSON.toJSONString(str));
				redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,chatPeoMap,-1);
			 }
				
		 
			
		 return conversationList;
	}		
	
	
	
	 
	/**
	 * 构建聊天弹框左侧会话消息列表
	 * @param userId
	 * @param custPhone
	 * @param customer
	 * @return
	 */
	private List<Object> buildConversationByCustomerNotnull(String userId, String custPhone, CustomerXnh  customer,
			List<CustImRole> custRoleList,Integer  mesType) {
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
	    List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + userId, 0, -1);//构建人员聊天关系
		if (null == list || list.size() == 0) {			
			ImMsgConversationListVO conversationListVO = genConversationVO(custPhone, customer, custRoleList,mesType);			 
			list.add(conversationListVO);		 
			
		}else {
			boolean isAdd=true;
			for (int i=0;i<list.size(); i++) {
				ImMsgConversationListVO conversationListVO = (ImMsgConversationListVO) list.get(i);
				if(conversationListVO.getcId() != null) {
					if(!conversationListVO.getcId().equals("-1")&&conversationListVO.getcId().equals(customer.getId()) ) {	
							//更新一下微信聊天用户
							ResultModel<List<CustWx>> result= iCustomerService.getCustListWxByCid(customer.getId().toString());
							List<CustWx>  custWxList= result.getData();
							if(custWxList.size()!=conversationListVO.getCustWxvoList().size()){//长度不一样则更新一下聊天李彪
									/**
									 * 构建左侧弹框列表
									 */
									List<CustWxInfoVO> custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
										@Override
										public void callBack(CustWx s, CustWxInfoVO t) {
											// TODO Auto-generated method stub
											
											SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
											t.setSysUserWxInfo(sysUserWxInfo);
										}
									});
									conversationListVO.setCustWxvoList(custWxvoList);
							}
							
							conversationListVO.setAddTimestamp(System.currentTimeMillis());
							
					 
						isAdd=false;
						//list.add(0, list.remove(1));
						break;
					}
				}
			}
			if(isAdd) {
				ImMsgConversationListVO conversationListVO = genConversationVO(custPhone, customer, custRoleList,mesType);
				
				//redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + userId, conversationListVO, -1);
				list.add(conversationListVO);
			}
		}
        sortList(list);//排序
		return list;
	}

	/**
	 * 构建VO
	 * @param custPhone
	 * @param customer
	 * @param custRoleList
	 * @param custWxList
	 * @return
	 */
	private ImMsgConversationListVO genConversationVO(String custPhone, CustomerXnh  customer,
			List<CustImRole> custRoleList,Integer  mesType) {
		ImMsgConversationListVO conversationListVO =new ImMsgConversationListVO();
		conversationListVO.setCustName(customer.getCustName());
		conversationListVO.setSessionId(UuidUtil.get32UUID());
		conversationListVO.setCustType(customer.getCustType());
		conversationListVO.setCustPhone(custPhone);//拨打电话设置电话号码
		conversationListVO.setcId(customer.getId());
		conversationListVO.setCustRoleList(custRoleList);
		conversationListVO.setCompany(customer.getAddressInfo());
		conversationListVO.setAddress(customer.getAddressInfo());
		conversationListVO.setHasWxRole("0");
		conversationListVO.setPhoneNoReadNum(0);
		conversationListVO.setWxNoReadNum(1);
		conversationListVO.setMesType(mesType); 
		conversationListVO.setShow(false);
		conversationListVO.setIfTop(false);
		conversationListVO.setWxWarm(false);
		conversationListVO.setWxStatus(customer.getWxStatus());
		conversationListVO.setCjNum(0);//成交
		conversationListVO.setFhNum(0);//发货
		conversationListVO.setXyNum(new Long(0));//信用
		if(mesType==0){//消息类型   0微信  /1群/2拨打/3来电
			conversationListVO.setIfDealWith(false);//处理状态
		}else{
			conversationListVO.setIfDealWith(true);//虚拟号 群聊 来电 默认已处理状态
		}
	
		if(StringUtil.isNotBlank(customer.getCustAdminUserIdTem())){
			conversationListVO.setTrans(true);//是否转交
		}else{
			conversationListVO.setTrans(false);//是否转交	
		}
	
		ResultModel<List<CustWx>> result= iCustomerService.getCustListWxByCid(customer.getId().toString());
		List<CustWx>  custWxList= result.getData();
				 
		
		if(custWxList != null && custWxList.size() >0) {
			conversationListVO.setHasWxRole("1");
			if(StringUtil.isNotBlank(custWxList.get(0).getWxRemark())){
				conversationListVO.setCustPhone(custWxList.get(0).getWxRemark());//聊天设置此次聊天的号码
			}
			 
			conversationListVO.settCustWxId(custWxList.get(0).getId().toString());
			/**
			 * 构建左侧弹框列表
			 */
			List<CustWxInfoVO> custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
				@Override
				public void callBack(CustWx s, CustWxInfoVO t) {
					// TODO Auto-generated method stub
					
					SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
					t.setSysUserWxInfo(sysUserWxInfo);
				}
			});
			String lastMsg= imApiService.getLastHisMsgList(custWxvoList.get(0).getWxId(), custWxvoList.get(0).getSysWxId());
			conversationListVO.setLastMsg(lastMsg);

			conversationListVO.setCustWxvoList(custWxvoList);
		}
		
		
		
		conversationListVO.setAddTimestamp(System.currentTimeMillis());
		
		conversationListVO.setAddTime(LocalTime.now().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		return conversationListVO;
	}
	

	@PostMapping("/save")
	@ResponseBody
	public ResultModel<String> saveCustomer(IMCustomerVO customerVO) {
		ResultModel<CustomerXnh> rs = iCustomerService.saveCustomer(customerVO);

		if (rs.getCode().equals(ResultStatus.FAIL.getCode())) {
			return ResultModel.defaultSuccess(rs.getMsg());
		}

		return ResultModel.defaultSuccess("");
	}
    
}
