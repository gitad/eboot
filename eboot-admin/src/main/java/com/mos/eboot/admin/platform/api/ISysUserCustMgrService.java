package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.SysuserCustomerMgr;
import com.mos.eboot.tools.result.ResultModel;

@FeignClient("boot-service")
public interface ISysUserCustMgrService {

	@PostMapping("api/sysuser/cust/add")
	public ResultModel<String> addUserCust(@RequestParam("cids") String cids,@RequestParam("userId") String userId);
	
	@GetMapping("api/sysuser/cust/query-by-userid")
	public ResultModel<List<SysuserCustomerMgr>> queryCustByUserId(@RequestParam("userId") String userId);

	@PostMapping("api/sysuser/cust/del-by-cids")
	public ResultModel<String> delUserCustByCIds(@RequestParam("cids") String cids,@RequestParam("userId") String userId);

	@PostMapping("api/sysuser/cust/del-by-userid")
	public ResultModel<String> delUserCustByUserId(@RequestParam("userId") String userId);
}
