package com.mos.eboot.admin.platform.api;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-03
 */
@FeignClient("boot-service")
public interface ICCallLogService  {


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCallLog/query",method = RequestMethod.POST)
	ResultModel<Page<CCallLog>> query(@RequestBody Page<CCallLog> page);
	 /**列表
		 * @param page
		 * @throws Exception
		 */
		 @RequestMapping(value = "cCallLog/dataFilterQuery",method = RequestMethod.POST)
		ResultModel<Page<CCallLog>> dataFilterQuery(@RequestBody Page<CCallLog> page);
		 
	 
	/**通话记录数据同步
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCallLog/call_log_syn",method = RequestMethod.POST)
	 ResultModel<Boolean> callLogSyn();
		
	/**查询用户信息
	 * @param page
	 * @return 
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCallLog/queyCustomer",method = RequestMethod.POST)
	  Customer queyCustomer(@RequestParam("customerId") String customerId);
	 
	 
	 
	 /**
	  * 通话记录筛选 插入数据
	  * */
	@PostMapping("cCallLog/insertCarInfo")
	Boolean insertCarInfo(@RequestBody Map<String,Object> map);
		
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCallLog/save")
	ResultModel<CCallLog> save( @RequestBody CCallLog cCallLog);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCallLog/del")
	ResultModel<Boolean> del(@RequestBody CCallLog cCallLog);
	
	/**根据实体查询list
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCallLog/query_by_entity")
	ResultModel<List<CCallLog>> queryByEntity(@RequestBody CCallLog cCallLog);
	
	
}
