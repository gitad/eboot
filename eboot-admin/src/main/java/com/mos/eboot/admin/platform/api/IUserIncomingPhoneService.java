package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.IncomingPhoneConf;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.vo.UserIncomingPhoneVO;

@FeignClient("boot-service")
public interface IUserIncomingPhoneService {

	/**
	 * 根据用户ID，查询来电弹屏配置信息
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("sys/incoming/phone/by-user-id")
	public ResultModel<List<UserIncomingPhoneVO>> getIncomingPhoneByUserId(@RequestParam(name = "userId") String userId);
	/**
	 * 根据电话id
	 * 
	 * @param userId
	 * @return
	 */
	@GetMapping("sys/incoming/phone/by-conf-id")
	public ResultModel<List<UserIncomingPhoneVO>> getIncomingPhoneByConfId(@RequestParam(name = "confId") String confId);

	
	/**
	 * 根据用confId
	 * 
	 * @param confId
	 * @return
	 */
	@GetMapping("sys/incoming/phone/by-no-select")
	public ResultModel<List<UserIncomingPhoneVO>> getByNoSelect(@RequestParam(name = "userId") String userId);

	
	/**
	 * 保存来电记录
	 * @param data  云智websocket phonecallLog数据
	 * @param userId
	 * @return
	 */
	@PostMapping("sys/incoming/phone/save/phone-call-log")
	public ResultModel<String> savePhoneCallLog(@RequestParam(name = "data") String data,
			@RequestParam(name = "userId") String userId);

	/**
	 * 获取所有来电弹屏配置号码
	 * @return
	 */
	@GetMapping("sys/incoming/phone/all")
	public ResultModel<List<IncomingPhoneConf>> getAllIncomingPhoneConf();
	
	/**
	 * 保存来电弹屏号码
	 * @return
	 */
	@PostMapping("sys/incoming/phone/save-phone")
	public IncomingPhoneConf saveComingPhone(@RequestBody IncomingPhoneConf icomingPhoneConf);
	
	
	/**
	 * 根据号码查询关系
	 * */
	@GetMapping("sys/incoming/phone/by-phone-num")
	public ResultModel<IncomingPhoneConf> getIncomingPhoneConfByPhoneNum(@RequestParam(name = "phoneNum") String phoneNum);
	
	
	/**
	 * 电话关联删除或者新增
	 * @return
	 */
	@GetMapping("sys/incoming/phone/rel/addOrdel")
	public ResultModel<Boolean> getPhoneRelOrDel(@RequestParam(name = "userId") String userId,
												@RequestParam(name = "confId") String confId,
												@RequestParam(name = "type") boolean type);
										
}
