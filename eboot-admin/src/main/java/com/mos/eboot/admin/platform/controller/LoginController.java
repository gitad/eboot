package com.mos.eboot.admin.platform.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author 小尘哥
 */
@Controller
public class LoginController   {

    @GetMapping("toLogin")
    public String toLogin(){
        return "login";
    }
    @RequestMapping("/loginOut")
    public String logout(HttpSession session) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
       
        return "login";
    }
}