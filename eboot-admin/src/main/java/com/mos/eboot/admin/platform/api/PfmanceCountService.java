package com.mos.eboot.admin.platform.api;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@FeignClient("boot-service")
public interface PfmanceCountService  {

	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "pfManceCount/queryCount",method = RequestMethod.POST)
	 List<JSONObject> queryCount(@RequestBody Map<String, String> map);

 
}
