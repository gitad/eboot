package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mos.eboot.platform.entity.PhoneCallOutLog;
import com.mos.eboot.tools.result.ResultModel;

@FeignClient("boot-service")
public interface IPhoneCallLogService {

	@PostMapping("/api/PhoneCallOutLog/save")
	public ResultModel<String> save(@RequestBody PhoneCallOutLog phoneCallOutLog);

	@PostMapping("/api/PhoneCallOutLog/update")
	public ResultModel<String> update(@RequestBody PhoneCallOutLog phoneCallOutLog);

	@PostMapping("/api/PhoneCallOutLog/get")
	public ResultModel<PhoneCallOutLog> selectByCondition(@RequestBody PhoneCallOutLog phoneCallOutLog);

	@PostMapping("/api/PhoneCallOutLog/list")
	public ResultModel<List<PhoneCallOutLog>> selectList(@RequestBody PhoneCallOutLog phoneCallOutLog);
}
