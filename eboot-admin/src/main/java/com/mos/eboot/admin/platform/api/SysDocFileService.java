package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysDocFile;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since  2022-03-02
 */
@FeignClient("boot-service")
public interface SysDocFileService  {


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "sysDocFile/query",method = RequestMethod.POST)
	 List<SysDocFile>  query(  @RequestParam(value = "businessId", required = false)  String businessId,
			   							@RequestParam(value = "businessTable", required = false) String businessTable,
			   									@RequestParam(value = "businessType", required = false) String businessType);
	 
	 
	  
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("sysDocFile/save")
	ResultModel<Boolean> save( @RequestBody SysDocFile sysDocFile);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("sysDocFile/del")
	ResultModel<Boolean> del(@RequestBody SysDocFile sysDocFile);
	 
	
	
}
