package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.platform.entity.CCompanyDetail;
import com.mos.eboot.platform.entity.CCompanyDetailRel;
import com.mos.eboot.tools.result.ResultModel;

 
 
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-01-07
 */
@FeignClient("boot-service")
public interface ICCompanyDetailService  {

	 
	 @RequestMapping(value = "cCompanyDetail/query",method = RequestMethod.POST)
	  ResultModel<CCompanyDetail> query( @RequestBody CCompanyDetail CCompanyDetail);
	 
	 
	 
	@PostMapping("cCompanyDetail/relQuery")
	  ResultModel<List<CCompanyDetailRel>>  relQuery(@RequestParam("companyId")  String companyId);
	
	
	 
	 
	@PostMapping("cCompanyDetail/relDel")
	 ResultModel<Boolean>  relDel( @RequestBody CCompanyDetailRel  cCompanyDetailRel);
	
	 
	@PostMapping("cCompanyDetail/save")
	ResultModel<CCompanyDetail>  save( @RequestBody CCompanyDetail  cCompanyDetail);
	
	
	 
	@PostMapping("cCompanyDetail/relSave")
	ResultModel<CCompanyDetailRel>  relSave( @RequestBody CCompanyDetailRel  companyDetailRel);
	
}
