package com.mos.eboot.admin.platform.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CustChat;
import com.mos.eboot.tools.result.ResultModel;

@FeignClient("boot-service")
public interface ICustChatService {
	@GetMapping("api/custchat/list/all")
	public ResultModel<Page<CustChat>> queryList(@RequestBody Page<CustChat> page);

	@GetMapping("api/custchat/list/by-cid")
	public ResultModel<Page<CustChat>> queryList(@RequestBody Page<CustChat> page,
			@RequestParam(name = "cid") Integer cid);

	@GetMapping("api/custchat/get/by/condition")
	public ResultModel<CustChat> queryCustChatByCondition(@RequestBody CustChat condition);

	@PostMapping("api/custchat/save")
	public ResultModel<String> saveCustChat(@RequestBody CustChat custChat);
}
