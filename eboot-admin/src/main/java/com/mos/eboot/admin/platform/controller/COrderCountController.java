package com.mos.eboot.admin.platform.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICOrderService;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.tools.controller.BaseController;

/**
 * <p>
 * 收入统计 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@Controller
@RequestMapping("/cOrderCount")
public class COrderCountController extends BaseController {
	
	@Autowired
	ICOrderService iCOrderService;
	
	@Autowired
	ImApiService imApiService;
	
	@Autowired
	ISysUserService iSysUserService;
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		return "platform/orderCount";
	}
	
	@PostMapping("/query")
	@ResponseBody
	public List<JSONObject> query(Page<COrder> page) {		 
		Map<String,Object> condition=page.getCondition(); 
		if(condition==null){
			condition=new HashMap<String, Object>();
		}
		//condition.put("customerId", customerId);		 
		page.setCondition(condition);
		 List<JSONObject>  dataPage=iCOrderService.queryCount(page);		
		return dataPage;
    }
	
	
	@PostMapping("/queryCountDetail")
	@ResponseBody
	public List<COrder> queryCountDetail(Page<COrder> page) {		 
		Map<String,Object> condition=page.getCondition(); 
		if(condition==null){
			condition=new HashMap<String, Object>();
		}
		//condition.put("customerId", customerId);		 
		page.setCondition(condition);
		 List<COrder>  dataPage=iCOrderService.queryCountDetail(page);		
		return dataPage;
    }
	
 
}
