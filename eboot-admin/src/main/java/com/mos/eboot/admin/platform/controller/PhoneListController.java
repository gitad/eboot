package com.mos.eboot.admin.platform.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.IPhoneCallLogService;
import com.mos.eboot.platform.entity.PhoneCallOutLog;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;

@Controller
@RequestMapping("phone")
public class PhoneListController {

	private final String CALL_STATUS_1 = "1";// 呼叫状态，1已拨打,2未拨打，3拒接，4拨打中   5 空错号   
	private final String CALL_STATUS_2 = "2";// 呼叫状态，1已拨打,2未拨打，3拒接，4拨打中    5 空错号
	private final String CALL_STATUS_3 = "3";// 呼叫状态，1已拨打,2未拨打，3拒接，4拨打中   5 空错号
	private final String CALL_STATUS_4 = "4";// 呼叫状态，1已拨打,2未拨打，3拒接，4拨打中   5 空错号
	
	@Autowired
	private IPhoneCallLogService iPhoneCallLogService; 
	
	@Autowired
	ICustomerService ICustomerService;
		
	@Autowired
	private RedisUtil redisUtil;
	
	@GetMapping("/view")
	public String view(Model model) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_PHONE_LIST);
		 Integer phoneNoTel=0; 
		 List<JSONObject> list=new ArrayList<JSONObject>();  
	
		if(map.get(currentUser.getId())!=null){
			  list=JSONArray.parseArray(map.get(currentUser.getId()).toString(), JSONObject.class);
			 for(JSONObject js:list){
				 if(js.getString("status").equals("-1")){
					 phoneNoTel+=1;
				 }			
			 }
		}
		 model.addAttribute("culist",list);
		 model.addAttribute("phoneAll",list.size());
		 model.addAttribute("phoneTel",list.size()-phoneNoTel);
		 model.addAttribute("phoneNoTel",phoneNoTel);
		
		return "im/tel_list";
	}
	
	@GetMapping("/view1")
	public String view1(Model model) {
		 return "im/tel_list1";
	}
	
	
	@PostMapping("/getPhoneList")
	@ResponseBody
	public JSONObject getPhoneList(){
		JSONObject json=new JSONObject();
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_PHONE_LIST);
		 Integer phoneNoTel=0; 
		 List<JSONObject> list=new ArrayList<JSONObject>();  
	
		if(map.get(currentUser.getId())!=null){
			  list=JSONArray.parseArray(map.get(currentUser.getId()).toString(), JSONObject.class);
			 for(JSONObject js:list){
				 if(js.getString("status").equals("-1")){
					 phoneNoTel+=1;
				 }			
			 }
		}
		json.put("culist",list);
		json.put("phoneAll",list.size());
		json.put("phoneTel",list.size()-phoneNoTel);
		json.put("phoneNoTel",phoneNoTel);
		return json;
	}
	/**
	*手机号码状态变更
	**/
	@ResponseBody
	@PostMapping("/phone_type_change")
	public  Map<Object, Object> phoneTypeChange(String id, String status ) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_PHONE_LIST);  
		 if(map.get(currentUser.getId())!=null){
			 List<JSONObject> list=JSONArray.parseArray(map.get(currentUser.getId()).toString(), JSONObject.class);
			 for(JSONObject js:list){
				 if(js.get("id").equals(id)){
					 js.put("status",status);
					 break;
				 }
			 }
			 map.put(currentUser.getId(), JSON.toJSONString(list));		
		 }			 
			 redisUtil.hmset(Constants.REDIS_CUST_MANAGER_PHONE_LIST,map,-1);
			 return map;	
	}

	@PostMapping("/call")
	public String phoneCallLog(String phoneNum, String virtualPhone, Integer cid) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		PhoneCallOutLog callLog = new PhoneCallOutLog();
		callLog.setPhoneNum(phoneNum);
		callLog.setVirtualPhone(virtualPhone);
		callLog.setUserId(currentUser.getId());
		callLog.setcId(cid);
		callLog.setCallTime(Calendar.getInstance().getTime());
		callLog.setCallStat(CALL_STATUS_4);
		iPhoneCallLogService.save(callLog);
		return "";
	}

	@GetMapping("/list")
	public String list() {
		return "";
	}

}