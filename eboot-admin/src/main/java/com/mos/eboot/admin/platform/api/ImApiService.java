package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.platform.entity.WxFriends;

/**
 * IM接口
 * 
 */
@FeignClient("boot-im")
public interface ImApiService {

	@RequestMapping(value = "/imapi/get-list", method = RequestMethod.GET)
	public Object getList(@RequestParam( "userName") String userName,@RequestParam("userId") String userId);

	@RequestMapping(value = "/imapi/get-members", method = RequestMethod.GET)
	public Object getMembers();

	
	/**
	 * 修改状态(在线隐身、个性签名、皮肤)接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/imapi/edit-state", method = RequestMethod.POST)
	public Object editState(@RequestParam("userName") String userName);

	/**
	 * 消息盒子未读消息总数接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/imapi/get-msg-count")
	public Object getMsgCount(@RequestParam("userName") String userName);

	/**
	 * 获取未读消息(离线消息)接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/imapi/get-his-msg")
	public Object getNoreadMsg(@RequestParam("userName") String userName);

	
	/**消息盒子数据接口
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/imapi/msgboxdata")
	@ResponseBody
	public Object msgboxdata(@RequestParam("userName") String userName ,@RequestParam("syswxid") String sysWxId);
	
	/**打开聊天记录窗口获取数据
	 * @param page
	 * @throws Exception
	 */
	@GetMapping(value="/imapi/himsglist")
	public Object himsgList(@RequestParam("userName") String userName ,@RequestParam("syswxid") String sysWxId);
	
	/** 获取最后消息
	 * @param page
	 * @throws Exception
	 */
	@GetMapping(value="/imapi/get-last-history-msg")
	public String getLastHisMsgList(@RequestParam("userName") String userName ,@RequestParam("syswxid") String sysWxId);
	
	
	/** 根据手机号获取微信好友
	 * @param page
	 * @throws Exception
	 */
	@GetMapping(value="/imapi/get-friend-by-phone")
	public Object getFriendByPhone(@RequestParam("phone") String phone);
	
	
	
	/** 根据微信号获取自己全部好友
	 * @param page
	 * @throws Exception
	 */
	@GetMapping(value="/imapi/get-friend-by-fromWxId")
	public List<WxFriends> getFiendsList(@RequestParam("fromWxId") String fromWxId);
	
	/**
	 * 发送好友文本消息
	 * 
	 * @param type
	 * @param content
	 * @param wxId
	 * @param cid
	 * @return
	 */
//	@PostMapping(value = "/imapi/send")
//	public Object send(@RequestParam("type") String type, @RequestParam("content") String content,
//			@RequestParam("wxId") String wxId, @RequestParam("groupId") String groupId,@RequestParam("userId") String userId);

	@PostMapping(value = "/imapi/send-wx-msg")
	public Object send_v1(@RequestParam("content") String content, 
			@RequestParam("wxId") String wxId, @RequestParam("sysWxId") String sysWxId,
			@RequestParam("cid") String cid,@RequestParam("sid") String sid);
	
	/**
	 * 查询所有微信登陆用户
	 * 
	 * @return
	 */
	@GetMapping("/imapi/get-all-wx-login")
	public List<SysUserWxInfo> getAllWxLogin();

	@GetMapping("/imapi/get-wx-login-by-wxid")
	public SysUserWxInfo getwxLoginByWxId(@RequestParam("wxId") String wxId);
	
	/**
	 * 根据微信Id，和系统管理员微信ID，查询好友
	 * @param wxId
	 * @param sysWxId
	 * @return
	 */
	@GetMapping("/imapi/get-wx-friend")
	public CustWx getWxfriend(@RequestParam("wxId") String wxId,@RequestParam("sysWxId") String sysWxId);

	/**
	 * 根据微信Id 查询所有已加业务员微信号
	 * @param wxId
	 * @return
	 */
	@GetMapping("/imapi/get-wx-friends")
	public List<CustWx> getWxfriends(@RequestParam("wxId") String wxId);

	
	/**
	 * 修改微信备注
	 * @param wxId  微信ID
	 * @param sysWxId 系统管理员微信ID
	 * @param value 微信备注
	 */
	@PostMapping("/imapi/modify-wx-remark")
	public void modifyRemark(@RequestParam("wxId") String wxId, @RequestParam("sysWxId") String sysWxId,
			@RequestParam("value") String value);

}
