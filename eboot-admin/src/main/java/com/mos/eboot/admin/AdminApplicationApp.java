package com.mos.eboot.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.mos.eboot.admin.config.MyPropsConstants;

/**
 * @author 小尘哥
 */
@EnableEurekaClient
@SpringBootApplication
@EnableFeignClients
@EnableConfigurationProperties({ MyPropsConstants.class })
public class AdminApplicationApp {
	private static int a = 100;
	//test test 待合并分支
	public static void main(String[] args) {
		SpringApplication.run(AdminApplicationApp.class, args);
	}

}
