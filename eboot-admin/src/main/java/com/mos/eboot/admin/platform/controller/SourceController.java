package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCarInfoService;
import com.mos.eboot.admin.platform.api.ICCargoEntryService;
import com.mos.eboot.admin.platform.api.ICCargoInfoService;
import com.mos.eboot.admin.platform.api.ICContactPeoService;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.BaiDuOcrUtil;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  货源调度
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/source")
public class SourceController  extends BaseController {
	
	@Autowired
	ICCargoEntryService iCCargoEntryService;
	
	@Autowired
	ICCargoInfoService iCCargoInfoService;
	
	@Autowired
	ICCarInfoService iCCarInfoService;
	
	@Autowired
	ICContactPeoService iCContactPeoService;
	
	@Autowired
	private ICustomerService iCustomerService;
	
	@Autowired
	private RedisUtil redisUtil;
	 
	@Value("${baidu.API_Key}")
    private String ak;
    
    @Value("${baidu.Secret_Key}")
    private String sk;
    
    @Value("${baidu.minioUrl}")
    private String minioUrl;
    
    
	private static String customerId="";
	private static String phone="";
	
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 
	   return "platform/source/source";
	}
	@RequestMapping("to-page1")
	public String toPage1( String cid,String sid,Model model) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 
	   return "platform/source/list1";
	}
	 /**
	  * 百度ocr识别   type为jsz 驾驶证xsz行驶证
	  * */
	@PostMapping("/baiduOcr")
	@ResponseBody
	public JSONObject baiduOcr(String  imgUrl,String  type){
		JSONObject datajson=new JSONObject();
		String result=  BaiDuOcrUtil.idcard(minioUrl+imgUrl, ak, sk,type);
		JSONObject json=JSONObject.parseObject(result);
		if(json.getString("words_result")!=null){
			JSONObject data=JSONObject.parseObject(json.getString("words_result"));
			if(type.equals("jsz")){
				CContactPeo peo=new CContactPeo();
				peo.setContactName(JSONObject.parseObject(data.getString("姓名")).getString("words"));
				peo.setContactCard(JSONObject.parseObject(data.getString("证号")).getString("words"));
				peo.setContactSex(JSONObject.parseObject(data.getString("性别")).getString("words"));
				peo.setAdress(JSONObject.parseObject(data.getString("住址")).getString("words"));
				peo.setCarType(JSONObject.parseObject(data.getString("准驾车型")).getString("words"));
				peo.setCarUseStart(JSONObject.parseObject(data.getString("有效期限")).getString("words") );
				peo.setCarUseEnd(JSONObject.parseObject(data.getString("至")).getString("words"));
				datajson.put("peo", peo);
			}
			if(type.equals("xsz")){
				CCarInfo carInfo=new CCarInfo();
			 	carInfo.setCompany(JSONObject.parseObject(data.getString("所有人")).getString("words"));
				carInfo.setCarTravelNum(JSONObject.parseObject(data.getString("车辆识别代号")).getString("words"));
				carInfo.setAdress(JSONObject.parseObject(data.getString("住址")).getString("words"));
				carInfo.setEngNum(JSONObject.parseObject(data.getString("发动机号码")).getString("words"));
				carInfo.setCarLincese(JSONObject.parseObject(data.getString("号牌号码")).getString("words"));
			    datajson.put("car", carInfo);
			}
		}
	 	return datajson;
		
	}
	/**
	 * 数据校验  暂停
	 * */
	@PostMapping("/checkPeoData")
	@ResponseBody
	public CContactPeo checkData(@RequestBody CContactPeo cContactPeo){
		cContactPeo=iCContactPeoService.queryByEntity(cContactPeo);
		return  cContactPeo;
	}
	/**
	 * 数据校验  暂停
	 * */
	@PostMapping("/checkCarData")
	@ResponseBody
	public CCarInfo checkCarData(@RequestBody CCarInfo cCarInfo){
		cCarInfo=iCCarInfoService.queryByEntity(cCarInfo);
		return  cCarInfo;
		
	} 
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CCargoEntry> query(Page<CCargoEntry> page) {		
		ResultModel<Page<CCargoEntry>> dataPage=iCCargoEntryService.querySource(page);		
		return this.getLayPage(dataPage);
    }
	
	
	/**查询货源
	 * */
	@PostMapping("/queryCargo")
	@ResponseBody
	public List<CCargoInfo> queryCargo(@RequestBody CCargoEntry CCargoEntry ,@RequestParam("type") String type) {		
		Map<String, Object> map=new HashMap<String, Object>();;
	    map.put("type", type);
		map.put("CCargoEntry", JSONObject.toJSONString(CCargoEntry));
		List<CCargoInfo> dataPage=iCCargoInfoService.queryCarGo(map);		
		return dataPage;
    }
 
}
