package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustPhone;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.vo.IMCustomerVO;

@FeignClient("boot-service")
public interface ICustomerService {
	@RequestMapping(value = "api/sys/cust/query-custWx", method = RequestMethod.POST)
	List<CustWx> getCustomerQueryCustWx(@RequestBody CustWx custWx);	
	
	@RequestMapping(value = "api/sys/cust/custWx-save", method = RequestMethod.POST)
	CustWx getCustomercustWxSave(@RequestBody CustWx custWx);	
	
	
	@RequestMapping(value = "api/sys/cust/query-cust-by-entity", method = RequestMethod.GET)
	List<Customer> getCustByEntity(Customer customer);
 
	@PostMapping("api/sys/cust/query-page")
	ResultModel<Page<Customer>> queryPage(@RequestBody Page<Customer> page,@RequestParam("userId") String userId);

	
	@PostMapping("api/sys/cust/shujuhebing")
	ResultModel<Boolean> shujuhebing(@RequestBody  List<Customer> culist,@RequestParam("uuid") String uuid);


	@PostMapping("api/sys/cust/shujuUpdate")
	ResultModel<Boolean> shujuUpdate();

	
	
	
	@RequestMapping(value = "api/sys/cust/queryNameAndPhone", method = RequestMethod.POST)
	ResultModel<Page<Customer>>  queryNameAndPhone(@RequestBody Page<Customer> page);
	
	/**
	 * 客户转交
	 * @param token
	 * @return
	 */
	
	@RequestMapping(value = "api/sys/cust/user-distri", method = RequestMethod.POST)
	Boolean usrDistri(@RequestParam("id") String id,@RequestParam("name") String name,@RequestBody List<Customer> list);

	/**
	 * 联系人及备注信息创建
	 * @param token
	 * @return
	 */
	
	@RequestMapping(value = "api/sys/cust/cust-create-peoRel", method = RequestMethod.POST)
	Boolean createPeoRel(@RequestBody Customer customer);


	@RequestMapping(value = "api/sys/cust/get-cust-by-id", method = RequestMethod.GET)
	ResultModel<Customer> getCustomerById(@RequestParam("id") String id);
	//客户自动分配
	@RequestMapping(value = "api/sys/cust/cust-distribution", method = RequestMethod.GET)
	ResultModel<Boolean> custDistribution();
	
	//异常数据处理
	@RequestMapping(value = "api/sys/cust/sjcl", method = RequestMethod.GET)
	ResultModel<Boolean> sjcl();
	
	
	
	//redis数据初始化
	@RequestMapping(value = "api/sys/cust/cust-redis-init", method = RequestMethod.GET)
	ResultModel<Boolean> custRedisInit();
	
	//客户数据同步
	@RequestMapping(value = "api/sys/cust/cust-syn", method = RequestMethod.GET)
	ResultModel<Boolean> custSyn();

	@RequestMapping(value = "api/sys/cust/update-cust-by-id", method = RequestMethod.POST)
	ResultModel<Boolean> updateCustomerById(@RequestBody Customer customer);
	
	@RequestMapping(value = "api/sys/cust/get-cust-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerWxByCustId(@RequestParam("cid") String cid,@RequestParam("syswxid") String syswxid);
	

	@RequestMapping(value = "api/sys/cust/get-cust-wx-list-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerWxListByWxId(@RequestParam("wxid") String wxid,@RequestParam("syswxid") String syswxid);

	@RequestMapping(value = "api/sys/cust/get-cust-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<CustWx> getCustomerWxByWxIdAndSysWxId(@RequestParam("wxid") String wxid,
			@RequestParam("syswxid") String syswxid);	
 

	
	
	@RequestMapping(value = "api/sys/cust/get-cust-listwx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxId(@RequestParam("wxid") String wxid );
	
	
	@RequestMapping(value = "api/sys/cust/get-cust-list-wx-by-wxid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustomerListWxByWxIdAndSysWxId(@RequestParam("wxid") String wxid);	
	
	@RequestMapping(value = "api/sys/cust/get-cust-wx-by-id", method = RequestMethod.GET)
	ResultModel<CustWx> getCustWxById(@RequestParam("id") String id);
	
	@RequestMapping(value = "api/sys/cust/get-cust-wx-all", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustWxAll();


	@RequestMapping(value = "api/sys/cust/get-cust-list-wx-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustWx>> getCustListWxByCid(@RequestParam("cid") String cid);
	


	/***
	 * 更具t_cust_wx电话去查询用户数据
	 * */
	@RequestMapping(value = "api/sys/cust/get-cust-list-wx-by-phone", method = RequestMethod.POST)
	ResultModel<List<CustWx>> getCustListWxByPhone(@RequestParam("phone") String phone);

	/***
	 * 更新t_cust_wx得cid
	 * */
	@RequestMapping(value = "api/sys/cust/update-cust-list-cid", method = RequestMethod.POST)
	ResultModel<List<CustWx>> updateCustListCid(@RequestParam("cid") String cid,@RequestBody List<CustWx> list);

	
	

	@RequestMapping(value = "api/sys/cust/get-cust-phone-by-cid", method = RequestMethod.GET)
	ResultModel<List<CustPhone>> getCustPhoneByCustId(@RequestParam("cid") String cid);

	@RequestMapping(value = "api/sys/cust/get-cust-phone-by-id", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneById(@RequestParam("id") String id);

	@RequestMapping(value = "api/sys/cust/get-cust-phone-by-phone", method = RequestMethod.GET)
	ResultModel<CustPhone> getCustPhoneByPhone(@RequestParam("phone") String phone);
	
	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "api/sys/cust/get-cust-by-phone", method = RequestMethod.GET)
	ResultModel<Customer> getCustomerByPhone(@RequestParam("phone") String phone);
	/**
	 *
	 * @param id 角色id
	 * @return 角色对象
	 */
	@RequestMapping(value = "api/sys/cust/get-cust-by-vail-other-phone", method = RequestMethod.GET)
	ResultModel<Customer> getCustomerByvailOrOherPhone(@RequestParam("phone") String phone);

	
	@RequestMapping(value = "api/sys/cust/get-cust-by-rel-phone", method = RequestMethod.GET)
	ResultModel<CContactPeoRel> getCustByRelPhone(@RequestParam("phone") String phone);
	
	@PostMapping("api/sys/cust/save-cust")
	ResultModel<Customer> saveCustomer(@RequestBody IMCustomerVO customerVO);
	
	@PostMapping("api/sys/cust/save-cust-wx")
	ResultModel<CustWx> saveCustWx(@RequestBody CustWx custWx);
	
	@PostMapping("api/sys/cust/save-sample-cust")
	ResultModel<Customer> saveSampleCustomer(@RequestBody IMCustomerVO customerVO);
	
	@GetMapping("api/sys/cust/get-cust-im-role")
	ResultModel<List<CustImRole>> getCustImRole(@RequestParam("userId") String userId,
			@RequestParam("custType") String custType);
	
	@PostMapping("api/sys/cust/get-all-cust")
	ResultModel<List<Customer>> getAllCust();

	/**
	 * 修改微信备注
	 * @param wxId  微信ID
	 * @param sysWxId 系统管理员微信ID
	 * @param value 微信备注
	 * type  0备注 1电话
	 */
	@PostMapping("api/sys/cust/modify-wx-remark-phone")
	public void modifyRemarkPhone(@RequestParam("cid") String cid,@RequestParam("value") String value,@RequestParam("type") String type);


	@PostMapping("api/sys/cust/customerZh")
	ResultModel<Boolean>  customerZh(@RequestBody List<Customer> culist);

}
