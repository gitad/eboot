package com.mos.eboot.admin.config;

import io.minio.BucketExistsArgs;
import io.minio.GetPresignedObjectUrlArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.http.Method;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Value;
 
@Controller
@RequestMapping("/uploadfile")
public class UploadController {

	     
	    @Value("${minio.url}")
	    private String endpoint;
	    
	    @Value("${minio.accessKey}")
	    private String ACCESS_KEY;
	    
	    @Value("${minio.secretKey}")
	    private String SECRET_KEY;
 
    @RequestMapping(value="/upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(@RequestParam( value="file",required=false) MultipartFile file, HttpServletRequest request) throws Exception {
    	 MinioClient minioClient = 
    			 MinioClient.builder()
    	              .endpoint(endpoint)
    	              .credentials(ACCESS_KEY,SECRET_KEY)
    	              .build();
    	 	 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
 			 String datePath ="jinmayi";// sdf.format(new Date());
    	    
 			 //查询一下桶是否存在  一天一个桶
    	     boolean found =    minioClient.bucketExists(BucketExistsArgs.builder().bucket(datePath).build());
    	      if (!found) {
    	        // Make a new bucket called 'asiatrip'.
    	        minioClient.makeBucket(MakeBucketArgs.builder().bucket(datePath).build());
    	      } else {
    	        System.out.println("Bucket 'asiatrip' already exists.");
    	      }
    	      
    	     	// 获取文件的后缀名
    	        String fileName=file.getOriginalFilename();
    			String suffixName = fileName.substring(fileName.lastIndexOf("."));
    		 	String prefixName =new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    			//拼接成带有UUID和后缀名的文件名
    			String name = prefixName + suffixName;
    			 File uploadFile = multipartFileToFile(file);
    			 FileInputStream in=new FileInputStream(uploadFile);
    		 
    	      PutObjectArgs arg= PutObjectArgs.builder().bucket(datePath).object(name).stream(in,uploadFile.length(),-1).build();
               minioClient.putObject(arg);
              String url = minioClient.getPresignedObjectUrl(new GetPresignedObjectUrlArgs().builder().bucket(datePath).object(name).method(Method.GET).build());

    /*
    	
    	String path="D:\\jinmayi";
    	String fileName=file.getOriginalFilename();
		System.out.println(file.getOriginalFilename());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String datePath = sdf.format(new Date());
		// 获取文件的后缀名
		String suffixName = fileName.substring(fileName.lastIndexOf("."));
	 	String prefixName =new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		//拼接成带有UUID和后缀名的文件名
		String name = prefixName + suffixName;
		
	 //创建文件			
		File newfile = new File(path+"\\"+datePath);
		if(!newfile.exists()){
			  newfile.mkdirs();//没有则创建文件夹
	    }
	  
		 FileOutputStream imgOut=new FileOutputStream(new File(newfile,name));    //根据 dir 抽象路径名和 img 路径名字符串创建一个新 File 实例。
	   
	        imgOut.write(file.getBytes());//返回一个字节数组文件的内容
	        imgOut.close();
 */   System.out.println(endpoint+datePath+"\\"+name);
        return datePath+"/"+name;	
		
	}
    public static File multipartFileToFile(MultipartFile file) throws Exception {

        File toFile = null;
        if (file.equals("") || file.getSize() <= 0) {
            file = null;
        } else {
            InputStream ins = null;
            ins = file.getInputStream();
            toFile = new File(file.getOriginalFilename());
            inputStreamToFile(ins, toFile);
            ins.close();
        }
        return toFile;
    }
    //获取流文件
    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
}
