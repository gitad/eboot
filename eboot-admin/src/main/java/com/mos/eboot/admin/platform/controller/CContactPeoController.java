package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mos.eboot.admin.platform.api.ICContactPeoRelService;
import com.mos.eboot.admin.platform.api.ICContactPeoService;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.PhoneUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cContactPeo")
public class CContactPeoController  extends BaseController {
	
	@Autowired
	ICContactPeoService cContactPeoService;
	
	@Autowired
	private ICContactPeoRelService iCContactPeoRelService;
	
	@Autowired
	private ICustomerService iCustomerService;
	
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	
	
	private static String customerId="";
 
	
	
	@RequestMapping("to-page")
	public String toPage(String cid) {
		 if(cid!=null){
			this.customerId=cid; 
		 }		 
	   return "platform/contact/contacts";
	}
	
	
	@RequestMapping("to-page1")
	public String toPage1(String cid) {
		 if(cid!=null){
			this.customerId=cid; 
		 }		 
	   return "platform/contact/contacts1";
	}
	
	 
	
	@PostMapping("/query")
	@ResponseBody
	public List<CContactPeo> query() {
		ResultModel<Customer> result=iCustomerService.getCustomerById(customerId);
		Customer customer=result.getData();	
		List<CContactPeo> list = createPeoList(customer);
		return list;
    }
	
	@PostMapping("/checkByPhone")
	@ResponseBody
	public String checkByPhone(@RequestParam("phone")  String  phone,@RequestParam("customerId")  String  customerId) {
		 String result=cContactPeoService.checkByPhone(phone,customerId).getData(); 
		return result;
    }
	
	
	
	/**创建联系人      暂时弃用
	 * */
	@PostMapping("/createNewPeo")
	@ResponseBody
	public List<CContactPeo> createNewPeo(@RequestParam("phone") String phone,@RequestParam("customerId") String customerId) {	
		List<CContactPeo> list=null;
		Customer customer=iCustomerService.getCustomerByvailOrOherPhone(phone).getData();
		if(customer==null){
			customer=new Customer();
			customer.setId(customerId);
			customer.setValidPhone(phone);
		} 
		list=createPeoList(customer);
		 return  list;
    }
	public List<CContactPeo> createPeoList(Customer customer){
		//peoId人员id用来查
				List<CContactPeo> list=null;
				list=cContactPeoService.query(customer.getId());	
				if(list.size()==0){
				 	CContactPeo  cContactPeo=new CContactPeo();
					cContactPeo.setCreateTime(new Date());
					cContactPeo.setCustomerId(customer.getId());  	
					cContactPeo.setUseFlag(1);
					cContactPeo.setContactSort("0");
					cContactPeo.setContactName(customer.getCustName());
					save(cContactPeo);
					String vaildPhone=customer.getValidPhone();
					String otherPhone=customer.getOtherPhone();
					if(StringUtils.isNotBlank(vaildPhone)){
						List<String> str=StringUtil.str_all_Phone(vaildPhone);
						if(str.size()>0){
							for(int i=0;i<str.size();i++){
								CContactPeoRel  cContactPeoRel=new CContactPeoRel();
								String[] str1=PhoneUtil.getAttr(str.get(i).toString());  	    
					    		cContactPeoRel.setPhoneArea(str1[0]);
								cContactPeoRel.setPhoneAtt(str1[1]);
								cContactPeoRel.setContactId(cContactPeo.getContactId());
								//cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
								cContactPeoRel.setContactPhone(str.get(i));
								cContactPeoRel.setCustomerId(customer.getId());
								cContactPeoRel.setPhoneDefault("默认");//是否默认手机号 
								cContactPeoRel.setContactState("正常");//状态0分离1正常
								cContactPeoRel.setWxType("企业号");	
								iCContactPeoRelService.save(cContactPeoRel);
							}
							
						}
						 
					}
					if(StringUtils.isNotBlank(otherPhone)){
						List<String> str=StringUtil.str_all_Phone(otherPhone);
						if(str.size()>0){
							for(int i=0;i<str.size();i++){
								CContactPeoRel  cContactPeoRel=new CContactPeoRel();
								cContactPeoRel.setContactId(cContactPeo.getContactId());
								String[] str1=PhoneUtil.getAttr(str.get(i).toString());  	    
					    		cContactPeoRel.setPhoneArea(str1[0]);
								cContactPeoRel.setPhoneAtt(str1[1]);
								//cContactPeoRel.setContactRelId(UuidUtil.get32UUID());
								cContactPeoRel.setContactPhone(str.get(i));
								cContactPeoRel.setCustomerId(customer.getId());
								cContactPeoRel.setPhoneDefault("1");//是否默认手机号   0 是 1否
								cContactPeoRel.setContactState("1");//状态0分离1正常
								cContactPeoRel.setWxType("企业号");	
								iCContactPeoRelService.save(cContactPeoRel);
							}
							
						}
						 
					}
					list=cContactPeoService.query(customer.getId());	
				}
			return list;
	} 


	
	@PostMapping("/save")
	@ResponseBody
	public  CContactPeo save(@RequestBody  CContactPeo  cContactPeo) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		  ResultModel<Boolean> result;
		 cContactPeo.setCreateUser(user.getNickname());	
		 if(StringUtil.isNotBlank(customerId)){
			 cContactPeo.setCustomerId(customerId);
		 }
		
		 cContactPeo.setCreateTime(new Date()); 	
		 cContactPeo.setUseFlag(1);
		 if(cContactPeo.getContactId()==null){
			 cContactPeo.setContactId(UuidUtil.get32UUID());
			 result=cContactPeoService.save(cContactPeo);	
		 }else{
			 result=cContactPeoService.update(cContactPeo);	
		 }
		  if(StringUtil.isNotBlank(cContactPeo.getContactId())){
				 
			  iSysEbootLogService.save( SysEbootLogController.insert(cContactPeo.getContactId(), "c_contact_peo", "update", cContactPeo.toString(),cContactPeo.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cContactPeo.getContactId(), "c_contact_peo", "add", cContactPeo.toString(),cContactPeo.getCustomerId())) ;
		  }
	    return cContactPeo;
	}

	
	@PostMapping("/queryById")
	@ResponseBody
	public  CContactPeo queryById(@RequestParam("contactId")  String  contactId) {
		   CContactPeo  result=cContactPeoService.queryById(contactId);	
		   if(result==null){
			   result=new CContactPeo();
		   }
		   return result;
	}

	@PostMapping("/queryByCustomerId")
	@ResponseBody
	public  List<CContactPeo> queryByCustomerId(@RequestParam("customerId")  String  customerId) {
		List<CContactPeo>  result=cContactPeoService.queryByCustomerId(customerId);	
		   return result;
	}

	
	
	@PostMapping("/add")
	@ResponseBody
	public   CContactPeo add() {
		 CContactPeo  cContactPeo=new CContactPeo();
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 cContactPeo.setCustomerId(customerId);
		 cContactPeo.setCreateUser(user.getNickname());
		 cContactPeo.setCreateTime(new Date()); 	
		 cContactPeo.setUseFlag(1);
	   cContactPeo.setContactSort("0");
	    return cContactPeo;
	}
	
	@PostMapping("/relAdd")
	@ResponseBody
	public   CContactPeoRel relAdd() {
		CContactPeoRel  cContactPeo=new CContactPeoRel();
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 cContactPeo.setCustomerId(customerId);
		 cContactPeo.setCreateUser(user.getNickname());
		 cContactPeo.setCreateTime(new Date()); 	
		 cContactPeo.setUseFlag(1);
		
	    return cContactPeo;
	}
	
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestParam("contactId") String contactId) {
	    ResultModel<Boolean> result=cContactPeoService.del(contactId);
	    return result;
	}
 
}
