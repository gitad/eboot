package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@FeignClient("boot-service")
public interface ICContactPeoRelService {

	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cContactPeoRel/query",method = RequestMethod.POST)
	ResultModel<Page<CContactPeoRel>> query(@RequestBody Page<CContactPeoRel> page);
	 

	/**电话查重
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cContactPeoRel/queryPhone",method = RequestMethod.POST)
	ResultModel<CContactPeoRel> queryPhone(@RequestParam("phone") String  phone);
	 
		 
		
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
 
	 @RequestMapping(value = "cContactPeoRel/save",method = RequestMethod.POST)
	 CContactPeoRel save( @RequestBody CContactPeoRel cContactPeoRel);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cContactPeoRel/del",method = RequestMethod.POST)
	ResultModel<Boolean>del(@RequestBody CContactPeoRel cContactPeoRel);
	
	/**查询
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cContactPeoRel/quer-by-entity",method = RequestMethod.POST)
	ResultModel<CContactPeoRel> querByEntity(@RequestBody CContactPeoRel cContactPeoRel);
	
	 
	/**主键查询
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeoRel/queryById")
	CContactPeoRel  queryById(@RequestParam("contactRelId") String contactRelId);
	
	
	@PostMapping("cContactPeoRel/queryByContactId")
	List<CContactPeoRel>  queryByContactId(@RequestParam("contactId") String contactId);
	
}
