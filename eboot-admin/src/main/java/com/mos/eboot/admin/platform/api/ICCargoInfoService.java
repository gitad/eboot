package com.mos.eboot.admin.platform.api;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@FeignClient("boot-service")
public interface ICCargoInfoService {
	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoInfo/query",method = RequestMethod.POST)
	 ResultModel<Page<CCargoInfo>> query(@RequestBody Page<CCargoInfo> page);
	
	 
	/**货源调度查询
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoInfo/queryCarGo",method = RequestMethod.POST)
	 List<CCargoInfo> queryCarGo(@RequestBody Map<String, Object> map);
	
		 
	 
	/**列表
	 * @param page
	 * @throws Exception
	 */
    @RequestMapping(value = "cCargoInfo/cCargoQuery",method = RequestMethod.POST)
	ResultModel<Page<CCargoEntry>> cCargoQuery(@RequestBody Page<CCargoEntry> page);
   
	 /**保存
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoInfo/save",method = RequestMethod.POST)
	 CCargoInfo save(@RequestBody  CCargoInfo cCargoInfo);
	 

	 /**保存
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoInfo/sourceSave",method = RequestMethod.POST)
	 CCargoInfo sourceSave(@RequestBody  CCargoInfo cCargoInfo);
	 
	 
	 /**查询列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoInfo/query_by_entity",method = RequestMethod.POST)
	 ResultModel<List<CCargoInfo>> queryByEntity(@RequestBody  CCargoInfo cCargoInfo);
			 
}
