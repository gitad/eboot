package com.mos.eboot.admin.platform.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.ISysUserWxInfoService;
import com.mos.eboot.admin.platform.api.IUserIncomingPhoneService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.IncomingPhoneConf;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.PhoneUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.SelectorDataVO;
import com.mos.eboot.vo.UserIncomingPhoneVO;
import com.mos.eboot.vo.WxInfoVO;

/**
 * @author 小尘哥
 * @Time 2018/5/6 21:01
 */
@Controller
@RequestMapping("user/wx")
public class SysUserWxInfoController extends BaseController{

    @Autowired
    private ISysUserWxInfoService iSysUserWxInfoService;
    
    @Autowired
    private ImApiService imApiService;
    
    @Resource
    private ISysUserService sysUserService;

	@Autowired
	private RedisUtil redisUtil;	
	
	@Autowired
	IUserIncomingPhoneService userIncomingPhoneService;
	
    @RequestMapping(value = "to-page",method = RequestMethod.GET)
    public String toPage(@RequestParam(name = "userId", required = false) String userId, Model model) {
    	model.addAttribute("userId",userId);
        return "platform/wxinfo/list";
    }

    @RequestMapping(value = "to-edit",method = RequestMethod.GET)
//    @RequiresPermissions("sys:dict:basic:op")
    public String toEdit(@RequestParam(name = "userId", required = false) String userId, Model model) {
    	List<SysUserWxInfo> sysUserWxInfo = iSysUserWxInfoService.getByUserId(userId).getData();
        if (sysUserWxInfo == null){
        	sysUserWxInfo = new ArrayList<SysUserWxInfo>();
        }
        model.addAttribute("sysUserWxInfo", sysUserWxInfo);
        model.addAttribute("userId",userId);
        return "platform/wxinfo/edit";
    }
    
    
    @ResponseBody
    @RequestMapping(value = "save-phone",method = RequestMethod.POST)
  	public IncomingPhoneConf saveComingPhone(@RequestBody IncomingPhoneConf icomingPhoneConf) {
    	if(icomingPhoneConf.getId()==null){
    		String[] str=PhoneUtil.getAttr(icomingPhoneConf.getPhoneNum());
    		icomingPhoneConf.setPhoneAttr(str[0]);
    		icomingPhoneConf.setPhoneQcellcore(str[1]);
    	}
    	return userIncomingPhoneService.saveComingPhone(icomingPhoneConf);
		  
	}
    
    
    /**
	 * 查询微信登陆用户
	 * @return
	 */
    @ResponseBody
	@PostMapping("/all-wx-login")
	public LayPage<SysUserWxInfo> getAllWxLogin() {
		List<SysUserWxInfo> list = imApiService.getAllWxLogin();
		Page<SysUserWxInfo> voPage = new Page<>();
		voPage.setRecords(list);

		ResultModel<Page<SysUserWxInfo>> model = new ResultModel<>();
		model.setData(voPage);
		model.setCode(ResultStatus.SUCCESS.getCode());
		model.setMsg(ResultStatus.SUCCESS.getMsg());

		return getLayPage(model);
	}
	
   
    @ResponseBody
    @RequestMapping(value = "save-or-update",method = RequestMethod.POST)
//    @RequiresPermissions("sys:dict:basic:op")
	public ResultModel<String> save(@RequestBody WxInfoVO wxInfoVO) {
		SysUserWxInfo sysUserWxInfo = null;

		iSysUserWxInfoService.delById(wxInfoVO.getUserId());
		for (String wxId : wxInfoVO.getWxIds()) {
			sysUserWxInfo = imApiService.getwxLoginByWxId(wxId);
			sysUserWxInfo.setUserId(wxInfoVO.getUserId());
			iSysUserWxInfoService.saveOrUpdate(sysUserWxInfo);
		}
		return ResultModel.defaultSuccess("成功");
	}

	/***
	 * 对数据选中下拉进行增删
	 * userid 用户id
	 * wxid  微信id
	 * selected  增加还是删除
	 * */
    @ResponseBody
    @RequestMapping(value = "/update-wxinfo",method = RequestMethod.POST)
    public void updateWxInfo(@RequestParam(name = "userId", required = false) String userId,
    		@RequestParam(name = "wxid", required = false) String wxid,
    		@RequestParam(name = "type", required = false) boolean type) {    
    	   Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_REL);  
    	 	if(type==true){//增加
    			 SysUserWxInfo  sysUserWxInfo = imApiService.getwxLoginByWxId(wxid);   			    			 
    	    	 sysUserWxInfo.setUserId(userId);
    	    	 if(StringUtil.isNotBlank(sysUserWxInfo.getWxId())){
    	    	 	 iSysUserWxInfoService.saveOrUpdate(sysUserWxInfo);	   	    	 
        	    	 map.put(wxid, userId);
    	    	 }
    	   
    		}
    		if(type==false){//删除
    			iSysUserWxInfoService.delByUserIdAndWxid(userId, wxid);
    			 map.remove(wxid);
    			
    		}
    	//	redisUtil.del(Constants.REDIS_CUST_MANAGER_REL);
    		redisUtil.hmset(Constants.REDIS_CUST_MANAGER_REL,map,-1);		
    }
 
    
    
    /**
     * 微信管理
     * */
    @ResponseBody
    @RequestMapping(value = "/query-selector",method = RequestMethod.GET)
    public LayPage<SelectorDataVO> querySelector(@RequestParam(name = "userId", required = false) String userId) {
    	
    	List<SysUserWxInfo> allWxList = imApiService.getAllWxLogin();
    	
		List<SelectorDataVO> selectorList = new ArrayList<SelectorDataVO>();
		
		for (SysUserWxInfo sysUserWxInfo : allWxList) {			
			SelectorDataVO selector =new SelectorDataVO();
			selector.setName(sysUserWxInfo.getWxNickname());
			selector.setValue(sysUserWxInfo.getWxId());
			selector.setUserId(userId);
			//查询是否已分配了人
			SysUserWxInfo wxInfo=iSysUserWxInfoService.getWxInfoByUidAndWxid(userId,sysUserWxInfo.getWxId());
			if(wxInfo!=null){
				selector.setSelected(true);
			}	else{
				selector.setSelected(false);
			}		
			selectorList.add(selector);
		}
//    	List<SysUserWxInfo> list = iSysUserWxInfoService.queryPage(page).getData().getRecords();
    	
    	
        return this.getLayPage4CustomizeList(selectorList);
    }
    
    /***
	 * 对数据选中下拉进行增删
	 * userid 用户id
	 * wxid  微信id
	 * selected  增加还是删除
	 * */
    @ResponseBody
    @RequestMapping(value = "/update-phone-Info",method = RequestMethod.POST)
    public Boolean updatePhoneInfo(@RequestParam(name = "userId", required = false) String userId,
    		@RequestParam(name = "confId", required = false) String confId,
    		@RequestParam(name = "type", required = false) boolean type) {    
    	    userIncomingPhoneService.getPhoneRelOrDel(userId,confId,type);
    	    return true;
    }
 
    
    /**
     * 电话管理
     * */
    @ResponseBody
    @RequestMapping(value = "/query-phone-selector",method = RequestMethod.GET)
    public LayPage<SelectorDataVO> queryPhoneSelector(@RequestParam(name = "userId", required = false) String userId) {
    	//List<IncomingPhoneConf> phoneList=userIncomingPhoneService.getAllIncomingPhoneConf().getData();//所有得
    	
    	List<UserIncomingPhoneVO> voList=userIncomingPhoneService.getByNoSelect(userId).getData();//已选中得
    	List<UserIncomingPhoneVO> selPhoneList=userIncomingPhoneService.getIncomingPhoneByUserId(userId).getData();//个人选中得
    	Map<Object, Object>  map=selPhoneList.stream().collect(Collectors.toMap(UserIncomingPhoneVO::getPhoneNum, UserIncomingPhoneVO::getId));
		List<SelectorDataVO> selectorList = new ArrayList<SelectorDataVO>();
		
		for (UserIncomingPhoneVO sysUserWxInfo : voList) {		
			SelectorDataVO selector =new SelectorDataVO();
				  selector.setName(sysUserWxInfo.getPhoneNum()+"("+sysUserWxInfo.getPhoneQcellcore()+")");
			 
					selector.setValue(sysUserWxInfo.getId().toString());
					selector.setUserId(userId);
					//查询是否已分配了人
					
					if(map.get(sysUserWxInfo.getPhoneNum())!=null){
						selector.setSelected(true);
					}	else{
						selector.setSelected(false);
					}		
					selectorList.add(selector);
			 
			
		}
//    	List<SysUserWxInfo> list = iSysUserWxInfoService.queryPage(page).getData().getRecords();
    	
    	
        return this.getLayPage4CustomizeList(selectorList);
    }
    @ResponseBody
    @RequestMapping(value = "del",method = RequestMethod.POST)
//    @RequiresPermissions("sys:dict:basic:del")
    public ResultModel<String> del(@RequestParam("id") String id){
        return iSysUserWxInfoService.delById(id);
    }

    @RequestMapping(value = "detail",method = RequestMethod.GET)
    public String detail(@RequestParam("id")String id, Model model){
        SysUserWxInfo dict = iSysUserWxInfoService.getById(id).getData();
        model.addAttribute("dict", dict);
        return "platform/wxinfo/detail";
    }
}
