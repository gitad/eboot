package com.mos.eboot.admin.platform.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.IWxGroupService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.platform.entity.WxGroup;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 *  数据筛选
 * </p>
 *
 * @author jiangweijie
 * @since 2021-11-23
 */
@Controller
@RequestMapping("/wxGroup") 
public class wxGroupController  extends BaseController {
	 
	@Autowired
	IWxGroupService wxGroupService;
	@Autowired
	ImApiService imApiService;
	@Autowired
	ICustomerService customerService;
	private static String customerId="";
	
	@RequestMapping("/to-page")
	public String toPage() {	 
	   return "platform/wxGroup/wxGroup";
	}
	

	@RequestMapping("/to-page1")
	public String toPage1() {	 
	   return "platform/wxGroup/list";
	}
	
	
	@PostMapping("/query")
	@ResponseBody
	public LayPage<WxGroup> query(Page<WxGroup> page) {	 
		ResultModel<Page<WxGroup>> dataPage=wxGroupService.queryGroup(page);		
		return this.getLayPage(dataPage);
    }
 
	
 
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<Boolean> save(@RequestBody WxGroup  wxGroup) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser(); 
	    ResultModel<Boolean> result=wxGroupService.save(wxGroup);	
	    return result;
	}
	
	@PostMapping("/queryCustList")
	@ResponseBody
	public   List<CustWx>   queryCustList(String wxid) {
		List<CustWx>  list= customerService.getCustomerWxListByWxId(wxid,"").getData();
		for (int i = 0; i < list.size(); i++) {
			CustWx cust=list.get(i);
			SysUserWxInfo user=imApiService.getwxLoginByWxId(cust.getSysWxId());
		    cust.setWxNickname(user.getWxNickname());
		}
	
	    return list;
	}
	
	
	
}
