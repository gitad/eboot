package com.mos.eboot.admin.platform.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.IMenuService;
import com.mos.eboot.admin.platform.api.ISysUserWxInfoService;
import com.mos.eboot.platform.entity.SysMenu;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.MenuNode;

/**
 * @author 小尘哥
 */
@Controller
public class IndexController {

	@Value("${sys.stock.url}")
	private String sotckUrl;
	
	@Value("${sys.stock.phoneUrl}")
	private String phoneSotckUrl;
	
    @Resource
    private IMenuService menuService;

    @Autowired
    private ISysUserWxInfoService iSysUserWxInfoService;

	@Autowired
	private RedisUtil redisUtil;
	
    @GetMapping("index")
    public String index() {
    	
    	//登陆系统界面时 初始化一遍人与微信的管理关系
    	Page<SysUserWxInfo> page1=new Page<SysUserWxInfo>();
    	page1.setSize(200);
    	ResultModel<Page<SysUserWxInfo>> page=	iSysUserWxInfoService.queryPage(page1);
    
    	if(page.getData()!=null){
    		  Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_REL);  
    	 	List<SysUserWxInfo>  list=page.getData().getRecords();
    	    for(SysUserWxInfo li:  list){
    	    	if(StringUtil.isNotBlank(li.getWxId())){
    	    		 map.put(li.getWxId(), li.getUserId());
    	    	}
    	    	
    	    }
    	    redisUtil.hmset(Constants.REDIS_CUST_MANAGER_REL,map,-1);		
    	}
    	
    	
		return "index";
    }
    
    @ResponseBody
    @GetMapping("index/getData")
    public  JSONObject getData(){
    	SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
	    List<MenuNode> menus;
        if (SysUser.SUPER_ID.equals(user.getId())){
			menus = menuService.queryMenus().getData();
		}else{
			menus = menuService.queryByUser(user.getId()).getData();
		}
        List<MenuNode> firstNode = menus.stream().filter(menu -> SysMenu.ROOT_ID.equals(menu.getpId())).collect(Collectors.toList());
        List<MenuNode> otherNode = menus.stream().filter(menu -> !SysMenu.ROOT_ID.equals(menu.getpId())).collect(Collectors.toList());
        firstNode.forEach(item -> {
            List<MenuNode> nodes = item.getChildren();
            for (MenuNode node : otherNode) {
                if (node.getpId().equals(item.getId())) {
                    nodes.add(node);
                }
            }
        });
        JSONObject json=new JSONObject();
        json.put("userName", user.getId());
        json.put("menus", firstNode);
        json.put("sotckUrl",sotckUrl);
        json.put("phoneSotckUrl",phoneSotckUrl);
    	
    	return json;
    }
    @GetMapping("index1")
    public String index1(Model model) {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
	    List<MenuNode> menus;
        if (SysUser.SUPER_ID.equals(user.getId())){
			menus = menuService.queryMenus().getData();
		}else{
			menus = menuService.queryByUser(user.getId()).getData();
		}
        List<MenuNode> firstNode = menus.stream().filter(menu -> SysMenu.ROOT_ID.equals(menu.getpId())).collect(Collectors.toList());
        List<MenuNode> otherNode = menus.stream().filter(menu -> !SysMenu.ROOT_ID.equals(menu.getpId())).collect(Collectors.toList());
        firstNode.forEach(item -> {
            List<MenuNode> nodes = item.getChildren();
            for (MenuNode node : otherNode) {
                if (node.getpId().equals(item.getId())) {
                    nodes.add(node);
                }
            }
        });

		model.addAttribute("userName", user.getId());
		model.addAttribute("menus", firstNode);
		model.addAttribute("sotckUrl",sotckUrl);
		return "index1";
    }


    @GetMapping("welcome")
	public String welcome(Model model) {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		model.addAttribute("userName", user.getUsername());
		return "welcome";
	}

}
