package com.mos.eboot.admin.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICustChatService;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.platform.entity.CustChat;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;

@Controller
@RequestMapping("sys/custchat")
public class CustChatController extends BaseController {

	@Autowired
	private ICustChatService iCustChatService;
	
	@Autowired
	private ICustomerService iCustomerService;

	@GetMapping("/open")
	public String toPage(@RequestParam(name = "cid") String cid, Model model) {
		Customer cust= iCustomerService.getCustomerById(cid).getData();
		
		model.addAttribute("cid", cid);
		model.addAttribute("data", cust);
		return "im/cust_chat_window";
	}

	@ResponseBody
	@RequestMapping("/query/cid")
	public LayPage<CustChat> queryPageByCid(@RequestBody Page<CustChat> page, @RequestParam(name = "cid") String cid) {
		ResultModel<Page<CustChat>> rs = iCustChatService.queryList(page, Integer.parseInt(cid));
		return getLayPage(rs);
	}

	@ResponseBody
	@RequestMapping("/query/list")
	public LayPage<CustChat> queryPage(@RequestBody Page<CustChat> page) {
		ResultModel<Page<CustChat>> rs = iCustChatService.queryList(page);
		return getLayPage(rs);
	}
	
	@PostMapping("/save")
	public ResultModel<String> save(@RequestBody CustChat custChat){
		iCustChatService.saveCustChat(custChat);
		return ResultModel.defaultSuccess("成功");
	}

}
