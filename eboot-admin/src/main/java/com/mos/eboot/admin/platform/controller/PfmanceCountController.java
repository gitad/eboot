package com.mos.eboot.admin.platform.controller;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.admin.platform.api.PfmanceCountService;
import com.mos.eboot.tools.controller.BaseController;

/**
 * <p>
 *   业绩统计
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-09
 */
@Controller
@RequestMapping("/pfManceCount")
public class PfmanceCountController extends BaseController {
	
	@Autowired
	PfmanceCountService pfmanceCountService;
	
	@Autowired
	ImApiService imApiService;
	
	@Autowired
	ISysUserService iSysUserService;
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		return "platform/pfManceCount";
	}
	
	@PostMapping("/query")
	@ResponseBody
	public List<JSONObject> query(@RequestBody Map<String, String> map) {		 
	 
		 List<JSONObject>  dataPage=pfmanceCountService.queryCount(map);		
		return dataPage;
    }
 
}
