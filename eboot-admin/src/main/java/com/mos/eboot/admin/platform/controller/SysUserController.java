package com.mos.eboot.admin.platform.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.IRoleService;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.ISysUserWxInfoService;
import com.mos.eboot.admin.platform.api.IUserIncomingPhoneService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.SysRole;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.result.ResultStatus;
import com.mos.eboot.tools.shiro.utils.PasswordHelper;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.FileUtil;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.SysUserVO;
import com.mos.eboot.vo.UserIncomingPhoneVO;

/**
 * @author 小尘哥
 */
@Controller
@RequestMapping("sys/user")
public class SysUserController extends BaseController {

    @Resource
    private ISysUserService sysUserService;

    @Resource
    private IRoleService roleService;
    
    @Autowired
    private ISysUserWxInfoService iSysUserWxInfoService;
    
    @Autowired
    private ImApiService imApiService;
    
	@Autowired
	IUserIncomingPhoneService userIncomingPhoneService;
    @RequestMapping("to-page1")
    public String toPage1() {
        return "platform/user/list";
    }
    @RequestMapping("to-page")
    public String toPage() {
        return "platform/user/user";
    }
    @RequestMapping("to-edit")
    @RequiresPermissions(value = {"sys:user:add", "sys:user:edit"}, logical = Logical.OR)
    public String toEdit(@RequestParam(name = "id", required = false) String id, Model model) {
        SysUser user = sysUserService.getById(id).getData();
        if (user == null) {
            user = new SysUser();
        }
        model.addAttribute("user", user);
        List<SysRole> roles = roleService.queryList(new SysRole()).getData();
        model.addAttribute("roles", roles);
        return "platform/user/edit";
    }

    @RequestMapping("add") 
    @ResponseBody
    public  SysUser add(@RequestParam(name = "id", required = false) String id) {
        SysUser user = sysUserService.getById(id).getData();
        if (user == null) {
            user = new SysUser();
        }
        //权限
        List<SysRole> roles = roleService.queryList(new SysRole()).getData();            
        user.setRoles(roles);
        if(user.getRoleIds()==null)
        {
        	List<String> roleIds=new ArrayList<String>();
        	 user.setRoleIds(roleIds);
        }
        //微信
        List<SysUserWxInfo> allWxList = imApiService.getAllWxLogin();     
        for (SysUserWxInfo sysUserWxInfo : allWxList) {			    		 
			//查询是否已分配了人
			SysUserWxInfo wxInfo=iSysUserWxInfoService.getWxInfoByUidAndWxid("",sysUserWxInfo.getWxId());
			 if(wxInfo==null){//代表未分配
				 sysUserWxInfo.setChceked(false);
			 }else{
				 if(wxInfo.getUserId().equals(id)){
					 sysUserWxInfo.setChceked(false); 
				 }else{
					 sysUserWxInfo.setChceked(true); 
				 }
				
			 }
		}
        List<SysUserWxInfo> li= iSysUserWxInfoService.getByUserId(user.getId()).getData();
        user.setUserWxInfo(li);
        user.setUserWxInfoAll(allWxList);
        
        
        //电话
    	List<UserIncomingPhoneVO> voList=userIncomingPhoneService.getByNoSelect(user.getId()).getData();//已选中得
    	List<UserIncomingPhoneVO> owPhoneList=userIncomingPhoneService.getIncomingPhoneByUserId(user.getId()).getData();//个人选中得
		if(voList!=null){
			for(UserIncomingPhoneVO vo:voList ){
				List<UserIncomingPhoneVO> selPhoneList=userIncomingPhoneService.getIncomingPhoneByConfId(vo.getId().toString()).getData();//个人选中得
				if(selPhoneList.size()>0){
					vo.setChecked(true);
				}else{
					vo.setChecked(false);
				}
			}
		}
    	
    	user.setUserPhone(owPhoneList);
    	user.setUserPhoneAll(voList);  
        return user;
    }

   
    
    @ResponseBody
    @PostMapping("/user-syn")
    public Boolean  userSyn() {
        List<SysUser> user = sysUserService.getTblAllUser();
        Boolean result=true;
        try {
        	for(SysUser us:user){//对查询的人进行校验  系统中有则进行更新  没有则进行覆盖
            	SysUser sysuser=sysUserService.getByUsername(us.getUsername());      	
            	if(sysuser!=null){//说明存在用户 进行更新
            		sysuser.setPassword(new PasswordHelper().encrypt(us.getPassword()));
            		 sysUserService.saveOrUpdate(sysuser);
            		 
            	}else{//新用户 创建
            		 
            		//sysuser.setId(UuidUtil.get32UUID());;//主键
            		us.setPassword(new PasswordHelper().encrypt(us.getPassword()));
            		us.setIsDel(Constants.NEGATIVE);
            		us.setCreateTime(new Date());
            		us.setDisabled(Constants.NEGATIVE);
            		us.setLocked(Constants.NEGATIVE);
            		 sysUserService.saveOrUpdate(us);
            	}
        	}	
            	 
		} catch (Exception e) {
			result=false;
		}
        
        	
        return result;
      
       
    }

    
    
    @ResponseBody
    @RequestMapping("save-or-update")
    public ResultModel<String> save(@RequestBody  SysUser sysUser) {
      //  this.validError(result);

        SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();

        if (StringUtil.isBlank(sysUser.getId())) {
            sysUser.setCreateTime(new Date());
            sysUser.setCreateUser(currentUser.getId());
            sysUser.setDisabled(Constants.NEGATIVE);
            sysUser.setIsDel(Constants.NEGATIVE);
            String pwd = StringUtil.isBlank(sysUser.getPassword()) ? SysUser.DEFAULT_PWD : sysUser.getPassword();
            sysUser.setPassword(new PasswordHelper().encrypt(pwd));
            sysUser.setLocked(Constants.NEGATIVE);
        }
        return sysUserService.saveOrUpdate(sysUser);
    }
    
    @ResponseBody
    @RequestMapping("savePassword")
    public ResultModel<String> savePassword(@RequestBody  SysUser sysUser) {
      //  this.validError(result);

    	SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
        	String pwd = StringUtil.isBlank(sysUser.getPassword()) ? SysUser.DEFAULT_PWD : sysUser.getPassword();
            sysUser.setPassword(new PasswordHelper().encrypt(pwd));
         return sysUserService.saveOrUpdate(sysUser);
    }
    @ResponseBody
    @RequestMapping("query-page")
    public LayPage<SysUserVO> queryPage(Page<SysUser> page) {
    	Page<SysUser> user =sysUserService.queryPage(page).getData();
    	List<SysUser> list =user.getRecords();
    	
    	
    	List<SysUserVO> vo=BeanCopyUtil.copyListProperties(list, SysUserVO::new);
    	
    	for (SysUserVO sysUser : vo) {
    		
    		List<SysUserWxInfo> allWxList = imApiService.getAllWxLogin();
    	/*	for (SysUserWxInfo sysUserWxInfo : allWxList) {			    		 
    			//查询是否已分配了人
    			SysUserWxInfo wxInfo=iSysUserWxInfoService.getWxInfoByUidAndWxid(sysUser.getId(),sysUserWxInfo.getWxId());
    			 if(wxInfo!=null){//代表存在
    				 allWxList.remove(sysUserWxInfo);
    			 }
    		}*/
    		List<SysUserWxInfo> li= iSysUserWxInfoService.getByUserId(sysUser.getId()).getData();
    		sysUser.setUserWxInfo(li);
    		sysUser.setUserWxInfoAll(allWxList);
    		
    		
		} 
    	Page<SysUserVO> voPage = new Page<>(user.getCurrent(),user.getSize(),user.getOrderByField());
    	voPage.setRecords(vo);
    	voPage.setCondition(user.getCondition());
    	voPage.setTotal(user.getTotal());
		ResultModel<Page<SysUserVO>> model = new ResultModel<>();
		model.setData(voPage);
		model.setCode(ResultStatus.SUCCESS.getCode());
		model.setMsg(ResultStatus.SUCCESS.getMsg());
        return this.getLayPage(model);
    }


    
 
    
    @ResponseBody
    @RequestMapping("del")
    @RequiresPermissions("sys:user:del")
    public ResultModel<String> del(@RequestParam("id") String id) {
        return sysUserService.deleteById(id);
    }

    @RequestMapping("detail")
    public String detail(@RequestParam("id") String id, Model model) {
        SysUser user = sysUserService.getById(id).getData();
        model.addAttribute("user", user);
        return "platform/user/detail";
    }

    @RequestMapping("export")
    public void export(SysUser user, HttpServletResponse response) {
        List<SysUser> list = sysUserService.all(user).getData();
        FileUtil.exportExcel(list, "用户统计", "用户统计", SysUser.class, "user.xls", response);
    }
    
    @RequestMapping("allUser")
    @ResponseBody
    public List<SysUser> allUser(SysUser user, HttpServletResponse response) {
        List<SysUser> list = sysUserService.all(user).getData();
        return list;
    }
}
