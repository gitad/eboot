package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCargoInfoService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.ImMsgConversationListVO;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cCargoInfo")
public class CCargoInfoController extends BaseController {
	
	 @Autowired
	 ICCargoInfoService cCargoInfoService;
	 
		@Autowired
		ISysEbootLogService iSysEbootLogService;
		
		
	 @Autowired
	 private RedisUtil redisUtil;
	 
	 private static String customerId="";

	 private static String phone="";
	 private static String wxid="";
	 @RequestMapping("to-page")
	 public String toPage(String cid,String phoneNum,String wxid) {
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
    	 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			this.phone=StringUtil.str_Phone(phoneNum); 
			this.wxid=wxid; 
			/*List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				if(coV.getSessionId().equals(sid)){
					//有电话匹配电话  没有电话匹配当前微信号
					phone=StringUtil.str_Phone(coV.getCustPhone());
					if(StringUtil.isBlank(phone)){
						 
					}
					break;
				}
			}*/
		 }
	    return "platform/cargoinfo/cargoinfo";
	 }
	 @RequestMapping("to-page1")
	 public String toPage1(String cid,String sid) {
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
    	 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				if(coV.getSessionId().equals(sid)){
					phone=StringUtil.str_Phone(coV.getCustPhone());
					break;
				}
			}
		 }
	    return "platform/cargoinfo/list";
	 }
	
	 @PostMapping("/query")
	 @ResponseBody
	 public LayPage<CCargoInfo> query(Page<CCargoInfo> page) {  
		 Map<String,Object> condition=new HashMap<String, Object>(); 	
		/* if(customerId.equals("-1")){
			condition.put("phone", phone);
		}*/
		 condition.put("customerId", customerId);
		 page.setCondition(condition);
		 ResultModel<Page<CCargoInfo>> dataPage=cCargoInfoService.query(page);		
		return this.getLayPage(dataPage);
    }
	 @PostMapping("/add")
	 @ResponseBody
	 public  CCargoInfo  add( ) {  
		 CCargoInfo cargoInfo=new CCargoInfo();
		 if(StringUtil.isNotBlank(phone)){
			 cargoInfo.setPhone(phone);
		 }else  if(StringUtil.isNotBlank(wxid)){
			 cargoInfo.setPhone(wxid);
		 }
		return cargoInfo;
    }
	@PostMapping("/cCargoQuery")
	@ResponseBody
	public LayPage<CCargoEntry> query(Page<CCargoEntry> page,String address) {	
		//车讯货
		if(address!=null){				 
			Map<String, Object>	map=new HashMap<String, Object>();
			map.put("address", address);
			page.setCondition(map);
		}			
		ResultModel<Page<CCargoEntry>> dataPage=cCargoInfoService.cCargoQuery(page);		
		return this.getLayPage(dataPage);
    }
	 
	 
	 @PostMapping("/save")
	 @ResponseBody
	 public  CCargoInfo save(@RequestBody CCargoInfo cCargoInfo) {  
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
    	 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 if(cCargoInfo.getCargoInfoId()==null){
			 cCargoInfo.setCreateUser(currentUser.getNickname());
			 cCargoInfo.setCreateTime(new Date());  
			 cCargoInfo.setUseCar(0);
			 cCargoInfo.setUpdateUser(currentUser.getNickname());
			 cCargoInfo.setUpdateTime(new Date()); 
		 } else{
			 cCargoInfo.setUpdateUser(currentUser.getNickname());
			 cCargoInfo.setUpdateTime(new Date()); 
		 }
		 if(StringUtil.isNotBlank(customerId)){
			 cCargoInfo.setCustomerId(customerId);
		 }
		
	
		 if(customerId.equals("-1")){
			 cCargoInfo.setPhone(phone);	
		 } 
		 CCargoInfo cr= cCargoInfoService.save(cCargoInfo);
		  if(StringUtil.isNotBlank(cCargoInfo.getCargoInfoId())){
				 
			  iSysEbootLogService.save( SysEbootLogController.insert(cr.getCargoInfoId(), "c_cargo_info", "update", cr.toString(),cr.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cr.getCargoInfoId(), "c_cargo_info", "add", cr.toString(),cr.getCustomerId())) ;
		  }
		 return cr;
    }

	 //通过货源调度页面保存
	 @PostMapping("/sourceSave")
	 @ResponseBody
	 public  CCargoInfo sourceSave(@RequestBody CCargoInfo cCargoInfo) {  
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
    	 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 cCargoInfo.setUpdateUser(currentUser.getNickname());
		 cCargoInfo.setUpdateTime(new Date()); 
	 
		 return cCargoInfoService.sourceSave(cCargoInfo);
    }

}
