package com.mos.eboot.admin.platform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.admin.platform.api.ISysUserCustMgrService;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 * 系统用户管理客户表 前端控制器
 * </p>
 *
 * @author zhang
 * @since 2021-06-14
 */
@Controller
@RequestMapping("sys/user/cust")
public class SysuserCustomerMgrController {

	@Autowired
	private ISysUserCustMgrService iSysUserCustMgrService;
	
	public ResultModel<String> addCust(@RequestParam(name = "cids") String cids,
			@RequestParam("userId") String userId) {
		iSysUserCustMgrService.addUserCust(cids, userId);
		return ResultModel.defaultSuccess("");
	}
}
