package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@FeignClient("boot-service")
public interface ICContactPeoService   {
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/query")
	List<CContactPeo>  query(@RequestParam("cid") String cid);
	
	/**号码校验
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/checkByPhone")
	ResultModel<String> checkByPhone(@RequestParam("phone") String phone,@RequestParam("customerId")  String  customerId);
	
	
	/**主键查询
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/queryById")
    CContactPeo  queryById(@RequestParam("contactId") String contactId);
	
	/**数据查询
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/queryByEntity")
    CContactPeo  queryByEntity(@RequestBody  CContactPeo cContactPeo);
	
	
	/**customerId查询
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/queryByCustomerId")
	 List<CContactPeo>  queryByCustomerId(@RequestParam("customerId") String customerId);
	
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/save")
	ResultModel<Boolean> save(@RequestBody  CContactPeo cContactPeo);
	
	/**更新
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/update")
	ResultModel<Boolean> update(@RequestBody  CContactPeo cContactPeo);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cContactPeo/del")
	ResultModel<Boolean>del(  @RequestParam("contactId") String contactId);
	
 
}
