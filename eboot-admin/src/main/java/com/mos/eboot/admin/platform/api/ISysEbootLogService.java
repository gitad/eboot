package com.mos.eboot.admin.platform.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.mos.eboot.platform.entity.SysEbootLog;


/**
* @author jiangweijie
* @date   2021/09/29 
* @remark  日志记录工具类
* 
*/
@FeignClient("boot-service")
public interface ISysEbootLogService {
	
	
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("sysEbootLog/queryPage")
	public  List<SysEbootLog> queryPage(@RequestBody SysEbootLog sysEbootLog);

	
	
	/**  日志数据插入
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("sysEbootLog/save")
	public SysEbootLog save(@RequestBody  SysEbootLog sysEbootLog  );
 
}
