package com.mos.eboot.admin.platform.controller;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICContactPeoRelService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-02
 */
@Controller
@RequestMapping("/cContactPeoRel")
public class CContactPeoRelController  extends BaseController {
	 @Autowired
	 private ICContactPeoRelService iCContactPeoRelService;
 
	 @Autowired
	 ISysEbootLogService iSysEbootLogService;
		
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CContactPeoRel> query(Page<CContactPeoRel> page) {
		Map<String,Object> condition= page.getCondition();
		EntityWrapper<CContactPeoRel>  Wrapper=new EntityWrapper<CContactPeoRel>();
		 
		ResultModel<Page<CContactPeoRel>> dataPage=iCContactPeoRelService.query(page);		
		return this.getLayPage(dataPage);
    }
 
	
	@PostMapping("/save")
	@ResponseBody
	public   CContactPeoRel save(@RequestBody  CContactPeoRel  cContactPeoRel) {
		CContactPeoRel ic=iCContactPeoRelService.save(cContactPeoRel);
		  if(StringUtil.isNotBlank(cContactPeoRel.getContactRelId())){
				 
			  iSysEbootLogService.save( SysEbootLogController.insert(ic.getContactRelId(), "c_contact_peo_rel", "update", ic.toString(),ic.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(ic.getContactRelId(), "c_contact_peo_rel", "add", ic.toString(),ic.getCustomerId())) ;
		  }
	    return ic;
	}
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestBody  CContactPeoRel  cContactPeoRel) {
		
	    ResultModel<Boolean> result=iCContactPeoRelService.del(cContactPeoRel);
		  iSysEbootLogService.save( SysEbootLogController.insert(cContactPeoRel.getContactRelId(), "c_contact_peo_rel", "del", cContactPeoRel.toString(),cContactPeoRel.getCustomerId())) ;
			 
	    return result;
	}
	
	@PostMapping("/queryById")
	@ResponseBody
	public  CContactPeoRel queryById(@RequestParam("contactRelId")  String  contactRelId) {
		CContactPeoRel  result=iCContactPeoRelService.queryById(contactRelId);	
		   return result;
	}
	
	@PostMapping("/queryByContactId")
	@ResponseBody
	public  List<CContactPeoRel> queryByContactId(@RequestParam("contactId")  String  contactId) {
		List<CContactPeoRel>  result=iCContactPeoRelService.queryByContactId(contactId);	
		   return result;
	}

}
