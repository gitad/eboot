package com.mos.eboot.admin.platform.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICOrderService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.COrder;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 * 订单表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-02-24
 */
@Controller
@RequestMapping("/cOrder")
public class COrderController extends BaseController {
	
	@Autowired
	ICOrderService iCOrderService;
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	@Autowired
	ImApiService imApiService;
	private static String customerId="";
	private static String phone="";
	
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		if(cid!=null){
			this.customerId=cid; 
		}
	   return "platform/cOrder";
	}
	
	@PostMapping("/query")
	@ResponseBody
	public LayPage<COrder> query(Page<COrder> page) {		 
		Map<String,Object> condition=page.getCondition(); 
		if(condition==null){
			condition=new HashMap<String, Object>();
		}
		//condition.put("customerId", customerId);
		 
		page.setCondition(condition);
		ResultModel<Page<COrder>> dataPage=iCOrderService.query(page);		
		return this.getLayPage(dataPage);
    }
	
	@PostMapping("/queryByCargoInfoId")
	@ResponseBody
	public COrder  queryByCargoInfoId( @RequestParam("cargoInfoId") String   cargoInfoId) {		 
		 
		COrder cOrder =iCOrderService.queryByCargoId(cargoInfoId);		
		return cOrder==null?new COrder():cOrder ;
    }
	
	@PostMapping("/queryDetail")
	@ResponseBody
	public  JSONObject queryDetail(@RequestParam("orderId") String orderId) {		 
		JSONObject json=iCOrderService.queryDetail(orderId);	
		return  json;
    }
	
	@PostMapping("/save")
	@ResponseBody
	public  COrder save(@RequestBody COrder  cOrder) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		if(cOrder.getOrderId()==null){
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	        String newDate=sdf.format(new Date());
			cOrder.setOrderNum(newDate);
			cOrder.setCreatePeo(user.getNickname());
			cOrder.setCreateTime(new Date());
			cOrder.setCreateId(user.getId());
			cOrder.setOrderStates("待确认");
			cOrder.setPayFreNumStates("待付款");
			cOrder.setPayStates("待付款");
			cOrder.setPayFreNumStates("待付款");
			 
		}else{
			cOrder.setUpdatePeo(user.getNickname());
			cOrder.setUpdateTime(new Date());
		}
		COrder cr=iCOrderService.save(cOrder);	
		  if(StringUtil.isNotBlank(cOrder.getOrderId())){
				 
			  iSysEbootLogService.save( SysEbootLogController.insert(cr.getOrderId(), "c_order", "update", cr.toString(),null)) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cr.getOrderId(), "c_order", "add", cr.toString(),null)) ;
		  }
		return  cr;
    }
	
	
	//获取微信所有用户
	@PostMapping("/getAllWxLogin")
	@ResponseBody
	public List<SysUserWxInfo> getAllWxLogin(){
		return imApiService.getAllWxLogin();
	}
}
