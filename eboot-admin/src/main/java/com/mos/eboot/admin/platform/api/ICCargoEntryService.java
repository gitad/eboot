package com.mos.eboot.admin.platform.api;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCargoEntry;
import com.mos.eboot.platform.entity.CCargoEntryRel;
import com.mos.eboot.platform.entity.CCargoInfo;
import com.mos.eboot.tools.result.ResultModel;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-04
 */
@FeignClient("boot-service")
public interface ICCargoEntryService  {
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoEntry/query",method = RequestMethod.POST)
	ResultModel<Page<CCargoEntry>> query(@RequestBody Page<CCargoEntry> page);
	 
	 
	/**主键查询
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoEntry/queryById",method = RequestMethod.POST)
	 CCargoEntry  queryById(@RequestParam("cargoId") String cargoId);
	 
		 
	/**主表保存
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoEntry/saveCCargoEntry",method = RequestMethod.POST)
	 CCargoEntry  saveCCargoEntry(@RequestBody CCargoEntry  cargoEntry);
	 
	 
	 /**from 列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoEntry/queryForm",method = RequestMethod.POST)
	ResultModel<List<CCargoEntryRel>> queryForm(@RequestParam("cargoId") String cargoId,@RequestParam("type") String type);
 
	
	 /**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCargoEntry/query-source",method = RequestMethod.POST)
	 ResultModel<Page<CCargoEntry>> querySource(@RequestBody Page<CCargoEntry> page);
 
		
	 
	/**查询历史报价
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCargoEntry/queryPirce")
	ResultModel<List<CCargoInfo>> queryPirce(@RequestParam Map<String, String> map);
	
	 
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCargoEntry/save")
	ResultModel<Boolean> save(@RequestParam Map<String, String> map);
	
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCargoEntry/saveAll")
	ResultModel<CCargoEntry> saveAll(@RequestParam Map<String, String> map);
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCargoEntry/del")
	ResultModel<Boolean>del(@RequestParam("cargoId") String cargoId);
	
}
