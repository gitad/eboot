package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ICCustomerMarketService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 * 客户营销表 前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
@Controller
@RequestMapping("/cCustomerMarket")
public class CCustomerMarketController  extends BaseController {
	@Autowired
	ICCustomerMarketService customerMarketService;
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	
	private static String customerId="";
	private static String phone="";
	
	
	@RequestMapping("to-page")
	public String toPage( String cid,String phoneNum,Model model) {
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			this.phone=StringUtil.str_Phone(phoneNum); 
		}
	   return "platform/customerMarket";
	}
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CCustomerMarket> query(Page<CCustomerMarket> page) {		 
		Map<String,Object> condition=new HashMap<String, Object>(); 	
		 condition.put("customerId", customerId);
	 
		 page.setCondition(condition);
		ResultModel<Page<CCustomerMarket>> dataPage=customerMarketService.query(page);		
		return this.getLayPage(dataPage);
    }
	
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<CCustomerMarket> save(@RequestBody CCustomerMarket  customerMarket) {
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
			if(StringUtils.isNotBlank(customerMarket.getMarketId())){		
				 customerMarket.setUpdateTime(new Date());
				 customerMarket.setUpdateUser(currentUser.getNickname());
			}
		
		 
		  ResultModel<CCustomerMarket> result=customerMarketService.save(customerMarket);	
		  CCustomerMarket log=result.getData();
		  if(StringUtil.isNotBlank(customerMarket.getMarketId())){
			 
			  iSysEbootLogService.save( SysEbootLogController.insert(log.getMarketId(), "c_customer_market", "update", log.toString(),log.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(log.getMarketId(), "c_customer_market", "add", log.toString(),log.getCustomerId())) ;
		  }
		 
		 //  iSysEbootLogService.save(new SysEbootLog());
	    return result;
	}
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestBody  CCustomerMarket  customerMarket) {
	    ResultModel<Boolean> result=customerMarketService.del(customerMarket);
	    iSysEbootLogService.save( SysEbootLogController.insert(customerMarket.getMarketId(), "c_customer_market", "del", customerMarket.toString(),customerMarket.getCustomerId())) ;
	    return result;
	}
	
	
	@PostMapping("/add")
	@ResponseBody
	public CCustomerMarket  add() {
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 CCustomerMarket  customerMarket=new CCustomerMarket();
		 customerMarket.setUseFlag(0);
		 customerMarket.setCustomerId(customerId);
		 customerMarket.setMarketPhone(phone);
		 customerMarket.setCreateTime(new Date());
		 customerMarket.setCreateUserId(currentUser.getId());
		 customerMarket.setCreateUser(currentUser.getNickname());
		return customerMarket;
	}
	
}
