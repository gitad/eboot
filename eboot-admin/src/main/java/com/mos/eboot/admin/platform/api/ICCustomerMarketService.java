package com.mos.eboot.admin.platform.api;

 
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCustomerMarket;
import com.mos.eboot.tools.result.ResultModel;

/**
 * <p>
 * 客户营销表 服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-29
 */
@FeignClient("boot-service")
public interface ICCustomerMarketService   {
	
	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cCustomerMarket/query",method = RequestMethod.POST)
	ResultModel<Page<CCustomerMarket>> query(@RequestBody Page<CCustomerMarket> page);
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCustomerMarket/save")
	ResultModel<CCustomerMarket> save( @RequestBody CCustomerMarket customerMarket);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCustomerMarket/del")
	ResultModel<Boolean> del(@RequestBody CCustomerMarket customerMarket);
		
}
