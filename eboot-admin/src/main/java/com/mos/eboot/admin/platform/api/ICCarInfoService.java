package com.mos.eboot.admin.platform.api;

import java.util.List;
import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.tools.result.ResultModel;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

 
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-03
 */
@FeignClient("boot-service")
public interface ICCarInfoService  {

 
	 @RequestMapping(value = "cCarInfo/query",method = RequestMethod.POST)
	  ResultModel<List<CCarInfo>> query(@RequestBody Page<CCarInfo> page);
	 
	 
	 
	@PostMapping("cCarInfo/relSave")
	CCarInfoRel  relSave( @RequestBody CCarInfoRel  cCarInfoRel);
	
	
	@PostMapping("cCarInfo/queryById")
	CCarInfo  queryById(@RequestParam("carId") String carId);
	
	@PostMapping("cCarInfo/queryByCustomerId")
	List<CCarInfo>  queryByCustomerId(@RequestParam("customerId") String customerId);
	

	 
	@PostMapping("cCarInfo/save")
	CCarInfo  save( @RequestBody  CCarInfo  cCarInfo);
	
	@PostMapping("cCarInfo/del")
	CCarInfo  del( @RequestBody  CCarInfo  cCarInfo);
	
	 
	@PostMapping("cCarInfo/addRel")
	CCarInfoRel  addRel( @RequestBody  CCarInfoRel  cCarInfoRel);
	
	
	 
	@PostMapping("cCarInfo/relDel")
	CCarInfoRel  relDel( @RequestBody  CCarInfoRel  cCarInfoRel);
	
	
	/**数据查询
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cCarInfo/queryByEntity")
	CCarInfo  queryByEntity(@RequestBody  CCarInfo  cCarInfo);
	
}
