package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.SysDocFileService;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.SysDocFile;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jiangweijie
 * @since 2022-03-02
 */
@Controller
@RequestMapping("/sysDocFile")
public class SysDocFileController {
	
	@Value("${minio.url}")
	private String endpoint;
	  
	@Autowired
	SysDocFileService sysDocFileService;
	
	
	/**
	 * businessId  业务id
	 * businessTable 业务表
	 * businessType  业务类型 自定义
	 * */	
	@PostMapping("/query")	 
	@ResponseBody
	public List<SysDocFile> query(String businessId,String businessTable,String businessType) {	
		List<SysDocFile> list=sysDocFileService.query(businessId, businessTable, businessType);
		 return list ;
    }
	
	
	
	@PostMapping("/save")	
	@ResponseBody
	public ResultModel<Boolean> save(@RequestBody SysDocFile sysDocFile) {	
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 sysDocFile.setUrl(endpoint+sysDocFile.getName());
		 sysDocFile.setCreateTime(new Date());
		 sysDocFile.setCreateUser(user.getNickname());
		 sysDocFile.setUseFlag(1);
		 return sysDocFileService.save(sysDocFile);
    }
	
	@PostMapping("/del")	
	@ResponseBody
	public ResultModel<Boolean> del(@RequestBody SysDocFile sysDocFile) {	
 
		 return sysDocFileService.del(sysDocFile);
    }
	
}
