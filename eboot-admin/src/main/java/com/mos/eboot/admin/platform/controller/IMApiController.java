package com.mos.eboot.admin.platform.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.ISysUserWxInfoService;
import com.mos.eboot.admin.platform.api.IUserIncomingPhoneService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.HisWxMsg;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.CustWxInfoVO;
import com.mos.eboot.vo.ImMsgConversationListVO;

/**
 */
@RestController
@RequestMapping("imapi")
public class IMApiController extends BaseController {

	@Autowired
	private ImApiService imApiService;
	
	@Autowired
	private ISysUserWxInfoService iSysUserWxInfoService;
	
	@Autowired
	private IUserIncomingPhoneService iUserIncomingPhoneService;

	@Autowired
	private ICustomerService iCustomerService;

	@Autowired
	private RedisUtil redisUtil;
	

 	
	
	/**
	 * 获取好友(群组)列表(聊天接口请求)
	 * 
	 * @param
	 * @throws Exception
	 */
	@GetMapping(value = "/getList")
	public Object getList() throws Exception {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		return imApiService.getList(user.getUsername(),user.getId());
	}

	/**
	 * 获取群成员接口
	 * 
	 * @return
	 */
	@RequestMapping(value = "/getMembers")
	public Object getMembers() {
		return imApiService.getMembers();
	}

	/**
	 * 聊天上传文件接口
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/upload/file")
	public Object updateFile(@RequestParam(required = false) MultipartFile file) throws Exception {
		return "";
	}

	/**
	 * 聊天上传图片接口
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/upload/img")
	public Object uploadImg(@RequestParam(required = false) MultipartFile file) throws Exception {
		return "";
	}

	/**
	 * 修改状态(在线隐身、个性签名、皮肤)接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/editState")
	public Object editState() throws Exception {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		return imApiService.editState(user.getUsername());
	}

	/**
	 * 消息盒子未读消息总数接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/getMsgCount")
	public Object getMsgCount() throws Exception {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		return imApiService.getMsgCount(user.getUsername());
	}

	/**
	 * 获取未读消息(离线消息)接口
	 * 
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/getNoreadMsg")
	public Object getNoreadMsg() throws Exception {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		return imApiService.getNoreadMsg(user.getUsername());
	}
  
	/**
	 * 发送好友文本消息
	 * 
	 * @param type
	 * @param content
	 * @param wxId
	 * @param clientId
	 * @return
	 */
	@PostMapping(value = "/send")
	public ResultModel<String> send(@RequestParam(value = "content",required = false) String content,
			@RequestParam(value = "wxid",required = false) String wxid,
			@RequestParam(value = "sysWxId",required = false) String sysWxId,
			@RequestParam(value = "cid",required = false) String cid,
			@RequestParam(value = "sid",required = false) String sid) {
		//wxid 发送人  syswxid 收信
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();	 
		  if(user==null){
			  user= WeChatController.sysUser;
		  }
		List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + user.getId(), 0, -1);	
		ImMsgConversationListVO conversationListVO=null;
		String mesContent=null;
		try {
		 
		
			if(StringUtils.isNotBlank(sid)&&StringUtils.isBlank(cid)){//sid不为空  cid为空 代表群发				
				 Integer k=0;				
				for (int i=0;i<list.size();i++) {
					conversationListVO = (ImMsgConversationListVO)list.get(i);					
					if (conversationListVO.getSessionId().equals(sid)) {	
						conversationListVO.setLastMsg(content);
						redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + user.getId(), i, conversationListVO);
						
								List<JSONObject> custList=conversationListVO.getCustJsonList();
									if(custList!=null&&custList.size()>0){						
										for(JSONObject cu:custList){
													
											 ResultModel<List<CustWx>>  result=iCustomerService.getCustomerWxByCustId(cu.getString("id"),"");
											 List<CustWx> custWxList =result.getData();
											 if(custWxList!=null&&custWxList.size()>0){
												 for(CustWx custWx:custWxList){
													 if(k!=0){//为了保证历史记录只有一条消息
														  imApiService.send_v1(content,custWx.getWxId(),custWx.getSysWxId(),cu.getString("id"),"");
													 }else{
														  imApiService.send_v1(content,custWx.getWxId(),custWx.getSysWxId(),cu.getString("id"),sid);
													 }										
													 	k++;
												 }
											 }
										}
									}
						
						 
						break;
					}
				}
			}else{    
				Object a = imApiService.send_v1(content,wxid,sysWxId,cid,"");
				for (int i=0;i<list.size();i++) {
					conversationListVO = (ImMsgConversationListVO)list.get(i);					
					if (conversationListVO.getSessionId().equals(sid)) {	
						conversationListVO.setLastMsg(content);
						conversationListVO.setWxWarm(false);//超两小时未回
						conversationListVO.setTrans(false);//转交
						conversationListVO.setWxNoReadNum(0);
						conversationListVO.setIfDealWith(true);
						conversationListVO.setGenjin(false);
						redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + user.getId(), i, conversationListVO);
						break;
					}
				}
				
			}
			
			
			
		    return ResultModel.defaultSuccess("发送成功");
		
		} catch (Exception e) {
			//插入日志
			mesContent=new SimpleDateFormat("YYYY/MM/dd HH:mm:ss.SSS").format(new Date())+"--发送出现异常"+content; 
		 
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 获取来电弹屏账户
	 * 
	 * @param
	 * @throws Exception
	 */
	@GetMapping(value = "/incomming/phone/list")
	public Object getIncomingPhoneConf() throws Exception {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		return iUserIncomingPhoneService.getIncomingPhoneByUserId(user.getId());
	}
	
	@PostMapping(value="/incomming/phone/savelog")
	public Object savePhoneCallLog(String data) {
		SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		JSONObject dataJson = JSONObject.parseObject(data);

		String phone=dataJson.getString("phone");
		String type=dataJson.getString("type");
		String status=dataJson.getString("status");//1成功 2.外拨失败 3.对方未接 4.号码错误 5.拒接 6.通话中 8.来电未接	 	
		//存在cid代表去电  cid为空代表来电
		List<Object> redisList= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + user.getId(), 0, -1);//构建人员聊天关系
		  if(redisList.size()>0){
			 for (int i=0;i<redisList.size(); i++) {
				   ImMsgConversationListVO cvo = (ImMsgConversationListVO) redisList.get(i);
				   if(StringUtil.isNotBlank(cvo.getCustPhone())){
					   if(cvo.getCustPhone().equals(phone)){
					    	 if( status.equals("8")){
							   cvo.setPhoneStatus(status);
							   cvo.setPhonetype("0");
							   cvo.setPhoneNoReadNum(cvo.getPhoneNoReadNum()+1);
					    	 }  
					    	 if( status.equals("1")){
								   cvo.setPhoneStatus("0");
								   cvo.setPhonetype("0");
								   cvo.setPhoneNoReadNum(0);
					         }   
							   redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + user.getId(),i, cvo);
						   
					   }
				   }
			 }
		 }
		
		
		//根据websocket返回的状态 去更新电话实时状态
		 Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_PHONE_LIST);  
		 if(map.get(user.getId())!=null){
			 List<JSONObject> list=JSONArray.parseArray(map.get(user.getId()).toString(), JSONObject.class);
			 for(JSONObject js:list){
				 if(js.get("phone").equals(phone)){
					 js.put("status",status);
				     Customer cust=  iCustomerService.getCustomerById(js.getString("id")).getData();
					 if(cust!=null){
						 //	1成功 2.外拨失败 3.对方未接 4.号码错误 5.拒接 6.通话中 8.来电未接
						 String[] str=new String[]{"","成功","外拨失败","对方未接","号码错误","拒接","通话中","","来电未接"};
						 cust.setLastCallTime(new Date());
						 cust.setLastCallStatus(str[Integer.valueOf(status)]);
						 iCustomerService.updateCustomerById(cust);
						 
						
					 }				     
					 break;
				 }
			 }
			 map.put(user.getId(), JSON.toJSONString(list));		
		 }			 
			redisUtil.hmset(Constants.REDIS_CUST_MANAGER_PHONE_LIST,map,-1);

		 
		return iUserIncomingPhoneService.savePhoneCallLog(data, user.getId());
	}

	/**
	 * 获取历史消息接口
	 * 
	 * @param wxid
	 * @param syswxid
	 * @return
	 */
	@GetMapping(value = "/get/history/msg/bywxid")
	public Object getHimsgList(@RequestParam(name = "sid", required = false) String sid,
			@RequestParam(name = "cid", required = false) String cid,
			@RequestParam(name = "wxid", required = false) String wxid,
			@RequestParam(name = "sysWxid", required = false) String  sysWxid, HttpServletResponse response, HttpServletRequest request) {
		List msgList = new ArrayList();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(StringUtils.isNotBlank(sid)&&StringUtils.isBlank(cid)){//sid不为空 cid为空代表群发
			 Map<Object, Object> histmap=redisUtil.hmget(Constants.REDIS_CHAT_HISTORY);
				Object obj= histmap.get(sid);
				 if(obj!=null){
					 msgList=JSONArray.parseArray(obj.toString(), HisWxMsg.class) ;
				 }
		}else{
		 
				 
	      	//首先从redis查询聊天记录如果没有聊天记录则插入(正常websocket推送消息时已插入到redis中 为了保险多家一条)
	  
				 Map<Object, Object> histmap=redisUtil.hmget(Constants.REDIS_CHAT_HISTORY);
				Object obj= histmap.get(cid);
				 if(obj!=null){
					 msgList=JSONArray.parseArray(obj.toString(), HisWxMsg.class) ;
				 }
				 
				 
				if(msgList==null||msgList.size()==0){
					Map<String,Object> map =(Map<String, Object>)imApiService.himsgList(wxid, sysWxid);					
					msgList = (List) map.get("varList");
					histmap.put(cid,JSON.toJSONString(msgList));
					redisUtil.hmset(Constants.REDIS_CHAT_HISTORY,histmap,-1);
				}
				try {
					Cookie	my_wxid= new Cookie("my_wxid",sysWxid);
					my_wxid.setMaxAge(-1);
					my_wxid.setPath(request.getContextPath()+"/");//设置cookie路径
					Cookie to_wxid= new Cookie("to_wxid",wxid);
					to_wxid.setMaxAge(-1);
					to_wxid.setPath(request.getContextPath()+"/");//设置cookie路径
				//	Cookie  to_nick= new Cookie("to_nick", custWx.getWxRemark()==null?"":custWx.getWxRemark() );
					//to_wxid.setMaxAge(-1);
					//to_nick.setPath(request.getContextPath()+"/");//设置cookie路径
					response.addCookie(my_wxid);
					response.addCookie(to_wxid);
					//response.addCookie(to_nick);
				} catch (Exception e) {
					// TODO: handle exception
				}
			
			
		 
		
	 
			 
		}
		
		return msgList;
	}
	

	/**
	 * 构建VO
	 * @param custPhone
	 * @param customer
	 * @param custRoleList
	 * @param custWxList
	 * @return
	 */
	private ImMsgConversationListVO genConversationVO(String custPhone, Customer customer,
			List<CustImRole> custRoleList, List<CustWxInfoVO> custWxvoList) {
		 
		ImMsgConversationListVO conversationListVO =new ImMsgConversationListVO();
		conversationListVO.setCustName(customer.getCustName());
		conversationListVO.setSessionId(UuidUtil.get32UUID());
		conversationListVO.setCustType(customer.getCustType());
		conversationListVO.setCustPhone(custPhone);//拨打电话设置电话号码
		conversationListVO.setcId(customer.getId());
		conversationListVO.setCustRoleList(custRoleList);
		conversationListVO.setCompany(customer.getAddressInfo());
		conversationListVO.setAddress(customer.getAddressInfo());
		conversationListVO.setHasWxRole("0");
		conversationListVO.setPhoneNoReadNum(0);
		conversationListVO.setWxNoReadNum(0);
		conversationListVO.setMesType(3); 
		conversationListVO.setShow(false);
		conversationListVO.setIfTop(false);
		conversationListVO.setWxWarm(false);
		conversationListVO.setWxStatus(customer.getWxStatus());
		conversationListVO.setCjNum(0);//成交
		conversationListVO.setFhNum(0);//发货
		conversationListVO.setXyNum(new Long(0));//信用
		if(StringUtil.isNotBlank(customer.getCustAdminUserIdTem())){
			conversationListVO.setTrans(true);//是否转交
		}else{
			conversationListVO.setTrans(false);//是否转交	
		}
		//if(custWxvoList != null && custWxvoList.size() >0) {
		//	conversationListVO.setHasWxRole("1");
		//	conversationListVO.setWxStatus("1");//微信状态,1正常，2没有微信，3微信被删
		//}
		
		conversationListVO.setCustWxvoList(custWxvoList);
		conversationListVO.setAddTimestamp(System.currentTimeMillis());
		
//		conversationListVO.setSessionTime(Calendar.getInstance().getTime());
		return conversationListVO;
	}
	
}
