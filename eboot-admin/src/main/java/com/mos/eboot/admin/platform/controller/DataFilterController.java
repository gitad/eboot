package com.mos.eboot.admin.platform.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCallLogService;
import com.mos.eboot.admin.platform.api.ICContactPeoRelService;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;

/**
 * <p>
 *  数据筛选
 * </p>
 *
 * @author jiangweijie
 * @since 2021-11-23
 */
@Controller
@RequestMapping("/dataFilter")
public class DataFilterController  extends BaseController {
	
	@Autowired
	ICCallLogService iCCallLogService;
	
	private static String customerId="";
	
	@RequestMapping("/to-page")
	public String toPage() {	 
	   return "platform/dataFilter/dataFilter";
	}
	@RequestMapping("/to-page1")
	public String toPage1() {	 
	   return "platform/dataFilter/list";
	}
	@Autowired
	ICContactPeoRelService cContactPeoRelService;
 
	@Autowired
	private RedisUtil redisUtil;
	
	@PostMapping("/queryCallLog")
	@ResponseBody
	public LayPage<CCallLog> query(Page<CCallLog> page) {
	 
		ResultModel<Page<CCallLog>> dataPage=iCCallLogService.dataFilterQuery(page);		
		return this.getLayPage(dataPage);
    }
 	
	/**
	 * 通话记录同步
	 * */
	@PostMapping("/call_log_syn")
	@ResponseBody
	public  ResultModel<Boolean> callLogSyn() {
	 ResultModel<Boolean> result=iCCallLogService.callLogSyn();		
		return result;
    }
 
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<CCallLog> save(@RequestBody CCallLog  cCallLog) {
		 SysUser user = (SysUser) PrincipalUtils.getCurrentUser();
		 cCallLog.setCustomerId(customerId);
		 cCallLog.setCreateUser(user.getNickname());
		 cCallLog.setCreateTime(new Date()); 
	    ResultModel<CCallLog> result=iCCallLogService.save(cCallLog);	
	    return result;
	}
	
	

	/**
	 * 插入车辆信息，数据筛选-通话记录  打标签
	 * */
	
	@PostMapping("/insertCarInfo")
	@ResponseBody
	public Boolean insertCarInfo( @RequestBody List<CCallLog>  list, 
								 	    @RequestParam(value = "selCarLen", required = false)  String selCarLen,
								 	   @RequestParam(value = "selCarType", required = false)   String selCarType,
										
								 	  @RequestParam(value = "routeType", required = false)  String routeType,
										
									
								 	 @RequestParam(value = "countryStart", required = false) String countryStart,										
								 	@RequestParam(value = "provinceStart", required = false) String provinceStart,										
								 	@RequestParam(value = "cityStart", required = false) String cityStart,	
										
										
								 	@RequestParam(value = "countryTrans", required = false) String countryTrans,
								 	@RequestParam(value = "provinceTrans", required = false) String provinceTrans,
								 	@RequestParam(value = "cityTrans", required = false) String cityTrans,
										 
										
										
										
								 	@RequestParam(value = "countryEnd", required = false) String countryEnd,										
								 	@RequestParam(value = "provinceEnd", required = false) String provinceEnd,
								 	@RequestParam(value = "cityEnd", required = false) String cityEnd,

	
									
										
								 	@RequestParam(value = "provinceCode", required = false) String provinceCode,
								 	@RequestParam(value = "cityCode", required = false) String cityCode,
								 	@RequestParam(value = "areaCode", required = false) String areaCode 
			  ) {		 
		//Map<String, Object> condition
		Map<String,Object> map=new HashMap<String, Object>(); 	
		map.put("list", JSONObject.toJSONString(list));
		map.put("selCarLen", selCarLen);
		map.put("selCarType", selCarType);	
		map.put("routeType", routeType);
		
		map.put("countryStart", countryStart);
		map.put("provinceStart", provinceStart);
		map.put("cityStart", cityStart);
		
		map.put("countryTrans", countryTrans);
		map.put("provinceTrans", provinceTrans);
		map.put("cityTrans", cityTrans);
		
		map.put("countryEnd", countryEnd);
		map.put("provinceEnd", provinceEnd);
		map.put("cityEnd", cityEnd);
		
		
		map.put("provinceCode", provinceCode);
		map.put("cityCode", cityCode);
		map.put("areaCode", areaCode);
		Boolean  dataPage= iCCallLogService.insertCarInfo(map);		
		return  dataPage;
    }
	
	
	
	
	
	@PostMapping("/del")
	@ResponseBody
	public ResultModel<Boolean>  del(@RequestBody CCallLog  cCallLog) {
	    ResultModel<Boolean> result=iCCallLogService.del(cCallLog);
	    return result;
	}
 
	  /**
     * 电话拨打
     * */    
    @ResponseBody
    @RequestMapping("/telPhone")
    public Boolean telPhone(@RequestBody List<CCallLog> culist) {	
    	SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
    	//redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), conversationListVO, -1);
    	 Map<Object, Object> map=new HashMap<Object, Object>();
    	List<JSONObject> list=new ArrayList<JSONObject>();
    	if(culist!=null){
    		for(CCallLog cu: culist){
    			String phone=StringUtil.str_Phone(cu.getValidPhone());
    			 
    			if(StringUtils.isNotBlank(phone)){   		 				
    				JSONObject obj=new JSONObject();
        			obj.put("name",cu.getCustName());
        			obj.put("phone",phone);     			
        			obj.put("id",cu.getCustomerId());        			
        			obj.put("status",-1);			
        			CContactPeoRel con=new CContactPeoRel();
        			con.setCustomerId(cu.getCustomerId());
        			con.setContactPhone(phone);
        			con=cContactPeoRelService.querByEntity(con).getData();
        			if(con==null){
        				con=new CContactPeoRel();
        			}
        			 obj.put("att",con.getPhoneAtt()==null?"":con.getPhoneAtt());
        			 obj.put("area",con.getPhoneArea()==null?"":con.getPhoneArea());
        			list.add(obj);
    			}
    			
    		}
    	}
    	map.put(currentUser.getId(), JSON.toJSONString(list));		 
		redisUtil.hmset(Constants.REDIS_CUST_MANAGER_PHONE_LIST,map,-1);	 
        return  true;
    }
}
