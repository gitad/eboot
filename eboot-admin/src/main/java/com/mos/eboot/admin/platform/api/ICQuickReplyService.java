package com.mos.eboot.admin.platform.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.CContactPeoRel;
import com.mos.eboot.platform.entity.CQuickReply;
import com.mos.eboot.tools.result.ResultModel;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-03
 */
@FeignClient("boot-service")
public interface ICQuickReplyService  {


	/**列表
	 * @param page
	 * @throws Exception
	 */
	 @RequestMapping(value = "cQuickReply/query",method = RequestMethod.POST)
	ResultModel<Page<CQuickReply>> query(@RequestBody Page<CQuickReply> page);
	 
	/**保存
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cQuickReply/save")
	ResultModel<Boolean> save( @RequestBody CQuickReply cQuickReply);
	
		
	/**删除
	 * @param page
	 * @throws Exception
	 */
	@PostMapping("cQuickReply/del")
	ResultModel<Boolean>del(@RequestBody CQuickReply cQuickReply);
	
}
