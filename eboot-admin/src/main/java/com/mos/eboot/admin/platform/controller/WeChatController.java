package com.mos.eboot.admin.platform.controller;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICustomerService;
import com.mos.eboot.admin.platform.api.ISysUserCustMgrService;
import com.mos.eboot.admin.platform.api.ISysUserService;
import com.mos.eboot.admin.platform.api.ISysUserWxInfoService;
import com.mos.eboot.admin.platform.api.IUserIncomingPhoneService;
import com.mos.eboot.admin.platform.api.ImApiService;
import com.mos.eboot.platform.entity.CustImRole;
import com.mos.eboot.platform.entity.CustWx;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.service.IUserService;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.BeanCopyUtil;
import com.mos.eboot.tools.util.BeanCopyUtilCallBack;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.tools.util.UuidUtil;
import com.mos.eboot.vo.CustWxInfoVO;
import com.mos.eboot.vo.IMCustomerVO;
import com.mos.eboot.vo.ImMsgConversationListVO;

 
 
/**
 *  
 *与c#工具结合 实现聊天页面直接访问 
 *  
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/weChat")
public class WeChatController  extends BaseController {
	
	@Value("${sys.stock.url}")
	private String sotckUrl;
	
	@Autowired
	private ISysUserService sysUserService;
	
	@Autowired
	private ICustomerService iCustomerService;

	@Autowired
	private IUserIncomingPhoneService iUserIncomingPhoneService;
	
	@Autowired
	private ImApiService imApiService;
	
    @Autowired
    private ISysUserWxInfoService iSysUserWxInfoService;

	@Autowired
	private ISysUserCustMgrService iSysUserCustMgrService;
	
	@Autowired
	private RedisUtil redisUtil;
	
	@Resource
	private IUserService userService;
 
	public static  SysUser  sysUser=null;
	List<CustImRole> custRoleList;
	List<Object> conversationList;
	
	List msgList;
	IMCustomerVO vo;
	CustWx custWx;

    /**
	 * 微信消息弹屏
	 * wxid  发送消息人的id  客户
	 * syswxid  接受人的wxid  业务员
	 * @param phone
	 * @param model
	 * @return
	 */
	@GetMapping("/openChat")
	public String getCustomerByWxId(@RequestParam(name = "wxid", required = false) String wxid,
			@RequestParam(name = "syswxid", required = false) String syswxid, @RequestParam(name = "userName") String userName,
			@RequestParam(name = "phone", required = false) String phone, @RequestParam("token") String token,Model model) {	
		//默认查询走系统登陆
		SysUser  currentUser=null; 
		try {
			userName=URLDecoder.decode(userName, "UTF-8");
 		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	      //先去校验系统是否有这个人  并且分配微信号
		// SysUser suer=sysUserService.checkByUserNameToken(userName, token);
		SysUser suer=(SysUser)userService.findUserByUsername(userName);
		   List<CustWx> custWxList=null;
		  if(StringUtils.isNotBlank(phone)){//如何用手机号聊天 去im查询当前两个人聊天信息		 
			  custWxList= JSONArray.parseArray(JSON.toJSONString(imApiService.getFriendByPhone(phone)), CustWx.class);
              
		  }
	      if(custWxList.size()!=0&&custWxList!=null){
	    	  	//存在一个手机号存在于多个业务员情况  随机选择一条
	    	  wxid=custWxList.get(0).getWxId();
	    	  syswxid=custWxList.get(0).getSysWxId();    	  
					  
			   AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(suer, suer.getPassword(),suer.getUsername());		 
				Subject subject = SecurityUtils.getSubject(); 
				 UsernamePasswordToken tok = new UsernamePasswordToken(suer.getUsername(),suer.getPassword());
				 try {
					 subject.login(tok);//第一次构建默认数据会有问题
					//默认查询走系统登陆
					 currentUser = (SysUser) PrincipalUtils.getCurrentUser();
			 
				} catch (Exception e) {
					currentUser=suer;
				} 
				 sysUser= currentUser;
		    List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		    ImMsgConversationListVO conversationListVO = null;
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				List<CustWxInfoVO> custList= coV.getCustWxvoList();
				if(custList!=null){	
				for(int i=0;i<custList.size();i++){
						CustWxInfoVO cust=custList.get(i);				 
						if(wxid.equals(cust.getWxId())&&syswxid.equals(cust.getSysWxId())){//如果发消息能找到这个人 则说明这个人存在直接跳出反回
							 conversationListVO=coV;
							 coV.setAddTimestamp(System.currentTimeMillis());
						     coV.setWxNoReadNum(coV.getWxNoReadNum()+1);
							 // redisUtil.lUpdateIndex(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), k, coV);//更新聊天消息
							 break;				
						}
				  }
				}
				
			}
			if(conversationListVO==null){
				list= buildChatData(wxid,syswxid);
			}
	    	
			
	    	 /**
				 * 根据微信ID，查询历史消息
				 */
				  msgList = new ArrayList();
				Map<String,Object> map =(Map<String, Object>) imApiService.himsgList(wxid, syswxid);

				 msgList = (List) map.get("varList");

			
		 
			model.addAttribute("role",custRoleList);
			model.addAttribute("data", vo);
			model.addAttribute("conversation",conversationList);
			model.addAttribute("currentMsgList", msgList);
			
			
			//model.addAttribute("custWx",custWx);
			model.addAttribute("userName", currentUser.getId());
			model.addAttribute("sotckUrl",sotckUrl);
			 
	      }
		  
		return "im/im_window";
	}
   
	 
	 /**
		 *  构建聊天消息
		 * 
		 * @param phone
		 * @param model
		 * @return
		 */
		public List<Object>  buildChatData(@RequestParam(name = "wxid", required = false) String wxid,
				@RequestParam(name = "syswxid") String syswxid) {
			//默认查询走系统登陆
			SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
			 if(currentUser==null){
				 currentUser= WeChatController.sysUser;
			  }	 	
			 vo = new IMCustomerVO();		 
			/**
			 * 根据微信ID，查询微信用户
			 */
			ResultModel<CustWx> custWxModel= iCustomerService.getCustomerWxByWxIdAndSysWxId(wxid,syswxid);			
			custWx = custWxModel.getData();	
			/**
			 * 根据客户ID，查询客户信息
			 */
			ResultModel<Customer> customerModel = null;
			Customer customer = null;	
			if(StringUtil.isBlank(custWx.getcId()) ) {			 
				//判断微信id，是否存在已有用户信息。
				//custWxList=iCustomerService.getCustomerWxListByWxId(wxid,syswxid).getData();	 
				//对于已存在微信与用户关联关系，不用新增用户信息，不存在，则新增。
				IMCustomerVO customerVO =new IMCustomerVO();
				 
				if(StringUtils.isNotBlank(custWx.getWxRemark())){
					customerVO.setCustName(custWx.getWxRemark());//x先将备注加上  防止数据查不出来无法覆盖
				}else{
					customerVO.setCustName(custWx.getWxNickname());//x先将备注加上  防止数据查不出来无法覆盖
				}
			
				customerVO.setCustWxId(wxid);
				customerVO.setSysWxId(syswxid);
				//customerVO.setWxStatus("1");
				customerVO.setIsDel("0");
				customerVO.setIsUpload("1");
				customerVO.setIfNormal("0");
				customerVO.setCustAdminNickname(currentUser.getUsername());
				customerVO.setCustAdminUserId(currentUser.getId());
				//从备注中匹配出手机号
				customerVO.setCustPhone(StringUtil.str_Phone(custWx.getWxRemark()));	
				customerVO.setValidPhone(StringUtil.str_Phone(custWx.getWxRemark()));
				customerVO.setCustType("其他");
				customerModel = iCustomerService.saveSampleCustomer(customerVO);
				customer = customerModel.getData();
				custWxModel = iCustomerService.saveCustWx(custWx);//更新cid
				iCustomerService.createPeoRel(customer);//创建联系人及备注
			}else {
			 
				customerModel= iCustomerService.getCustomerById(custWxModel.getData().getcId()+"");
				if(customerModel==null){//先根据cid去查询这个人 如果查不到存在两种情况  1是这个人不存在  2是以前建立过关联关系 但是后来关系丢失了 需要进行二次处理
					//首先先根据电话去查询一下
					customerModel=iCustomerService.getCustomerByPhone(StringUtil.str_Phone(custWxModel.getData().getWxRemark()));
				}
				customer = customerModel.getData();
			}
		 
			
			/**
			 * 根据微信ID，查询历史消息
			 */
			  msgList = new ArrayList();
			 Map<String,Object> map =(Map<String, Object>) imApiService.himsgList(wxid, syswxid);

			 msgList = (List) map.get("varList");
		
			BeanCopyUtil.copyProperties(customer, vo);
			vo.setCompany(customer.getAddressInfo());
			if(StringUtil.isBlank(customer.getValidPhone())){
				vo.setCustPhone("号码X");
			}else{
				vo.setCustPhone(customer.getValidPhone());
			}
		
			/**
			 * 查询权限
			 */
		 
			 custRoleList= iCustomerService.getCustImRole(currentUser.getId(), customer.getCustType()).getData();
			 conversationList = buildConversationByCustomerNotnull(currentUser.getId(), vo.getCustPhone(), customer,custRoleList,0);	
			
			 /**
			   构建微信人员聊天关系并实时进行更行
			 * */
			 
				 Map<Object, Object> chatPeoMap=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
				 if(chatPeoMap.get(wxid+"-"+syswxid)==null){
					String[] str=new String[2];
					str[0]=customer.getId();
					str[1]=customer.getCustAdminUserId();
					chatPeoMap.put(wxid+"-"+syswxid, JSON.toJSONString(str));
					redisUtil.hmset(Constants.REDIS_CHAT_PEO_MANAGER,chatPeoMap,-1);
				 }
					
			 
				
			 return conversationList;
		}
 
	/**
	 * 构建聊天弹框左侧会话消息列表
	 * @param userId
	 * @param custPhone
	 * @param customer
	 * @return
	 */
 private List<Object> buildConversationByCustomerNotnull(String userId, String custPhone, Customer customer,
				List<CustImRole> custRoleList,Integer  mesType) {
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
	    List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + userId, 0, -1);//构建人员聊天关系
		if (null == list || list.size() == 0) {			
			ImMsgConversationListVO conversationListVO = genConversationVO(custPhone, customer, custRoleList,mesType);			 
			list.add(conversationListVO);		 
			
		}else {
			boolean isAdd=true;
			for (int i=0;i<list.size(); i++) {
				ImMsgConversationListVO conversationListVO = (ImMsgConversationListVO) list.get(i);
				if(conversationListVO.getcId() != null) {
					if(!conversationListVO.getcId().equals("-1")&&conversationListVO.getcId().equals(customer.getId()) ) {	
							//更新一下微信聊天用户
							ResultModel<List<CustWx>> result= iCustomerService.getCustListWxByCid(customer.getId().toString());
							List<CustWx>  custWxList= result.getData();
							if(custWxList.size()!=conversationListVO.getCustWxvoList().size()){//长度不一样则更新一下聊天李彪
									/**
									 * 构建左侧弹框列表
									 */
									List<CustWxInfoVO> custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
										@Override
										public void callBack(CustWx s, CustWxInfoVO t) {
											// TODO Auto-generated method stub
											
											SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
											t.setSysUserWxInfo(sysUserWxInfo);
										}
									});
									conversationListVO.setCustWxvoList(custWxvoList);
							}
							
							conversationListVO.setAddTimestamp(System.currentTimeMillis());
							
					 
						isAdd=false;
						//list.add(0, list.remove(1));
						break;
					}
				}
			}
			if(isAdd) {
				ImMsgConversationListVO conversationListVO = genConversationVO(custPhone, customer, custRoleList,mesType);
				
				//redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + userId, conversationListVO, -1);
				list.add(conversationListVO);
			}
		}
		Collections.sort(list,new Comparator<Object>() {//更具时间进行排序

			@Override
			public int compare(Object o1, Object o2) {
				// TODO Auto-generated method stub
				ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
				ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
				return vo2.getAddTimestamp().compareTo(vo1.getAddTimestamp());
			}
		});
	 	Collections.sort(list,new Comparator<Object>() {

		@Override
		public int compare(Object o1, Object o2) {//更具是否置顶进行排序一次
			// TODO Auto-generated method stub
			ImMsgConversationListVO vo1=(ImMsgConversationListVO) o1;
			ImMsgConversationListVO vo2=(ImMsgConversationListVO) o2;
			int i=0;
			 boolean st1=vo1.isIfTop();
			 boolean st2=vo2.isIfTop();
			 if(st1==true&&st2==false){
				 i=-1;
			 }
			 if(st1==false&&st2==true){
				 i=1;
			 }
			 
			return i;
		}
	 	}); 
	 	redisUtil.del(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId());
	 	redisUtil.lSet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), list, -1);		 
		return list;
		 
	}

	/**
	 * 构建VO
	 * @param custPhone
	 * @param customer
	 * @param custRoleList
	 * @param custWxList
	 * @return
	 */
	private ImMsgConversationListVO genConversationVO(String custPhone, Customer customer,
			List<CustImRole> custRoleList,Integer  mesType) {
		ImMsgConversationListVO conversationListVO =new ImMsgConversationListVO();
		conversationListVO.setCustName(customer.getCustName());
		conversationListVO.setSessionId(UuidUtil.get32UUID());
		conversationListVO.setCustType(customer.getCustType());
		conversationListVO.setCustPhone(custPhone);//拨打电话设置电话号码
		conversationListVO.setcId(customer.getId());
		conversationListVO.setCustRoleList(custRoleList);
		conversationListVO.setCompany(customer.getAddressInfo());
		conversationListVO.setAddress(customer.getAddressInfo());
		conversationListVO.setHasWxRole("0");
		conversationListVO.setPhoneNoReadNum(0);
		conversationListVO.setWxNoReadNum(1);
		conversationListVO.setMesType(mesType); 
		conversationListVO.setShow(false);
		conversationListVO.setIfTop(false);
		conversationListVO.setWxWarm(false);
		conversationListVO.setWxStatus(customer.getWxStatus());
		conversationListVO.setCjNum(0);//成交
		conversationListVO.setFhNum(0);//发货
		conversationListVO.setXyNum(new Long(0));//信用
		if(mesType==0){//消息类型   0微信  /1群/2拨打/3来电
			conversationListVO.setIfDealWith(false);//处理状态
		}else{
			conversationListVO.setIfDealWith(true);//虚拟号 群聊 来电 默认已处理状态
		}
	
		if(StringUtil.isNotBlank(customer.getCustAdminUserIdTem())){
			conversationListVO.setTrans(true);//是否转交
		}else{
			conversationListVO.setTrans(false);//是否转交	
		}
	
		ResultModel<List<CustWx>> result= iCustomerService.getCustListWxByCid(customer.getId().toString());
		List<CustWx>  custWxList= result.getData();
				 
		
		if(custWxList != null && custWxList.size() >0) {
			conversationListVO.setHasWxRole("1");
			if(StringUtil.isNotBlank(custWxList.get(0).getWxRemark())){
				conversationListVO.setCustPhone(custWxList.get(0).getWxRemark());//聊天设置此次聊天的号码
			}
			 
			conversationListVO.settCustWxId(custWxList.get(0).getId().toString());
			/**
			 * 构建左侧弹框列表
			 */
			List<CustWxInfoVO> custWxvoList = BeanCopyUtil.copyListProperties(custWxList, CustWxInfoVO::new,new BeanCopyUtilCallBack<CustWx, CustWxInfoVO>() {
				@Override
				public void callBack(CustWx s, CustWxInfoVO t) {
					// TODO Auto-generated method stub
					
					SysUserWxInfo sysUserWxInfo=imApiService.getwxLoginByWxId(s.getSysWxId());
					t.setSysUserWxInfo(sysUserWxInfo);
				}
			});
			String lastMsg= imApiService.getLastHisMsgList(custWxvoList.get(0).getWxId(), custWxvoList.get(0).getSysWxId());
			conversationListVO.setLastMsg(lastMsg);

			conversationListVO.setCustWxvoList(custWxvoList);
		}
		
		
		
		conversationListVO.setAddTimestamp(System.currentTimeMillis());
		
		conversationListVO.setAddTime(LocalTime.now().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
		return conversationListVO;
	}
	
	
 	
}