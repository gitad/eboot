package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCallLogService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCallLog;
import com.mos.eboot.platform.entity.Customer;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.ImMsgConversationListVO;

/**
 * <p>
 *  通话记录
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cCallLog")
public class CCallLogController  extends BaseController {
	
	@Autowired
	ICCallLogService iCCallLogService;
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	
	@Autowired
	private RedisUtil redisUtil;
	
	
	private static String customerId="";
	private static String phone="";
	
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				if(coV.getSessionId().equals(sid)){
					phone=StringUtil.str_Phone(coV.getCustPhone());
					break;
				}
			}
			
			 
		 }
	   return "platform/calllog/calllog";
	}
	
	@RequestMapping("to-page1")
	public String toPage1( String cid,String sid,Model model) {
		 SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				if(coV.getSessionId().equals(sid)){
					phone=StringUtil.str_Phone(coV.getCustPhone());
					break;
				}
			}
			
			Customer customer=iCCallLogService.queyCustomer(customerId);
			model.addAttribute("customer", customer);
		 }
	   return "platform/calllog/list";
	}
	
	
	@PostMapping("/query")
	@ResponseBody
	public LayPage<CCallLog> query(Page<CCallLog> page) {		 
		Map<String,Object> condition=new HashMap<String, Object>(); 	
		condition.put("customerId", customerId);
		if(customerId.equals("-1")){
			condition.put("contactPhone", phone);
		}
		 page.setCondition(condition);
		ResultModel<Page<CCallLog>> dataPage=iCCallLogService.query(page);		
		return this.getLayPage(dataPage);
    }
	
	@PostMapping("/addLog")
	@ResponseBody
	public CCallLog addLog() {		 
		Customer customer=iCCallLogService.queyCustomer(customerId);	
		CCallLog calLog=new CCallLog();
		calLog.setValidPhone(customer.getValidPhone());
		calLog.setOtherPhone(customer.getOtherPhone());
		calLog.setAddressInfo(customer.getAddressInfo());
		 
		return calLog;
    }
	
	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<CCallLog> save(@RequestBody CCallLog  cCallLog) {
		SysUser  currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		 if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		 
		
		 
		 if(customerId.equals("-1")){
			 cCallLog.setContactPhone(phone);
		 }
		 cCallLog.setCustomerId(customerId);
		
		 if(StringUtil.isNotBlank(cCallLog.getCallId())){//有主键
			 cCallLog.setUpdateTime(new Date());
			 cCallLog.setUpdateUser(currentUser.getNickname());
		 }else{
			 cCallLog.setUpdateTime(new Date());
			 cCallLog.setUpdateUser(currentUser.getNickname());
			 cCallLog.setCreateUser(currentUser.getNickname());
			 cCallLog.setCreateTime(new Date());
		 }
		 
		  ResultModel<CCallLog> result=iCCallLogService.save(cCallLog);	
		  CCallLog log=result.getData();
		  if(StringUtil.isNotBlank(cCallLog.getCallId())){
			 
			  iSysEbootLogService.save( SysEbootLogController.insert(log.getCallId(), "c_car_info", "update", log.toString(),log.getCustomerId())) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(log.getCallId(), "c_car_info", "add", log.toString(),log.getCustomerId())) ;
		  }
		 
		 //  iSysEbootLogService.save(new SysEbootLog());
	    return result;
	}
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestBody CCallLog  cCallLog) {
	    ResultModel<Boolean> result=iCCallLogService.del(cCallLog);
	    iSysEbootLogService.save( SysEbootLogController.insert(cCallLog.getCallId(), "c_car_info", "del", cCallLog.toString(),cCallLog.getCustomerId())) ;
	    return result;
	}
 
	
}
