package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.config.RedisUtil;
import com.mos.eboot.admin.platform.api.ICCarInfoService;
import com.mos.eboot.admin.platform.api.ISysEbootLogService;
import com.mos.eboot.platform.entity.CCarInfo;
import com.mos.eboot.platform.entity.CCarInfoRel;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;
import com.mos.eboot.vo.ImMsgConversationListVO;

/**
 * <p>
 *  通话记录
 * </p>
 *
 * @author jiangweijie
 * @since 2021-10-01
 */
@Controller
@RequestMapping("/cCarInfo")
public class CCarInfoController  extends BaseController {
	
	@Autowired
	ICCarInfoService iCCarInfoService;
	@Autowired
	ISysEbootLogService iSysEbootLogService;
	
	
	
	@Autowired
	private RedisUtil redisUtil;
	
	
	private static String customerId="";
	private static String phone="";
	
	
	@RequestMapping("to-page")
	public String toPage( String cid,String sid,Model model) {
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();	
		if(currentUser==null){
			 currentUser= WeChatController.sysUser;
		  }
		if(cid!=null){
			this.customerId=cid; 
			List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);		 
			for (Object object : list) {
				ImMsgConversationListVO coV = (ImMsgConversationListVO) object;
				if(coV.getSessionId().equals(sid)){
					phone=StringUtil.str_Phone(coV.getCustPhone());
					break;
				}
			}	
			 
		 }
	   return "platform/cCarInfo";
	}
	
	 
	
	@PostMapping("/query")
	@ResponseBody
	public List<CCarInfo> query(Page<CCarInfo> page) {		 
		Map<String,Object> condition=new HashMap<String, Object>(); 	
		condition.put("customerId", customerId);
		if(customerId.equals("-1")){
			condition.put("contactPhone", phone);
		}
		 page.setCondition(condition);
		 List<CCarInfo> dataPage=iCCarInfoService.query(page).getData();		
		return  dataPage;
    }
	
	
	
	
	
	
	@PostMapping("/queryById")
	@ResponseBody
	public  CCarInfo queryById( String carId) {		 
		CCarInfo  dataPage=iCCarInfoService.queryById(carId);		
		return  dataPage;
    }
	
	@PostMapping("/queryByCustomerId")
	@ResponseBody
	public  List<CCarInfo> queryByCustomerId( String customerId) {		 
		List<CCarInfo>  dataPage=iCCarInfoService.queryByCustomerId(customerId);		
		return  dataPage;
    }
	
	
	@PostMapping("/relSave")
	@ResponseBody
	public CCarInfoRel  relSave( @RequestBody CCarInfoRel  cCarInfoRel) {		 
		  cCarInfoRel.setUseFlag(1);
		  CCarInfoRel   cCar=iCCarInfoService.relSave(cCarInfoRel);	
		  if(StringUtil.isNotBlank(cCarInfoRel.getCarRelId())){
				 
			  iSysEbootLogService.save( SysEbootLogController.insert(cCarInfoRel.getCarRelId(), "c_car_info_rel", "update", cCar.toString(),customerId)) ;
		  }else{
			  
			  iSysEbootLogService.save( SysEbootLogController.insert(cCarInfoRel.getCarRelId(), "c_car_info_rel", "add", cCar.toString(),customerId)) ;
		  }
		return  cCar;
    }
	
	
	
	@PostMapping("/save")
	@ResponseBody
	public CCarInfo  save( @RequestBody CCarInfo  cCarInfo) {	
		SysUser currentUser = (SysUser) PrincipalUtils.getCurrentUser();
		  if(StringUtil.isBlank(cCarInfo.getCarId())){
			  cCarInfo.setCustomerId(customerId);
			  cCarInfo.setCreateTime(new Date());
			  cCarInfo.setCreateUser(currentUser.getNickname());
		  }else{
			  cCarInfo.setUpdateTime(new Date());
			  cCarInfo.setUpdateUser(currentUser.getNickname());
		  }
		cCarInfo.setUseFlag(1);
		CCarInfo	cCar =iCCarInfoService.save(cCarInfo);	
		  if(StringUtil.isNotBlank(cCarInfo.getCarId())){
			 iSysEbootLogService.save( SysEbootLogController.insert(cCarInfo.getCarId(), "c_car_info", "update", cCar.toString(),cCarInfo.getCustomerId())) ;
		  }else{
			   iSysEbootLogService.save( SysEbootLogController.insert(cCarInfo.getCarId(), "c_car_info", "add", cCar.toString(),cCarInfo.getCustomerId())) ;
		  }
		return  cCar;
    }
	@PostMapping("/del")
	@ResponseBody
	public CCarInfo  del( @RequestBody CCarInfo  cCarInfo) {		 
	 
		cCarInfo=iCCarInfoService.del(cCarInfo);		
		return  cCarInfo;
    }
	@PostMapping("/relDel")
	@ResponseBody
	public CCarInfoRel  relDel( @RequestBody CCarInfoRel  cCarInfoRel) {		 
		cCarInfoRel=iCCarInfoService.relDel(cCarInfoRel);		
		return  cCarInfoRel;
    }
	 
	@PostMapping("/addRel")
	@ResponseBody
	public CCarInfoRel  addRel() {		 
		CCarInfoRel  cCarInfoRel=new CCarInfoRel();
		return  cCarInfoRel;
    }
	 
	
 
}
