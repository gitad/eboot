package com.mos.eboot.admin.platform.controller;


import java.util.Date;
import java.util.List;

 






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.admin.platform.api.ISysDictionariesService;
import com.mos.eboot.platform.entity.CContactPeo;
import com.mos.eboot.platform.entity.SysDictionaries;
import com.mos.eboot.platform.entity.SysUser;
import com.mos.eboot.tools.controller.BaseController;
import com.mos.eboot.tools.result.LayPage;
import com.mos.eboot.tools.result.ResultModel;
import com.mos.eboot.tools.shiro.utils.PrincipalUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhang
 * @since 2021-10-07
 */
@Controller
@RequestMapping("/sysDictionaries")
public class SysDictionariesController extends BaseController {
	
	@Autowired
	ISysDictionariesService iSysDictionariesService;
	
	
	@RequestMapping("to-page")
	public String toPage() {
	   return "platform/dict/listfh";
	}
	@RequestMapping("to-page1")
	public String toPage1() {
	   return "platform/dict/listfh1";
	}
	/**
	 * 通用下拉 弹窗查询
	 * */
	@PostMapping("/query")
	@ResponseBody
	public ResultModel<List<SysDictionaries>> query(@RequestParam("parentId")   String  parentId) {
		List<SysDictionaries> list=iSysDictionariesService.query(parentId);	
		ResultModel<List<SysDictionaries>> dataPage=new ResultModel();
		dataPage.setCode("0");
		dataPage.setMsg("成功");
		dataPage.setData(list);
		 return dataPage;
    }
 
	/**
	 * 树形组件查询  递归 比较慢
	 * */
	@PostMapping("/queryByparentId")
	@ResponseBody
	public Object queryByparentId() {
		List<SysDictionaries> dataPage=iSysDictionariesService.queryByparentId("");	
		String arr = JSON.toJSONString(dataPage);
		arr = arr.replaceAll("name", "label").replaceAll("dictionariesId", "id");		 
		return arr;
    }
 

	@PostMapping("/save")
	@ResponseBody
	public   ResultModel<Boolean> save(@RequestBody  List<SysDictionaries> list) {
	 
	    ResultModel<Boolean> result=iSysDictionariesService.save(list);	
	    return result;
	}
	@PostMapping("/sysAdd")
	@ResponseBody
	public  SysDictionaries   sysAdd() {
	   return new SysDictionaries();
	}
 
	
	@PostMapping("/del")
	 @ResponseBody
	public ResultModel<Boolean>  del(@RequestBody  SysDictionaries  sSysDictionaries) {
	    ResultModel<Boolean> result=iSysDictionariesService.del(sSysDictionaries);
	    return result;
	}
 
 
}
