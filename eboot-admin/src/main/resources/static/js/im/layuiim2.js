var httpurl = "http://127.0.0.1:8882/";
var websocket = null;
var $;
var table;
var layer;
var form;
layui.use(['layim','form', 'layedit', 'laydate', 'layer', 'table'], function(layim){

	websocket = new WebSocket(encodeURI(sotckUrl)); //wimadress参数在 assets/js-v/sys/index.js 中定义并赋值
	websocket.onopen = function() {
		
		websocket.send(',fhadmin-join,'+user); 	//发送上线消息(把上线状态发送给好友，对方好友栏目本头像从黑白色变彩色)(前提条件：本身在线状态设置为在线状态，否则对方看到的依然是离线状态)
		layer.msg('即时通讯已连接成功');
	};
	websocket.onerror = function() { 			//连接失败
		layer.msg('连接发生错误,刷新或者更换浏览器试试', {icon: 5});
	};
	websocket.onclose = function() {			//连接断开
		layer.msg('已经断开连接');
		websocket.send(',fhadmin-leave,');		//发送下线消息
	};
	websocket.onmessage = function(message) {	//消息接收
		console.log(message);
		var message = JSON.parse(message.data);
		if('friend' == message.TYPE || 'group' == message.TYPE){ //接收好友或群发送的消息
			getFHMessage(message);
		}else if('user_join' == message.type){
			friendOnline(message.user);
			layer.msg("好友"+message.name+"上线了");
		}else if('user_leave' == message.type){
			friendOffline(message.user);
			layer.msg("好友"+message.name+'下线了');
		}else if('user_online' == message.type){
			friendOnline(message.user);		//好友设置在线状态
		}else if('user_hide' == message.type){
			friendOffline(message.user);	//好友设置隐身状态
		}else if('user_applyfriend' == message.type){
			applyIFriend();					//申请加好友
		}else if('user_emove' == message.type){
			removeFriend(message.user);		//删除好友(被动删，被拉黑)
		}else if('agreefriend' == message.type){
			addFriend(message);				//对方同意添加好友
		}else if('applyqgroup' == message.type){
			applyIQgroup();					//申请加群申请
		}else if('agreeqgroup' == message.type){
			addIQgroup(message);			//对方同意群申请
		}else if('kickoutqgroup' == message.type){
			kickoutIQgroup(message.id);		//踢出群
		}else if('delqgroup' == message.type){
			delQgroup(message.QGROUP_ID);	//群解散
			layer.msg(message.CONTENT);
		}
	};
	//加载层-默认风格

	layer.msg('通讯连接中...', {
	  icon: 16
	  ,shade: 0.01
	});
  //基础配置
  layim.config({

    //初始化接口
    init: {
      url: basePath+'imapi/getList'
      ,data: {}
    }
    
    //查看群员接口
    ,members: {
      url: basePath+'imapi/getMembers'
      ,data: {}
    }

    ,uploadImage: {
      url: basePath+'imapi/upload/img'			//上传图片接口
      ,type: 'post' 								//默认post
    } 
    
    ,uploadFile: {
      url: basePath+'imapi/upload/file'			//上传文件接口
      ,type: 'post' 								//默认post
    }
    
    //扩展工具栏
    ,tool: [{
      alias: 'code'
      ,title: '代码'
      ,icon: '&#xe64e;'
    }]
    
    //,brief: true //是否简约模式（若开启则不显示主面板）
    
    //,title: 'WebIM' //自定义主面板最小化时的标题
    //,right: '100px' //主面板相对浏览器右侧距离
    //,minRight: '90px' //聊天面板最小化时相对浏览器右侧距离
    ,initSkin: '5.jpg' //1-5 设置初始背景
    //,skin: ['aaa.jpg'] //新增皮肤
    //,isfriend: false //是否开启好友
    //,isgroup: false //是否开启群组
    //,min: true //是否始终最小化主面板，默认false
    ,notice: true //是否开启桌面消息提醒，默认false
    //,voice: false //声音提醒，默认开启，声音文件为：default.wav
    ,chatLog: basePath+'imapi/chatlog' //聊天记录页面地址，若不开启，剔除该项即可
  });


  //监听在线状态的切换事件
  layim.on('online', function(data){
	  websocket.send(',fhadmin-'+data+','+user);	//发送在线隐身消息
		$.ajax({
			xhrFields: {
	            withCredentials: true
	        },
			type: "POST",
			url: basePath+'imapi/editState',
	    	data: {ONLINE:data,TYPE:"online",tm:new Date().getTime()},
			dataType:'json',
			success: function(data){}
		});
  });
  
  //监听签名修改
  layim.on('sign', function(value){
	  $.ajax({
			xhrFields: {
	            withCredentials: true
	        },
			type: "POST",
			url: basePath+'imapi/editState',
	    	data: {AUTOGRAPH:data,TYPE:"auto",tm:new Date().getTime()},
			dataType:'json',
			success: function(data){}
		});
  });
  
  //监听layim建立就绪
  layim.on('ready', function(res){

    //console.log(res.mine);
//	layui.readyMenu.init(); 					//更新右键点击事件
	$.ajax({
		xhrFields: {
            withCredentials: true
        },
		type: "POST",
		url: basePath+'imapi/getMsgCount',			//未读消息(加好友群等)总数
    	data: {tm:new Date().getTime()},
		dataType:'json',
		success: function(data){
			if('01' == data.result){
				layim.msgbox(data.count); 		//消息盒子有新消息
			}
		}
	});
	$.ajax({
		xhrFields: {
            withCredentials: true
        },
		type: "POST",
		url: basePath+'imapi/getNoreadMsg',		//未读消息(离线消息)接口
    	data: {tm:new Date().getTime()},
		dataType:'json',
		success: function(data){
			if('has' == data.result){
				$.each(data.list, function(i, list){
					layim.getMessage({
						  username: list.NAME 		//消息来源用户名
						  ,avatar: list.PHOTO 		//消息来源用户头像
						  ,id: list.USERNAME 		//消息的来源ID（如果是私聊，则是用户id，如果是群聊，则是群组id）
						  ,type: "friend" 			//聊天窗口来源类型，从发送消息传递的to里面获取
						  ,content: list.CONTENT 	//消息内容
						  ,timestamp:list.CTIME
					});
				 });
			}
		}
	});
	layer.closeAll('loading');
  });

 var $ = layui.jquery;
  //接收到好友或群的消息
  function getFHMessage(data){
	  myFname.window.closefuns();
	  layer.open({
          type: 2,//弹出框类型
          title:false,
          closeBtn: false,
		  btn: false,
		  id: 'chat_modal_wrap',
		  skin: 'chat_modal_wrap',
		  moveType: 1,
          shadeClose: false, //点击遮罩关闭层
          moveType: 1,
          zIndex: layer.zIndex,
          area: ["50%", '550px'],
          shift:1,//弹出框动画效果
          content: basePath+'/sys/cust/by/im/wxid?wxid='+data.USERNAME+"&syswxid="+data.TOID, //发送一个请求，后台处理数据返回到一个html页面加载到layer弹出层中
          success: function(layero, index){
			  //form.render('select');
			  //modalClick({ $, layer, form, table, chatOffset: chatDom.offset(), chatWidth: chatDom.width(), chatHeight: chatDom.height() });
          }
    });
	  
	 /*layim.getMessage({
	  username: data.NAME 		//消息来源用户名
	  ,avatar: data.PHOTO 		//消息来源用户头像
	  ,id: data.USERNAME 		//消息的来源ID（如果是私聊，则是用户id，如果是群聊，则是群组id）
	  ,type: "friend" 			//聊天窗口来源类型，从发送消息传递的to里面获取
	  ,content: data.CONTENT 	//消息内容
	  ,timestamp:data.CTIME
		});*/
  }
  
  //接收到好友上线消息，头像取消置灰
  function friendOnline(id){
	  layim.setFriendStatus(id, 'online'); //设置指定好友在线，即头像取消置灰
  }
  
  //接收到好友下线消息，头像置灰
  function friendOffline(id){
	  layim.setFriendStatus(id, 'offline'); //设置指定好友下线，即头像置灰
  }
  
  //申请好友消息
  function applyIFriend(){
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //申请加群消息
  function applyIQgroup(){
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //踢出消息
  function kickoutIQgroup(QGROUP_ID){
	  layui.layim.removeList({
		  type: 'group' 
		  ,id: QGROUP_ID //群ID
		});
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }

  //删除好友(被对方拉黑)
  function removeFriend(FUSERNAME){
	  layim.removeList({
		  type: 'friend' //或者group
		  ,id: FUSERNAME //好友用户名
		});
  }
  
  //对方同意添加好友(加入好友栏目)
  function addFriend(message){
	  layim.addList({
		  type: 'friend' 				//列表类型，只支持friend和group两种
		  ,avatar: message.avatar 		//好友头像
		  ,username: message.username	//好友昵称
		  ,groupid: message.groupid		//所在的分组id
		  ,id: message.id 				//好友id
		  ,status: message.status 		//在线状态
		  ,sign: message.sign		 	//好友签名
	  });
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
	  layui.readyMenu.init(); 			//更新右键点击事件
  }
  
  //对方同意添群(加入群组栏目)
  function addIQgroup(message){
	  layim.addList({
		  type: 'group' 					//列表类型，只支持friend和group两种
		  ,avatar: message.avatar			//群组头像
		  ,groupname: message.groupname 	//群组名称
		  ,id: message.id 					//群组id
	  });
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //群解散
  function delQgroup(QGROUP_ID){
	  layui.layim.removeList({
		  type: 'group' 
		  ,id: QGROUP_ID //群ID
		});
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //监听发送消息
  layim.on('sendMessage', function(data){
    var To = data.to;
    //console.log(data);
    
    var mine = data.mine; 	//发送者数据(消息内容以及发送者资料)
    console.log(data);
    /** 
     *	user:发送者用户名
	 *	uname：发送者用户姓名
	 *	To.id：对方用户名
	 *	To.type：消息类型 group or friend
	 *	mine.avatar:发送者用户头像
	 *	mine.content：消息内容
	 */
    console.log(",fhadmin-msg,"+user+",fh,"+user+",fh,"+To.id+",fh,"+To.type+",fh,"+mine.avatar+",fh,"+mine.content)
    weChatSend(To.type, To.id, mine.content, To.groupid,mine.id);
	websocket.send(",fhadmin-msg,"+user+",fh,"+user+",fh,"+To.id+",fh,"+To.type+",fh,"+mine.avatar+",fh,"+mine.content);
    
  });

  function weChatSend(type, wxId, content, groupId,userId) {
	  $.ajax({
		  xhrFields : {
				withCredentials : true
		  },
		  type : "POST",
		  url : basePath+ 'imapi/send', // 发送好友文本消息
		  data : {
			  type : type,
			  content : content,
			  wxId : wxId,
			  groupId : groupId,
			  userId : userId
		  },
		  dataType : 'json',
		  success : function(data) {
			  if ('01' == data.result) {
				  layim.msgbox(data.count); // 消息盒子有新消息
			  }
		  }
	  });
  }
  //监听查看群员
  layim.on('members', function(data){
    //console.log(data);
  });
  
  //监听聊天窗口的切换
  layim.on('chatChange', function(res){
    var type = res.data.type;
    console.log(res.data.id)
    if(type === 'friend'){
      //模拟标注好友状态
      //layim.setChatStatus('<span style="color:#FF5722;">在线</span>');
    } else if(type === 'group'){
      //模拟系统消息
    }
  });

});




//从自己好友栏删除好友
function removeFriendByI(FUSERNAME){
layui.layim.removeList({
	  type: 'friend' 
	  ,id: FUSERNAME //好友用户名
	});
}

//从对方好友栏里面删除自己
function removeIFromFriend(FUSERNAME){
	websocket.send(',fhadmin-remove,'+user+',fh,'+FUSERNAME); 
}

//从自己手机好友栏里面删除对方
function removeFriendFromMobile(FUSERNAME){
	websocket.send(',fhadmin-remove,'+FUSERNAME+',fh,'+user); 
}

//退出群
function removeQgroup(QGROUP_ID,TYPE){
layui.layim.removeList({
	  type: 'group' 
	  ,id: QGROUP_ID //群ID
	});
if('del'==TYPE){
	websocket.send(',fhadmin-delqgroup,'+QGROUP_ID); 	//解散群，通知所有群成员
}else{
	websocket.send(',fhadmin-applyqgroup,'+QGROUP_ID);  //申请群和退群，都是通知群主，此函数可共用一个
}
}

//新建群时，把群添加到群组栏里面
function addQgroup(id,avatar,groupname){
	layui.layim.addList({
		type: 'group' 			//列表类型
	  	,avatar: avatar 		//群组头像
	  	,groupname: groupname 	//群组名称
	  	,id: id					//群组id
	});
}

//同意对方申请好友，发送同意信息给对方
function agreeFriend(FUSERNAME){
	websocket.send(',fhadmin-agreefriend,'+user+",fh,"+FUSERNAME);
}

//同意对方申请加群，发送同意信息给对方
function agreeQgroup(uid,group){
	websocket.send(',fhadmin-agreeqgroup,'+uid+",fh,"+group);
}

//踢出群，发踢出信息给对方
function kickoutQgroup(uid,group){
	websocket.send(',fhadmin-kickoutqgroup,'+uid+",fh,"+group);
}

//申请好友，发送消息
function applyFriend(uid){
	websocket.send(',fhadmin-applyfriend,'+uid);
}

//申请加群，发送消息
function applyQgroup(QGROUP_ID){
	websocket.send(',fhadmin-applyqgroup,'+QGROUP_ID);
}
