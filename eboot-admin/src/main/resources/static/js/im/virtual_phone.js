let list = [{
		value: 'zhinan',
		label: '指南',
		children: [{
				value: 'shejiyuanze',
				label: '设计原则设计原则设计原则设计原则',
				children: [{
						value: 'yizhi',
						label: '一致',
					},
					{
						value: 'fankui',
						label: '反馈',
					},
					{
						value: 'xiaolv',
						label: '效率',
					},
					{
						value: 'kekong',
						label: '可控',
					},
				],
			},
			{
				value: 'daohang',
				label: '导航',
				children: [{
						value: 'cexiangdaohang',
						label: '侧向导航',
					},
					{
						value: 'dingbudaohang',
						label: '顶部导航',
					},
				],
			},
		],
	},
	{
		value: 'zujian',
		label: '组件',
		children: [{
				value: 'basic',
				label: 'Basic',
				children: [{
						value: 'layout',
						label: 'Layout 布局',
					},
					{
						value: 'color',
						label: 'Color 色彩',
					},
					{
						value: 'typography',
						label: 'Typography 字体',
					},
					{
						value: 'icon',
						label: 'Icon 图标',
					},
					{
						value: 'button',
						label: 'Button 按钮',
					},
				],
			},
			{
				value: 'form',
				label: 'Form',
				children: [{
						value: 'radio',
						label: 'Radio 单选框',
					},
					{
						value: 'checkbox',
						label: 'Checkbox 多选框',
					},
					{
						value: 'input',
						label: 'Input 输入框',
					},
					{
						value: 'input-number',
						label: 'InputNumber 计数器',
					},
					{
						value: 'select',
						label: 'Select 选择器',
					},
					{
						value: 'cascader',
						label: 'Cascader 级联选择器',
					},
					{
						value: 'switch',
						label: 'Switch 开关',
					},
					{
						value: 'slider',
						label: 'Slider 滑块',
					},
					{
						value: 'time-picker',
						label: 'TimePicker 时间选择器',
					},
					{
						value: 'date-picker',
						label: 'DatePicker 日期选择器',
					},
					{
						value: 'datetime-picker',
						label: 'DateTimePicker 日期时间选择器',
					},
					{
						value: 'upload',
						label: 'Upload 上传',
					},
					{
						value: 'rate',
						label: 'Rate 评分',
					},
					{
						value: 'form sheet',
						label: 'Form 表单',
					},
				],
			},
			{
				value: 'data',
				label: 'Data',
				children: [{
						value: 'table',
						label: 'Table 表格',
					},
					{
						value: 'tag',
						label: 'Tag 标签',
					},
					{
						value: 'progress',
						label: 'Progress 进度条',
					},
					{
						value: 'tree',
						label: 'Tree 树形控件',
					},
					{
						value: 'pagination',
						label: 'Pagination 分页',
					},
					{
						value: 'badge',
						label: 'Badge 标记',
					},
				],
			},
			{
				value: 'notice',
				label: 'Notice',
				children: [{
						value: 'alert',
						label: 'Alert 警告',
					},
					{
						value: 'loading',
						label: 'Loading 加载',
					},
					{
						value: 'message',
						label: 'Message 消息提示',
					},
					{
						value: 'message-box',
						label: 'MessageBox 弹框',
					},
					{
						value: 'notification',
						label: 'Notification 通知',
					},
				],
			},
			{
				value: 'navigation',
				label: 'Navigation',
				children: [{
						value: 'menu',
						label: 'NavMenu 导航菜单',
					},
					{
						value: 'tabs',
						label: 'Tabs 标签页',
					},
					{
						value: 'breadcrumb',
						label: 'Breadcrumb 面包屑',
					},
					{
						value: 'dropdown',
						label: 'Dropdown 下拉菜单',
					},
					{
						value: 'steps',
						label: 'Steps 步骤条',
					},
				],
			},
			{
				value: 'others',
				label: 'Others',
				children: [{
						value: 'dialog',
						label: 'Dialog 对话框',
					},
					{
						value: 'tooltip',
						label: 'Tooltip 文字提示',
					},
					{
						value: 'popover',
						label: 'Popover 弹出框',
					},
					{
						value: 'card',
						label: 'Card 卡片',
					},
					{
						value: 'carousel',
						label: 'Carousel 走马灯',
					},
					{
						value: 'collapse',
						label: 'Collapse 折叠面板',
					},
				],
			},
		],
	},
	{
		value: 'ziyuan',
		label: '资源',
		children: [{
				value: 'axure',
				label: 'Axure Components',
			},
			{
				value: 'sketch',
				label: 'Sketch Templates',
			},
			{
				value: 'jiaohu',
				label: '组件交互文档',
			},
		],
	},
	{
		value: 1,
		label: 'xxx'
	},
];
let openModal = {}
let modelPosType = []

function modalClick({
	$,
	layer,
	form,
	table,
	chatOffset,
	chatWidth,
	chatHeight,
	isMove
}) {
	if (isMove) return
	let layer_modal = {}
	$(".tab_select span").on('click', function() {
		var othis = $(this),
			type = othis.data('type');
		if (type === 7) {
			openModal[`${type}Right`] = !openModal[`${type}Right`]
			openModal[`${type}Right`] ? (layer_modal[`${type}Right`] = layer.open({
				type: 1,
				title: false,
				closeBtn: false,
				btn: false,
				tipsMore: true,
				zIndex: layer.zIndex,
				shade: false,
				offset: [chatOffset.top, chatOffset.left + chatWidth],
				skin: 'supply_of_goods',
				id: 'supply_of_goods',
				// move : '.user_info',
				// skin: 'chat_modal_wrap',
				moveType: 1,
				area: ["32%", '550px'],
				success: function(layero, index) {
					// 重新渲染弹层中的下拉选择框select
					form.render('select');
					table.render({
						elem: '#chat-table',
						height: 312,
						width: $('#chat_model_table').width(),
						// , url: '/demo/table/user/' //数据接口
						// , page: true //开启分页
						cols: [
							[ // 表头
								{
									field: 'action',
									title: '出发地-目的地',
									width: 150
								},
								{
									field: 'detail',
									title: '货车及用车详情',
									width: 150
								},
								{
									field: 'tel',
									title: '手机号码',
									width: 100
								},
								{
									field: 'price',
									title: '客户出价',
									width: 100
								},
								{
									field: 'time',
									title: '处理日期及状态'
								},
								{
									field: 'person',
									title: '操作人',
									fixed: 'right',
									width: 140
								}
							]
						]
					});
				},
				content: supplyGoodsDom()
			})) : layer.close(layer_modal[`${type}Right`])
			console.log(chatOffset, layer_modal)
		}
		modelPosType[0] = 'right'
	});

	$(".tab_select span i").on('click', function(e) {
		e.stopPropagation();
		var othis = $(this),
			modalType = othis.data('modal-type');
		if (modalType === 7) {
			openModal[`${modalType}Bottom`] = !openModal[`${modalType}Bottom`]
			openModal[`${modalType}Bottom`] ? (layer_modal[`${modalType}Bottom`] = layer.open({
				type: 1,
				title: false,
				closeBtn: false,
				btn: false,
				tipsMore: true,
				id: 'supply_of_goods_top',
				zIndex: layer.zIndex,
				shade: false,
				offset: [chatOffset.top + chatHeight, chatOffset.left],
				skin: 'supply_of_goods_top',
				moveType: 1,
				area: ["32%", '550px'],
				success: function(layero, index) {
					// 重新渲染弹层中的下拉选择框select
					form.render('select');
					table.render({
						elem: '#chat-table',
						height: 312,
						width: $('#chat_model_table').width(),
						// , url: '/demo/table/user/' //数据接口
						// , page: true //开启分页
						cols: [
							[ // 表头
								{
									field: 'action',
									title: '出发地-目的地',
									width: 150
								},
								{
									field: 'detail',
									title: '货车及用车详情',
									width: 150
								},
								{
									field: 'tel',
									title: '手机号码',
									width: 100
								},
								{
									field: 'price',
									title: '客户出价',
									width: 100
								},
								{
									field: 'time',
									title: '处理日期及状态'
								},
								{
									field: 'person',
									title: '操作人',
									fixed: 'right',
									width: 140
								}
							]
						]
					});
				},
				content: supplyGoodsDom()
			})) : layer.close(layer_modal[`${modalType}Bottom`])
		}
		modelPosType[1] = 'top'
		return false
	});

	$(".chat_left_friend .frends").on('click', function(e) {
		var othis = $(this)
		e.stopPropagation()
		othis.siblings().removeClass('select_friends');
		othis.addClass('select_friends')
	})
	$(".tab_select span").on('click', function() {
		var othis = $(this)
		othis.siblings().removeClass('select_tab');
		othis.addClass('select_tab')
	})
	$('.chat_left_friend .friends_close').on('click', function(e) {
		e.stopPropagation()
		console.log(111)
	})
	
	$(".close_modal").on('click', function () {
					layer.closeAll()
	})
}

layui.use(['form', 'layedit', 'laydate', 'layer', 'table'], function() {
	let chat_modal = null
	var $ = layui.jquery
	var table = layui.table
	var layer = layui.layer
	var form = layui.form;
	
	//form.render('select');
	$(".close_modal").on('click', function() {
		//layer.closeAll();
		var index = parent.layer.getFrameIndex(window.name);
		parent.layer.close(index);
	})
	/*$(".friends_close").on('click', function() {
		//layer.closeAll();
		var index = parent.layer.getFrameIndex(window.name);
		var sid=$(this).attr("data-sid");
		console.log("you ready close me .sid="+sid);
		parent.layer.close(index);
	});*/
	
	/*$(".chat_left_friend .frends").on('click', function () {
		var othis = $(this), custType = othis.data('cust-type')
		console.log(custType)
		$('.user_data_info').html(typeTel[custType]);
		othis.siblings().removeClass('select_friends');
		othis.addClass('select_friends')
	})*/
	
	// 监听提交
  form.on('submit(saveCust)', function(data){
    $.ajax({
		xhrFields: {
			withCredentials: true
		},
		type: "POST",
		url: httpurl+'/sys/cust/save',
		data: data.field,
		dataType: 'json',
		success: function(data) {
			layer.msg(data.msg,{icon:6});
		}
	});
    return false;
  });
});


layui.use('laydate', function() {
	var laydate = layui.laydate
	laydate.render({
		elem: '#goods-laydate',
		btns: ['clear'],
		change: function(value, date, endDate) {
			console.log(value); // 得到日期生成的值，如：2017-08-18
			console.log(date); // 得到日期时间对象：{year: 2017, month: 8, date: 18,
			// hours: 0, minutes: 0, seconds: 0}
			console.log(endDate); // 得结束的日期时间对象，开启范围选择（range:
			// true）才会返回。对象成员同上。
		}
	});
});
