var httpurl = "http://127.0.0.1:8882/";
var websocket = null;
var $;
var table;
var layer;
var form;

/**
 * | 属性 | 必填 | 类型 | 默认值 | 描述 |
| ------ | ------ | ------ | ------ | ------ |
| url | true | string | none | websocket服务端接口地址 |
| pingTimeout | false | number | 15000 | 每隔15秒发送一次心跳，如果收到任何后端消息定时器将会重置 |
| pongTimeout | false | number | 10000 | ping消息发送之后，10秒内没收到后端消息便会认为连接断开 |
| reconnectTimeout | false | number | 2000 | 尝试重连的间隔时间 |
| pingMsg | false | string | "heartbeat" | ping消息值 |
| repeatLimit | false | number | null | 重连尝试次数。默认不限制 |
 */
layui.use(['form', 'layedit', 'laydate', 'layer', 'table'], function(layim){
	
	 var $ = layui.jquery;
 	  var user =$.cookie('user');
	  var sotckUrl = $.cookie("sotckUrl");

	 const options = {
        url: sotckUrl,
        pingTimeout: 60000, 
        pongTimeout: 10000, 
        reconnectTimeout: 2000,
        pingMsg: "heartbeat"
    }
	  
	websocket = new WebsocketHeartbeatJs(options);
    websocket.onopen = function () {
        
        websocket.send(',fhadmin-join,'+user);;
      //  console.log(user);
	  //	console.log(sotckUrl);
       //console.log('im connect success');
    }
 
    websocket.onreconnect = function () {
    	
       // console.log('im reconnecting...');
    } 
	websocket.onerror = function() {//连接失败
		
		//  console.log('连接发生错误,刷新或者更换浏览器试试', {icon: 5});
	};
	websocket.onclose = function() {//连接断开	
			 
		websocket.send(',fhadmin-leave,');		//发送下线消息
	};
	websocket.onmessage = function(message) {	//消息接收
		debugger
	//	console.log(message);
		  
		var message = JSON.parse(message.data);
		//if('friend' == message.TYPE || 'group' == message.TYPE){ //接收好友或群发送的消息
		if('friend' == message.TYPE ){ //接收好友或群发送的消息
			getFHMessage(message);
		}else if('user_join' == message.type){
			friendOnline(message.user);
			layer.msg("好友"+message.name+"上线了");
		}else if('user_leave' == message.type){
			friendOffline(message.user);
			layer.msg("好友"+message.name+'下线了');
		}else if('user_online' == message.type){
			friendOnline(message.user);		//好友设置在线状态
		}else if('user_hide' == message.type){
			friendOffline(message.user);	//好友设置隐身状态
		}else if('user_applyfriend' == message.type){
			applyIFriend();					//申请加好友
		}else if('user_emove' == message.type){
			removeFriend(message.user);		//删除好友(被动删，被拉黑)
		}else if('agreefriend' == message.type){
			addFriend(message);				//对方同意添加好友
		}else if('applyqgroup' == message.type){
			applyIQgroup();					//申请加群申请
		}else if('agreeqgroup' == message.type){
			addIQgroup(message);			//对方同意群申请
		}else if('kickoutqgroup' == message.type){
			kickoutIQgroup(message.id);		//踢出群
		}else if('delqgroup' == message.type){
			delQgroup(message.QGROUP_ID);	//群解散
			layer.msg(message.CONTENT);
		}
	}; 
	//加载层-默认风格

	/*layer.msg('通讯连接中...', {
	  icon: 16
	  ,shade: 0.01
	});*/

 

  //接收到好友或群的消息
  function getFHMessage(data){
 		debugger
 		vm.getFHMessage(data);
	  	
  }
  
  //接收到好友上线消息，头像取消置灰
  function friendOnline(id){
	//  layim.setFriendStatus(id, 'online'); //设置指定好友在线，即头像取消置灰
  }
  
  //接收到好友下线消息，头像置灰
  function friendOffline(id){
	//  layim.setFriendStatus(id, 'offline'); //设置指定好友下线，即头像置灰
  }
  
  //申请好友消息
  function applyIFriend(){
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //申请加群消息
  function applyIQgroup(){
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //踢出消息
  function kickoutIQgroup(QGROUP_ID){
	  layui.layim.removeList({
		  type: 'group' 
		  ,id: QGROUP_ID //群ID
		});
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }

  //删除好友(被对方拉黑)
  function removeFriend(FUSERNAME){
	  layim.removeList({
		  type: 'friend' //或者group
		  ,id: FUSERNAME //好友用户名
		});
  }
  
  //对方同意添加好友(加入好友栏目)
  function addFriend(message){
	  layim.addList({
		  type: 'friend' 				//列表类型，只支持friend和group两种
		  ,avatar: message.avatar 		//好友头像
		  ,username: message.username	//好友昵称
		  ,groupid: message.groupid		//所在的分组id
		  ,id: message.id 				//好友id
		  ,status: message.status 		//在线状态
		  ,sign: message.sign		 	//好友签名
	  });
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
	  layui.readyMenu.init(); 			//更新右键点击事件
  }
  
  //对方同意添群(加入群组栏目)
  function addIQgroup(message){
	  layim.addList({
		  type: 'group' 					//列表类型，只支持friend和group两种
		  ,avatar: message.avatar			//群组头像
		  ,groupname: message.groupname 	//群组名称
		  ,id: message.id 					//群组id
	  });
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }
  
  //群解散
  function delQgroup(QGROUP_ID){
	  layui.layim.removeList({
		  type: 'group' 
		  ,id: QGROUP_ID //群ID
		});
	  layim.msgbox('新');
	  $("#fhsmsobj").html('<audio style="display: none;" id="fhsmstsy" src="../../../assets/sound/'+fhsmsSound+'.mp3" autoplay controls></audio>');
  }

  function weChatSend(type, wxId, content, groupId,userId) {
	  $.ajax({
		  xhrFields : {
				withCredentials : true
		  },
		  type : "POST",
		  url : basePath+ 'imapi/send', // 发送好友文本消息
		  data : {
			  type : type,
			  content : content,
			  wxId : wxId,
			  groupId : groupId,
			  userId : userId
		  },
		  dataType : 'json',
		  success : function(data) {
			  if ('01' == data.result) {
				  layim.msgbox(data.count); // 消息盒子有新消息
			  }
		  }
	  });
  }

});




//从自己好友栏删除好友
function removeFriendByI(FUSERNAME){
layui.layim.removeList({
	  type: 'friend' 
	  ,id: FUSERNAME //好友用户名
	});
}

//从对方好友栏里面删除自己
function removeIFromFriend(FUSERNAME){
	websocket.send(',fhadmin-remove,'+user+',fh,'+FUSERNAME); 
}

//从自己手机好友栏里面删除对方
function removeFriendFromMobile(FUSERNAME){
	websocket.send(',fhadmin-remove,'+FUSERNAME+',fh,'+user); 
}

//退出群
function removeQgroup(QGROUP_ID,TYPE){
layui.layim.removeList({
	  type: 'group' 
	  ,id: QGROUP_ID //群ID
	});
if('del'==TYPE){
	websocket.send(',fhadmin-delqgroup,'+QGROUP_ID); 	//解散群，通知所有群成员
}else{
	websocket.send(',fhadmin-applyqgroup,'+QGROUP_ID);  //申请群和退群，都是通知群主，此函数可共用一个
}
}

//新建群时，把群添加到群组栏里面
function addQgroup(id,avatar,groupname){
	layui.layim.addList({
		type: 'group' 			//列表类型
	  	,avatar: avatar 		//群组头像
	  	,groupname: groupname 	//群组名称
	  	,id: id					//群组id
	});
}

//同意对方申请好友，发送同意信息给对方
function agreeFriend(FUSERNAME){
	websocket.send(',fhadmin-agreefriend,'+user+",fh,"+FUSERNAME);
}

//同意对方申请加群，发送同意信息给对方
function agreeQgroup(uid,group){
	websocket.send(',fhadmin-agreeqgroup,'+uid+",fh,"+group);
}

//踢出群，发踢出信息给对方
function kickoutQgroup(uid,group){
	websocket.send(',fhadmin-kickoutqgroup,'+uid+",fh,"+group);
}

//申请好友，发送消息
function applyFriend(uid){
	websocket.send(',fhadmin-applyfriend,'+uid);
}

//申请加群，发送消息
function applyQgroup(QGROUP_ID){
	 
	websocket.send(',fhadmin-applyqgroup,'+QGROUP_ID);
}
function webSocketClose(){
	debugger
	websocket.close();
}
