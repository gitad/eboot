var phoneWs;
initPhoneWs();

function initPhoneWs() {
	if ('WebSocket' in window) {
		websocket = new ReconnectingWebSocket(
				"ws://http://www.phpernote.com/websocket/get/overview");
	} else if ('MozWebSocket' in window) {
		websocket = new MozWebSocket(
				"ws://http://www.phpernote.com/websocket/get/overview");
	} else {
		websocket = new SockJS("http://host/websocket/get/overview");
	}
}