var ws={};
//var sotckUrl = "ws://120.79.141.14:17115/yuntel";
//var sotckUrl = "ws://127.0.0.1:8899/";
var sotckUrl = "ws://192.168.0.78:8899/";
var $;
var table;
var layer;
var form;
 var webSocket=[];
layui.use(['form', 'layedit', 'laydate', 'layer', 'table'], function () {
	 const options = {
        url: sotckUrl,
        pingTimeout: 60000, 
        pongTimeout: 60000, 
        reconnectTimeout: 2000,
        pingMsg: "heartbeat"
    }

    
	let chat_modal = null
	$ = layui.jquery;
	table = layui.table;
	layer = layui.layer;
	form = layui.form;
	$.ajax({
		xhrFields: {
			withCredentials: true
		},
		type: "GET",
		url: basePath + '/imapi/incomming/phone/list',
		data: {},
		dataType: 'json',
		success: function(data) {
			
			if ('0' == data.code) {
				var rs = data.data;
				$.each(rs, function(i, val) {	 
					//layer.msg('来电通信链接成功');
					   ws[i] = new WebsocketHeartbeatJs(options);
					   ws[i].onopen = function () {
					     	//wslogin(ws[i],val.loginUserName,val.loginPwd);
					     	wslogin(ws[i],val.phoneNum,val.loginPwd);
					     	
							wsInit(ws[i],'');					      
					    }
					    debugger
					     var  obj = {};
					     obj['socket']=ws[i];　
					     obj['isOutBound']=val.isOutBound;
					     obj['phoneatt']=val.phoneAttr;					 
					     webSocket.push(obj);
				});
			}
		}
	});
	
	
	
	 
});


let chatDom = null;
function wsInit(wt,val) {
	// 重连先关闭
	if (val == 'reConn') {
		wt.close();
	}

	if ("WebSocket" in window) {
		// 打开一个 web socket，全局共用一个
		wt.onmessage = function(evt) {
			 
			 console.log(evt.data);			 
			var data = JSON.parse(evt.data);			
			switch (data['cmd']) {
				// 服务端ping客户端
				case 'USB':
					ws.send('{"cmd":"USB","connected":"true","success":"true","message":"成功"}');
					break;
				case 'CORG':
					ws.send('{"cmd":"CORG","number":"10010","success":"true","message":"成功"}');
					break;
				case 'CALLING':
					ws.send('{"cmd":"CALLING","number":"10010","success":"true","message":"成功"}');
					break;
				case 'CBEGIN':
					ws.send('{"cmd":"CBEGIN","success":"true","message":"成功"}');
					break;
				case 'ALERT':
					ws.send('{"cmd":"ALERT","success":"true","message":"成功"}');
					break;
				case 'CEND':
					ws.send('{"cmd":"CEND","success":"true","message":"成功"}');
					break;
			}
			
			if(data.hasOwnProperty("messageType")){
			 		
				var messageType=data.messageType;				
				if("phoneInit" == messageType){
					 window.parent.showPop(data.data.phone,"");
				}
				if("phoneDisconnect" ==messageType ){//电话销毁
					
					if(data.data.outgoing==false){//来电未接 挂掉
							$.ajax({
								xhrFields: {
									withCredentials: true
								},
								type: "POST",
								url: basePath + 'sys/cust/update/phone',
								data: {
									"phone":data.data.phone
								},
								dataType: 'json',
								success: function(data) {
									 
									 
								}
							});
					}
					 
				}
				if("phoneCallLog" == messageType){
				 
					var callid=data.data.callid;
					$.ajax({
						xhrFields: {
							withCredentials: true
						},
						type: "POST",
						url: basePath + '/imapi/incomming/phone/savelog',
						data: {"data":JSON.stringify(data.data)},
						dataType: 'json',
						success: function(data) {
							
							if ('0' == data.code) {
								wt.send('{"messageType":"phoneCallLogResult","data":{"callid":"'+callid+'"}}');
							}
						}
					});
				}
				
			}
			
			
		};
		// 出现错误
		wt.onerror = function(evt) {
			  
		}
		// 连接断开
		wt.onclose = function(evt) {
			  
		}
	} else {
		 
		 
	}
}

function wslogin(wt,userName, userPwd) {
	
	var cmd = '{ "messageType":"clientLoginIn", "data":{"username":"' + userName + '","password":"' + userPwd + '"} }';
	wt.send(cmd);
}

  

function sendMsg(val,number,area,cid,att) {
	  debugger
	var wt;
	var newWt; 
	var phoneAtt=att.split(",");
	for (var i=0; i < webSocket.length; i++) {
 		var socketVal=webSocket[i];
 	 
	 	if(cid=='-1'&&socketVal.isOutBound==3){//判断是不是虚拟号
		    wt=socketVal.socket;
		   
		    break;
	 	}else  if(area=='0535'&&phoneAtt[phoneAtt.length-1]!="电信"&&socketVal.isOutBound==2){//先去判断是否是烟台的号码   1默认 2烟台  3 本地
     		 wt=socketVal.socket;
     		 
     		 break;
     	}  
     	if (socketVal.isOutBound==1&&newWt==undefined){
     		 
 			 newWt=socketVal.socket; 		 
 		} 
	};
	if(wt==null||wt==undefined){
		wt=newWt;
	}
	/*$.each(webSocket, function(i, val) {
		   
		
	});*/
	 
	// Web Socket 已连接上，使用 send() 方法发送数据
	 var state = wt.readyState;

	 
	if (val == "CALLING") {
		 
		val = '{"messageType":"phoneCall","data":{"phone":"' + number + '"}}';
	}
	if (val == "hungup") {
		val = '{ "messageType":"phoneHangup", "data":{} }';
	}
	if (val == "ATA") {
		val = '{"cmd":"ATA"}';
	}
	if (val == "READIMEI") {
		val = '{"cmd":"READIMEI"}';
	}
	if (val == "READVER") {
		val = '{"cmd":"READVER"}';
	}
	if (val == "READSTATUS") {
		val = '{"cmd":"READSTATUS"}';
	}
	if (val == "startrecord") {
		function getLocalTime(nS) {
			return new Date(parseInt(nS) * 1000).Format("yyyyMMddhhmmss");
		}
		Date.prototype.Format = function(fmt) { // author: meizz
			var o = {
				"M+": this.getMonth() + 1, // 月份
				"d+": this.getDate(), // 日
				"h+": this.getHours(), // 小时
				"m+": this.getMinutes(), // 分
				"s+": this.getSeconds(), // 秒
				"q+": Math.floor((this.getMonth() + 3) / 3), // 季度
				"S": this.getMilliseconds() // 毫秒
			};
			if (/(y+)/.test(fmt))
				fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
			for (var k in o)
				if (new RegExp("(" + k + ")").test(fmt))
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
			return fmt;
		}
		// E:\\qtproject\\PhoneService\\kaerHjSDK\\recordfile\\20180514.mp3
		var time = Math.round(new Date() / 1000);
		var times = (getLocalTime(time));
		 
		var filename = "E:/qtproject/PhoneService/kaerHjSDK/recordfile/" + times + ".mp3";
		val = '{"cmd":"startrecord","filename":"' + filename + '"}';
		// alert(filename);
	}
	if (val == "stoprecord") {
		val = '{"cmd":"stoprecord"}';
	}
	if (val == "HIDENUMBER") {
		function getRadioValue(laidian) {
			var obj = document.getElementsByName(laidian);
			for (i = 0; i < obj.length; i++) {
				if (obj[i].checked) {
					return obj[i].value;
				}
			}
			return "undefined";
		}

		function getRadioLValue(haoma) {
			var obj = document.getElementsByName(haoma);
			for (i = 0; i < obj.length; i++) {
				if (obj[i].checked) {
					return obj[i].value;
				}
			}
			return "undefined";
		}
		var type = getRadioValue('laidian');
		var hidden = getRadioLValue('haoma');
		 
		val = '{"cmd":"HIDENUMBER","type":"' + type + '","hidden":"' + hidden + '"}';
	}
	if (val == "GETNUMBERHIDEN") {
		function getRadioDqValue(duqvhj) {
			var obj = document.getElementsByName(duqvhj);
			for (i = 0; i < obj.length; i++) {
				if (obj[i].checked) {
					return obj[i].value;
				}
			}
			return "undefined";
		}
		var type = getRadioDqValue('duqvhj');
		val = '{"cmd":"GETNUMBERHIDEN","type":"' + type + '"}';
	}
	if (val == "SETRECORDVOICE") {
		var voice = document.getElementById("voice").value;
		val = '{"cmd":"SETRECORDVOICE","voice":"' + voice + '"}';
	}
	 
	
	wt.send(val);
	 
}
