/**
 * layselect 下拉框插件，只支持单选
 * render参数{
 * 	elem：元素ID，待#号必传
 *  url：请求路径的URL，必传
 *  data:请求url所携带的参数，可选
 *  type:请求方式，默认为get，可选
 *  format:格式化方法映射，将返回的Data元素映射乘标准格式
 *  select:指定选中的索引项，默认第一个
 *  onselect:点击选择时鉴定
 * }
 
 */
layui.define(['element','form','jquery'],function(exports){
	 
	var element = layui.element;
	var form = layui.form;
	var $ = layui.jquery;
	var obj={
		//{elem,url,data,type,format}
		render:function(param){
			
			var that = this;
			var eid = param.elem;
			if(param.type == null || param.type==undefined){
				param.type = 'get';//默认get请求
			}
			if(param.url == null || param.url==undefined){
				param.url = '/error?msg=请求路径不存在';
			}
			if(param.data){
				param.data = JSON.stringify(param.data);
			}else{
				param.data = {}
			}
			$.ajax({
		        url: param.url,
		        type: param.type,
		        data: param.data,
		        async: 'false',
		        dataType: 'json',
		        headers: {Accept: "application/json; charset=utf-8"},
		        contentType : 'application/json',//指定json头
		        success: function(obj){
		        		debugger
		        		data=obj.data;
		        	$(eid).empty();//请求成功时清空
		        	$(eid).prepend("<option value=''>请选择</option>");//添加第一个option值
		        	var option = new Array();
		        if (data != undefined && data != null && data != '') {
		        
		        	for (var i = 0; i < data.length; i++) {		
		        		     		  
	       				 //node = ('<option value="'+data[i].typeCode+'">'+data[i].name+'</option>');
	       				  node = ('<option value="'+data[i].name+'">'+data[i].name+'</option>');
	       				$(eid).append(node)	        			    
		        	}
		        }
		        	//监听事件
		        	form.render('select');//select是固定写法 不是选择器
    				//这里做自己想做的事情
                    if(param.onselect){//选中事件
                    	form.on('select('+eid.replace('#','')+')', function(select){
                    		param.onselect(select.value);
                    	});
                    }
		        },
		        error: function (e) {
					console.log('url:'+param.url);
		        	console.log("ajax request error");
		        }
		    });
		}
	};
	exports('layselect',obj);
});