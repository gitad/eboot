  
 var vm = new Vue({
	el: '#app',
	
	data:{
		dialogVisible:false,//屏蔽页面显示
		otherStyle:"success",
	    cContactPeoData:[],  
	    identityData :[],//身份
	     dialogVisiblepic:false,
	       dialogImageUrl:'',
	    form: {},
	    phoneDefaultSel:[
		    {
		      value: '默认',
              label: '默认'
		    },{
		      value: '非默认',
              label: '非默认'
		    }
	    ],
	    wxTypeSel:[{//电话类型
	          value: '企业号',
	          label: '企业号'
	         },
	         {
	          value: '个人号',
	          label: '个人号'
        	}
        ],  
         sexSelData:[{//性别
	          value: '男',
	          label: '男'
	         },
	         {
	          value: '女',
	          label: '女'
        	}
        ],      
        contactStateSel:[
        	{
	          value: '正常',
	          label: '正常'
	        }, {
	          value: '分离',
	          label: '分离'
	        },
        ],
        CerData:[//认证
          {
	          value: '已认证',
	          label: '已认证'
	       },
	        {
	          value: '未认证',
	          label: '未认证'
	       }
        ],
        mainId:'',
         xszfileList:[],//图片列表
    },
 	watch: {
 
    }, 
	methods: {	
        //初始执行
        init() {       
          	this.query()    
          	this.identityData=this.getDropValue("719f904379094b0780927983fec7cfce").data;
          	console.log(this.identityData);
        },
        getId:function(val){
        	debugger
        	vm.mainId=val;
        },
          handlePictureCardPreview(file) {
          	debugger
	           vm.dialogImageUrl = file.url;
        	  vm.dialogVisiblepic = true;
        },
          xszuploadSuccess:function(val){
        	debugger
        	vm.uploadFile(val,"LXR",vm.mainId);
   
        },
        queryPicList:function(id,type){
        	
         	var data={};
      		
      		$.ajax({
				  type: "POST",
		          url: '/sysDocFile/query',	      
		          data:{
	            	"businessId":id,
	            	"businessType":type,
	              },	
	                
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	   data=obj;   
		         },
		         
		     });
		     return data;
      }, 
            query:function(){ 
        	 
	        	$.ajax({
		 		    type: "POST",
		 		   url: '/cContactPeo/query?peoId=1',
		           dataType:"json",
		           cache:false, // 设置为 false 将不缓存此页面	           
		           success: function(obj) {
		          		   debugger 
		          	   vm.cContactPeoData=obj;
		          	   for (var i=0; i <  vm.cContactPeoData.length; i++) {
						   
						    vm.xszfileList.push(vm.queryPicList( vm.cContactPeoData[i].contactId,"LXR")) ;
						 };
		 		   },
		           error: function(data) {
		               // 请求失败函数
		               console.log(data);
		           }
	       		});  
	        	
        },
          picRemove:function(val){
          	debugger
         	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/del',	      
		          data: JSON.stringify(val),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
         uploadFile:function(val,businessType,businessId){
         	debugger
        	var file={};
        	file.name=val;
        	file.businessId=businessId;
        	file.businessType=businessType;
        	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/save',	      
		          data: JSON.stringify(file),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
     
        radioClick:function(val,tableindex, rowindex){
        	debugger
        	 if(val=="默认"){
        	 	for(var i=0;i<vm.cContactPeoData[tableindex].contactPeoList.length;i++){
        				vm.cContactPeoData[tableindex].contactPeoList[i].phoneDefault="非默认"
	        	}
	         	vm.cContactPeoData[tableindex].contactPeoList[rowindex].phoneDefault="默认"
        	 }
        	
        },
        dbClick:function(){
        	debugger
        },
             //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 debugger			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
        contactSortChange:function(index,val,domain){
        	debugger
        
        	if(vm.cContactPeoData[index].contactSort==0){
        			var k=0;
        		 for(var i=0;i<vm.cContactPeoData.length;i++){
        		 
        			if(vm.cContactPeoData[i].contactSort==1){
        				k++;
        			}
        	 	}
        	 	if(k==0){
        	 		vm.cContactPeoData[index].contactSort=1;
        	 	}else{
        	 		 vm.$message('主联系人只能有一个');
        	 		 return
        	 	}
        		
        	}else{
        	 	vm.cContactPeoData[index].contactSort=0;
        	}
        	  $.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeo/save',
	            data: JSON.stringify(vm.cContactPeoData[index]),
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
	            	 
		              vm.query();
		      		 
	          },
       	  }); 
        },
        butclick:function(index){
        	debugger
        	vm.cContactPeoData[index].contactPeoList.splice(0,0,vm.addRelData()); 
        },
        add:function(){//添加数据
        	debugger
        	vm.dialogVisible=true;
        	vm.form=vm.addData();
        },
        del:function(row){
        	debugger
        	$.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeoRel/del',
	            data: JSON.stringify(row),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
		             if(data.data==true){
		             	 vm.query();
		            	 vm.$message('删除成功');
	 			    	 return;
		             }
		 			 if(data.data==false){
		 				 vm.$message('删除失败');
	 			    	 return;
		             }
	          },
       	  }); 
        },
        save:function(data){
        	
           $.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeo/save',
	            data: JSON.stringify(data),
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
	            	vm.dialogVisible=false;
		              vm.query();
		      		 
	          },
       	  }); 
        },
        relSave:function(data){
        	
           $.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeoRel/save',
	            data: JSON.stringify(data),
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
		          
	 			    	 
		            
	          },
       	  }); 
        },
        phoneCheck:function(val,customerId){
        	debugger
        	if(val.length==11){
        			 $.ajax({
			   		    type: "POST", 		   
			             url: '/cContactPeo/checkByPhone?phone='+val+'&customerId='+customerId,
			             async:false,//取消异步请求  要不返回的值都是false
			             cache:false, // 设置为 false 将不缓存此页面	           
			             success: function(data) {           	 
			            	 debugger
			            	 if(data!=null&&data!=""){
			            	 	  vm.$message({
						          message: data,
						          type: 'warning'
						        });
			            	 }
		             },          
		         }); 
        	}
        },
        saveAll:function(){
        	debugger
        	//保存之前先执行号码校验
         
        	
        	//执行保存
        	for(var i=0;i<vm.cContactPeoData.length;i++){
        		  vm.save(vm.cContactPeoData[i]);
	        		if(vm.cContactPeoData[i].contactPeoList.length>0){
	        			for(var k=0;k<vm.cContactPeoData[i].contactPeoList.length;k++){
	        				vm.cContactPeoData[i].contactPeoList[k].contactId=vm.cContactPeoData[i].contactId;
	        				vm.relSave(vm.cContactPeoData[i].contactPeoList[k]);       				
	        			}
	        		}
        	}       	
        	  vm.$message('保存成功');
        },
        addData:function(){
			  var newData;
		    	 $.ajax({
		   		    type: "POST", 		   
		             url: '/cContactPeo/add',
		             async:false,//取消异步请求  要不返回的值都是false
		             cache:false, // 设置为 false 将不缓存此页面	           
		             success: function(data) {           	 
		            	 newData= data;
		             },          
		         });  
		     return newData;
 		 },
 		  addRelData:function(){
			  var newData;
		    	 $.ajax({
		   		    type: "POST", 		   
		             url: '/cContactPeo/relAdd',
		             async:false,//取消异步请求  要不返回的值都是false
		             cache:false, // 设置为 false 将不缓存此页面	           
		             success: function(data) {           	 
		            	 newData= data;
		             },          
		         });  
		     return newData;
 		 },
 		 
 		 
 		 
 		 
 		 
 		 
 		 
 		 
         delEnd:function(val,index){
         	
         },
         delStart:function(val,index){
         	debugger
         	vm.startForm.splice(index,index)
         },
            	  	 
        
        
		
	},
	
	mounted(){
       this.init();
   } , 
    
  
})