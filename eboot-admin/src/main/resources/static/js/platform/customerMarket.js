  
 var vm = new Vue({
	el: '#app',
	
	data:{
	  vx:false,
	  dh:false,
	  form: {},
      tableData:[],
	  callTypeData:[],
	  communicateData:[{value: '2', label: '2'},{value: '7', label: '7'},{value: '10', label: '10'},{value: '30', label: '30'},{value: '60', label: '60'}]
    },
 	watch: {
	  
  }, 
	methods: {	
        //初始执行
        init() {       
           this.query();     
           // this.callTypeData=this.getDropValue("a4d74551487f4b988398e90a31b33736").data;//通话类别
	     
        },
        gtfs:function(val){//沟通方式
        	debugger
        	if(val=="微信"){
        		vm.vx=true;
        		vm.dh=false;
        	}
        	if(val=="电话"){
        		vm.vx=false;
        		vm.dh=true;	
        	}
        },
        
        
        
        
        
        
        
        
        query:function(){ 
        	
        	 $.ajax({
			 	  type: "POST",
			       url: '/cCustomerMarket/query',
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {			     	  
		         	   vm.tableData=obj.data;  
		         	   vm.add();	      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        }, 
         
          //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
         add:function(){ 
        	
        	$.ajax({
	  		    type: "POST",
	            url: '/cCustomerMarket/add',	           
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) {
	            	
	            		 vm.form=data;
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });      	
        }, 
        rowDbclick:function(row, column, event){
         
        	vm.form=row;     
         	 
        }, 
        formatDate:function(row, column) {
        	debugger
		    // 获取单元格数据
		    let data = row[column.property]
		    if(data!=null){
		    	 let dt = new Date(data)//+ ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds()
		   		 return dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() 
		    }else{
		    	return "";
		    }
		   
		},	  	 
         save:function(){ 
        	
        	$.ajax({
	  		    type: "POST",
	            url: '/cCustomerMarket/save',
	            data: JSON.stringify(vm.form),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) {
	            	 //vm.$message.error('保存成功');
	            	 	vm.query();
	            	 	  
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });      	
        },
        del:function(){
        	debugger
        	if(vm.form.callId==null){
        		 vm.$message.error('请先双击选择数据');
        		return
        	}
        	  $.ajax({
	  		    type: "POST",
	            url: '/cCustomerMarket/del',
	            data: JSON.stringify(vm.form),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(obj) {
	            	 vm.$message.error('删除成功');
	            	vm.query();
	             
	            	
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        }); 
        	
        }, 
        
		
	},
	
	mounted(){
       this.init();
   } , 
    
  
})