 let _layer=null;
  layui.use(["jquery","layer","element"], function(){
      var $ = layui.$, layer = layui.layer,element = layui.element;;
      _layer=layui.layer;
       //监听导航点击
    
  });
var vm = new Vue({
	el: '#app',
	data:{		  
	 	type:{
	 		display: 'none',
	 	},
	  
 		wxtableData:[],
	    wxform:{
	   		wxRemark:'',wxNickname:''
	    }, 
	    weixindialogVisible:false,//微信关联
	    wxDomain:'',
	   
	   
	 	wxonlineStat:'在线',
	 	showClass:'layui-icon-down',
		visible: false,
		top: 0,
		left: 0,
		phoneType:"虚拟号",//号码类型 虚拟号 真实号
		chatStatus:"",//类型
		wxStatus:"",//微信状态
		workStatus:"",//处理状态
		conversation:[],  
		currentMsgList:[],//存储聊天记录 		 
		editIndex:'',	 
		sendMes:'',
		custRoleList:[],// 车主  货物  专线  货站
		chatId:'',
		wxid:'',
		sysWxid:'',
		popright:false,//右侧功能窗口
	 	editPhone:true,//显示修改的电话
	 	 
		rightPopNmae:'',//右侧窗口名字
		editName:true,
		delChat:false,//是否在执行关闭聊天方法   可能会删除和切换同时点击 导致页面不进行动态刷新
		chatIndex:'',//用来更新聊天框滚动条数据
		checkedAll:'',
		checkedInp:[],
		replayIndex:'',//快捷回复
		replayDomain:'',//快捷回复
	   
    },
 	watch: {
 		checkedAll:function(val){
 			
 		 for (var i=0; i < vm.checkedInp.length; i++) {
				vm.checkedInp[i]=val;
		  };
 			 
 		}, 
 	 
 	  visible:function(value) {
	      if (value) {
	        document.body.addEventListener('click', this.closeMenu)
	      } else {
	        document.body.removeEventListener('click', this.closeMenu)
	      }
   	  },
   	  chatStatus:function(val){  	  	
   	  	vm.getChatData();	  
   	  },
   	  wxStatus:function(val){  	  	
   	  	vm.getChatData();	  
   	  }, 
   	  workStatus:function(val){  	  	
   	  	vm.getChatData();	  
   	  },
   
   	  currentMsgList:function(){
   	  	
   	  		this.$nextTick(function(){ //dom渲染完成后执行得函数
				vm.bottomScroll(vm.chatIndex)
			}) 
   	  }
    }, 
	methods: {	
		wxBangding:function(domain,index){
				vm.weixindialogVisible=true	
		 		vm.wxDomain=domain
	       		vm.wxform.cId=domain.cId; 
	       		  		
	       	 	$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sys/cust/query-custWx',
	        	 	data: JSON.stringify(vm.wxform),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			 vm.wxtableData=data;	
	        			 vm.wxform.cId=null;    
	        		}
	        	});	 
		},
		
		wxSave:function(index,type){
        		debugger
        		vm.wxtableData[index].type=type
        		vm.wxtableData[index].cId=vm.wxDomain.cId    
        	 	 
        		$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sys/cust/custWx-save',
	        	 	data: JSON.stringify(vm.wxtableData[index]),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		async:false,//取消异步请求
	        		success: function(data){   
	        			debugger
	        			vm.wxtableData[index]=data;	
	        			vm.wxUpdate();   
	        		}
	        	}); 
        		 
           },
		  wxUpdate:function(){
		  	debugger
		  	$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sys/cust/custWx-update',
	        	 	data: {
	        	 		sessionId:vm.wxDomain.sessionId,
	        	 	},
	        		 
	        		dataType:"json",
	        		async:false,//取消异步请求
	        		success: function(data){   
	        			debugger
	        			vm.conversation=data;	   
	        		}
	        	}); 
		  	
		  },
		 wxQuery:function(){
	       		debugger
	       		 
	       			if(vm.wxform.wxRemark.length<5){
	       			    vm.$message({
				          message: '请输入长度不低于5得数字',
				          type: 'warning'
				        });
		       			return
	       			 }
	       		       
	       		
	       	 	$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sys/cust/query-custWx',
	        	 	data: JSON.stringify(vm.wxform),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			 vm.wxtableData=data;	   
	        		}
	        	}); 
	       },
		
		
		
		
		
		
		
        //初始执行
        init:function() {        	      
         	this.query()             	 
        },
        openShow:function(){
        	 if(vm.type.display=="none"){
        		vm.type.display="block"        	
        		vm.showClass='layui-icon-up'
        		
        	}else{ 
        		vm.type.display="none"
        			vm.showClass='layui-icon-down'
        	} 
        
        },
   	
        closePeo:function (sid,peoId){//聊天群人员删除
  	 
				$.ajax({
					  xhrFields : {
							withCredentials : true
					  },
					  type : "GET",
					  url : '/sys/cust/im/closePeo', 
					  data : {
						  sid : sid,
						  peoId:peoId,
					  },
					  dataType : 'json',
					  success : function(data) {
					  	
						  vm.conversation=data;
					  }
				  });
			},
          getChatData:function(){
	     
	    	$.ajax({
	 		   type: "GET",
	 		   url: '/sys/cust/get/chat/data/filter',
	           dataType:"json",
	           data : {
					chatStatus : JSON.stringify(vm.chatStatus),
					wxStatus:JSON.stringify(vm.wxStatus),	
					workStatus:JSON.stringify(vm.workStatus),		 
				},
	            cache:false, // 设置为 false 将不缓存此页面	           
	           async:false,//取消异步       
	           success: function(obj) {	
	           	 vm.conversation=obj;
	          	       
	 		   },
	          
       		});    
	    	 
	    },
	    handleClick(val) {
	    	debugger
	       var data=[];
	    
	     	if(val==0){//拨打电话	  
     		 	for (var i=0; i < vm.checkedInp.length; i++) {
				    if(vm.checkedInp[i]==true){
				    	data.push(vm.conversation[i])
				    }
				};   		
				 if(data.length>0){
				 	$.ajax({
					  type : "POST",
					  contentType : "application/json;charset=utf-8",
					  url : '/sys/cust/telPhonebyim', // 拨打电话
					  data:JSON.stringify(data) ,
					  dataType : 'json',
					  success : function(data) {	
					  	if(window.parent.telBox!=undefined){
					  		window.parent.telBox();	
					  	}  
						  
					  }
				    }); 
				 }
	     		if(window.parent.telBox!=undefined){
				  window.parent.telBox();
				  }
	     	}
	     	if(val==1){//微信回复
	     		for (var i=0; i < vm.checkedInp.length; i++) {
				    if(vm.checkedInp[i]==true){
				    	data.push(vm.conversation[i])
				    }
				};
	     		 if(data.length>0){
				 	$.ajax({
					  type : "POST",
					  contentType : "application/json;charset=utf-8",
					  url : '/sys/cust/messMassByim', // 拨打电话
					  data:JSON.stringify(data) ,
					  dataType : 'json',
					  success : function(data) {	  
						    vm.conversation=data;
					  }
				    }); 
				 }
	     	}
          	if(val==2){//批量删除	   
          	   for (var i=0; i < vm.checkedInp.length; i++) {
				    if(vm.checkedInp[i]==true){
				    	  vm.closeCurrSession( vm.conversation[i].sessionId,i) //关闭聊天    
				    }
			  };  		
               
          	}
      },
        handleUpdate(domain,ifTop){//置顶
        	
        
        	$.ajax({
				  type : "POST",
				  url : '/sys/cust/im/updateTop', 
				  data : {
					  sessionId : domain.sessionId,
					  ifTop:ifTop,			 
				  },
				  dataType : 'json',
				  success : function(data) {
   					  	 
   					  	 vm.conversation=data;
			  	 }
			  	});   
        },
         openMenu(e, item) {
		      this.rightClickItem = item;
		
		      let x = e.clientX;
		      let y = e.clientY;
		
		      this.top = y;
		      this.left = x;
		      
		      this.visible = true;
		    },
	    closeMenu() {
	      this.visible = false;
	    },
	  
        query:function(){   
        	debugger      	     	 
        	$.ajax({
	 		   type: "GET",
	 		   url: '/sys/cust/get/chat/data',
	           dataType:"json",
	           cache:false, // 设置为 false 将不缓存此页面	           
	           success: function(obj) {	
	           	
	           		var noDealWith=0;          		  
	          	   vm.conversation=obj; 
	          	   //查询一下当前show为true的数据 加载聊天数据
	          	   for(var i=0;i<obj.length;i++){
	          	   		vm.checkedInp[i]=false;
		          	   	 if(obj[i].show==true){	   
		          	   	 	debugger
		          	   	 	   	   	 	
		          	   	 	//vm.getMsgHistory(obj[i].sessionId,obj[i].cId);	
		          	   	 	vm.chatId=obj[i].custWxvoList[0].id;
			        		vm.wxid=obj[i].custWxvoList[0].sysWxId;//当前微信号 发送人的 
			        		vm.sysWxid=obj[i].custWxvoList[0].wxId;//接收人 	          	   	   	   	  
		          	   	 	vm.chatIndex=i; 
		          	   	 }
		          	      if(obj[i].ifDealWith==false){
						  	 noDealWith=noDealWith+1
						  }
				 
	            	
					
	          	   }
	          	   
	          	   if(window.parent.addNoread!=undefined){
	          	   	 window.parent.addNoread(noDealWith)
	          	   }
	          	  
	          	       
	 		   },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
       		});          	
        },
         getMsgHistory:function(sid,cid){//获取聊天历史纪录
          
      	    $.ajax({				 
				type: "GET",
				url: '/imapi/get/history/msg/bywxid',
				data: {"sid":sid,"cid":cid,"wxid":vm.wxid,"sysWxid":vm.sysWxid},
				dataType: 'json',
				async:false,
				success: function(data) {
					debugger
					vm.currentMsgList=data;	 			
				 	 	 			
				}
    	 	  });      	
      },
        
        bottomScroll:function(index){//设置滚动条置底
			//滚动条置底		 
			 var div = document.getElementById('chatWindow'+index);
			 div.scrollTop = div.scrollHeight;
		 
        },
     
        closePopRight:function(){//关闭右侧弹窗
        	
       
        	$("#liaotianPop").find(".popright").hide();    
		    $(".liaotianPop", parent.document).removeClass("liaotianPop2");
		    $("#liaotianPop").removeClass("leftclass")
        },
        closeAll:function(){
        	
        	 window.parent.closePop()	
        },
           
        closeCurrSession:function(sid,index){//关闭聊天       
        		debugger
        		vm.delChat=true;
        	$.ajax({
				  xhrFields : {
						withCredentials : true
				  },
				  type : "GET",
				  url : '/sys/cust/im/closeCurrSession', 
				  data : {
					  sid : sid,
				  },
				  dataType : 'json',
				  success : function(data) {
				  	vm.delChat=false;
				      vm.conversation=data;
					 //vm.conversation.splice(index, 1);  
					  
				  }
			  });
        },
        
        editNameOrPhone:function(index,states,val){//states  0名字 1电话       	
        	              	 
        	vm.editIndex=index;	
        	if(states==0){
        		vm.editName=true;	
        	}
        	if(states==1){
        		vm.editPhone=true;	
        	}
        	if(vm.conversation[vm.editIndex].cId==null){
        		vm.$message('请填写真实号码保存后 再进行修改');
        		return;
        	}
        	        
        	$.ajax({
				  type : "POST",
				  url : '/sys/cust/modify/wx/remark', 
				  data : {
					  sessionId : vm.conversation[vm.editIndex].sessionId,
					  value : val,
					  states:states
				  },
				  dataType : 'json',
				  success : function(data) {
   						
			  		}
		  	}); 
		  	vm.$message('修改成功');
        },
       relAndXnh:function(domain,type){
   			debugger
   			if(type==0){//虚拟号
   				domain.custPhoneXnh="";
   			}
   			if(type==1){//真实号
   				domain.custPhoneXnh=domain.custPhone;
   			}
   		},
       saveRelPhone:function(index){//states  0名字 1电话       	
       			debugger
	     
	       	
       	
        	//vm.editXnPhone=false;             	 
        	vm.editIndex=index;	
        	vm.editPhone=true;	 
           	if(vm.conversation[vm.editIndex].mesType==3){
           		if(vm.conversation[vm.editIndex].custPhoneXnh==null||vm.conversation[vm.editIndex].custPhoneXnh==""){
           			vm.$message('请输入号码');
	       			return;
           		} 			
	       	}
        	        
        	$.ajax({
				  type : "POST",
				  url : '/sys/cust/modify/wx/saveRelPhone', 
				  data : {
					  sessionId : vm.conversation[vm.editIndex].sessionId,
					  value :vm.conversation[vm.editIndex].custPhoneXnh,
					  pyhonType:vm.phoneType,
				  },
				  dataType : 'json',
				  success : function(data) {
   						vm.conversation=data;
			  		}
		  	}); 
		  	vm.$message('修改成功');
        },
        changeWx:function(domain){//微信下拉选择
        	 debugger
           var sid=domain.sessionId;
           
           	 $.ajax({
				  type : "POST",
				  url : '/sys/cust/im/updateWxonlineStat', 
				  data : {
					  sessionId : sid			 
				  },
				  dataType : 'json',
				  success : function(data) {
				  	debugger
				  	var wxList=data.custWxvoList;
   				   if(wxList.length>0){
		        		for(var i=0;i<wxList.length;i++){
		        			if(wxList[i].id==vm.chatId){
		        			    vm.wxid=wxList[i].sysWxId;//当前微信号 发送人的 
		    					vm.sysWxid=wxList[i].wxId;//接收人
		    					if(wxList[i].sysUserWxInfo.onlineStat=="0"){
		    						vm.wxonlineStat="离线";
		    					}else{
		    						vm.wxonlineStat="在线";
		    					}
		   					 	break;
		        			}
		        		}       		
		        	}
	     
			  	}
		  	}); 
           
         
      },
     
     
       sendMsg:function(index,domain) {//消息发送
			  
		 
			 if(vm.sendMes==""){
			 	vm.$message.error('不能发送空消息');
			 	return
			 }
			 if(domain.cId=="-1"){
			 	vm.$message.error('虚拟号不能发送消息');
			 	return
			 }
			  if(vm.currentMsgList==""){
			  	 if(domain.cId!=null){
			     	 vm.getMsgHistory(domain.sessionId,domain.cId);   //查询历史聊天记录
			  	 	 
			  	 }
			  }
			 
			 vm.conversation[index].wxNoReadNum=0;
			$.ajax({
				  xhrFields : {
					  withCredentials : true
				  },
				  type : "POST",
				  url : '/imapi/send', // 发送好友文本消息
				  data : {
					  content : vm.sendMes,
	                  cid:domain.cId,
	                  sid:domain.sessionId,
	                  sysWxId : vm.wxid,
					  wxid :vm.sysWxid
				  },
				  dataType : 'json',
				  success : function(data) {
				  		
				  vm.currentMsgList.push({content:vm.sendMes,ctime: "",sysflag: "1"});
					  vm.sendMes="";	
						
						 
						  					 
				  }
		   	}); 
		   
		},
		
		 getFHMessage:function(data){
         	
         	 $.ajax({
	  		    type: "POST",
	            url: '/sys/cust/im/getChatPeo',
	            data:{
	            	"syswxid":data.TOID,
	            	"wxid":data.USERNAME,
	            	"content":data.CONTENT
	            } ,	           
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) {	  
	            	  
	            	    vm.conversation=data;
	            	  //推送更新index。html 待办数量
	                 var noDealWith=0;
            		 for (var i=0; i < data.length; i++) {
            		   if(data[i].ifDealWith==false){
					  	 noDealWith=noDealWith+1
					    }
					     
					  	if(data[i].show==true){//判断当前窗口是否开启 开启的话 添加消息			  		 
				  		 vm.getMsgHistory(data[i].sessionId,data[i].cId);
				  		  
				  	  }
					} 
					
					 if(window.parent.addNoread!=undefined){
		            	window.parent.addNoread(noDealWith)
		             }						 
				  
				  
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        }); 	 
         },	
         openTop:function(cust,domain,index){//打开浮出窗口给
         	
         	 let openURI=cust.url+"?cid="+domain.cId+"&sid="+domain.sessionId+"&phoneNum="+domain.custPhone+"&wxid="+vm.wxid;
         	  var temp= $("#imRightIframe").is(":visible");//是否可见 	  
			  window.parent.openNewPop(openURI,cust.buttonName);
		    },
         openRight:function(cust,domain,index){//打开右侧弹出窗
         	  if(cust.buttonName=="微信聊天"){
        		  if(domain.custWxvoList==null){
        		 	vm.$message('暂无微信消息');
        		 	return;
        		 }
        		 vm.getMsgHistory(domain.sessionId,domain.cId);   //查询历史聊天记录  	
			 }else{
        		 vm.popright=true//右侧功能窗口
			     let openURI=cust.url+"?cid="+domain.cId+"&sid="+domain.sessionId+"&phoneNum="+domain.custPhone+"&wxid="+vm.wxid;
	     	     vm.rightPopNmae=cust.buttonName;
	     	     
	     	      
    			 
	     		 $("#imRightIframe").attr("src",openURI);
	     		 $(".liaotianPop", parent.document).addClass("liaotianPop2");
				 $("#liaotianPop").find(".popright").show();			 
				 $("#liaotianPop").find(".popright").find(".box").show().siblings().hide() 
				
        	}
		   
        },
          updateCustType:function(domain,index){//修改司机类型       
        	
        	
        	$.ajax({
			  type : "POST",
			  url : '/sys/cust/im/update-custType', 
			  data : {
				  custType : domain.custType,
				  sessionId : domain.sessionId 
			  },
			  dataType : 'json',
			  success : function(data) {	 				  
			    // vm.conversation[index]=data;
			    
					  vm.openRightCommon(domain,index)
        	
		        	 
			     vm.query();//模拟点击一下聊天窗口
			  }
		  }); 
        },
        openRightCommon:function(domain,index){
        	 var roleList=domain.custRoleList;
	        	if(roleList!=null&&roleList.length>0){
	        		 for (var i=0; i <roleList.length; i++) {
					    if(roleList[i].buttonName=="货源录入"&&domain.custType=="货主"){
					    	  vm.openRight(roleList[i],domain,index)//打开右侧弹出窗
					    	  break;
					     }else  if(roleList[i].buttonName=="车询货"&&domain.custType=="车主"){
					    	 vm.openRight(roleList[i],domain,index)//打开右侧弹出窗
					    	  break;
					    }else  if(roleList[i].buttonName=="通话记录"&&domain.custType=="其他"){
					    	 vm.openRight(roleList[i],domain,index)//打开右侧弹出窗
					    	  break;
					    	  
					    }else{
					    	 vm.popright=false//右侧功能窗口
					    	
					    }
					};
	        	}else{
	        			 vm.popright=false//右侧功能窗口
	        	}
		        	 
        	
        },
        changeChat:function(domain,index){//切换聊天      
        	 debugger
        	vm.chatIndex=index;
        	if(vm.delChat==false){
        		vm.currentMsgList="";
        	if(domain.custWxvoList!=null&&domain.custWxvoList.length!=0){//切换聊天时
        		vm.chatId=domain.custWxvoList[0].id;
        		vm.wxid=domain.custWxvoList[0].sysWxId;//当前微信号 发送人的 
        		vm.sysWxid=domain.custWxvoList[0].wxId;//接收人
        	}else{
        		vm.chatId="";
        		vm.wxid="";//当前微信号 发送人的 
        		vm.sysWxid="";//接收人
        	}
        	
        	if(domain.cId==null){//当前为群聊时  直接查询历史记录
        		 vm.getMsgHistory(domain.sessionId,domain.cId);   //查询历史聊天记录  	
        	}
        	
         
    		  vm.openRightCommon(domain,index)
        	
        	
        	
        	  $.ajax({
				  type : "POST",
				  url : '/sys/cust/im/updateChatType', 
				  data : {
					  sessionId : vm.conversation[index].sessionId				 
				  },
				  dataType : 'json',
				  success : function(data) {				  	
   					 vm.conversation=data;	   					         	   	      			
			  	  }
		  	});  	 
		 
        	}
        		 
	
        },
      
        kuaijiehuifu:function(index,domain){	 
   			 vm.replayIndex=index;
   			 vm.replayDomain=domain;
   			 var openURI="/cQuickReply/to-page";
	         vm.popright=true//右侧功能窗口
		 	 $("#imRightIframe").attr("src",openURI);
		     $(".liaotianPop", parent.document).addClass("liaotianPop2");
			 $("#liaotianPop").find(".popright").show();			 
			 $("#liaotianPop").find(".popright").find(".box").show().siblings().hide() 
	  		},
	},
	mounted(){
       this.init();
   }, 
    
  
})
 window.selectReoly= function(remark){	
 		
 	vm.sendMes=remark; 
 	vm.sendMsg(vm.replayIndex,vm.replayDomain)
 };
