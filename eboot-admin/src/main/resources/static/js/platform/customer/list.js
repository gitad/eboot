
let layers,forms,tables,initTable,layer;
layui.use(["jquery","layer",'table', 'element','form'], function(){
	
    var $ = layui.$, 
    layer = layui.layer ,
    table = layui.table ,
    element = layui.element,
    form = layui.form;
    layers=layui.layer;   
    forms = layui.form  ;
    tables = layui.table ;
    
    var cols=[[
        {field:'checkBox',checkbox: true, fixed: true}
      // ,{field:'id', title: '客户编号', sort: true}
       ,{field:'if_normal', title: '数据状态',templet:function(d){
    	   
    	   if(d.ifNormal==0){
    		   return  "正常"
    	   }
    	   if(d.ifNormal==1){
    		   return  "缺少微信"
    	   }
    	   if(d.ifNormal==2){
    		   return  "电话异常"
    	   }
       		 
       }}
      // ,{field:'custWxId',width:120, title: '客户微信'}
       ,{field:'custAdminNickname',width: 80, title: '归属业务员',sort: true} //width 支持：数字、百分比和不填写。你还可以通过 minWidth 参数局部定义当前单元格的最小宽度，layui 2.2.1 新增
       ,{field:'custName', width: 80,title: '客户姓名',event: 'openIm'}
       ,{field:'nickname', width: 80,title: '所属地区',templet:function(d){
       		return d.provinceName+'-'+d.cityName+'-'+d.areaName;
       }}
       ,{ field: 'addressInfo', width: 80,title: '公司名称'}
       ,{field:'validPhone', width: 135,title: '主联系人手机'}
       ,{field:'otherPhone', width: 135,title: '其他手机'}
       ,{field:'custType', title: '类别'} //单元格内容水平居中
       ,{field:'lastCallTime', title: '最后通话时间'}
       ,{field:'lastCallStatus', title: '最后通话状态'}
       ,{field:'remark', title: '跟进状态'}
       ,{field:'wxStatus', title: '微信状态'}
       ,{field:'remark', title: '备注'}
      // ,{field:'-', title: '平台积分'}
     //  ,{field:'-', title: '工商状态'}
       ,{field:'op',title: '操作', align:'center', toolbar: '#toolBars'} //这里的toolbar值是模板元素的选择器
   ]];
    
    table.on('tool(tableFilter)', function(obj){
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
        if(layEvent === 'openIm'){
        	
        	  $.ajax({
					type: "GET",
					url: '/sys/cust/by/im/id?id='+data.id+"&idx=1",
					dataType: 'json',
					async:false,
					success: function(data) {
						 window.parent.wx_open()	 			
					}
	    	 	  });
        	
        	
        	
         
        }
    });
      var limits=[10, 50, 100,500,1000,2000,5000,10000];
      initTable = Common.initTable('#userTable','/sys/cust/query-page',cols,table,limits);
    
    $('#searchBtn').on('click',function () {
    	
        Common.searchTable('searchForm',initTable);
    });
    $('#yhfp').on('click',function () {
    	  $.ajax({
	  		    type: "POST",
	            url: '/sys/cust/cust-distribution',
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(obj) {
	            	
	            	 if(obj==true){
	            		 layer.msg('自动分配成功', {time: 5000, icon:6});
	            	 
	     		    	return;
	            	 }
	            	if(obj==false){
	            		 layer.msg('自动分配失败', {time: 5000, icon:6});
	     		    	return;
	            	}
	            	
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });  
    	
    	
    });
    $('#sjtb').on('click',function () {
    	debugger
    	layer.msg('加载中', {   icon: 16   ,shade: 0.01 });
  $.ajax({
	  		    type: "POST",
	            url: '/sys/cust/cust-distribution',
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(obj) {
	            	
	            	 if(obj.data==true){
	            		 layer.msg('分配成功', {time: 5000, icon:6});
	            	 
	     		    	return;
	            	 }
	            	if(obj.data==false){
	            		 layer.msg('分配失败', {time: 5000, icon:6});
	     		    	return;
	            	}
	            	
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });   
  	
  	
  });
    $('#redisCsh').on('click',function () {
    	layer.msg('加载中', {   icon: 16   ,shade: 0.01 });
    	  $.ajax({
  	  		    type: "POST",
  	            url: '/sys/cust/cust-redis-init',
  	           contentType : "application/json;charset=utf-8",
  	            dataType:"json",
  	            cache:false, // 设置为 false 将不缓存此页面	           
  	            success: function(obj) {
  	     		 layer.msg('初始化成功', {time: 5000, icon:6});           	
  	            },
  	            error: function(data) {
  	                // 请求失败函数
  	                console.log(data);
  	            }
  	        });  
    	
    	
    });  
    
  
    $(".layui-nav-third-child").hide();
    $(".third-class").on('mouseover',function () {
    
   		 
    	if($(this).context.innerText=="客户转交"){  			
    		$.ajax({
    			  type : "POST",
    			  url : '/sys/cust/query-all-user', // 发送好友文本消息 		 
    			  dataType : 'json',
    			  success : function(data) {
    				
    				for(var i=0;i<data.length;i++){
    					$("#customerZj").append("<li><a onclick=peoZj('"+data[i].id+"','"+data[i].username+"')>"+data[i].username+"</a></li>");	
    	  				
    				}
    				
    				  
    			  }
    		  }); 
    	}
    	
        $(".layui-nav-third-child").hide();
        $(this).next().show();
    });
 
  
    
    $(".layui-nav").on('mouseleave',function () {
        $(".layui-nav-third-child").hide();
        $(this).next().hide();
    });

    $(".customerManagement").on("click",".input1",function(){
       popform6($(this))
    })
    let s1="",s2='',s3="";  
  $("body").on("click",".linkageRegional p",function(){   
    let t = $(this).text();
    console.log("x")
    let type= $(this).parent("li").attr("class");   
    $(this).addClass("act").siblings().removeClass("act");
      if(type=="s1"){
        s1=t;
      }else if(type=="s2"){
        s2=t;
      }else{
        s3=t;
        if(s1==""||s2==""){
            layer.msg("请选择区域")
            return false
        }
        $("#shiquliandond").val(s1+s2+s3)
      }
  })
  

  
})





//选择地区
function popform6(that){
  layer.open({
      type: 1,
      title: "选择地区",
      shadeClose:true,
      area: ['500px', '90%'],
      content: $("#sourceEntryPOP6").html(),
      btnAlign: 'c' ,//按钮居中
      btn: ['确定','取消'],
    success: function(layero, index){
      
      },
      btn1: function(index, layero){
      //按钮新增的回调
      that.val("111") //回填值
        layer.close(index);
      },
                    
  }); 
}

function popform(){
  layers.open({
      type: 1,
      title: "自动剔除字符规则",
      shadeClose:true,
      area: ['450px', '400px'],
      content: $("#replyQuickPop").html(),
      btnAlign: 'c' ,//按钮居中
      btn: ["确认",'取消'],
    success: function(layero, index){
      forms.render();
      },
      btn1: function(index, layero){
      //按钮新增的回调
      console.log("1")
        layer.close(index);
      },
                    
  }); 
}
function tel(){
	
	var selects=tables.checkStatus('userTable');
	 
	if(selects.data.length==0){
		layer.msg('请先选择人员', {time: 5000, icon:6});
		return
	}else{
		 
	
	$.ajax({
		  type : "POST",
		  contentType : "application/json;charset=utf-8",
		  url : '/sys/cust/telPhone', // 拨打电话
		  data:JSON.stringify(selects.data) ,
		  dataType : 'json',
		  success : function(data) {	  
			  window.parent.telBox();	
		  }
	  
	    }); 
	}
   
}
//消息群发
function messMass(){
	debugger
 
	var selects=tables.checkStatus('userTable');
	 
	if(selects.data.length<2){
		layer.msg('群发请至少选择两个人', {time: 5000, icon:6});
		return
	}
	if(selects.data.length>500){
		//默认prompt
		layer.prompt(function(val, index){
			layer.close(index);
			layer.msg('发送中', {
			  icon: 16
			  ,shade: 0.01
			});
		  $.ajax({
			  type : "POST",
			  contentType : "application/json;charset=utf-8",
			  url :'/sys/cust/messMass?content='+val, // 发送好友文本消息 	
			  data:JSON.stringify(selects.data) ,
			  dataType : 'json',
			  success : function(data) {
				  layer.msg('群发成功', {time: 5000, icon:6});
				   
			  }
		  
		    }); 
		  
		  
		});		 
	}else{
		$.ajax({
			  type : "POST",
			  contentType : "application/json;charset=utf-8",
			  url :'/sys/cust/messMass', // 发送好友文本消息 	
			  data:JSON.stringify(selects.data) ,
			  dataType : 'json',
			  success : function(data) {
				  
				  window.parent.wx_open()
			  }
		  
		    }); 
	}
	
}

//消息群发
function messMassAll(){
	debugger

 
	 
		//默认prompt
		layer.prompt(function(val, index){
			//加载层-风格4
 			layer.close(index);
			layer.msg('发送中', {
			  icon: 16
			  ,shade: 0.01
			});
			
			
		  $.ajax({
			  type : "POST",
			  contentType : "application/json;charset=utf-8",
			  url :'/sys/cust/messMassAll?content='+val, // 发送好友文本消息 	
			  dataType : 'json',
			  success : function(data) {
				  layer.msg('群发成功', {time: 5000, icon:6});
				   
			  }
		  
		    }); 
		  
		  
		});		 
	 
	
}
function closefuns(){
	  console.log("000")
	  layer.closeAll()
}
//查询操作员的微信
function queryUserWx(){
	
}
function peoZj(id,name){
	
	 
	 var checkStatus = tables.checkStatus("userTable");
		if(checkStatus.data.length==0){
			layer.msg('请选择要分配的用户', {time: 5000, icon:6});
			return
		}else{
			
			$.ajax({
  			  type : "POST",
  			 contentType : "application/json;charset=utf-8",
  			  url : '/sys/cust/user-distri?id='+id+"&name="+name, // 发送好友文本消息 	
  			  data:JSON.stringify(checkStatus.data) ,
  			  dataType : 'json',
  			  success : function(data) {
  				 Common.searchTable('searchForm',initTable);   
 			       if(data==true){
 			    	  layer.msg('用户分配成功', {time: 5000, icon:6});
 			    	  return
 			       }
 			      if(data==false){
 			    	 layer.msg('用户分配失败', {time: 5000, icon:6});
			    	  return  
 			       }
 			    
  			  }
  				
  				  
  			  
  		    }); 
			
			
		}
 
}
 