  
 var vm = new Vue({
	el: '#app',
	
	data:{
	    current:1,//当前页
		size:10,//页面条数
		count:0,//总共条数
		tableData:[],
		form:{ mobile:'',username:'',},
		newPassword:'', 
		allData:[],//所有待操作数据
		checkData:[],//已选数据
		addForm:{username:'',mobile:'',nickname:'',roleIds:[],roles:[],},
		dialogVisible:false,//区域选择
		xgdialogVisible:false,//微信修改
		dhdialogVisible:false,//电话
		dialogVisiblepassword:false,//密码修改
    },
 	watch: {
	  
    }, 
	methods: {	
        //初始执行
        init() {       
            this.query();      
        },
        query:function(){    	
        	 var   url="/sys/user/query-page?current="+this.current+"&size="+this.size  	
        	 var forData=this.form;
        	 Object.keys(forData).some(function(key){
        	 	   if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
        	     
        	      
        	 })     	 
        	$.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: url,      	 	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){   
        			    	        				 
				     vm.tableData=data.data; 				     
				     vm.count=data.count;	 
        		}
        	}); 
        },
         clear:function(){
		 
		 	vm.form.mobile='';
			vm.form.username='';
		 
			 
		},
		addPhone:function(){
			 vm.allData.splice(0,0,{checked:false});   
		},
		savePhone:function(row){
			debugger
			
				 $.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: "/user/wx/save-phone",      	 	
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		data:JSON.stringify(row),
	        		success: function(data){   
	        		  vm.$message({  message: '保存成功',   type: 'success'    });
	        		}
	        	}); 
			 
		},
	   phoneDataChange(userId,confId,sltype){//修改微信数据
			$.ajax({
	 		    type: "POST",
	 		    data:{
	 		    	"userId":userId,
	 		    	"confId":confId,
	 		    	"type":sltype
	 		    },
	 		   url: '/user/wx/update-phone-Info',
	           dataType:"json",
	           cache:false, // 设置为 false 将不缓存此页面	           
	           success: function(obj) {
	        	   
	 		   },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       }); 
		},
		transChangeWx:function(leftDatas,type,rightDatas){
			
			if(type=="right"){//右侧增加
				if(rightDatas.length>0){
					for (var i=0; i < rightDatas.length; i++) {
						
			  			vm.wxDataChange(vm.addForm.id,rightDatas[i],true)
					};	
				}
			
			}
			 if(type=="left"){//右侧增加
				if(rightDatas.length>0){
					for (var i=0; i < rightDatas.length; i++) {
						
			  			vm.wxDataChange(vm.addForm.id,rightDatas[i],false)
					};	
				}
			
			}
		},
		
	
	 
		transChangeDh:function(row,type){
			debugger
		     if(type==0){//绑定
		     	if(row.checked==true){
		     		 vm.$message({  message: '该数据已被绑定',   type: 'success'    });
					 return;
		     	}
		     	var num=0;
		     	 for (var i=0; i < vm.allData.length; i++) {
			         if(vm.allData[i].checked==true){
							num=num+1; 
					 }
					 if(num>=3){
					 		 vm.$message({  message: '一个人最多只能绑定三个号码',   type: 'success'    });
					 		return;
					 }
				 }; 
		     	
		     	
		     	
		     	row.checked=true;
		     	vm.phoneDataChange(vm.addForm.id,row.id,true);
		     }
		     if(type==1){
		     		row.checked=false;
		     	 vm.phoneDataChange(vm.addForm.id,row.id,false);
		     }
		},
		editData:function(val,type){
			 
			vm.queryData(val);	 
			if(type==0){//电话
				debugger
			  vm.dhdialogVisible=true;
			  var allWx=vm.addForm.userPhoneAll;
			  vm.allData=[];
			  vm.checkData=[];
			  vm.allData=allWx;			
				 for (var i=0; i < allWx.length; i++) {
				     // vm.allData.push({ key: allWx[i].id,   label: allWx[i].phoneNum, disabled:allWx[i].chceked,isOutBound: allWx[i].isOutBound,phoneQcellcore:allWx[i].phoneQcellcore}); 
						 for (var k=0; k< checkWx.length; k++) {
							 if(allWx[i].id==checkWx[k].id){
							 	allWx[i].checked=true;
							 }
						}; 
				 };  
				var checkWx=vm.addForm.userPhone;
			 	vm.checkData=checkWx;
				/* for (var i=0; i < checkWx.length; i++) {
					 vm.checkData.push(checkWx[i].id); 
				};  */
				
			}
			if(type==1){//微信
				vm.xgdialogVisible=true;
				var allWx=vm.addForm.userWxInfoAll;
				 vm.allData=[];
				 vm.checkData=[];
				for (var i=0; i < allWx.length; i++) {
				      vm.allData.push({key: allWx[i].wxId,     label: allWx[i].wxNickname,      disabled:allWx[i].chceked,     }); 
				};
				var checkWx=vm.addForm.userWxInfo;
				for (var i=0; i < checkWx.length; i++) {
					 vm.checkData.push(checkWx[i].wxId); 
				};
				
				
			}
			
			
		},
		del:function(val){
		 debugger
	       	$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: "/sys/user/del?id="+val,      	 	
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		 async:false,
	        		success: function(data){   
	        			  vm.$message.error('删除成功');   	        				   
					       vm.query();
					      
	        		}
	        	});  
			
		},
		passwordSave:function(){
			debugger
			 vm.addForm.password=vm.newPassword;
				$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: "/sys/user/savePassword",      	 	
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		data:JSON.stringify(vm.addForm),
	        		success: function(data){   
	        			     vm.$message.error('修改成功');   	        				 
					     vm.roles=data;	 
					     vm.addForm.username='';
						 vm.addForm.mobile='';
						 vm.addForm.nickname='';
						 vm.addForm.roleIds=[];
						 vm.addForm.roles=[];
					    vm.addForm.password='';
						 vm.dialogVisiblepassword=false;
						 vm.query();
	        		}
	        	}); 
		},
			wxDataChange(userId,wxid,sltype){//修改微信数据
			$.ajax({
	 		    type: "POST",
	 		    data:{
	 		    	"userId":userId,
	 		    	"wxid":wxid,
	 		    	"type":sltype
	 		    },
	 		   url: '/user/wx/update-wxinfo',
	           dataType:"json",
	           cache:false, // 设置为 false 将不缓存此页面	           
	           success: function(obj) {
	        	   
	 		   },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       }); 
		},
		
       add:function(val,type){
	       	if(type==0){
	       		  vm.dialogVisible=true;
	       	}
	        if(type==1){
	       		  vm.dialogVisiblepassword=true;
	       	}
       	
	       vm.queryData(val);
       	
      	 }, 
      	 queryData:function(val){
      	 		$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: "/sys/user/add?id="+val,      	 	
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		 async:false,
	        		success: function(data){   
	        			    	        				   
					     vm.addForm=data;
					      
	        		}
	        	}); 
      	 },
        onSubmit:function(){
        	
        		$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: "/sys/user/save-or-update",      	 	
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		data:JSON.stringify(vm.addForm),
	        		success: function(data){   
	        			    	        				 
					     vm.roles=data;	 
					     vm.addForm.username='';
						 vm.addForm.mobile='';
						 vm.addForm.nickname='';
						 vm.addForm.roleIds=[];
						 vm.addForm.roles=[];
						 vm.dialogVisible=false;
						 vm.query();
	        		}
	        	}); 
        	
        },
	   	pehzsjtb:function(){
	   		
	   		 $.ajax({
	 	  		    type: "POST",
	 	  			 url: '/sys/user/user-syn',
	 	           contentType : "application/json;charset=utf-8",
	 	            dataType:"json",
	 	            cache:false, // 设置为 false 将不缓存此页面	           
	 	            success: function(obj) {
	 	            	
	 	            	 if(obj==true){	 	            	 
	 	            	 	vm.$message.error('自动分配成功');
	 	     		    	return;
	 	            	 }
	 	            	if(obj==false){	 	            		 
	 	            		 vm.$message.error('自动分配失败');
	 	     		    	return;
	 	            	}
	 	            	
	 	            },
	 	            error: function(data) {
	 	                // 请求失败函数
	 	                console.log(data);
	 	            }
	 	        });  
	   		
	   	},
           //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
	    handleSizeChange(val) {//分页
         	
         	vm.size=val;
       		vm.query();
        },
		 handleCurrentChange(val) {//分页
         	
         	vm.current=val;
      		 vm.query();
        }, 
		 
	},
	
	mounted(){
       this.init();
   } , 
    
  
})