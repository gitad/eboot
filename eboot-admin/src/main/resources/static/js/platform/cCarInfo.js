  
 var vm = new Vue({
	el: '#app',
	
	 data:{
		 forms: [],
		 show:false,
	     tableData:[],
		 dialogVisible:false,//控制区域选择页面隐藏
		 addressdialogVisible:false,//线路
	     selectData:{
	     	 provinceCode:'',
	     	 cityCode:'',
	     	 areaCode:'',
	     },
	     relForm:{},//字表保存
	    
  	 	 dialogVisiblepic:false,
  	 	 dialogImageUrl:'',
  	 	 carInfo:'',
	     dataType:0,//数据类型 0 出发 1中专 2目的
	     provinceData:[],//省
		 cityData:[],//市
		 areaData:[],//区
		 ifAddress:true,//标记是否是长跑地址 
		 carLens:[],//车长选择
		 carTypes:[],//车型
		 carUseType:[],//承运类型
    },
 	watch: {
	 
    }, 
	methods: {	
        //初始执行
        init() { 
        	 this.carLens= this.getDropValue("4f8fb1d0089f47bb8912f4d3cfaff7fe").data//车长
        	 this.carTypes= this.getDropValue("b08212412ac54616a6b6d1f947dea2be").data//车型
        	 this.carUseType= this.getDropValue("7bca4d3f2b8d4dc9a848dd3ea144a050").data//承运类型
             this.query();      
        },
          picRemove:function(val){
        	
         
        	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/del',	      
		          data: JSON.stringify(val),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
          handlePictureCardPreview(file) {
          	debugger
	           vm.dialogImageUrl = file.url;
        	  vm.dialogVisiblepic = true;
        },
        jszuploadSuccess:function(val){
           	debugger
         	 vm.uploadFile(val,"CAR", vm.carInfo.carId);
         },
         picClick:function(form){
         	debugger
         		if(form.carId==null||form.carId==undefined){
     	 		 vm.$message('请先保存车辆数据');
        	 		 return
     	 	}
         	vm.carInfo=form;
         },      
          query:function(){ 
        	
        	 $.ajax({ 
			 	  type: "POST",
			       url: '/cCarInfo/query',
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {	
			       	
			       	if(obj.length>0){
			       		for (var i=0; i < obj.length; i++) {
			       			if(obj[i].carUseType!=null){
			       				obj[i].carUseType=obj[i].carUseType.split(",");
			       			}
							obj[i].docFileList=vm.queryPicList(obj[i].carId,"CAR");
							    
						   };
						
			       	}      		 		     	  
		         	    vm.forms=obj;      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        }, 
        queryPicList:function(id,type){
        	debugger
         	var data={};    		
      		$.ajax({
				  type: "POST",
		          url: '/sysDocFile/query',	      
		          data:{
	            	"businessId":id,
	            	"businessType":type,
	              },	
	                
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	   data=obj;   
		         },
		         
		     });
		     return data;
      }, 
        uploadFile:function(val,businessType,businessId){
        	
        	var file={};
        	file.name=val;
        	file.businessId=businessId;
        	file.businessType=businessType;
        	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/save',	      
		          data: JSON.stringify(file),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
        radioChange:function(val){
        	
        	if(val=="直达"){
        		vm.show=false;
        		vm.relForm.provinceTrans="";
     	 		vm.relForm.cityTrans="";
     	 		vm.relForm.countryTrans="";
        	}
        	if(val=="中转"){
        		vm.show=true;
        	}
        },
       
        clear:function(){//清空
        	
        	  vm.add();	
        }, 
         handleClose:function(done) {     
         	   	
	       // this.$confirm('确认关闭？') .then(_ => { done();  })  .catch(_ => {});
	        $.ajax({
					  type: "POST",
			          url: '/cCarInfo/relDel',				          
			          data:  JSON.stringify(done),
		                contentType : "application/json;charset=utf-8",
			          dataType:"json",
			          async:false,//取消异步请求
			          cache:false, // 设置为 false 将不缓存此页面	  		              
			          success: function(obj) {	 
			          		
			          	 	vm.query();
 			          },
		         
		    	 });
	       
     	 },
     	 carSave:function(form){
     	 	
     	 	 var carUseType="";
        		if(form.carUseType.length!=0){ 
        			for (var k=0; k <  form.carUseType.length; k++) {
					   if(k==0){
					   		carUseType= form.carUseType[k]
					   }else{
					   		carUseType=carUseType+","+form.carUseType[k]
					   }
					};	
				}
        	 form.carUseType=carUseType;
     	 	 $.ajax({
				  type: "POST",
		          url: '/cCarInfo/save',				          
		          data:  JSON.stringify(form),
	                contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		          	
		             vm.$message.error('保存成功');
		          	 vm.query();
		          		 
		          },
		         
		    	 });
     	 },
     	  del:function(form){
     	 	
     	 	   	 var carUseType="";
        		if(form.carUseType.length!=0){ 
        			for (var k=0; k <  form.carUseType.length; k++) {
					   if(k==0){
					   		carUseType= form.carUseType[k]
					   }else{
					   		carUseType=carUseType+","+form.carUseType[k]
					   }
					};	
				}
        	 form.carUseType=carUseType;
     	 	 $.ajax({
				  type: "POST",
		          url: '/cCarInfo/del',				          
		          data:  JSON.stringify(form),
	                contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:true,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		          	
		             vm.$message.error('删除成功');
		          	 vm.query();
		          		 
		          },
		         
		    	 });
     	 },
     	 relSave:function(){
     	 	
     	 	vm.addressdialogVisible=false;
     	 	 $.ajax({
					  type: "POST",
			          url: '/cCarInfo/relSave',				          
			          data:  JSON.stringify(vm.relForm),
		                contentType : "application/json;charset=utf-8",
			          dataType:"json",
			          async:false,//取消异步请求
			          cache:false, // 设置为 false 将不缓存此页面	  		              
			          success: function(obj) {	 
			          		
			          		//vm.relForm=obj;
				          	vm.query();
				        
					     	vm.selectData.provinceCode='';
					     	vm.selectData.cityCode='';
					     	vm.selectData.areaCode='';
 			          },
		         
		    	 });
     	 },
     	 addCar:function(){  	 	
     	 	vm.forms.push({});
     	 },
     	  save:function(){
     	 	
     	 		vm.dialogVisible=false;  
     	 	if(vm.dataType==0){ //出发  	 		
     	 	  	vm.relForm.provinceStart=vm.selectData.provinceCode;
     	 		vm.relForm.cityStart=vm.selectData.cityCode;
     	 		vm.relForm.countryStart=vm.selectData.areaCode;   
     	 		if(vm.ifAddress==true){
     	 			vm.relSave();
     	 		}	 		
     	  	}
     	 	if(vm.dataType==1){//中转
     	 		
     	 		vm.relForm.provinceTrans=vm.selectData.provinceCode;
     	 		vm.relForm.cityTrans=vm.selectData.cityCode;
     	 		vm.relForm.countryTrans=vm.selectData.areaCode;   	 
     	 	}
     	 	if(vm.dataType==2){//目的
     	 		
     	 		vm.relForm.provinceEnd=vm.selectData.provinceCode;
     	 		vm.relForm.cityStartEnd=vm.selectData.cityCode;
     	 		vm.relForm.countryEnd=vm.selectData.areaCode;   	 
     	 	}
     	 	
     	 },
     	 openAddress:function(val){//弹窗省市区选择
     	 	  
     	 	  vm.dataType=val; 
     	 	  vm.addRessOpen(0,1) 	 	
     	 },
     	 addLuxian:function(form){
     	 	if(form.carId==null||form.carId==undefined){
     	 		 vm.$message('请先保存车辆数据');
        	 		 return
     	 	}
     	 	 vm.relForm=vm.addRel();
     	 	    vm.ifAddress=false;
     	 	    vm.relForm.carId=form.carId;
     	 	     vm.relForm.routeType='直达';
     	 		vm.relForm.type=2;//路线
     	 		vm.addressdialogVisible=true
     	 },
     	 addAddress:function(form){
     	 	debugger
     	 	
     	 	if(form.carId==null||form.carId==undefined){
     	 		 vm.$message('请先保存车辆数据');
        	 		 return
     	 	}
     	 	 vm.relForm=vm.addRel();
     	 	 vm.ifAddress=true;
     	 	 vm.relForm.carId=form.carId;
     	     vm.relForm.type=1;//地点
     	 	 vm.addRessOpen(0,1)
     	  
     	 	
     	 },
     	 
     	 addRel:function(){
     	 	
     	 	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/cCarInfo/addRel',		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
     	 },    	 
		 addRessOpen:function(type,val,data,name){  
		 	debugger    	
        		//type:0 省 1市 2区
        	 vm.dialogVisible = true
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	  
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.selectData.provinceCode=name;
        	 	vm.selectData.cityCode="";
        	 	vm.selectData.areaCode="";
        	 } 
        	  if(type==2){
        	 	vm.areaData=obj.data;
        	 	vm.selectData.cityCode=name;
        	 	vm.selectData.areaCode="";
        	 } 
        	   if(type==3){		        	  
        	 	vm.selectData.areaCode=name;
        	 }    	
         },
           //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
	},
	
	mounted(){
       this.init();
   } , 
    
  
})