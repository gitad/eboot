  
 var vm = new Vue({
	el: '#app',
	
	data:{
		form:{},
		address:'',
		carType:[],
		 options: [{value: '车型不符', label: '车型不符'}, {value:'已经报价', label: '已经报价'}, {value: '同行忽悠',label: '同行忽悠'}],
	    tableShow:true,
	    firstTable:[],
	    secondTable:[],
	    titleShow:'',
    },
 	watch: {
	  	 
		 'form.carLen':function(newVal,oldVal){
		 	
		 	if(newVal.indexOf("米")==-1){
		 		if(newVal!=""){
		 		 	 vm.form.carLen=newVal+"米";
		 		}
		 	}else{	 		 
		 		vm.form.carLen=newVal.replace("米","")+"米";
		 	}
		 }, 
  }, 
	methods: {	
        //初始执行
        init() {   
        	   
      	 	this.carType=this.getDropValue("b08212412ac54616a6b6d1f947dea2be").data; 
          this.query();  
            this.form=this.add(); 
          
        },
        query:function(){     	
        	  this.firstTable=this.queryData('/cCargoInfo/query');   	
        }, 
     
        queryChildTable:function(){
        	
        	vm.secondTable=vm.queryData('/cCargoInfo/cCargoQuery?address='+vm.address);
  			 vm.tableShow=false;
        },
          add:function(){    
         	 
         	 var data={};
        	 $.ajax({
			 	  type: "POST",
			       url: '/cCargoInfo/add',
			       dataType:"json",
			       async:false,//取消异步请求
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {			
			       	     	  
		         	  data=obj;  		         	 	      	 
			       },
			       
			  });      	
			 return data;
        }, 
        queryData:function(url){    
         	var returnDate="";	
        	 $.ajax({
			 	  type: "POST",
			       url: url,
			       dataType:"json",
			       async:false,//取消异步请求
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {			     	  
		         	   returnDate=obj.data;  		         	 	      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
			  return returnDate;
        }, 
		 rowDbclick:function(row, column, event){
        	      
         	 vm.titleShow=row;
         	 vm.form.cargoId=row.cargoId;
         	vm.form.addressDetailEnd=row.addressDetailEnd;
         	vm.form.addressDetailStr=row.addressDetailStr;
        }, 
        rowClick:function(row, column, event){
        	      
         	 vm.form=row;
         	 
        }, 
         tableSave:function (){
         	debugger
         	if(vm.form.cargoId==""){
         		 vm.$message.error('请先双击选择货源数据');
         		return
         	}
         	if(vm.form.carLen==""||vm.form.carLen==null){
         		 vm.$message.error('请填写车长');
         		return
         	}
         	if(vm.form.carType==""||vm.form.carType==null){
         		 vm.$message.error('请选择车型');
         		return
         	}
         	var formData=vm.form;
         	if(vm.firstTable.length>0){
         		for(var i=0;i<vm.firstTable.length;i++){
         				if(vm.firstTable[i].carLen==formData.carLen&&
         					vm.firstTable[i].carType==formData.carType
         					&&vm.firstTable[i].cargoId==formData.cargoId){
         					debugger
         					 vm.$message.error('车长车型货源重复');
         						return
         				}
         		}
         	}
         	
         	
         	
		   //表格保存
		   $.ajax({
	 		   type: "POST",
	           url: '/cCargoInfo/save',
	           data: JSON.stringify(formData),
	           contentType : "application/json;charset=utf-8",
	           dataType:"json",
	           cache:false, // 设置为 false 将不缓存此页面 
	           success: function(obj) {
	           		 vm.tableShow=true;
	           		 vm.firstTable=vm.queryData('/cCargoInfo/query');  
	           		  
	           },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       });
   },
    //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },	
	},
	
	mounted(){
       this.init();
   } , 
    
  
})