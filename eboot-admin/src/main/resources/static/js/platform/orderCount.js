  
 var vm = new Vue({
	el: '#app',
	
	data:{
		allPayNum:0,
		allReturnNum:0,
		allCount:0,
		allAddPeo:0,
		selShow:true,
		form:{
			type:"0",
		},
		detailRow:'',
	  dialogVisible:false,//申请退款
	  undialogVisible:false,//取消退款
	  order:{},//订单
	  carInfo:{},//车辆
	  cargoInfo:{},//车寻货
	  cargoEntry:{},//货源录入
	  cargoEntryRelz:[],
	  cargoEntryRelx:[],
		tab1:true,
		tab2:false,
		tab3:false,
		tableData:[], 
		detailTableData:[],//详细表格
		payPathList:[],//支付路径 微信
		createIdList:[],//人员
		optionsType: [{value: '0',label: '微信号' }, {value: '1',  label: '业务员'} ],
		options: [{value: '定金',  label: '定金'  },   {value: '运费',    label: '运费' },     {value: '服务费',  label: '服务费'  } ],
		numStates: [{  value: '待付款',  label: '待付款'    }, {   value: '已付款',  label: '已付款'  },  	{ value: '已到账', label: '已到账'    }, {    value: '已退款',    label: '已退款'   } ],
		resion: [{  value: '约定司机到厂家后退还'  }, {   value: '货已送达退还' }, 	{ value: '因厂家原因发货退还'  }, {    value: '后台管理可以增加或删除'  } ],
		unresion: [{  value: '不想退了'  }, {   value: '已联系到货主' } ],			
		activeNames: ['1','2','3','4','5','6','7','8'],
    },
 	watch: {
	  
    }, 
	methods: {	
        //初始执行
        init() {       
          //this.query();      
        },
	   typeChange:function(val){
	   	debugger
		   	if(val==0){
		   		vm.selShow=true;
		   	}
		   	if(val==1){
		   		vm.selShow=false;
		   	}
	   },
	   back:function(){
	   		vm.tab1=false;
			vm.tab2=true;
			vm.tab3=false; 
	   	
	   },
	  back1:function(){
	   		vm.tab1=true;
			vm.tab2=false;
			vm.tab3=false; 
	   	
	   },
	        payPathChange:function(val){
        	debugger
        	for (var i=0; i < vm.payPathList.length; i++) {
			  if(vm.payPathList[i].wxId==val){
			  	vm.order.payPath=vm.payPathList[i].wxNickname;
			  		vm.order.payPathId=val;
			  	  return
			  }
			};
        	
        },
        queryOrder:function(){
        	debugger
        	vm.handleEdit(vm.detailRow)
        },
	   handleEdit:function(row){
	   	debugger
		 	vm.tab1=false;
			vm.tab2=true;
			vm.tab3=false; 
			vm.detailRow=row;
			var forData={};
			forData.createTimeStart= vm.form.createTimeStart;
 			forData.createTimeEnd=	vm.form.createTimeEnd ;
 			forData.type=vm.form.type;
 			forData.orderNum=vm.form.orderNum;
 			if(row.payPathId!=undefined&&row.payPathId!=""&&row.payPathId!=null){	
				forData.payPathId=row.payPathId; 
			}
			if(row.createId!=undefined&&row.createId!=""&&row.createId!=null){
				forData.createId=row.createId;
			}
			var   url="/cOrderCount/queryCountDetail?"  	
        	  Object.keys(forData).some(function(key){
        	 	  if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
         		 })
        	   $.ajax({
			 	  type: "POST",
			       url: url,			      
		           contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       	debugger
			       	   vm.detailTableData=data;
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });   
	   },
	    leaveTime :function(value)
	{
		debugger
	         var date_ = new Date(value);
            var year = date_.getFullYear();
            var month = date_.getMonth()+1;
            var day = date_.getDate();
            if(month<10) month = "0"+month;
            if(day<10) day = "0"+day;

            var hours = date_.getHours();
            var mins = date_.getMinutes();
            var secs = date_.getSeconds();
            var msecs = date_.getMilliseconds();
            if(hours<10) hours = "0"+hours;
            if(mins<10) mins = "0"+mins;
            if(secs<10) secs = "0"+secs;
            if(msecs<10) secs = "0"+msecs;
            return year+"/"+month+"/"+day+" "+hours+":"+mins;
	},
	    detail:function(row){
	      	 
	      
	      	  $.ajax({
			 	  type: "POST",
			       url: '/cOrder/queryDetail' ,		
			       data:{
			       	"orderId":row.orderId
			       },	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {
			       		debugger	
			       		vm.tab1=false;
						vm.tab2=false;
						vm.tab3=true; 
			        
			       	  vm.order=data.order//订单
			       	  vm.carInfo=data.carInfo;
					  vm.cargoInfo=data.cargoInfo//车寻货
					  vm.cargoEntry=data.cargoEntry//货源录入
					  vm.cargoEntryRelz=data.cargoEntryRelz	
					  vm.cargoEntryRelx=data.cargoEntryRelx	 
					  
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
			  
	     },
		   getAllWxLogin:function(){
       	
       			$.ajax({
			 	  type: "POST",
			       url: '/cOrder/getAllWxLogin' ,		
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		
			       	  vm.payPathList=data;
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
       }, 
       getAllUser:function(){
       	debugger
       			$.ajax({
			 	  type: "POST",
			       url: '/sys/user/allUser' ,		
			       	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		
			       	  vm.createIdList=data;
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
       }, 
       clean:function(){
       	vm.form={};
       	
       },
        query:function(){ 
         debugger
          var forData=vm.form;
         if(forData.createTimeStart==undefined||forData.createTimeStart==null){
         	 this.$message('请选择开始时间');
         	return;
         }
          if(forData.createTimeEnd==undefined||forData.createTimeEnd==null){
         	 this.$message('请选择结束时间');
         	return;
         }
           if(forData.type==undefined||forData.type==null||forData.type==""){
         	 this.$message('请选择统计类型');
         	return;
         }
           var   url="/cOrderCount/query?"  	
        	  Object.keys(forData).some(function(key){
        	 	  if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
         		 })
        	   $.ajax({
			 	  type: "POST",
			       url: url,			      
		           contentType : "application/json;charset=utf-8",
		           async:true,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       	debugger
			       	    vm.tableData=data; 
			       	    if(data.length>0){
			       	    	for (var i=0; i < data.length; i++) {
	   							  vm.allPayNum=vm.allPayNum+data[i].payNum;//总收入
							      vm.allReturnNum=vm.allReturnNum+data[i].returnNum;//总退款
							      vm.allCount=vm.allCount+data[i].trueNum;//累计收入
							      vm.allAddPeo=vm.allAddPeo+data[i].addWx
							};
			       	    }   
			       	    
			       	    
			       	    
			       	    
			       	    	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        },
     	unReturnOrder:function(){//取消退款
        	
        	vm.order.orderStates="问题订单";
        	 vm.save();
        	 vm.$message('订单已取消');
        	 vm.undialogVisible=false;
        },
        //退款申请
        returnOrder:function(){
        	debugger
        	 vm.order.orderStates="待退款";
        	if(vm.overOrder==true){//订单取消
        		vm.order.orderStates="已取消";
        	}
         	vm.order.returnNumStates="待退款";
        	 vm.order.returnTime=new Date();
        	 vm.save();
        	 vm.$message('订单退款申请成功');
        	 vm.dialogVisible=false; 
        },
        orderSave:function(val){
        	vm.order.orderStates=val;
        	vm.save();
        	vm.$message('操作成功');
        },
        saveBut:function(){
        	vm.save();
        	vm.$message('保存成功');
        	
        },
          del:function(){
        	//vm.order.useFlag=0;
        	vm.order.orderStates="已取消";
         vm.order.unReturnResion="订单未确认,直接取消";
        	vm.save();
            vm.$message('订单取消成功');
        },
          save:function(){
        	debugger
        	 $.ajax({
	 		   type: "POST",
	           url: '/cOrder/save',
	           data: JSON.stringify(vm.order),
	           contentType : "application/json;charset=utf-8",
	           dataType:"json",
	           async:false,//取消异步请求
	           cache:false, // 设置为 false 将不缓存此页面 
	           success: function(obj) {
	           		vm.order =obj; 
	           },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       });
        	
        },
	},
	
	mounted(){
		debugger
       this.init();
   } , 
    
  
})