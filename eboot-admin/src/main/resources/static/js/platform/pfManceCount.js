  
 var vm = new Vue({
	el: '#app',
	
	data:{
		  tableData:[], 
	  form:{
			type:"0",
		},
		 createIdList:[],//人员
		 
    },
 	watch: {
	  
    }, 
	methods: {	
        //初始执行
        init() {       
            
        },
	    
	    leaveTime :function(value)
	{
		debugger
	         var date_ = new Date(value);
            var year = date_.getFullYear();
            var month = date_.getMonth()+1;
            var day = date_.getDate();
            if(month<10) month = "0"+month;
            if(day<10) day = "0"+day;

            var hours = date_.getHours();
            var mins = date_.getMinutes();
            var secs = date_.getSeconds();
            var msecs = date_.getMilliseconds();
            if(hours<10) hours = "0"+hours;
            if(mins<10) mins = "0"+mins;
            if(secs<10) secs = "0"+secs;
            if(msecs<10) secs = "0"+msecs;
            return year+"/"+month+"/"+day+" "+hours+":"+mins;
	},
	      
       getAllUser:function(){
       	debugger
       			$.ajax({
			 	  type: "POST",
			       url: '/sys/user/allUser' ,		
			       	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		
			       	  vm.createIdList=data;
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
       }, 
       clean:function(){
       	vm.form={};
       	
       },
        query:function(){ 
        	debugger         
			   var forData=vm.form;
	         if(forData.createTimeStart==undefined||forData.createTimeStart==null){
	         	 this.$message('请选择开始时间');
	         	return;
	         }
	          if(forData.createTimeEnd==undefined||forData.createTimeEnd==null){
	         	 this.$message('请选择结束时间');
	         	return;
     		  }
	          $.ajax({
			 	   type: "POST",
			       url: '/pfManceCount/query',			      
		            contentType : "application/json;charset=utf-8",
		           data:JSON.stringify(vm.form),
		           async:true,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       	debugger
			       	    vm.tableData=data; 
			      },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        },
     	 
	},
	
	mounted(){
		debugger
       this.init();
   } , 
    
  
})