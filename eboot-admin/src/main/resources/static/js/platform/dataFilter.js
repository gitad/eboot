  
 var vm = new Vue({
	el: '#app',
	
	data:{
		current:1,//当前页
		size:10,//页面条数
		count:0,//总共条数
		show:false,
		form:{
			callType:'',
   	    	goodsName:'',
		 	cityName:'',
		 	carlen:'',
		 	carType:'',
		},

	  dialogLenTypeVisible:false,//车长 车型  
	  dialogVisible:false,
	  relForm:{  
	      selCarLen:'',//车型
		  selCarType:'',//车长 
	  	  routeType:'直达',
	  	  countryStart:'',
	  	  countryTrans:'',
	  	  countryEnd:'',
	  	  
	  	  provinceCode:'',
     	  cityCode:'',
     	  areaCode:'',
	  },
	  
	  //字表保存
      selectData:{  //省市区选择
     	 provinceCode:'',
     	 cityCode:'',
     	 areaCode:'',
	  },	  
	  carLens:[],//车长选择
	  carTypes:[],//车型
	    provinceData:[],//省
		 cityData:[],//市
		 areaData:[],//区
	
     onebleData:[],
     twoTableData:[],
     threeTableData:[],

	 options: [{    value: '0',    label: '拨打电话'   }, 
	 { value: '1',    label: '微信回复'    }, 
	 {   value: '2',   label: '通话记录同步'},
	 {   value: '3',   label: '标签-车长/车型/地址/路线'}  ],
        mainUrl:'',
        mainIndex:'',
        dropValue:'',//下拉的值
    },
 	watch: {
	  
  }, 
	methods: {	
        //初始执行
        init() {   
        	debugger
        	 this.carLens= this.getDropValue("4f8fb1d0089f47bb8912f4d3cfaff7fe").data//车长
        	 this.carTypes= this.getDropValue("b08212412ac54616a6b6d1f947dea2be").data//车型    
          // this.query();      
        },
            radioChange:function(val){
        	
	        	if(val=="直达"){
	        		vm.show=false;
	        		vm.relForm.provinceTrans="";
	     	 		vm.relForm.cityTrans="";
	     	 		vm.relForm.countryTrans="";
	        	}
	        	if(val=="中转"){
	        		vm.show=true;
	        	}
        },
        lenTypeSave:function(){
        	debugger
        		if(vm.relForm.selCarLen==""){
        		vm.$message.error('请选择车长');
        		return
        	}
        		if(vm.relForm.selCarType==""){
        		vm.$message.error('请选择车型');
        		return
        	}
        	if(vm.relForm.areaCode==""){
        		vm.$message.error('请选择省市区');
        		return
        	}
        	
         if(vm.relForm.countryStart==""){
        		vm.$message.error('请选择路线起点');
        		return
        	}
         if(vm.relForm.countryEnd==""){
        		vm.$message.error('请选择路线终点');
        		return
        	}
        	
        	
        	
        	
        	 var seletcDate=vm.$refs.twoTableData.selection;
       	 		var forData=vm.relForm;
       	 		url='/dataFilter/insertCarInfo?1=1'
	        		 Object.keys(forData).some(function(key){
		        		 if(forData[key]!=""&&forData[key]!=null){
		        	     	  url+="&"+key+"="+forData[key]
		        	     	   // url="&condition["+key+"]="+forData[key]
		        	 	 }
	        	  })
        		$.ajax({
					  type : "POST",
					  contentType : "application/json;charset=utf-8",
					  //url : '/dataFilter/insertCarInfo?carLen='+vm.relForm.selCarLen+'&carType='+vm.relForm.selCarType+'&provinceCode='
							  //+vm.relForm.provinceCode+'&cityCode='+vm.relForm.cityCode+'&areaCode='+vm.relForm.areaCode, // 拨打电话
					  url : url, 
					  data:JSON.stringify(seletcDate),
					  dataType : 'json',
					  success : function(data) {	  
						  	
					  }					  
				 }); 
				vm.$message.error('批量操作成功');	 
				vm.dialogLenTypeVisible=false;		 
        },
       
       handleClose:function(done) {     
         	   	
	       // this.$confirm('确认关闭？') .then(_ => { done();  })  .catch(_ => {});
	        $.ajax({
					  type: "POST",
			          url: '/cCarInfo/relDel',				          
			          data:  JSON.stringify(done),
		                contentType : "application/json;charset=utf-8",
			          dataType:"json",
			          async:false,//取消异步请求
			          cache:false, // 设置为 false 将不缓存此页面	  		              
			          success: function(obj) {	 
			          		
			          	 	vm.query();
 			          },
		         
		    	 });
	       
     	 },
     	   openAddress:function(val){//弹窗省市区选择
     	   	debugger
     	 	   vm.dialogVisible = true 
     	 	  vm.dataType=val; 
     	 	  vm.addRessOpen(0,1) 	 	
     	 },
     	  save:function(){
     	 	debugger
	     	  vm.dialogVisible=false;  
	     	 	if(vm.dataType==0){ //出发  	 		
	     	 	  	vm.relForm.provinceStart=vm.selectData.provinceCode;
	     	 		vm.relForm.cityStart=vm.selectData.cityCode;
	     	 		vm.relForm.countryStart=vm.selectData.areaCode;   
	     	 		if(vm.ifAddress==true){
	     	 			vm.relSave();
	     	 		}	 		
	     	  	}
	     	 	if(vm.dataType==1){//中转
	     	 		
	     	 		vm.relForm.provinceTrans=vm.selectData.provinceCode;
	     	 		vm.relForm.cityTrans=vm.selectData.cityCode;
	     	 		vm.relForm.countryTrans=vm.selectData.areaCode;   	 
	     	 	}
	     	 	if(vm.dataType==2){//目的
	     	 		
	     	 		vm.relForm.provinceEnd=vm.selectData.provinceCode;
	     	 		vm.relForm.cityEnd=vm.selectData.cityCode;
	     	 		vm.relForm.countryEnd=vm.selectData.areaCode;   	 
	     	 	}
	     	 		if(vm.dataType==3){//常跑地址
	     	 		
	     	 		vm.relForm.provinceCode=vm.selectData.provinceCode;
	     	 		vm.relForm.cityCode=vm.selectData.cityCode;
	     	 		vm.relForm.areaCode=vm.selectData.areaCode;   	 
	     	 	}
	     	  
     	 },
         addRessOpen:function(type,val,data,name){      
          
        		//type:0 省 1市 2区
        		debugger 
        	 
        	  
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	  
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.selectData.provinceCode=name;
        	 	vm.selectData.cityCode="";
        	 	vm.selectData.areaCode="";
        	 } 
        	  if(type==2){
        	 	vm.areaData=obj.data;
        	 	vm.selectData.cityCode=name;
        	 	vm.selectData.areaCode="";
        	 } 
        	   if(type==3){		        	  
        	 	vm.selectData.areaCode=name;
        	 }    	
         
	      
         },
           //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
        query:function(){ 
        	
        	if(vm.mainIndex==0){//实时数据          
         	}
         	if(vm.mainIndex==1){//通话记录
         		vm.mainUrl="/dataFilter/queryCallLog?current="+vm.current+"&size="+vm.size ;	 
         		/*if(vm.cityName!=""){
         			    vm.mainUrl+=encodeURI("&condition[cityName]="+vm.cityName);
         		}
         		if(vm.goodsName!=""){
         			   vm.mainUrl+=encodeURI("&condition[goodsName]="+vm.goodsName);
         		}
         		if(vm.callType!=""){
         			   vm.mainUrl+=encodeURI("&condition[callType]="+vm.callType);
         		}*/
         		 var forData=vm.form;
	        		 Object.keys(forData).some(function(key){
		        		 if(forData[key]!=""&&forData[key]!=null){
		        	     	 var maps="&condition["+key+"]="+forData[key]
		        	     	 vm.mainUrl+=encodeURI(maps);
		        	 	 }
	        	  })
          	}
         	if(vm.mainIndex==2){//公众号货源
         		
         	}      	
        	  $.ajax({
			 	  type: "POST",
			       url: vm.mainUrl,			      
		           contentType : "application/json;charset=utf-8",
		           async:false,
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       	    vm.count=data.count;			     	  
		         	  	if(vm.mainIndex==0){//实时数据
         		
			         	}
			         	if(vm.mainIndex==1){//通话记录
			         		vm.twoTableData=data.data;
			         	}
			         	if(vm.mainIndex==2){//公众号货源
			         		
			         	}     	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        },
        
        
        selectChage:function(val){
        	 //下拉同步
        	 debugger
        	 if(val==""){
        	 	retrun
        	 }
        	 if(val==0){//拨打电话
    	 		if(vm.mainIndex==0){//实时数据
     				 var seletcDate=vm.$refs.oneTableData.selection;
     				 if(seletcDate.length==0){
     				 	this.$message.error('请先选择数据');
     				 	return
     				 }
	         	}
	         	if(vm.mainIndex==1){//通话记录
	         		 var seletcDate=vm.$refs.twoTableData.selection;
	         		  if(seletcDate.length==0){
     				 	this.$message.error('请先选择数据');
     				 	return
     				 }
     				 	
					$.ajax({
						  type : "POST",
						  contentType : "application/json;charset=utf-8",
						  url : '/dataFilter/telPhone', // 拨打电话
						  data:JSON.stringify(seletcDate) ,
						  dataType : 'json',
						  success : function(data) {	  
							  window.parent.telBox();	
						  }					  
					 }); 
					 
	         	}
	         	if(vm.mainIndex==2){//公众号货源
	         		 var seletcDate=vm.$refs.threeTableData.selection;
	         		
	         	}
        	 	
        	 	 
        	 }
        	  if(val==1){//微信回复
    	 		if(vm.mainIndex==0){//实时数据
     				 var seletcDate=vm.$refs.oneTableData.selection;
     				 if(seletcDate.length==0){
     				 	this.$message.error('请先选择数据');
     				 	return
     				 }
	         	}
	         	if(vm.mainIndex==1){//通话记录
	         		 var seletcDate=vm.$refs.twoTableData.selection;
	         		  if(seletcDate.length==0){
     				 	this.$message.error('请先选择数据');
     				 	return
     				 }
     				 	
					$.ajax({
						  type : "POST",
						  contentType : "application/json;charset=utf-8",
						  url : '/dataFilter/telPhone', // 拨打电话
						  data:JSON.stringify(seletcDate) ,
						  dataType : 'json',
						  success : function(data) {	  
							  window.parent.telBox();	
						  }					  
					 }); 
					 
	         	}
	         	if(vm.mainIndex==2){//公众号货源
	         		 var seletcDate=vm.$refs.threeTableData.selection;
	         		
	         	}
        	 	
        	 	 
        	 }
        	 
        	 
        	 if(val==2){//通话记录同步
	        	 	 this.$message.error('数据开始同步');
	        	 	 $.ajax({
			 		   type: "POST",
			           url: '/dataFilter/call_log_syn',
			           dataType:"json",
			           cache:false, // 设置为 false 将不缓存此页面	           
			           success: function(obj) {
			        	   
			           	 	if(obj.data==true){         		 
		      				  this.$message.error('数据同步成功');
			           		}
			           		if(obj.data==false){         		 
			     			  this.$message.error('数据同步失败');
			          		}
		           	},
		           error: function(data) {
		               // 请求失败函数
		               console.log(data);
		           }
		       	});
        	 }
        	 if(val==3){//车长 车型标签
        	 	debugger
		 		 var seletcDate=vm.$refs.twoTableData.selection;
				 if(seletcDate.length==0){
				 	this.$message.error('请先选择数据');
				 	return
				 }
        	 
        	 	    vm.addRessOpen(0,1) 
        	 	  	vm.dialogLenTypeVisible=true;		
        	 }
        	 
        }, 
         handleClick(tab, event) {
         	
         	vm.mainIndex=tab.index;
         	vm.query();
          
       },
        handleSizeChange(val) {
         	
         	vm.size=val;
       		vm.query();
        },
		 handleCurrentChange(val) {
         	
         	vm.current=val;
      		 vm.query();
        },
	},
	
	mounted(){
       this.init();
   } , 
    
  
})