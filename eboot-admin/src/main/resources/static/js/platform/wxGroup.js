  
 var vm = new Vue({
	el: '#app',
	
	data:{
	    current:1,//当前页
		size:10,//页面条数
		count:0,//总共条数
		tableData:[],
		form:{},
		selCarLen:[],
	    selCarType:[],
	     carlenData:[],//车长
	    carTypeData:[],//车型
		changeType:'',
		tags:[],//标签多选
		zjtableData:[],
		dialogVisible:false,//区域选择
		zjdialogVisible:false,//客户转交
		dialogTagVisible:false,//标签
		dialogSendVisible:false,//消息发送人
		dialogCarLenVisible:false,//车长
		dialogCarTypeVisible:false,//车型
		provinceData:[],//省
	    cityData:[],//市
	    areaData:[],//区
	    mainData:[],
	    sendList:[],
	    tagsList:['物流', '聊天', '货运'],
	     selectData:{ provinceCode:'', cityCode:'', areaCode:'' },//当前选择的form表格数据  选择省市区时用到
    },
 	watch: {
	 selCarLen:function(newVal){
	 	debugger
	 	 if(newVal.length>0){
    			for (var i=0; i < newVal.length; i++) {
		    		if(i==0){
		    			 for (var k=0; k < vm.mainData.length; k++) {
						    vm.mainData[k].carLen=newVal[i];
						     vm.save(vm.mainData[k]);
						 };
		    			 
		    		}else{
		    			 for (var k=0; k < vm.mainData.length; k++) {
						   vm.mainData[k].carLen= vm.mainData[k].carLen+","+newVal[i];
						   vm.save(vm.mainData[k]);
						 };
		    		
		    		}
	 			
				};
				
	    	}
	    },
	   selCarType:function(newVal){
	 	debugger
	 	 if(newVal.length>0){
    			for (var i=0; i < newVal.length; i++) {
		    		if(i==0){
		    			 for (var k=0; k < vm.mainData.length; k++) {
						    vm.mainData[k].carType=newVal[i];
						     vm.save(vm.mainData[k]);
						 };
		    			 
		    		}else{
		    			 for (var k=0; k < vm.mainData.length; k++) {
						   vm.mainData[k].carType= vm.mainData[k].carType+","+newVal[i];
						   vm.save(vm.mainData[k]);
						 };
		    		
		    		}
	 			
				};
				
	    	}
	    },
	 	selectData: {
		      handler: function (val) {
		        if (val.areaCode!=null&val.areaCode!="") {
		        	debugger
		        	 
		        	   for (var k=0; k < vm.mainData.length; k++) {
						    vm.mainData[k].remark=val.provinceCode+"-"+val.cityCode+"-"+ val.areaCode ;
						    vm.save(vm.mainData[k]);
						 };
		        	 
		        }
		          
		      },
		      deep: true //对象的深度验证
	    },
	    tags:function(newVal,oldVal){
	    	debugger
	    	if(newVal.length>0){
    			for (var i=0; i < newVal.length; i++) {
		    		if(i==0){
		    			 for (var k=0; k < vm.mainData.length; k++) {
						    vm.mainData[k].groupTag=newVal[i];
						    vm.save(vm.mainData[k]);
						 };
		    			 
		    		}else{
		    			 for (var k=0; k < vm.mainData.length; k++) {
						   vm.mainData[k].groupTag= vm.mainData[k].groupTag+","+newVal[i];
						   vm.save(vm.mainData[k]);
						 };
		    		
		    		}
	 			
				};
				
	    	}
	    	
	    
	    },
     }, 
	methods: {	
        //初始执行
        init() {       
            this.query();      
        },
        selectChange:function(val){
        	debugger
			 var selectData=vm.$refs.tableData.selection;
			 if(selectData.length==0){
				vm.$message.error('请先选择数据');
				return
			}
		    if(val==0){
				 vm.addRessOpen(0,1,selectData)
			}
			if(val==1){
				 vm.addTags(selectData);
			} 
			if(val==2){//承运车长
				 vm.dialogCarLenVisible=true;
			     vm.carlenData=vm.getDropValue("4f8fb1d0089f47bb8912f4d3cfaff7fe").data;//车长
			      vm.mainData=selectData;
			} 
			if(val==3){//车型
				vm.dialogCarTypeVisible=true;
			    vm.carTypeData=vm.getDropValue("b08212412ac54616a6b6d1f947dea2be").data;//车型
	  		   vm.mainData=selectData;
			} 
        },
         addTags:function(data){
        	debugger
        	vm.tags=[];
            vm.mainData=data;
            vm.dialogTagVisible=true
        	
        },
        cellClick:function(row, column, cell, event){
        	 debugger
    		if(column.property=="messName"){
    		 	vm.dialogSendVisible=true;
    		 	vm.mainData=row;
    		 	vm.queryCustList(row.groupId);
    		}
        	 
        }, 
        radioSave:function(val){
        	debugger
        	vm.save(vm.mainData);
        	vm.query();
        },
        queryCustList:function(wxid){
        	debugger
        	 $.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: '/wxGroup/queryCustList?wxid='+wxid,      	 	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){  
        			debugger
        			  vm.sendList=data;	        				 
				  
        		}
        	}); 
        },
       
          addRessOpen:function(type,val,data,name){
         	if(type==0){
         		vm.mainData=data;
         	}
         	
         	
        		//type:0 省 1市 2区
        	 vm.dialogVisible = true
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	  
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.selectData.provinceCode=name;
        	 	vm.selectData.cityCode="";
        	 	vm.selectData.areaCode="";
        	 } 
        	  if(type==2){
        	 	vm.areaData=obj.data;
        	 	vm.selectData.cityCode=name;
        	    vm.selectData.areaCode="";
        	 } 
        	   if(type==3){	
        	        	  
        	 	vm.selectData.areaCode=name;
        	 
        	 }  
         },
        
        query:function(){ 
        	 var   url="/wxGroup/query?current="+this.current+"&size="+this.size  	
        	 var forData=this.form;
        	 Object.keys(forData).some(function(key){
        	 	   if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
             })
          $.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: url,      	 	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){   
        			    	        				 
				     vm.tableData=data.data; 				     
				     vm.count=data.count;	 
        		}
        	}); 
        },
       
        save:function(row){
        	
        	 $.ajax({
	    		    type: "POST",
	    		    url: '/wxGroup/save',
	                data:JSON.stringify(row),
	            	contentType : "application/json;charset=utf-8",
	                dataType:"json",
	                cache:false, // 设置为 false 将不缓存此页面	           
	                success: function(obj) {
	                	
	                	 if(obj.data==true){
		            	 
		            		  //vm.$message.error('保存成功');
		     		    	return;
		            	 }
	    		    },
	                error: function(data) {
	                  // 请求失败函数
	                  console.log(data);
	               }
	          });  
        	
        },  	 
           //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
		clear:function(){
		 
		 	vm.form.provinceName='';
			vm.form.cityName='';
			vm.form.areaName='';
			vm.form.custName='';
			vm.form.custType='';
			vm.form.ifNormal='';
			 
		},
		openIm:function(id){
			
			 	  $.ajax({
					type: "GET",
					url: '/sys/cust/by/im/id?id='+id+"&idx=1",
					dataType: 'json',
					async:false,
					success: function(data) {
						 window.parent.wx_open()	 			
					}
	    	 	  });
        	
		},
		 
		redisCsh:function(){
		 $.ajax({
  	  		    type: "POST",
  	            url: '/sys/cust/cust-redis-init',
  	           contentType : "application/json;charset=utf-8",
  	            dataType:"json",
  	            cache:false, // 设置为 false 将不缓存此页面	           
  	            success: function(obj) {
  	     			vm.$message.error('初始化成功');           	
  	            },
  	            error: function(data) {
  	                // 请求失败函数
  	                console.log(data);
  	            }
  	        });  
		},
		phztb:function(){
			$.ajax({
		  		    type: "POST",
		            url: '/sys/cust/cust-distribution',
		            contentType : "application/json;charset=utf-8",
		            dataType:"json",
		            cache:false, // 设置为 false 将不缓存此页面	           
		            success: function(obj) {		            	
		            	 if(obj.data==true){
		            		 vm.$message.error('分配成功');		            	 
		     		    	return;
		            	 }
		            	if(obj.data==false){
		            		vm.$message.error('分配失败');
		     		    	return;
		            	}	            	
		            },
		            error: function(data) {
		                // 请求失败函数
		                console.log(data);
		            }
		        }); 
			
		},
		messMass:function(val){//消息群发
			
			if(vm.$refs.tableData.selection.length>500){
				 $.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/sys/cust/messMass?content='+val, // 发送好友文本消息 	
				  data:JSON.stringify(vm.$refs.tableData.selection) ,
				  dataType : 'json',
				  success : function(data) {
				  	  
					  vm.$message.error('群发成功');			   
				  }		  
		    	}); 
			}else{
				$.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/sys/cust/messMass', // 发送好友文本消息 	
				  data:JSON.stringify(vm.$refs.tableData.selection) ,
				  dataType : 'json',
				  success : function(data) {
					  
					  window.parent.wx_open()
				  }
			  
			    }); 
			}
			 
		},
		messMassAll:function(val){
			
			  $.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/sys/cust/messMassAll?content='+val, // 发送好友文本消息 	
				  dataType : 'json',
				  success : function(data) {
					   vm.$message.error('群发成功');
					   
				  }
		  
		    }); 
		},
		customerZj:function(){
			
			$.ajax({
			  type : "POST",
			  url : '/sys/cust/query-all-user', //获取待转交好友 		 
			  dataType : 'json',
			  success : function(data) {
				 
				  vm.zjtableData=data;
			  }
    		}); 
		},
		customerZjClick:function(){//用户转交确认
			
			 var selectData=vm.$refs.zjtableData.selection;
			 if(selectData.length>1){
			 	 vm.$message.error('请选择单条数据');
			 	 return
			 }
			  if(selectData.length==0){
			 	 vm.$message.error('请选择数据');
			 	 return
			 }
			$.ajax({
  			  type : "POST",
  			  contentType : "application/json;charset=utf-8",
  			  url : '/sys/cust/user-distri?id='+selectData[0].id+"&name="+selectData[0].name, // 用户转交
  			  data:JSON.stringify(vm.$refs.tableData.selection) ,
  			  dataType : 'json',
  			  success : function(data) {
  				 
 			       if(data==true){
 			    	 vm.$message.error('用户分配成功');
 			    	 vm.zjdialogVisible=false;
 			    	  return
 			       }
 			      if(data==false){
 			    	 vm.$message.error('用户分配失败');
			    	  return  
 			       }
 			    
  			  }
  		    }); 
		},
		
		 tel:function(){
			
			 var selectData=vm.$refs.tableData.selection;
				if(selectData.length==0){
					vm.$message.error('请先选择数据');
					return
				}
				$.ajax({
					  type : "POST",
					  contentType : "application/json;charset=utf-8",
					  url : '/sys/cust/telPhone', // 拨打电话
					  data:JSON.stringify(selectData) ,
					  dataType : 'json',
					  success : function(data) {	  
						  window.parent.telBox();	
					  }
				  
				    }); 
	},
  		handleSizeChange(val) {//分页
         	
         	vm.size=val;
       		vm.query();
        },
		 handleCurrentChange(val) {//分页
         	
         	vm.current=val;
      		 vm.query();
        },
	},
	
	mounted(){
       this.init();
   } , 
    
  
})