 
var httpurl = "";

var vm = new Vue({
	el: '#app',
	
	data:{
	  tabShow:true,
	  current:1,//当前页
	  size:10,//页面条数
	  count:0,//总共条数
	  search:'',
	  fileId:'',
	  fileType:'',
	  dialogImageUrl:'',
	  dialogVisiblepic:false,
	  dialogVisibledd:false,
	  dialogVisible:false,
	  dialogAddressVisible:false,
	  dialogVisible1:false,
	  dialogCarsUpVisible:false,
	  startForm:[],
	  endForm: [] ,
	  orderForm:[],
	  mainData:{},//主表数据
	  provinceData:[],//省
	  cityData:[],//市
	  areaData:[],//区
	  selectData:{},//当前选择的form表格数据  选择省市区时用到
	  packData:[], //包装形式
	  cargoData:[], //货物分类形式 
	  selCarLen:[],
	  selCarType:[],
	  cargoTypeData:[],//货源状态
  	  carInfo:{},//车辆
  	  carGoInfo:{},//车寻货
	  cContactPeo:{},//联系人及备注
	  contactPeoRel:{},//联系人备注得电话号码  
	  cContactPeoList:[],//人员选择
	  cContactPeoRelList:[],//电话选择
	  carInfoList:[],//车辆选择
	  jszfileList:[],//图片列表
	  xszfileList:[],//图片列表
	  payPathList:[],//支付路径
	  form:{ 	addressDetail:'', customerPrice:'',  	carsAndGoods:'', 	cargoType:['普通','急发'], 	planTime_start:"",  	planTime_end:""   },
	  tableData: [],
	  carTypeData:[],
	  tableDataCopy: [],
	  childrenTableData:[],
	  styleObject: {
	    color: 'blue',
	  },
	  expanType:false,//展开状态
	  wxstatus:'',
	  tcprogress:'',
	  carType:[  ],
	  cargoType: [ ],
      options: [
      		{    value: '正常',    label: '正常'    }, {    value: '删除',    label: '删除'   } 
    	],
      options1: [   {   value: '符合想订',   label: '符合想订'    } ,  {  value: '车型不符',  label: '车型不符'   } ,
      		{    value: '运费可谈',     label: '运费可谈'  },  {   value: '定了不准',   label: '定了不准'    },  
      		 {    value: '同行试探',  label: '同行试探'    },   {   value: '未谈',    label: '未谈'   }
    	],
	 expands: [],
		loading:false	//加载状态
    },
 
	methods: {
		
        //初始执行
        init() {
        	 
        	  var day1 = new Date();
			   day1.setTime(day1.getTime()-24*60*60*1000);
			    this.form.planTime_start=this.formatDate(day1);
		   		this.form.planTime_end=this.formatDate(new Date());
         
             this.query()
          	 this.carType=this.getDropValue("7bca4d3f2b8d4dc9a848dd3ea144a050").data;//用车类型
          	 this.cargoType=this.getDropValue("09cbc95b9ec2432fada3abfb10f21f73").data;//货源状态
           	 
        },
          payPathChange:function(val){
	        	debugger
	        	for (var i=0; i < vm.payPathList.length; i++) {
				  if(vm.payPathList[i].wxId==val){
				  	 vm.orderForm.payPath=vm.payPathList[i].wxNickname;
				  		vm.order.payPathId=val;
				  	  return
				  }
				};
        	
        },
         getAllWxLogin:function(){
       	
       			$.ajax({
			 	  type: "POST",
			       url: '/cOrder/getAllWxLogin' ,		
			       	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		debugger
			       		
			       		for (var i=0; i < data.length; i++) {
							 data[i].wxNickname=data[i].wxNickname.split('-')[0]
						   };
			       		 
			       	 vm.payPathList=data;
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
       },
        formatDate:function (date) {
			  var d = new Date(date),
			    month = '' + (d.getMonth() + 1),
			    day = '' + d.getDate(),
			    year = d.getFullYear();
			 
			  if (month.length < 2) month = '0' + month;
			  if (day.length < 2) day = '0' + day;
			 
			  return [year, month, day].join('-');
		},
        openIm:function(id){
			
			 	  $.ajax({
					type: "GET",
					url: '/sys/cust/by/im/id?id='+id+"&idx=1",
					dataType: 'json',
					async:false,
					success: function(data) {
						 window.parent.wx_open()	 			
					}
	    	 	  });
        	
		},
	 
          //保存页面全部数据
	    saveAll:function(){
	    	
	    	if(vm.orderForm.orderStates!=null&&vm.orderForm.orderStates!="待确认"){
	    		 vm.$message.error('订单已被确认无法进行修改');
	    		 return;
	    	}else{
		    	 vm.mainSave();//保存页面货物和地址主要数据
		    	  vm.carSave();//保存车辆数据
		   	     vm.peoSave();//保存关联人数据
		    	 vm.peoRelSave();//电话保存
		    	 vm.carGoInfoSave();//cargoinfo 保存
		    	 vm.orderSave();//订单保存 
	    		
	    	}
	      
	    },
	     detail:function(row,val){
	     	
     		 vm.carGoInfo=row;
	      	vm.tabShow=false;
	       // vm.mainData=row;  
	        vm.queryMainData(row); //查询主表数据  
	        //0起点
	        vm.startForm=vm.queryForm(row.cargoId,0);
	        //1终点 
         	vm.endForm=vm.queryForm(row.cargoId,1);
            //查询车辆信息
         	vm.queryCarData(row);
         	//人员查询
         	vm.queryPeoData(row);
         		//人员电话查询
         	vm.queryPeoRelData(row);
         	//查询图片
         	vm.jszfileList=vm.queryPicList(row.cargoInfoId,"JSZ");
         	vm.xszfileList=vm.queryPicList(row.cargoInfoId,"XSZ");
         	//人员选择查询
         	vm.queryPeoList(row);
         	 
         	//车辆选择
         	vm.queryCarList(row);
         	 
         	vm.queryOrder(row,1);
         	vm.cargoTypeData=vm.getDropValue("09cbc95b9ec2432fada3abfb10f21f73").data;
        },
	    orderSave:function(){
	    	
	    	vm.orderForm.custName=vm.carGoInfo.custName;
	    	vm.orderForm.vaildPhone=vm.carGoInfo.vaildPhone;
	    	vm.orderForm.cargoInfoId=vm.carGoInfo.cargoInfoId;
	    	vm.orderForm.remark=vm.mainData.addressDetailStr+vm.mainData.addressDetailEnd;
	    	  $.ajax({
	 		   type: "POST",
	           url: '/cOrder/save',
	           data: JSON.stringify(vm.orderForm),
	           contentType : "application/json;charset=utf-8",
	           dataType:"json",
	           async:false,//取消异步请求
	           cache:false, // 设置为 false 将不缓存此页面 
	           success: function(obj) {
	           		vm.orderForm =obj; 
	           },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       });
	    },
	    carGoInfoSave:function(){
	    	vm.carGoInfo.useCar=1;
	    	vm.carGoInfo.carId= vm.carInfo.carId;
	    	vm.carGoInfo.contactId= vm.cContactPeo.contactId;
	    	vm.carGoInfo.contactRelId= vm.contactPeoRel.contactRelId;
	    	  $.ajax({
	 		   type: "POST",
	           url: '/cCargoInfo/sourceSave',
	           data: JSON.stringify(vm.carGoInfo),
	           contentType : "application/json;charset=utf-8",
	           dataType:"json",
	           async:false,//取消异步请求
	           cache:false, // 设置为 false 将不缓存此页面 
	           success: function(obj) {
	           		vm.carGoInfo =obj; 
	           },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       });
	    	
	    },
	    carSave:function(){
	    	 vm.carInfo.customerId=vm.carGoInfo.customerId;
	    	  $.ajax({
				  type: "POST",
		          url: '/cCarInfo/save',				          
		          data:  JSON.stringify(vm.carInfo),
	              contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		          	vm.carInfo=obj;
		             //vm.$message.error('保存成功');
		          	 	 
		          },
		         
		    	 });
	    }, 
	  
        peoRelSave:function(){
        	vm.contactPeoRel.customerId=vm.cContactPeo.customerId;
        	vm.contactPeoRel.contactId=vm.cContactPeo.contactId; 
           $.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeoRel/save',
	            data: JSON.stringify(vm.contactPeoRel),
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	             async:false,//取消异步请求
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
		             vm.contactPeoRel=data;
	          },
       	  }); 
        },
	   mainSave:function(){
        	
        	var mainData=JSON.stringify(vm.mainData);
        	var startForm=JSON.stringify(vm.startForm);
        	var endForm=JSON.stringify(vm.endForm);
        	for(var i=0;i<vm.startForm.length;i++){
        		if(vm.startForm[i].areaCode==""){
        			 this.$message({
			          message: '请先选择发货地点',
			          type: 'warning'
			        });
			        return;
        		}
        	}
        	for(var i=0;i<vm.endForm.length;i++){
        		if(vm.endForm[i].areaCode==""){
        			 this.$message({
			          message: '请先选择收货地点',
			          type: 'warning'
			        });
			        return;
        		}
        	}
	    	 $.ajax({
				  type: "POST",
		          url: '/cCargoEntry/saveAll',	
		          
		          data:{
	            	"mainData":JSON.stringify(vm.mainData),
	            	"startForm":JSON.stringify(vm.startForm),
	            	"endForm":	JSON.stringify(vm.endForm),
	            
	            },	      
		       //   contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	  vm.$message.error('保存成功');		        	 
		          },
		         
		     });
        },
        goBack:function(){
        	vm.tabShow=true;
        	
        },
        picRemove:function(val){
        	
         
        	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/del',	      
		          data: JSON.stringify(val),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
        xszuploadSuccess:function(val){
        	
        	vm.uploadFile(val,"XSZ",vm.carGoInfo.cargoInfoId);
        	vm.baiduOcr(val,"xsz")//识别后检查是否存在重复人员
         
        },
        jszuploadSuccess:function(val){
        	 vm.uploadFile(val,"JSZ",vm.carGoInfo.cargoInfoId);
        	 vm.baiduOcr(val,"jsz")//识别后检查是否存在重复人员
        	 
        },
        
        baiduOcr:function(url,type){
        	
	    	$.ajax({
				  type: "POST",
		          url: '/source/baiduOcr',	      
		          data:{
	            	"imgUrl":url,
	            	"type":type,
	              },		                
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	
		          
		        	  if(obj.peo!=null){
		        	  	   vm.checkPeoData(type,obj.peo);
		        	       
		        	   }
		        	   if(obj.car!=null){
		        	   		vm.checkCarData(type,obj.car);
		        	   	 
		        	   } 
		         },
		         
		     });
        },
        peoSave:function(){
        	 debugger
        	vm.cContactPeo.customerId=vm.carGoInfo.customerId;
            $.ajax({
	  		    type: "POST", 		   
	            url: '/cContactPeo/save',
	            data: JSON.stringify(vm.cContactPeo),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	             async:false,//取消异步请求
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) { 
	            	debugger
		             vm.cContactPeo=data;
		          
	          },
       	  }); 
        },
        checkPeoData:function(type,obj){        	
        	
        	 $.ajax({
				  type: "POST",
		          url: '/source/checkPeoData',	      
		          data: JSON.stringify(obj),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(data) {	 
		          		 debugger         	
		        	 if(data==""){
		        	 	    vm.cContactPeo=obj;
		        	 	    vm.peoSave();//保存关联人数据		        	   	 
         					vm.queryPeoList(vm.carGoInfo);
		        	 	   vm.cContactPeoRelList=vm.cContactPeo.contactPeoList;//电话号码设置为空 因为新的人没有
		         	 }else{
		        	 	   vm.$message.error(data.contactCard+'重复,存在相同人员');
		        	 }
		         },
		         
		     });
        },
        checkCarData:function(type,obj){        	
        	
        	  $.ajax({
				  type: "POST",
		          url: '/source/checkCarData',	      
		          data: JSON.stringify(obj),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(data) {	 
		          	
		        	 if(data==""){
		        	 	  vm.carInfo=obj; 
		        	 }else{
		        	 	  vm.$message.error(data.carLincese+"/"+ data.carTravelNum+'重复,存在相同车架号或车牌');
		        	 }
		         },
		         
		     });
        },
        uploadFile:function(val,businessType,businessId){
        	var file={};
        	file.name=val;
        	file.businessId=businessId;
        	file.businessType=businessType;
        	$.ajax({
				  type: "POST",
		          url: '/sysDocFile/save',	      
		          data: JSON.stringify(file),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        },
         handlePictureCardPreview(file) {
	           vm.dialogImageUrl = file.url;
        	  vm.dialogVisiblepic = true;
        },
       
         queryOrder:function(row,type){
         
      		$.ajax({
				  type: "POST",
		          url: '/cOrder/queryByCargoInfoId',	      
		          data:{
	            	"cargoInfoId":row.cargoInfoId,
	              },	
	                
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	   vm.orderForm=obj;   
		         },
		         
		     });
		     
      }, 
        queryPicList:function(id,type){
        	
         	var data={};
      		
      		$.ajax({
				  type: "POST",
		          url: '/sysDocFile/query',	      
		          data:{
	            	"businessId":id,
	            	"businessType":type,
	              },	
	                
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	   data=obj;   
		         },
		         
		     });
		     return data;
      }, 
         queryCarList:function(row){
         	
         	$.ajax({
				  type: "POST",
		          url: '/cCarInfo/queryByCustomerId',	      
		          data:{
	            	"customerId":row.customerId,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.carInfoList=obj;   
		         },
		         
		     });
         },
      peoPhoneChange:function(val){
      	debugger
      		if(vm.cContactPeoList.length==0){
      	  		this.$message({message: '请先选择人员或者上传驾驶证后再创建手机号',type: 'success'  }); 
      	  		return
      	  	}
      	  //	vm.cContactPeoRelList;
      	  //	
      	   if(vm.cContactPeoRelList.length==0){
      	   		vm.contactPeoRel.contactPhone=val;
      	   } 
      	 
      },
        peoChange:function(val){
        	debugger
        	if(vm.cContactPeoList.length!=0){
        		for (var i=0; i < vm.cContactPeoList.length; i++) {
				  if(vm.cContactPeoList[i].contactId==val){
				  	debugger
				  	 
				  	 vm.cContactPeo=vm.cContactPeoList[i];
				     vm.queryPeoRelList(vm.cContactPeo);
				  	 
				  }
				};
        	} 
         
        	/*  if(val!=""){
        		
        		$.ajax({
				  type: "POST",
		          url: '/cContactPeo/createNewPeo',	      
		          data:{
	            	"customerId":vm.carGoInfo.customerId,
	            	"phone":val,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.contactPeoRel=obj;   
		         },
		         
		     }); 
        	}*/
        },
         queryPeoRelList:function(row){
      	
      		$.ajax({
				  type: "POST",
		          url: '/cContactPeoRel/queryByContactId',	      
		          data:{
	            	"contactId": row.contactId,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		          	debugger
		        	 vm.cContactPeoRelList=obj;   
		        	 
		         },
		         
		     });
      },   
        carChange:function(val){
        		if(vm.carInfoList.length!=0){
        		for (var i=0; i < vm.carInfoList.length; i++) {
				  if(vm.carInfoList[i].carId==val){
				  	  vm.carInfo=vm.carInfoList[i];
				  	  
				  }
				};
        	}
        },
       queryPeoList:function(row){
      	
      		$.ajax({
				  type: "POST",
		          url: '/cContactPeo/queryByCustomerId',	      
		          data:{
	            	"customerId":row.customerId,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.cContactPeoList=obj;   
		         },
		         
		     });
      }, 
    
	  queryPeoRelData:function(row){
      	
      		$.ajax({
				  type: "POST",
		          url: '/cContactPeoRel/queryById',	      
		          data:{
	            	"contactRelId":row.contactRelId,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.contactPeoRel=obj;   
		         },
		         
		     });
      },
      queryPeoData:function(row){
      	debugger
      		
      	$.ajax({
				  type: "POST",
		          url: '/cContactPeo/queryById',	      
		          data:{
	            	"contactId":row.contactId,
	            	 
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	
		          	debugger 
		          	if(row.contactId==null){
      			 		vm.cContactPeo=row;   
      				}else{
      					 vm.cContactPeo=obj;   
      				}
		        	
		         },
		         
		     });
      },
	    queryCarData:function(row){
	    	
	    		$.ajax({
				  type: "POST",
		          url: '/cCarInfo/queryById',	      
		          data:{
	            	"carId":row.carId,
	            	 
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.carInfo=obj;   
		         },
		         
		     });
	    	
	    },
	  
	    queryMainData:function(row){
	    	
	    		$.ajax({
				  type: "POST",
		          url: '/cCargoEntry/queryById',	      
		          data:{
	            	"cargoId":row.cargoId,
	            	 
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 vm.mainData=obj;   
		         },
		         
		     });
	    	
	    },
        goodsUp:function(){//货物
        	
        	vm.dialogVisible1=true;
        	vm.packData=vm.getDropValue("92dba6252d3e4c04bb442d617296ee7e").data;
        	vm.cargoData=vm.getDropValue("0d68b4b985aa491db7e9c4eab70f800c").data;
        },
        CarsUp:function(){//用车类型
        	
        	vm.dialogCarsUpVisible=true;
        	 vm.carUseTypeData=vm.getDropValue("7bca4d3f2b8d4dc9a848dd3ea144a050").data;//用车类型
	 		 vm.carlenData=vm.getDropValue("4f8fb1d0089f47bb8912f4d3cfaff7fe").data;//车长
	 	 	 vm.carTypeData=vm.getDropValue("b08212412ac54616a6b6d1f947dea2be").data;//车型
	    },
	     chcekBoxCarLen:function(newVal,old){
        	
        	if(vm.selCarLen.length>0){
    		    for(var i=0;i<vm.selCarLen.length;i++){
			    	if(i==0){
			    		vm.mainData.carLen=vm.selCarLen[i];
			    	}else{
			    		vm.mainData.carLen=vm.mainData.carLen+","+vm.selCarLen[i]
			    	}
    			}
        	}else{
        		vm.mainData.carLen="";
        	}
       
        },
         chcekBoxCarType:function(newVal,old){
        	
        
        	if(vm.selCarType.length>0){
    		    for(var i=0;i<vm.selCarType.length;i++){
			    	if(i==0){
			    		vm.mainData.carType=vm.selCarType[i];
			    	}else{
			    		vm.mainData.carType=vm.mainData.carType+","+vm.selCarType[i]
			    	}
    			}
        	}else{
        		vm.mainData.carType="";
        	}
        },
          redioChange:function(val,type){
        	
        	if(type==0)	{
    		  if(val=="零担"){
    	        vm.mainData.carLen="";
    	        vm.selCarLen=[];
    		  } 
    		   vm.mainData.carUseType=val;
        	}	 
 			if(type==1)	{
        	  vm.mainData.pack=val;    
        	}  		       	
        },
      
         queryForm(cargoId,type){
        	
        	var from=[];
        	$.ajax({
				  type: "POST",
		          url: '/cCargoEntry/queryForm',	      
		          data:{
	            	"cargoId":cargoId,
	            	 "type":type,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 from=obj; 
		         },
		         
		     });
        	return from;
        },
         addRessOpen:function(type,val,data,name){
        	
        		//type:0 省 1市 2区
        	 vm.dialogVisible = true
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	 	vm.selectData=data;
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.selectData.provinceCode=name;
        	 	vm.selectData.cityCode="";
        	 	vm.selectData.areaCode="";
        	 } 
        	  if(type==2){
        	 	vm.areaData=obj.data;
        	 	vm.selectData.cityCode=name;
        	 		vm.selectData.areaCode="";
        	 } 
        	   if(type==3){		        	  
        	 	vm.selectData.areaCode=name;
        	 }    	
         },
    
         addStart:function(){ 
        	 vm.startForm.push(vm.addNewData(1))
         },
           addEnd:function(){
          	vm.endForm.push(vm.addNewData(1))    	 
         } ,
        delEnd:function(val,index){
         	
         	vm.endForm.splice(index,index)
         },
         delStart:function(val,index){
         	
         	vm.startForm.splice(index,index)
         },
        addNewData:function(val){
          	var  newData={
		  	  provinceCode:'',
			  cityCode:'',
			  areaCode:'',
			  address:'',
			  contacts:'',
			  useFlag:'1',				 
			  transportType:val,//代表起点
	  		}
         	return newData;
         },
        selectChange:function(d){
        	
        	var newChild=[];
        	for (var i=0; i < vm.childrenTableData.length; i++) {
			 	if(vm.childrenTableData[i].wxStatus.indexOf(d)==0 ){
			 		newChild.push(vm.childrenTableData[i]);
			 	} 
			}
			vm.childrenTableData=newChild;
        	  
        },
     
        query:function(){
        	debugger
        	 var   url="/source/query?current="+this.current+"&size="+this.size  	
        	 var forData=this.form;
        	 Object.keys(forData).some(function(key){
        		 if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	 	 }
        	  })
         	$.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: url,
        	 	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){   
        			    	debugger
        				 
				     vm.tableData=data.data; 
				     
				     vm.count=data.count;	 
        		}
        	}); 
        },
     
         handleSizeChange(val) {
         	
         	vm.size=val;
       		this.query();
         },
         handleCurrentChange(val) {
         	
         	vm.current=val;
      		 this.query();
        },
      
   
        handleToogleExpand(row,type) {
        	 
          	    vm.expanType=!vm.expanType;		          
	          	if(vm.expanType==true){
	          		vm.tableDataCopy=vm.tableData;
	          		vm.tableData=[];
	          		vm.tableData.push(row);
	              
	          	}else{
	          		vm.tableData=vm.tableDataCopy;
	          	}  
		    
	        this.$nextTick(function(){ //dom渲染完成后执行得函数
	        	
	        	 let $table = this.$refs.table;
				 $table.toggleRowExpansion(row,vm.expanType);
	        	
            })
	       
	        //选中行  $table.toggleRowSelection(row)
	       
		        $.ajax({
	        	 
	        		type: "POST",
	        		//url: '/source/queryCargo?cargoId='+row.cargoId+"&customerId="+row.customerId+"&type="+type ,
	        	  	url: '/source/queryCargo?type='+type,
	        	  	data:JSON.stringify(row),
	        	    contentType : "application/json;charset=utf-8",
	            	dataType:"json",
	            	cache:false, // 设置为 false 将不缓存此页面	     
	        		success: function(data){       		 
					     vm.childrenTableData=data;  		 
	        		}
	        	}); 
	        
      } 
      
		
	},
	
	mounted(){
        this.init();
   } , 
    computed:{
      tables:function(){
      	 
        var search=this.search;
        var wxstatus=this.wxstatus;
        var tcprogress=this.tcprogress;
       
          return  this.childrenTableData.filter(function(dataNews){
          	 
            return Object.keys(dataNews).some(function(key){
 			  var bool=true;
            	   if(search){
	            	  	  var cu=String(dataNews['custName']).toLowerCase().indexOf(search) > -1;
	            	  	  var va=String(dataNews['vaildPhone']).toLowerCase().indexOf(search) > -1;
	            	  	  if(cu==false&&va==false){
	            	  	  	bool=false;
	            	  	  }	            	        	  	
            	   }
            	   if(wxstatus){           	  	  
            	  		bool=String(dataNews['wxStatus']).toLowerCase().indexOf(wxstatus) > -1;
            	   
            	  }
            	  if(tcprogress){           	   
            	  		bool=String(dataNews[key]).toLowerCase().indexOf(tcprogress) > -1;           	   	 
           	      }         	  
              	return bool;
            })
          })
       
   
        return this.childrenTableData
      }
      
    },
    watch:{
    	 'mainData.weightStr':function(newVal,oldVal){
	 	
	 	if(newVal.indexOf("吨")==-1){
	 		if(newVal!=""){
	 		 vm.mainData.weightStr=newVal+"吨";
	 		}
	 	}else{	 		 
	 		vm.mainData.weightStr=newVal.replace("吨","")+"吨";
	 	}
	 }, 
	   'mainData.weightEnd':function(newVal,oldVal){
		 	
		 	if(newVal.indexOf("吨")==-1){
		 		if(newVal!=""){
		 		 vm.mainData.weightEnd=newVal+"吨";
		 		}
		 	}else{	 		 
		 		vm.mainData.weightEnd=newVal.replace("吨","")+"吨";
		 	}
	 }, 
	   'mainData.volumeStr':function(newVal,oldVal){
		 	
		 	if(newVal.indexOf("方")==-1){
		 		if(newVal!=""){
		 		 vm.mainData.volumeStr=newVal+"方";
		 		}
		 	}else{	 		 
		 		vm.mainData.volumeStr=newVal.replace("方","")+"方";
		 	}
	 }, 
	   'mainData.volumeEnd':function(newVal,oldVal){
		 	
		 	if(newVal.indexOf("方")==-1){
		 		if(newVal!=""){
		 		 vm.mainData.volumeEnd=newVal+"方";
		 		}
		 	}else{	 		 
		 		vm.mainData.volumeEnd=newVal.replace("方","")+"方";
		 	}
	 },  
    },
  
})