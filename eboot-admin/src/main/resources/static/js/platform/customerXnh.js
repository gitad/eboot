  
 var vm = new Vue({
	el: '#app',
	
	data:{
	    current:1,//当前页
		size:10,//页面条数
		count:0,//总共条数
		tableData:[],
		form:{
			provinceName:'',
			cityName:'',
			areaName:'',
			custName:'',
			custType:'',
			ifNormal:'',
		},
		dropValue:'',
		zjtableData:[],
		dialogVisible:false,//区域选择
		zjdialogVisible:false,//客户转交
		provinceData:[],//省
	    cityData:[],//市
	    areaData:[],//区
	      radio: '0'
    },
 	watch: {
	 radio:function(val){
	 	debugger
	 	
	 	
	 }
    }, 
	methods: {	
        //初始执行
        init() {       
            this.query();      
        },
        queryDup:function(){
        	debugger
        },
          formatDate:function(row, column) {
        	debugger
		    // 获取单元格数据
		    let data = row[column.property]
		    if(data!=null){
		    	 let dt = new Date(data)//+ ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds()
		   		 return dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() 
		    }else{
		    	return "";
		    }
		   
		},
        query:function(){ 
        	
        	 var   url="/customerXnh/query-page?current="+this.current+"&size="+this.size  	
        	 var forData=this.form;
        	 Object.keys(forData).some(function(key){
        	 	   if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
        	     
        	      
        	 })
        	 
        	$.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: url,      	 	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){   
        			    	        				 
				     vm.tableData=data.data; 				     
				     vm.count=data.count;	 
        		}
        	}); 
        },
        addRessOpen:function(type,val,data,name){
        	
        		//type:0 省 1市 2区
        	 vm.dialogVisible = true
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	 	 
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.form.provinceName=name;
        	 	vm.form.cityName="";
        	 	vm.form.areaName="";
        	 } 
        	  if(type==2){
        	  	vm.areaData=obj.data;
        	 	vm.form.cityName=name;
        	    vm.form.areaName="";
        	 } 
        	  if(type==3){		        	  
        	 	vm.form.areaName=name;
        	 	 
        	 }    	
         },
         	 
           //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
		clear:function(){
		 
		 	vm.form.provinceName='';
			vm.form.cityName='';
			vm.form.areaName='';
			vm.form.custName='';
			vm.form.custType='';
			vm.form.ifNormal='';
			 
		},
		openIm:function(id){
			
			 	  $.ajax({
					type: "GET",
					url: '/customerXnh/by/im/id?id='+id+"&idx=1",
					dataType: 'json',
					async:false,
					success: function(data) {
						 window.parent.wx_open()	 			
					}
	    	 	  });
        	
		},
		selectChage:function(val){
			debugger
			if(val!=""){
				 
					if(val==0){//转为虚拟号
					 var selectData=vm.$refs.tableData.selection;
					 if(selectData.length==0){
						vm.$message.error('请先选择数据');
						return
					}
					vm.customerZh();
				}
				 
				 
			}
		},
		customerZh:function(){//虚拟号转换
			 $.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/customerXnh/customerZh',   
				  data:JSON.stringify(vm.$refs.tableData.selection) ,
				  dataType : 'json',
				  success : function(data) {
				  	  debugger
					  vm.$message.error('转换成功');		
					  vm.query();	   
				  }		  
		    	}); 
		},
		redisCsh:function(){
		 $.ajax({
  	  		    type: "POST",
  	            url: '/customerXnh/cust-redis-init',
  	           contentType : "application/json;charset=utf-8",
  	            dataType:"json",
  	            cache:false, // 设置为 false 将不缓存此页面	           
  	            success: function(obj) {
  	     			vm.$message.error('初始化成功');           	
  	            },
  	            error: function(data) {
  	                // 请求失败函数
  	                console.log(data);
  	            }
  	        });  
		},
		phztb:function(){
			$.ajax({
		  		    type: "POST",
		            url: '/customerXnh/cust-distribution',
		            contentType : "application/json;charset=utf-8",
		            dataType:"json",
		            cache:false, // 设置为 false 将不缓存此页面	           
		            success: function(obj) {		            	
		            	 if(obj.data==true){
		            		 vm.$message.error('分配成功');		            	 
		     		    	return;
		            	 }
		            	if(obj.data==false){
		            		vm.$message.error('分配失败');
		     		    	return;
		            	}	            	
		            },
		            error: function(data) {
		                // 请求失败函数
		                console.log(data);
		            }
		        }); 
			
		},
		messMass:function(val){//消息群发
			debugger
			if(vm.$refs.tableData.selection.length>500){
				 $.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/customerXnh/messMass?content='+val, // 发送好友文本消息 	
				  data:JSON.stringify(vm.$refs.tableData.selection) ,
				  dataType : 'json',
				  success : function(data) {
				  	  debugger
					  vm.$message.error('群发成功');			   
				  }		  
		    	}); 
			}else{
				$.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/customerXnh/messMass', // 发送好友文本消息 	
				  data:JSON.stringify(vm.$refs.tableData.selection) ,
				  dataType : 'json',
				  success : function(data) {
					  
					  window.parent.wx_open()
				  }
			  
			    }); 
			}
			 
		},
		messMassAll:function(val){
			debugger
			  $.ajax({
				  type : "POST",
				  contentType : "application/json;charset=utf-8",
				  url :'/customerXnh/messMassAll?content='+val, // 发送好友文本消息 	
				  dataType : 'json',
				  success : function(data) {
					   vm.$message.error('群发成功');
					   
				  }
		  
		    }); 
		},
		customerZj:function(){
			debugger
			$.ajax({
			  type : "POST",
			  url : '/customerXnh/query-all-user', //获取待转交好友 		 
			  dataType : 'json',
			  success : function(data) {
				 debugger
				  vm.zjtableData=data;
			  }
    		}); 
		},
		customerZjClick:function(){//用户转交确认
			debugger
			vm.zjdialogVisible=false;
			 var selectData=vm.$refs.zjtableData.selection;
			 if(selectData.length>1){
			 	 vm.$message.error('请选择单条数据');
			 	 return
			 }
			  if(selectData.length==0){
			 	 vm.$message.error('请选择数据');
			 	 return
			 }
			$.ajax({
  			  type : "POST",
  			  contentType : "application/json;charset=utf-8",
  			  url : '/customerXnh/user-distri?id='+selectData[0].id+"&name="+selectData[0].nickname, // 用户转交
  			  data:JSON.stringify(vm.$refs.tableData.selection) ,
  			  dataType : 'json',
  			  success : function(data) {
  				 debugger
 			       if(data==true){
 			    	 vm.$message.error('用户分配成功');
 			    	 vm.zjdialogVisible=false;
 			    	  vm.query();
 			    	  return
 			       }
 			      if(data==false){
 			    	 vm.$message.error('用户分配失败');
 			    	  vm.query();
			    	  return  
 			       }
 			     
  			  }
  		    }); 
		},
		
		 tel:function(){
			
			 var selectData=vm.$refs.tableData.selection;
				if(selectData.length==0){
					vm.$message.error('请先选择数据');
					return
				}
				$.ajax({
					  type : "POST",
					  contentType : "application/json;charset=utf-8",
					  url : '/customerXnh/telPhone', // 拨打电话
					  data:JSON.stringify(selectData) ,
					  dataType : 'json',
					  success : function(data) {	  
						  window.parent.telBox();	
					  }
				  
				    }); 
	},
  		handleSizeChange(val) {//分页
         	
         	vm.size=val;
       		vm.query();
        },
		 handleCurrentChange(val) {//分页
         	
         	vm.current=val;
      		 vm.query();
        },
	},
	
	mounted(){
       this.init();
   } , 
    
  
})