  
 var vm = new Vue({
	el: '#app',
	
	data:{
	  dialogVisible:false,//控制区域选择页面隐藏
	  dialogAddressVisible:false,
	  dialogVisible1:false,
	  dialogCarsUpVisible:false,//用车类型 
	  hydjdialogVisible:false,//货源等级
	  driveLen:0,//距离
	  dirveHours:0,//驾车时间
	  lngAndlat:[],
	   startForm:[],
	  endForm: [] ,
	  tableData: [],
	  provinceData:[],//省
	  cityData:[],//市
	  areaData:[],//区
	  packData:[], //包装形式
	  cargoData:[], //货物分类形式
	  selectData:{},//当前选择的form表格数据  选择省市区时用到
	  carUseTypeData:[],//用车车型
	  carlenData:[],//车长
	  carTypeData:[],//车型
	  cargoTypeData:[],//货源状态
	  selCarLen:[],
	  selCarType:[],
	  hydjList:[],//货源等级
	  hydjCheckList:[],
	  tag:'',//标签
	  tagChild:[],
	  otherInput:'',//其他类型input输入标签
	  otherShow:false,
	  radioGroup:[],//单选组
	  radioGroup1:[],//单选组
	  map: new BMapGL.Map('container'),
	  mainData:{//主表数据
	   	cargoId:'',  	useFlag:'1', 	cargoCatagory:'',
	 	cargoSubCatagory:'', 	weightStr:'', weightEnd:'',
	 	volumeStr:'', 	volumeEnd:'',  	pack:'',
	 	carLen:'', 	carUseType:'', 	carType:'',
	 	customerPrice:'', budgetPrice:'', 	matchType:'电话撮合',
	 	planTime:new Date(), cargoType:'待发', 	customerRemark:'', remark:'',tag:'',
	  },
	  
	  
    },
 	watch: {
 		lngAndlat:function(val,old){
 			debugger
 			if(val.length==2){
	 				 
				 var map = new BMapGL.Map("container");
				   var transit = new BMapGL.DrivingRoute( map,{
		                    renderOptions: {map: map},
		                    onSearchComplete: function(results){
		                    	debugger
		                        if (transit.getStatus() != BMAP_STATUS_SUCCESS){
		                            return ;
		                        }
		                        var plan = results.getPlan(0);
		                        vm.dirveHours = plan.getDuration(true);                //获取时间
							    vm.driveLen = plan.getDistance(true);             //获取距离
		                      },
		                });
				 var p1 = new BMapGL.Point(val[0].lng,val[0].lat);
		         var p2 = new BMapGL.Point(val[1].lng,val[1].lat);
    			 transit.search(p1, p2);
		         vm.lngAndlat=[];        
		 	 }
 			 
			
 		},
 		 
	 'mainData.weightStr':function(newVal,oldVal){
	 	 if(newVal.indexOf("吨")==-1){
	 		if(newVal!=""){
	 			vm.mainData.weightStr=newVal+"吨";
	 		}
	  	}else{ 		 
	 		vm.mainData.weightStr=newVal.replace("吨","")+"吨";
	 	}
	 }, 
	   'mainData.weightEnd':function(newVal,oldVal){
		 	 if(newVal.indexOf("吨")==-1){
		 		if(newVal!=""){
		 		 vm.mainData.weightEnd=newVal+"吨";
		 		}
		 	}else{	 		 
		 		vm.mainData.weightEnd=newVal.replace("吨","")+"吨";
		 	}
	 }, 
	   'mainData.volumeStr':function(newVal,oldVal){
		  	if(newVal.indexOf("方")==-1){
		 		if(newVal!=""){
		 		 vm.mainData.volumeStr=newVal+"方";
		 		}
		 	}else{	 	
		 		if(newVal!=""){	 
		 			vm.mainData.volumeStr=newVal.replace("方","")+"方";
		 		}
		 	}
	 }, 
	   'mainData.volumeEnd':function(newVal,oldVal){
		  	if(newVal.indexOf("方")==-1){
		 		 vm.mainData.volumeEnd=newVal+"方";
		 	}else{	 		 
		 		vm.mainData.volumeEnd=newVal.replace("方","")+"方";
		 	}
	 }, 
  }, 
	methods: {	
        //初始执行
        init() {
        	
        	this.cargoTypeData=this.getDropValue("09cbc95b9ec2432fada3abfb10f21f73").data;
        	this.startForm.push(this.addNewData(0));
        	this.endForm.push(this.addNewData(0));
          	this.query()        
        },
        rowDbclick:function(row, column, event){
        	
        	vm.mainData=row;     
         	vm.startForm=vm.queryForm(row.cargoId,0);//0起点
         	vm.endForm=vm.queryForm(row.cargoId,1);//1终点
         		var start=vm.startForm[0].cityCode+vm.startForm[0].areaCode;
        	 	var end=vm.endForm[vm.endForm.length-1].cityCode+vm.endForm[vm.endForm.length-1].areaCode
        	 	if(start!=""&&end!=""){
        	 		 vm.getLngAndLat(start);
        	         vm.getLngAndLat(end);
 			  	}
        },
        hydj:function(){//
        	debugger
        		/*if(vm.mainData.cargoId==""||vm.mainData.cargoId==null){
        		 this.$message({
			          message: '请先保存后在进行标签操作',
			          type: 'warning'
			        });
			        return;
        	}*/
          vm.hydjdialogVisible=true;
          vm.hydjList=vm.getDropValue("891b89f5cee542d996dd0b2264b78d83").data;
          
        },
        radioChange:function(domain){
        	debugger
        	vm.mainData.tag=domain.name;
        	 vm.hydjCheckList=vm.getDropValue(domain.dictionariesId).data;
        },
       /* tagChildChange:function(domain){
        	debugger
        
        	var tag="";
        	for (var i=0; i < domain.length; i++) {
			  if(i==0){
			  	tag=domain[i];
			  }else{
			  	tag=tag+","+domain[i]
			  }
			};
        	vm.mainData.tag=tag;
        	  	$.ajax({
				  type: "POST",
		          url: '/cCargoEntry/saveCCargoEntry',	      
		          data: JSON.stringify(vm.mainData),
	              contentType : "application/json;charset=utf-8",
	              ataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 
		         },
		         
		     });
        	
        	
        },*/
        queryForm(cargoId,type){
        	
        	var from=[];
        	$.ajax({
				  type: "POST",
		          url: '/cCargoEntry/queryForm',	      
		          data:{
	            	"cargoId":cargoId,
	            	 "type":type,
	              },	       
		          dataType:"json",
		          async:false,//异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 from=obj; 
		         },
		         
		     });
        	return from;
        },
        clearData:function(){
        	
        	 Object.keys(vm.mainData).some(function(key){
        	 	 vm.mainData[key]="";
        	 })
        	 vm.startForm=[];
        	 vm.endForm=[];
        	 vm.startForm.push(vm.addNewData(0));
        	 vm.endForm.push(vm.addNewData(0));
        },
        save:function(){
        	debugger
        	var mainData=JSON.stringify(vm.mainData);
        	var startForm=JSON.stringify(vm.startForm);
        	var endForm=JSON.stringify(vm.endForm);
        	for(var i=0;i<vm.startForm.length;i++){
        		if(vm.startForm[i].areaCode==""){
        			 this.$message({
			          message: '请先选择发货地点',
			          type: 'warning'
			        });
			        return;
        		}
        	}
        	for(var i=0;i<vm.endForm.length;i++){
        		if(vm.endForm[i].areaCode==""){
        			 this.$message({
			          message: '请先选择收货地点',
			          type: 'warning'
			        });
			        return;
        		}
        	}
	    	 $.ajax({
				  type: "POST",
		          url: '/cCargoEntry/saveAll',	
		          data:{
	            	"mainData":JSON.stringify(vm.mainData),
	            	"startForm":JSON.stringify(vm.startForm),
	            	"endForm":	JSON.stringify(vm.endForm),
	            
	            },	      
		       //   contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	  		              
		          success: function(obj) {	 
		        	 	
		        	   vm.$message.error('保存成功');		        	 
		        	 returnObj=obj	 
		        	 vm.query();  
		        	 
		        	 
		         },
		         
		     });
        },
        chcekBoxCarLen:function(newVal,old){
        	
        	if(vm.selCarLen.length>0){
    		    for(var i=0;i<vm.selCarLen.length;i++){
			    	if(i==0){
			    		vm.mainData.carLen=vm.selCarLen[i];
			    	}else{
			    		vm.mainData.carLen=vm.mainData.carLen+","+vm.selCarLen[i]
			    	}
    			}
        	}else{
        		vm.mainData.carLen="";
        	}
       
        },
         chcekBoxCarType:function(newVal,old){
        	
        
        	if(vm.selCarType.length>0){
    		    for(var i=0;i<vm.selCarType.length;i++){
			    	if(i==0){
			    		vm.mainData.carType=vm.selCarType[i];
			    	}else{
			    		vm.mainData.carType=vm.mainData.carType+","+vm.selCarType[i]
			    	}
    			}
        	}else{
        		vm.mainData.carType="";
        	}
        },
        redioChange:function(val,type){
        	vm.radioGroup1=[];	 
        	vm.otherShow=false;
        	if(type==0)	{
    		  if(val=="零担"){
    	        vm.mainData.carLen="";
    	        vm.selCarLen=[];
    		  } 
    		   vm.mainData.carUseType=val;
        	}	 
 			if(type==1)	{
        	  vm.mainData.pack=val;    
        	}  		       	
        },
        redioOtherChange:function(){
        	debugger
        	vm.otherShow=true;
        	vm.radioGroup=[];	       	
        },
	    saveInput:function(){
	      	debugger
	      	
	      	var newData={};//vm.packData[0];
	      	newData.name=vm.otherInput;
	        newData.parentId=vm.packData[0].parentId;
	      	vm.packData.push(newData);
	      	 
	      		$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		 url: '/sysDictionaries/save',
	        	 	data: JSON.stringify(vm.packData),
	        		contentType : "application/json;charset=utf-8",
	        		
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			 		   
	        		}
	        	}); 
	    },
	   
        addressMess:function(val){
        	
        	 vm.dialogAddressVisible = true   
        	 vm.selectData=val;
        	   	
        },
        goodsUp:function(){//货物
        	
        	vm.dialogVisible1=true;
        	vm.packData=vm.getDropValue("92dba6252d3e4c04bb442d617296ee7e").data;
        	vm.cargoData=vm.getDropValue("0d68b4b985aa491db7e9c4eab70f800c").data;
        },
        CarsUp:function(){//用车类型
        	
        	vm.dialogCarsUpVisible=true;
        	 vm.carUseTypeData=vm.getDropValue("7bca4d3f2b8d4dc9a848dd3ea144a050").data;//用车类型
	 		 vm.carlenData=vm.getDropValue("4f8fb1d0089f47bb8912f4d3cfaff7fe").data;//车长
	 	 	 vm.carTypeData=vm.getDropValue("b08212412ac54616a6b6d1f947dea2be").data;//车型
	  
	  
	  
        },
         handleClose:function(done) {
         	
	        this.$confirm('确认关闭？') .then(_ => { done();  })  .catch(_ => {});
     	 },
        addRessOpen:function(type,val,data,name){
        	
        		//type:0 省 1市 2区
        	 vm.dialogVisible = true
        	 var obj= vm.getDropValue(val)
        	  if(type==0){
        	 	vm.provinceData=obj.data;
        	 	vm.selectData=data;
		      } 
        	  if(type==1){
        	 	vm.cityData=obj.data;
        	 	vm.selectData.provinceCode=name;
        	 	vm.selectData.cityCode="";
        	 	vm.selectData.areaCode="";
        	 } 
        	  if(type==2){
        	 	vm.areaData=obj.data;
        	 	vm.selectData.cityCode=name;
        	   vm.selectData.areaCode="";
        	 } 
        	   if(type==3){	
        	 debugger       	  
        	 	vm.selectData.areaCode=name;
        	 	var start=vm.startForm[0].cityCode+vm.startForm[0].areaCode;
        	 	var end=vm.endForm[vm.endForm.length-1].cityCode+vm.endForm[vm.endForm.length-1].areaCode
        	 	if(start!=""&&end!=""){
        	 		 vm.getLngAndLat(start);
        	         vm.getLngAndLat(end);
 			  	}
        	  
 			  //vm.lngAndlat.push(address);
        	 }    	
         },
         getLngAndLat:function(add){//获取经纬度
         	debugger
         	 if(add){
         	 	 var myGeo = new BMapGL.Geocoder();
         	   		myGeo.getPoint(add, function(point){
           	
	                if (point) {
	                    var   address = new BMapGL.Point(point.lng, point.lat);
	                    vm.lngAndlat.push(address);
	                }
            });
         	 }
         	 
            
         },
          addNewData:function(val){
          	var  newData={
		  	  provinceCode:'',
			  cityCode:'',
			  areaCode:'',
			  address:'',
			  contacts:'',
			  useFlag:'1',				 
			  transportType:val,//代表起点
	  		}
         	return newData;
         },
         //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
        addStart:function(){ 
        	 vm.startForm.push(vm.addNewData(1))
         }, 
       
          addEnd:function(){
          	vm.endForm.push(vm.addNewData(1))    	 
         } ,
        
        
         delEnd:function(val,index){
         	
         	vm.endForm.splice(index,index)
         },
         delStart:function(val,index){
         	
         	vm.startForm.splice(index,index)
         },
        query:function(){ 
        	
        	 $.ajax({
			 	  type: "POST",
			       url: '/cCargoEntry/query',
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {		
			       		     	  
		         	   vm.tableData=obj.data;  	      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });
        	
        },     	  	 
        
        
		
	},
	
	mounted(){
       this.init();
   } , 
    
  
})