
var httpurl = "";

var vm = new Vue({
	el: '#app',
	
	data:{
		current:1,//当前页
		treeData: [],
        defaultProps: {
          children: 'children',
          label: 'label'
        },
        filterText:'',	
	 	 tableData: [],
	 	treeNode:'',
		loading:false	//加载状态
    },
 	computed:{
     
      
    },
     watch: {
      filterText(val) {
      	 
        this.$refs.tree.filter(val);
      }
    },
	methods: {
		
        //初始执行
        init() {
          this.query()
        },
        query:function(){
        	  	 
        	 $.ajax({
			 	  type: "POST",
			       url: '/sysDictionaries/queryByparentId',
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {
				     	 debugger
			         	   vm.treeData=obj;  	      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });
        },
         filterNode(value, data) {
         	debugger
	        if (!value) return true;
	        return data.label.indexOf(value) !== -1;
      	},
         handleNodeClick(treeNode) {
         	debugger
         	vm.treeNode=treeNode;
         	  $.ajax({
		 	 	  type: "POST",
		 	      url:'/sysDictionaries/query',
		 	       data: {
		 	     	  "parentId":treeNode.id,
		 	       },
		 	       dataType:"json",
		 	       cache:false, // 设置为 false 将不缓存此页面	           
		 	       success: function(tableObj) {
					     debugger
					     vm.tableData =	tableObj.data;   
		 	       }
	 	       }) 
         },
         save(){
         		$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sysDictionaries/save',
	        	 	data: JSON.stringify(vm.tableData),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			vm.childFrom=data;    
	        			 vm.$message('保存成功');				   
	        		}
	        	}); 
         	
         },
          del(index,row) {
      	  	  $.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/sysDictionaries/del',
	        	 	data: JSON.stringify(row),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			  vm.$message('删除成功');
	        			  vm.handleNodeClick(vm.treeNode);	
	        			  vm.query();		   
	        		}
        	}); 
          },
         add() {
         	debugger
         	  if(vm.treeNode==''){
         	  	 vm.$message('请先选择左侧树后再添加');
         	  	  return; 
         	  }
         	  $.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		url: '/sysDictionaries/sysAdd',
	        	 	success: function(data){   
	        			debugger
	        			 data.parentId=vm.treeNode.id;
	        			 vm.tableData.splice(0,0,data);       			   
	        		}
	        	}); 
         	  
			 
		 	    
         },
      handleEdit(index,row){
      	debugger
      	 vm.tableData[index]=false;
      }
		
	},
	
	mounted(){
        this.init();
   } , 
    
  
})