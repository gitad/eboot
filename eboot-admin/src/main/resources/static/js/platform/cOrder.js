  
 var vm = new Vue({
	el: '#app',
	
	data:{
	  current:1,//当前页
	  size:10,//页面条数
	  count:0,//总共条数
	  form:{},
	  dialogVisible:false,//申请退款
	  undialogVisible:false,//取消退款
 
	  detailShow:false,
	  sysLog:[],//日志
	  tableData:[] ,
	  payPathList:[],//支付路径
	  options: [{value: '定金',  label: '定金'  },
		  		   {value: '运费',    label: '运费' }, 
		  		   {value: '服务费',  label: '服务费'  } ],
	  numStates: [{  value: '待付款',  label: '待付款'    }, {   value: '已付款',  label: '已付款'  }, 
				{ value: '已到账', label: '已到账'    }, {    value: '已退款',    label: '已退款'   } ],
	  resion: [{  value: '约定司机到厂家后退还'  }, {   value: '货已送达退还' }, 
				{ value: '因厂家原因发货退还'  }, {    value: '后台管理可以增加或删除'  } ],
	 unresion: [{  value: '不想退了'  }, {   value: '已联系到货主' } ],			
	  activeNames: ['1','2','3','4','5','6','7','8'],
	  order:{},//订单
	  carInfo:{},//车辆
	  cargoInfo:{},//车寻货
	  cargoEntry:{},//货源录入
	  cargoEntryRelz:[],
	  cargoEntryRelx:[],
	  overOrder:false,//取消订单
    },
 	watch: {
	  
  }, 
	methods: {	
        //初始执行
        init() {       
           this.query();      
        },
        payPathChange:function(val){
        	debugger
        	for (var i=0; i < vm.payPathList.length; i++) {
			  if(vm.payPathList[i].wxId==val){
			  	vm.order.payPath=vm.payPathList[i].wxNickname;
			  		vm.order.payPathId=val;
			  	  return
			  }
			};
        	
        },
          handleClick(tab, event) {
         	
         	vm.mainIndex=tab.index;
         	vm.form.orderStates=tab.label;
         	if(tab.index==0){//全部订单查询全部
         		vm.form.orderStates="";
         	}
         	
         	vm.query();
       },
       leaveTime :function(value)
	    {
		    var date_ = new Date(value);
            var year = date_.getFullYear();
            var month = date_.getMonth()+1;
            var day = date_.getDate();
            if(month<10) month = "0"+month;
            if(day<10) day = "0"+day;

            var hours = date_.getHours();
            var mins = date_.getMinutes();
            var secs = date_.getSeconds();
            var msecs = date_.getMilliseconds();
            if(hours<10) hours = "0"+hours;
            if(mins<10) mins = "0"+mins;
            if(secs<10) secs = "0"+secs;
            if(msecs<10) secs = "0"+msecs;
            return year+"/"+month+"/"+day+" "+hours+":"+mins;
	},
		 query:function(){ 
         
           var   url="/cOrder/query?current="+this.current+"&size="+this.size  	
        	 var forData=this.form;
        	 Object.keys(forData).some(function(key){
        	 	  if(forData[key]!=""&&forData[key]!=null){
        	     	 var maps="&condition["+key+"]="+forData[key]
        	     	 url+=encodeURI(maps);
        	     }
         		 })
        	 
        	  $.ajax({
			 	  type: "POST",
			       url: url,			      
		           contentType : "application/json;charset=utf-8",
		           async:true,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       	
			       	    vm.count=data.count;			     	  
		         	    vm.tableData=data.data;    	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        },
        
        unReturnOrder:function(){//取消退款
        	
        	vm.order.orderStates="问题订单";
        	 vm.save();
        	 vm.$message('订单已取消');
        	 vm.undialogVisible=false;
        },
        //退款申请
        returnOrder:function(){
        	
        	 vm.order.orderStates="待退款";
        	if(vm.overOrder==true){//订单取消
        		vm.order.orderStates="已取消";
        	}
         	vm.order.returnNumStates="待退款";
        	 vm.order.returnTime=new Date();
        	 vm.save();
        	 vm.$message('订单退款申请成功');
        	 vm.dialogVisible=false; 
        },
        orderSave:function(val){
        	vm.order.orderStates=val;
        	vm.save();
        	vm.$message('操作成功');
        },
        saveBut:function(){
        	vm.save();
        	vm.$message('保存成功');
        	
        },
        del:function(){
        	//vm.order.useFlag=0;
        	vm.order.orderStates="已取消";
         vm.order.unReturnResion="订单未确认,直接取消";
        	vm.save();
            vm.$message('订单取消成功');
        },
        
          handleClose:function(done) {
         	
	        this.$confirm('确认关闭？') .then(_ => { done();  })  .catch(_ => {});
     	 },
        save:function(){
        	
        	 $.ajax({
	 		   type: "POST",
	           url: '/cOrder/save',
	           data: JSON.stringify(vm.order),
	           contentType : "application/json;charset=utf-8",
	           dataType:"json",
	           async:false,//取消异步请求
	           cache:false, // 设置为 false 将不缓存此页面 
	           success: function(obj) {
	           		vm.order =obj; 
	           },
	           error: function(data) {
	               // 请求失败函数
	               console.log(data);
	           }
	       });
        	
        },
	     detail:function(row){
	      	 
	      
	      	  $.ajax({
			 	  type: "POST",
			       url: '/cOrder/queryDetail' ,		
			       data:{
			       	"orderId":row.orderId
			       },	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		debugger
			       		if(data.msg!=null||data.msg!=undefined){
			       			 vm.$message(data.msg);
			       			 return;
			       		}
			       	  vm.detailShow=true;
			       	  vm.order=data.order//订单
			       	  vm.carInfo=data.carInfo;
					  vm.cargoInfo=data.cargoInfo//车寻货
					  vm.cargoEntry=data.cargoEntry//货源录入
					  vm.cargoEntryRelz=data.cargoEntryRelz	
					  vm.cargoEntryRelx=data.cargoEntryRelx	 
					   //sysLog
					   vm.querySysLog( vm.cargoEntry.cargoId,"c_cargo_entry","","");//货源录入
					   vm.querySysLog(vm.cargoInfo.cargoInfoId,"c_cargo_info","","");//车寻货
					   vm.querySysLog(vm.order.orderId,"c_order","","");//订单
					 	 
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
			  
	     },
	            //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
	      querySysLog:function(logBusinessId,logBusinessType,logStaties,customerId){ 
    		  
    		 
        	  $.ajax({
			 	   type: "POST",
			       url: '/sysEbootLog/query',
			       data:{
			       	  'logBusinessId':logBusinessId,
			   		  'logBusinessType':logBusinessType,
			   		  'logStaties':logStaties,
			   		  'customerId':customerId
			       },			      
		         //  contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		 debugger
		       		  if(data.length>0){
		       		  	 for (var i=0; i < data.length; i++) {
						    vm.sysLog.push(data[i]);
						 };
		       		  }
			       		 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
			  	
        },
       getAllWxLogin:function(){
       	
       			$.ajax({
			 	  type: "POST",
			       url: '/cOrder/getAllWxLogin' ,		
			       	
			       dataType:"json",      
		          // contentType : "application/json;charset=utf-8",
		           async:false,//异步请求  query得时候一定要开着
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(data) {	
			       		debugger
			       		
			       		for (var i=0; i < data.length; i++) {
							 data[i].wxNickname=data[i].wxNickname.split('-')[0]
						   };
			       		 
			       	 vm.payPathList=data;
					},
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  }); 
       },
        handleSizeChange(val) {
         	
         	vm.size=val;
       		vm.query();
        },
		 handleCurrentChange(val) {
         	
         	vm.current=val;
      		 vm.query();
        },
	},
	
	mounted(){
       this.init();
   } , 
    
  
})