
var httpurl = "";

var vm = new Vue({
	el: '#app',
	
	data:{
		  detailfrom:{
		  		companyId:'',
			  	commonGoods:'',
			   companyNameOther
			  	:'',vaildWay
			  	:'',productsOrServices
			  	:'',remark
			  	:'',companyName
			  	:'',companyNameOther
			  	:'',companyStatus
			  	:'',companyLegal
			  	:'',createTime
			  	:'',province
			  	:'',city
			  	:'',regCapital
			  	:'',otherPhone
			  	:'',email
			  	:'',unifySocilCode
			  	:'',taxNo
			  	:'',regNo
			  	:'',insuredNum
			  	:'',unitType
			  	:'',Industry
			  	:'',website
			  	:'',address
			  	:'',businessScope
			  	:'',
		  },
		  childFrom: [
			 
		  ],
	        
		  provinceData:[],//省
	  cityData:[],//市
		  companyStatus:[
		  	{
		  	  value: '在业',
		      label: '在业'
		     }, 
		     {
		  	  value: '倒闭',
		      label: '倒闭'
		     } 
		   ],
		   unitType:[
		   	{
		  	  value: '有限责任公司',
		      label: '有限责任公司'
		     }, 
		     	{
		  	  value: '有限责任公司(自然人投资或控股)',
		      label: '有限责任公司(自然人投资或控股)'
		     }, 
		     	{
		  	  value: '有限责任公司(自然人独资)',
		      label: '有限责任公司(自然人独资)'
		     }, 
		     	{
		  	  value: '有限责任公司(非自然人投资或控股的法人独资)',
		      label: '有限责任公司(非自然人投资或控股的法人独资)'
		     }, 
		     	{
		  	  value: '有限责任公司(外国法人独资)',
		      label: '有限责任公司(外国法人独资)'
		     }, 
		     	{
		  	  value: '有限责任公司(台港澳与境内合资)',
		      label: '有限责任公司(台港澳与境内合资)'
		     }, 
		     	{
		  	  value: '股份有限公司(中外合资、未上市)',
		      label: '股份有限公司(中外合资、未上市)'
		     },
		     	{
		  	  value: '其他有限责任公司',
		      label: '其他有限责任公司'
		     },
		     	{
		  	  value: '其他股份有限公司(非上市)',
		      label: '其他股份有限公司(非上市)'
		     }
		   ],
		   Industry:[
		  	{
		  	  value: '汽车制造业',
		      label: '汽车制造业'
		     }, 
		     {
		  	  value: '通用设备制造业',
		      label: '通用设备制造业'
		     } ,
		     	{
		  	  value: '专用设备制造业',
		      label: '专用设备制造业'
		     },
		     	{
		  	  value: '计算机、通信和其他电子设备制造业',
		      label: '计算机、通信和其他电子设备制造业'
		     },
		     {
		  	  value: '铁路、船舶、航空航天和其他运输设备制造业',
		      label: '铁路、船舶、航空航天和其他运输设备制造业'
		     },
		   ],
		   
         options1: [
        	  {
		          value: '符合想订',
		          label: '符合想订'
		        } ,
		        {
		          value: '车型不符',
		          label: '车型不符'
		        } ,
          		{
		          value: '运费可谈',
		          label: '运费可谈'
		        }, {
		          value: '定了不准',
		          label: '定了不准'
		        },  {
		          value: '同行试探',
		          label: '同行试探'
		        }, {
		          value: '未谈',
		          label: '未谈'
		        }
        	]
        	
	 
    },
 computed:{
     
      
    },
	methods: {
		
        //初始执行
        init() {
          this.query()
          this.provinceData=this.getDropValue(1);
        },
        cityChange:function(val){
        	debugger
        	 vm.cityData=this.getDropValue(val);
        },
         //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj.data	 
		         },
		         
		     });	
		      return  returnObj;
         },
         
        save:function(detailfrom){
  				$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/cCompanyDetail/save',
	        	 	data: JSON.stringify(vm.detailfrom),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			vm.detailfrom=data;
	        			 vm.$message('保存成功');	 
	        			 vm.relSave();       			   
	        		}
	        	}); 
	          
        },
      	relSave:function(){
      		debugger
      			$.ajax({
	        		xhrFields: {
	                    withCredentials: true
	                },
	        		type: "POST",
	        		  url: '/cCompanyDetail/relSave',
	        	 	data: JSON.stringify(vm.childFrom),
	        		contentType : "application/json;charset=utf-8",
	        		dataType:"json",
	        		success: function(data){   
	        			debugger
	        			vm.childFrom=data;    			   
	        		}
	        	}); 
      		
      		
      	},
        query:function(){
        	  	 
        	$.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: '/cCompanyDetail/query',	
        		contentType : "application/json;charset=utf-8",
        		dataType:"json",
        		success: function(data){   
        			    	       				 
				     vm.detailfrom=data; 
				     vm.relQuery();
				       
        		}
        	}); 
        },
     
        relQuery:function(){
        	  	 debugger       
        	$.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: '/cCompanyDetail/relQuery?companyId='+vm.detailfrom.companyId,	
        		//contentType : "application/json;charset=utf-8",
        		//dataType:"json",
        		success: function(data){   
        		     vm.childFrom=data
        		}
        	}); 
        }, 
        add:function(){
        	debugger
         
        	 $.ajax({
        		xhrFields: {
                    withCredentials: true
                },
        		type: "POST",
        		url: '/cCompanyDetail/relAdd?companyId='+vm.detailfrom.companyId,
        		 
        		success: function(data){   
        		    vm.childFrom.push(data);
        		}
        	});
        	 
        }, 
        del(row){
        	debugger
        	 
        	  
        	  $.ajax({
					type: "POST",
					url: '/cCompanyDetail/relDel',
					data:JSON.stringify(row),
					contentType : "application/json;charset=utf-8",			
					dataType: 'json',
					async:false,
					success: function(data) {
						 debugger	
						 if(data==true){
						 	vm.$message('删除成功');	  
						 	for (var i=0;i< vm.childFrom.length;i++)
							{	
								if(vm.childFrom[i].companyRelId==row.companyRelId){
									 vm.childFrom.splice(i,1)
								}
							}
						 }else{
						 	vm.$message('删除失败');	    
						 }	
					}
	    	 	  });  
        },
        
		
	},
	
	mounted(){
        this.init();
   } , 
    
  
})