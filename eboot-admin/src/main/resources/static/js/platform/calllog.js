  
 var vm = new Vue({
	el: '#app',
	
	data:{
	 form: {},
     tableData:[],
	  callTypeData:[],
    },
 	watch: {
	  
  }, 
	methods: {	
        //初始执行
        init() {       
           this.query();     
            this.callTypeData=this.getDropValue("a4d74551487f4b988398e90a31b33736").data;//通话类别
	     
        },
        query:function(){ 
        	
        	 $.ajax({
			 	  type: "POST",
			       url: '/cCallLog/query',
			       dataType:"json",
			       cache:false, // 设置为 false 将不缓存此页面	           
			       success: function(obj) {			     	  
		         	   vm.tableData=obj.data;  
		         	   vm.add();	      	 
			       },
			       error: function(data) {
			           // 请求失败函数
			           console.log(data);
			       }
			  });      	
        }, 
        clear:function(){//清空
        	
        	  vm.add();	
        },  
          //获取下拉数据
         getDropValue:function(parentId){
         	var returnObj;
          	 $.ajax({
				  type: "POST",
		          url: '/sysDictionaries/query?parentId='+parentId,		      
		          contentType : "application/json;charset=utf-8",
		          dataType:"json",
		          async:false,//取消异步请求
		          cache:false, // 设置为 false 将不缓存此页面	           
		          success: function(obj) {	 
		        	 			        	 
		        	 returnObj=obj	 
		         },
		         
		     });	
		      return  returnObj;
         },
         add:function(){ 
        	
        	$.ajax({
	  		    type: "POST",
	            url: '/cCallLog/addLog',	           
	           contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) {
	            	
	            		 vm.form=data;
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });      	
        }, 
        rowDbclick:function(row, column, event){
        	
        	row.validPhone  =vm.form.validPhone
        	row.otherPhone  =vm.form.otherPhone
        	row.addressInfo =vm.form.addressInfo
        	vm.form=row;     
         	 
        }, 
        formatDate:function(row, column) {
        	debugger
		    // 获取单元格数据
		    let data = row[column.property]
		    if(data!=null){
		    	 let dt = new Date(data)//+ ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds()
		   		 return dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate() 
		    }else{
		    	return "";
		    }
		   
		},	  	 
         save:function(){ 
        	
        	$.ajax({
	  		    type: "POST",
	            url: '/cCallLog/save',
	            data: JSON.stringify(vm.form),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(data) {
	            	 vm.$message.error('保存成功');
	            	 	vm.query();
	            	 	  
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        });      	
        },
        del:function(){
        	debugger
        	if(vm.form.callId==null){
        		 vm.$message.error('请先双击选择数据');
        		return
        	}
        	  $.ajax({
	  		    type: "POST",
	            url: '/cCallLog/del',
	            data: JSON.stringify(vm.form),
	            contentType : "application/json;charset=utf-8",
	            dataType:"json",
	            cache:false, // 设置为 false 将不缓存此页面	           
	            success: function(obj) {
	            	 vm.$message.error('删除成功');
	            	vm.query();
	             
	            	
	            },
	            error: function(data) {
	                // 请求失败函数
	                console.log(data);
	            }
	        }); 
        	
        }, 
        
		
	},
	
	mounted(){
       this.init();
   } , 
    
  
})