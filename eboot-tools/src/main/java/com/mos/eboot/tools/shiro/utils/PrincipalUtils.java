package com.mos.eboot.tools.shiro.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.mos.eboot.tools.shiro.entity.IUser;

public class PrincipalUtils {

	public static IUser getCurrentUser() {
		Object principal = SecurityUtils.getSubject().getPrincipal();
		if (principal == null) {
			return null;
		}
		return (IUser) principal;
	}

	public static Session getSession() {
		return SecurityUtils.getSubject().getSession();
	}

}
