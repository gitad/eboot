package com.mos.eboot.tools.util;

public interface Constants {

    public static final String POSITIVE="1";

    public static final String NEGATIVE = "0";
    
    public static final String REDIS_PREFIX_CONVERSATION="conversation_";//用来存储登陆用户的聊天人员信息
    
    public static final String REDIS_CHAT_HISTORY="chat_history";//存储聊天消息记录
    
    public static final String REDIS_CHAT_PEO_MANAGER="chat_peo_manager";//存储业务员管理的用户的微信号和用户主键
    
    public static final String REDIS_CUST_MANAGER_REL="cust_manager_rel";//存储业务员管理的微信号 
    public static final String REDIS_CUST_MANAGER_PHONE_LIST="cust_manager_phone_list";//存储拨打电话集合 一个业务员存一个
}
