//package org.fh.realm;
//
//import java.util.Collection;
//import java.util.HashSet;
//
//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.AuthenticationInfo;
//import org.apache.shiro.authc.AuthenticationToken;
//import org.apache.shiro.authc.UsernamePasswordToken;
//import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.SimpleAuthorizationInfo;
//import org.apache.shiro.realm.AuthorizingRealm;
//import org.apache.shiro.session.Session;
//import org.apache.shiro.subject.PrincipalCollection;
//import org.fh.entity.PageData;
//import org.fh.util.Const;
//
//import com.mos.eboot.platform.entity.SysUser;
//import com.mos.eboot.tools.shiro.utils.PrincipalUtils;
//
///**
// * 说明：Shiro身份认证 作者：FH Admin Q313596790 官网：www.fhadmin.org
// */
//public class MyShiroRealm extends AuthorizingRealm {
//
//	/**
//	 * 登录认证(只在system微服务端做登录认证，其它根据Redis判断认证状态)
//	 */
//	@Override
//	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
//			throws AuthenticationException {
//		UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken; // UsernamePasswordToken用于存放提交的登录信息
//		PageData pd = new PageData();
//		pd.put("USERNAME", token.getUsername());
//		try {
//		} catch (Exception e) {
//			return null;
//		}
//		return null;
//	}
//
//	@Override
//	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//		SysUser user = (SysUser) super.getAvailablePrincipal(principals);
//		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
//		Session session = PrincipalUtils.getSession();
//		Collection<String> shiroSet = new HashSet<String>();
//		shiroSet = (Collection<String>) session.getAttribute(user.getUsername() + Const.SHIROSET);
//		if (null != shiroSet) {
//			info.addStringPermissions(shiroSet);
//			return info;
//		} else {
//			return null;
//		}
//	}
//
//}
