//package org.fh.config;
//
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.shiro.authc.AuthenticationToken;
//import org.apache.shiro.subject.Subject;
//import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//public class MyAuthenticationFilter extends FormAuthenticationFilter {
//	Logger logger = LoggerFactory.getLogger(MyAuthenticationFilter.class);
//
//	/**
//	 ** 前置配置
//	 */
//	@Override
//	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
//		HttpServletResponse httpResponse = (HttpServletResponse) response;
//		HttpServletRequest httpRequest = (HttpServletRequest) request;
//		if (httpRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
//			setHeader(httpRequest, httpResponse);
//			return true;
//		}
//		return super.preHandle(request, response);
//	}
//
//	/**
//	 * 为response设置header，实现跨域
//	 */
//	private void setHeader(HttpServletRequest request, HttpServletResponse response) {
//		// 跨域的header设置
//		response.setHeader("Access-control-Allow-Origin", request.getHeader("Origin"));
//		response.setHeader("Access-Control-Allow-Methods", request.getMethod());
//		response.setHeader("Access-Control-Allow-Credentials", "true");
//		response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
//		// 防止乱码，适用于传输JSON数据
//		response.setHeader("Content-Type", "application/json;charset=UTF-8");
//		response.setStatus(HttpStatus.OK.value());
//	}
//
//	/**
//	 * 登录成功之后的操作
//	 *
//	 * @param token
//	 * @param subject
//	 * @param servletRequest
//	 * @param response
//	 * @return
//	 * @throws Exception
//	 */
//	@Override
//	protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest servletRequest,
//			ServletResponse response) throws Exception {
//
//		return true;
//	}
//}
