//package org.fh.config;
//
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.apache.shiro.web.filter.authc.AuthenticationFilter;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//public class IMFilter extends AuthenticationFilter {
//	private final Logger logger = LoggerFactory.getLogger(IMFilter.class);
//
//	@Override
//	protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) {
//		HttpServletRequest request = (HttpServletRequest) servletRequest;
//		HttpServletResponse response = (HttpServletResponse) servletResponse;
//		try {
//			// 这几句代码是关键
//			if ("OPTIONS".equals(request.getMethod())) {
//				response.setStatus(org.apache.http.HttpStatus.SC_NO_CONTENT);
//				;
//				logger.info("OPTIONS 放行");
//				return true;
//			}
//			
//		} catch (Exception e) {
//			logger.error("空指针异常", e);
//		}
//		logger.info("token有效放行");
//		return true;
//	}
//
//}
