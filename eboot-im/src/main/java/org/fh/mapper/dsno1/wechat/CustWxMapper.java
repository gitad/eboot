package org.fh.mapper.dsno1.wechat;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.fh.entity.wechat.CustWx;
 

 

public interface CustWxMapper {
	List<CustWx> getList(CustWx custWx) ;
	int checkIsExist(CustWx custWx);
	int insert(CustWx custWx);
	int update(CustWx custWx);	
	int updateCust(@Param("wxStatus") String  wxStatus,@Param("cId") String cId);
}
