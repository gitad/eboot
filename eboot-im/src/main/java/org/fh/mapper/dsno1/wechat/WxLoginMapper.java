package org.fh.mapper.dsno1.wechat;

import java.util.List;

import org.fh.entity.wechat.WxLogin;

public interface WxLoginMapper {

	WxLogin getByWxId(String wxId);
	String getOwnerByWxId(String wxId);
	List<WxLogin> findList(WxLogin login);
	int checkIsExist(WxLogin login);
	int insert(WxLogin login);
	int update(WxLogin login);
	WxLogin getByCid(String cid);
}
