package org.fh.mapper.dsno1.wechat;

 
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mos.eboot.platform.entity.SysEbootLog;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
public interface SysEbootLogMapper  {
	/**
	 * 查询所有日志信息
	 * @param roleId
	 * @return
	 */
	List<SysEbootLog> queryByContent(@Param("content")String content);
	int save(SysEbootLog SysEbootLog);
}