package org.fh.mapper.dsno1.wechat;

import java.util.List;

import org.fh.entity.wechat.WxFriends;

public interface WxFriendsMapper {
	List<WxFriends> getList(WxFriends friends) ;
	WxFriends get(WxFriends friends);
	int checkIsExist(WxFriends friends);
	int insert(WxFriends friends);
	int update(WxFriends friends);
	int updateRemark(WxFriends friends);
}
