package org.fh.mapper.dsno1.wechat;

import org.fh.entity.wechat.WxGroupFriends;

public interface WxGroupFriendsMapper {
	
	WxGroupFriends get(WxGroupFriends groupFriends);
	int checkIsExist(WxGroupFriends groupFriends);
	int insert(WxGroupFriends groupFriends);
	int update(WxGroupFriends groupFriends);
}
