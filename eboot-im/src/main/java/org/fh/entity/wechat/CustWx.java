package org.fh.entity.wechat;

import java.io.Serializable;
import java.util.Date;

 
 
public class CustWx implements Serializable  {

    private static final long serialVersionUID = 1L;

	 
	private Integer id;
	 
	private String cId;
 
	private String wxId;
 
	private String wxNumber;
	 
	private String wxRemark;
	 
	private String wxNickname;
 
	private String sysWxId;
 
	private String wxType;
 
	private String wxStat;

	private String wxAvatar;
	private Date createTime;
	private String userId;
	public Date getCreateTime() {
		return createTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxStat() {
		return wxStat;
	}

	public void setWxStat(String wxStat) {
		this.wxStat = wxStat;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNumber() {
		return wxNumber;
	}

	public void setWxNumber(String wxNumber) {
		this.wxNumber = wxNumber;
	}

	public String getWxRemark() {
		return wxRemark;
	}

	public void setWxRemark(String wxRemark) {
		this.wxRemark = wxRemark;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getSysWxId() {
		return sysWxId;
	}

	public void setSysWxId(String sysWxId) {
		this.sysWxId = sysWxId;
	}

	public String getWxType() {
		return wxType;
	}

	public void setWxType(String wxType) {
		this.wxType = wxType;
	}

	 
}
