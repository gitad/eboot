package org.fh.entity.wechat;

import java.io.Serializable;

public class WxGroupFriends implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4897702162538488329L;
	/**
	 * 
	 */

	private Long id; // 主键
	private String wxId; // 微信ID
	private String wxAvatar; // 微信头像
	private String wxNickname; // 微信昵称
	private String wxRoom; // 所属微信群

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getWxRoom() {
		return wxRoom;
	}

	public void setWxRoom(String wxRoom) {
		this.wxRoom = wxRoom;
	}
}
