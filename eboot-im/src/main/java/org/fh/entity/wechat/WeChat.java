package org.fh.entity.wechat;

import java.io.Serializable;

public class WeChat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3962819377675583324L;
	private String type;
	private String wxId;
	private String content;
	private String clientId;
	private String note;
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
