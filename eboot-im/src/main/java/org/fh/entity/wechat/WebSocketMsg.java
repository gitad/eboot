package org.fh.entity.wechat;

import java.io.Serializable;

public class WebSocketMsg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4092862271156288031L;
	private String type; // 消息类型
	private Object data; // 消息内容

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
