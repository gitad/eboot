package org.fh.entity.wechat;

import java.io.Serializable;

public class WxLogin implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2795286267936798415L;

	private Long id; // 主键
	private String wxId; // 登录人微信ID
	private String wxNumber; // 登录人微信号
	private String wxAvatar; // 登录人微信头像
	private String wxNickname; // 登录人微信昵称
	private String clientId; // 微信客户端ID
	private String terminalId; // 终端ID
	private String onlineStat;// 在线状态：0离线，1在线

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWxId() {
		return wxId;
	}

	public void setWxId(String wxId) {
		this.wxId = wxId;
	}

	public String getWxNumber() {
		return wxNumber;
	}

	public void setWxNumber(String wxNumber) {
		this.wxNumber = wxNumber;
	}

	public String getWxAvatar() {
		return wxAvatar;
	}

	public void setWxAvatar(String wxAvatar) {
		this.wxAvatar = wxAvatar;
	}

	public String getWxNickname() {
		return wxNickname;
	}

	public void setWxNickname(String wxNickname) {
		this.wxNickname = wxNickname;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getOnlineStat() {
		return onlineStat;
	}

	public void setOnlineStat(String onlineStat) {
		this.onlineStat = onlineStat;
	}
}
