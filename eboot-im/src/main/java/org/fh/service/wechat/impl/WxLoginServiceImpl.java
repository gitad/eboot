package org.fh.service.wechat.impl;

import java.util.List;

import org.fh.entity.wechat.WxLogin;
import org.fh.mapper.dsno1.wechat.WxLoginMapper;
import org.fh.service.wechat.WxLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class WxLoginServiceImpl implements WxLoginService {

	@Autowired
	public WxLoginMapper loginMapper;


	public WxLogin getByWxId(String wxid) throws DataAccessException {
		return loginMapper.getByWxId(wxid);
	}

	public String getOwnerByWxId(String wxid) throws DataAccessException {
		return loginMapper.getOwnerByWxId(wxid);
	}

	public List<WxLogin> findList(WxLogin login) throws DataAccessException {
		return loginMapper.findList(login);
	}

	public int checkIsExist(WxLogin login) throws DataAccessException {
		return loginMapper.checkIsExist(login);
	}

	@Transactional(readOnly = false)
	public int save(WxLogin login) throws DataAccessException {
		return loginMapper.insert(login);
	}

	@Transactional(readOnly = false)
	public int edit(WxLogin login) throws DataAccessException {

		return loginMapper.update(login);
	}

	@Override
	public WxLogin getByCid(String cid) throws DataAccessException {
		// TODO Auto-generated method stub
		return loginMapper.getByCid(cid);
	}
}
