package org.fh.service.wechat.impl;

import org.fh.entity.wechat.WxGroupFriends;
import org.fh.mapper.dsno1.wechat.WxGroupFriendsMapper;
import org.fh.service.wechat.WxGroupFriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class WxGroupFriendsServiceImpl implements WxGroupFriendsService {
	
	@Autowired
	private WxGroupFriendsMapper groupFriendsMapper;
	
	public WxGroupFriends get(WxGroupFriends groupFriends) throws DataAccessException {
		return groupFriendsMapper.get(groupFriends);
	}
	
	public int checkIsExist(WxGroupFriends groupFriends) throws DataAccessException {
		return groupFriendsMapper.checkIsExist(groupFriends);
	}

	@Transactional(readOnly = false)
	public int save(WxGroupFriends groupFriends) throws DataAccessException {
		return groupFriendsMapper.insert(groupFriends);
	}
	
	@Transactional(readOnly = false)
	public int edit(WxGroupFriends groupFriends) throws DataAccessException {
		return groupFriendsMapper.update(groupFriends);
	}
}
