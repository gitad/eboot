package org.fh.service.wechat;

import java.util.List;

import org.fh.entity.wechat.CustWx;
import org.springframework.dao.DataAccessException;

public interface CustWxService {

	 
	List<CustWx> getList(CustWx custWx) throws DataAccessException;
	int update(CustWx custWx) throws DataAccessException;
	
	int updateCust(String  wxStatus,String cId) throws DataAccessException;
	int checkIsExist(CustWx custWx) throws DataAccessException;
	int save(CustWx custWx) throws DataAccessException;
	
}
