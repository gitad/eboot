package org.fh.service.wechat;

import java.util.List;

import org.fh.entity.wechat.WxLogin;
import org.springframework.dao.DataAccessException;

public interface WxLoginService {
	
	WxLogin getByWxId(String wxid) throws DataAccessException;
	WxLogin getByCid(String cid) throws DataAccessException;
	String getOwnerByWxId(String wxid) throws DataAccessException;
	List<WxLogin> findList(WxLogin login) throws DataAccessException;
	int checkIsExist(WxLogin login) throws DataAccessException;
	int save(WxLogin login) throws DataAccessException;
	int edit(WxLogin login) throws DataAccessException;
}
