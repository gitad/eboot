package org.fh.service.wechat.impl;

 
import org.fh.mapper.dsno1.wechat.CustWxMapper;
import org.fh.mapper.dsno1.wechat.SysEbootLogMapper;
import org.fh.service.wechat.ISysEbootLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.SysEbootLog;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
@Service
public class SysEbootLogServiceImpl   implements ISysEbootLogService {
	
	@Autowired
	private SysEbootLogMapper sysEbootLogMapper;

	@Override
	public int save(SysEbootLog SysEbootLog) throws DataAccessException {
	   return  sysEbootLogMapper.save(SysEbootLog);
	}
	
}
