package org.fh.service.wechat;

import org.fh.entity.wechat.WxGroupFriends;
import org.springframework.dao.DataAccessException;

public interface WxGroupFriendsService {

	WxGroupFriends get(WxGroupFriends groupFriends) throws DataAccessException;
	int checkIsExist(WxGroupFriends groupFriends) throws DataAccessException;
	int save(WxGroupFriends groupFriends) throws DataAccessException;
	int edit(WxGroupFriends groupFriends) throws DataAccessException;
}
