package org.fh.service.wechat;

import java.util.List;

import org.fh.entity.wechat.WxFriends;
import org.springframework.dao.DataAccessException;

public interface WxFriendsService {

	WxFriends get(WxFriends friends) throws DataAccessException;
	List<WxFriends> getList(WxFriends friends) throws DataAccessException;
	int checkIsExist(WxFriends friends) throws DataAccessException;
	int save(WxFriends friends) throws DataAccessException;
	int update(WxFriends friends) throws DataAccessException;
	int updateRemark(WxFriends param) throws DataAccessException;
}
