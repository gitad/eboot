package org.fh.service.wechat.impl;

import java.util.List;

import org.fh.entity.wechat.WxFriends;
import org.fh.mapper.dsno1.wechat.WxFriendsMapper;
import org.fh.service.wechat.WxFriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class WxFriendsServiceImpl implements WxFriendsService {

	@Autowired
	private WxFriendsMapper friendsMapper;
	
	/**
	 * 
	 * @param friends
	 * @return
	 * @throws DataAccessException
	 */
	public WxFriends get(WxFriends friends) throws DataAccessException {
		return friendsMapper.get(friends);
	}
	
	/**
	 * 
	 * @param friends
	 * @return
	 * @throws DataAccessException
	 */
	public List<WxFriends> getList(WxFriends friends) throws DataAccessException {
		return friendsMapper.getList(friends);
	}
	
	
	/**
	 * 检查微信好友是否已经入库
	 */
	public int checkIsExist(WxFriends friends) throws DataAccessException {
		return friendsMapper.checkIsExist(friends);
	}
	
	/**
	 * 保存微信好友
	 */
	@Transactional(readOnly = false)
	public int save(WxFriends friends) throws DataAccessException {
		return friendsMapper.insert(friends);
	}
	
	@Transactional(readOnly = false)
	public int updateRemark(WxFriends info) throws DataAccessException {
		return friendsMapper.updateRemark(info);
	}


	@Transactional(readOnly = false)
	public int update(WxFriends friends) throws DataAccessException {
		// TODO Auto-generated method stub
		return friendsMapper.update(friends);
	}
}
