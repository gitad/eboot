package org.fh.service.wechat;

import org.springframework.dao.DataAccessException;

import com.mos.eboot.platform.entity.SysEbootLog;

 

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jiangweijie
 * @since 2021-09-29
 */
public interface ISysEbootLogService  {
	int save(SysEbootLog SysEbootLog) throws DataAccessException;
}
 