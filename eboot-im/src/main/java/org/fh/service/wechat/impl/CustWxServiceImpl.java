package org.fh.service.wechat.impl;

import java.util.List;

import org.fh.entity.wechat.CustWx;
import org.fh.mapper.dsno1.wechat.CustWxMapper;
import org.fh.mapper.dsno1.wechat.SysEbootLogMapper;
import org.fh.service.wechat.CustWxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.mos.eboot.platform.entity.SysEbootLog;

 

@Service
@Transactional(readOnly = true)
public class CustWxServiceImpl   implements CustWxService {

	@Autowired
	private CustWxMapper custWxMapper;
	
 
	/**
	 * 
	 * @param friends
	 * @return
	 * @throws DataAccessException
	 */
	public List<CustWx> getList(CustWx custWx) throws DataAccessException {
		return custWxMapper.getList(custWx);
	}
	
	
	/**
	 * 检查微信好友是否已经入库
	 */
	public int checkIsExist(CustWx custWx) throws DataAccessException {
		return custWxMapper.checkIsExist(custWx);
	}
	
	/**
	 * 保存微信好友
	 */
	@Transactional(readOnly = false)
	public int save(CustWx custWx) throws DataAccessException {
		return custWxMapper.insert(custWx);
	}
	
 

	@Transactional(readOnly = false)
	public int update(CustWx custWx) throws DataAccessException {
		// TODO Auto-generated method stub
		return custWxMapper.update(custWx);
	}
	
	@Transactional(readOnly = false)
	public int updateCust(String  wxStatus,String cId) throws DataAccessException {
		// TODO Auto-generated method stub
		return custWxMapper.updateCust(wxStatus, cId);
	}
}
