package org.fh.service.api;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.baomidou.mybatisplus.plugins.Page;
import com.mos.eboot.platform.entity.SysUserWxInfo;
import com.mos.eboot.tools.result.ResultModel;

/**
 * @author 小尘哥
 * @Time 2018/5/6 20:57
 */
@FeignClient("boot-service")
public interface ISysUserWxInfoService {

	/**
	 * 根据id查询
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "user/wx/get-by-id", method = RequestMethod.GET)
	ResultModel<SysUserWxInfo> getById(@RequestParam("id") String id);

	@RequestMapping(value = "user/wx/get-by-userId", method = RequestMethod.GET)
	ResultModel<List<SysUserWxInfo>> getByUserId(@RequestParam("userId") String userId);

	/**
	 * 根据id删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "user/wx/del-by-id", method = RequestMethod.POST)
	ResultModel<String> delById(@RequestParam("id") String id);

	/**
	 * 分页查询
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "user/wx/query-page", method = RequestMethod.POST)
	ResultModel<Page<SysUserWxInfo>> queryPage(Page<SysUserWxInfo> page);

	/**
	 * 新增或修改
	 * 
	 * @param dict
	 * @return
	 */
	@RequestMapping(value = "user/wx/save-or-update", method = RequestMethod.POST)
	ResultModel<String> saveOrUpdate(SysUserWxInfo sysUserWxInfo);

	@PostMapping("user/wx/update-cid-by-tid")
	boolean updClientIdByTerminalId(@RequestParam("terminalId") String terminalId,
			@RequestParam("clientId") String clientId);
}
