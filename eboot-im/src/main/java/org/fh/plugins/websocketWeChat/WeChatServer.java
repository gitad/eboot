package org.fh.plugins.websocketWeChat;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.fh.entity.PageData;
import org.fh.entity.wechat.CustWx;
import org.fh.entity.wechat.RedisUtil;
import org.fh.entity.wechat.WeChat;
import org.fh.entity.wechat.WebSocketMsg;
import org.fh.entity.wechat.WxFriends;
import org.fh.entity.wechat.WxGroupFriends;
import org.fh.entity.wechat.WxLogin;
import org.fh.plugins.websocketInstantMsg.ChatServerPool;
import org.fh.service.fhim.HismsgService;
import org.fh.service.wechat.CustWxService;
import org.fh.service.wechat.ISysEbootLogService;
import org.fh.service.wechat.WxFriendsService;
import org.fh.service.wechat.WxGroupFriendsService;
import org.fh.service.wechat.WxLoginService;
import org.fh.util.Const;
import org.fh.util.DateUtil;
import org.fh.util.SpringUtil;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mos.eboot.platform.entity.HisWxMsg;
import com.mos.eboot.platform.entity.SysEbootLog;
import com.mos.eboot.tools.util.Constants;
import com.mos.eboot.tools.util.StringUtil;


public class WeChatServer extends WebSocketServer {
	
	private WxLoginService wxLoginService = (WxLoginService)SpringUtil.getBean("wxLoginServiceImpl");
	
	private WxFriendsService wxFriendsService = (WxFriendsService)SpringUtil.getBean("wxFriendsServiceImpl");
	
	private CustWxService custwxService = (CustWxService)SpringUtil.getBean("custWxServiceImpl");
	 private ISysEbootLogService sysEbootService = (ISysEbootLogService)SpringUtil.getBean("sysEbootLogServiceImpl");
	
	private WxGroupFriendsService wxGroupFriendsService = (WxGroupFriendsService)SpringUtil.getBean("wxGroupFriendsServiceImpl");
	 
	private RedisUtil redisUtil=SpringUtil.getBean(RedisUtil.class);
	/**
	 * 客户端发送消息到服务器时触发事件
	 */
	@Override
	public void onMessage(WebSocket conn, String message) {
		//System.out.println("方法onMessage:"+message);
		if (null != message) {
			JSONObject json = JSONObject.parseObject(message);
			String type = json.getString("type");
			String data = json.getString("data");
			if (type.equals("wx.join")) { 
				 JSONObject jsonData = JSONObject.parseObject(data);
				String terminalId = jsonData.getString("terminalId");
				WeChatServerPool.removeUser(conn);
				WeChatServerPool.addUser(terminalId, conn);
				WebSocketMsg msg = new WebSocketMsg();
				msg.setType("im.connstate");
				String jsonMsg = JSONObject.toJSONString(msg);
				WeChatServerPool.sendMessageToUser( conn, "{\"type\":\"im.connstate\",\"data\":{\"state\":\"ok\"}}"); 

			} else if (type.equals("wx.login")) { // 
				if (null != data) {
					try {
						JSONObject jsonData = JSONObject.parseObject(data);
						String wxId = jsonData.getString("wxid");
						String clientId = jsonData.getString("clientid");
						String terminalId = jsonData.getString("terminalid");
						try {
							WebSocket socket =WeChatServerPool.getWebSocketByUser(terminalId);
							if(null==socket) {//已建立了websocket
								WeChatServerPool.addUser(terminalId, conn);
						      }
							// 根据分组ID查询微信所属客户端
							WxLogin wxLogin = wxLoginService.getByWxId(wxId);
							if (null == wxLogin) {
								
								WeChat weChat = new WeChat();
								weChat.setWxId(wxId);
								weChat.setClientId(clientId);
								WebSocketMsg msg = new WebSocketMsg();
								msg.setType("im.sync");
								msg.setData(weChat);
								String jsonMsg = JSONObject.toJSONString(msg);
								WeChatServerPool.sendMessageToUser(socket, jsonMsg);
								
							}	
						} catch (Exception e) {
						   System.out.println("链接有问题");
						}
						
						WxLogin login = new WxLogin();
						login.setWxId(wxId);
						login.setWxNumber(jsonData.getString("account"));
						login.setWxAvatar(jsonData.getString("headimg"));
						login.setWxNickname(jsonData.getString("nickname"));
						login.setClientId(clientId);
						login.setTerminalId(terminalId);
						login.setOnlineStat(Const.IM_ONLINE_STATUS_1);
						int result = wxLoginService.checkIsExist(login);
						if (result == 0) {
							wxLoginService.save(login);
						} else {
							wxLoginService.edit(login);
						}
						
						
						
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			} else if (type.equals("wx.friends")) { // 保存微信好友
				try {
					if (null != data) {
						JSONObject jsonData = JSONObject.parseObject(data);
					  	CustWx custwx = new CustWx();
						custwx.setWxId(jsonData.getString("wxid"));
						custwx.setWxAvatar(jsonData.getString("headimg"));
						custwx.setWxNumber(jsonData.getString("account"));
						custwx.setWxNickname(jsonData.getString("nickname"));
						custwx.setWxRemark(jsonData.getString("memo"));				
						custwx.setSysWxId(jsonData.getString("curwxid"));
						custwx.setWxType(jsonData.getString("utype"));
						custwx.setWxStat("1");
						custwx.setCreateTime(new Date());
						int result = custwxService.checkIsExist(custwx);
						
						try {
							if (result == 0) {
								int i=custwxService.save(custwx);
						   } else {
								int ii=custwxService.update(custwx);
							 
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
						
					}
				} catch(Exception e) {
					e.printStackTrace();
				}	
			} else if (type.equals("wx.groupfriends")) {
				try {
					if (null != data) {
						JSONObject jsonData = JSONObject.parseObject(data);
						WxGroupFriends groupFriends = new WxGroupFriends();
						groupFriends.setWxId(jsonData.getString("wxid"));
						groupFriends.setWxAvatar(jsonData.getString("avatar"));
						groupFriends.setWxNickname(jsonData.getString("nickname"));
						groupFriends.setWxRoom(jsonData.getString("room"));
						int result = wxGroupFriendsService.checkIsExist(groupFriends);
						if (result == 0) {
							wxGroupFriendsService.save(groupFriends);
						} else {
							wxGroupFriendsService.edit(groupFriends);
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			} else if (type.equals("wx.msg")) {
				try {
					if (null != data) {					 
						JSONObject jsonData = JSONObject.parseObject(data);
						if(jsonData.getString("sender").equals(jsonData.getString("curwxid"))){
							 	return;
						}
						PageData pd = new PageData();
						pd.put("HISMSG_ID", jsonData.getString("msgid"));
						pd.put("USERNAME", jsonData.getString("sender")); // 发送者ID
					 
						if (jsonData.getString("sender").startsWith("gh_")) {// 屏蔽公众号
							return;
						}
						String content = jsonData.getString("content");
						CustWx cust1=new CustWx();
						cust1.setWxId(jsonData.getString("sender"));
						cust1.setSysWxId(jsonData.getString("curwxid"));
						List<CustWx>  list1 = custwxService.getList(cust1);
						HisWxMsg his=new HisWxMsg();
					 	his.setUSERNAME(jsonData.getString("sender"));
						his.setSYSFLAG("0");
					    his.setTOID(jsonData.getString("curwxid"));
						his.setCONTENT(content);
						his.setTYPE("friend");
						his.setCTIME(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())  );
						 if(list1.size()>0){
							 if(StringUtil.isBlank(list1.get(0).getWxRemark())){
								 his.setSENDNAME(list1.get(0).getWxNickname());  
							 }else{
								 his.setSENDNAME(list1.get(0).getWxRemark()); 
							 }
							
						 }
					
						
						pd.put("CTIME", DateUtil.getTime()); // 发送时间
						pd.put("CONTENT", content); // 发送消息内容
						pd.put("DREAD", "0"); // 是否读取：0->未读；1->已读
						pd.put("OWNER", "superadmin"); // 所属系统用户

					 	String peo;//无人接收消息时默认发给管理员
						String[] str=null;
					 
					
 						if (null == jsonData.getString("room")&&jsonData.getString("sender").indexOf("@") ==-1) { // 个人消息
							pd.put("TOID", jsonData.getString("curwxid")); // 目标(好友)
							pd.put("TYPE", "friend"); // 类型(好友)
							WxFriends friends = new WxFriends();
							friends.setfWxId(jsonData.getString("sender"));
							friends.setFromWxId(jsonData.getString("curwxid"));
							friends = wxFriendsService.get(friends);
							
							
							
							CustWx custwx = new CustWx();
							custwx.setWxId(jsonData.getString("sender"));
							custwx.setSysWxId(jsonData.getString("curwxid"));
							List<CustWx> culist=custwxService.getList(custwx);
							if (culist.size()==0) {
	 
								WxLogin login = wxLoginService.getByWxId(jsonData.getString("curwxid"));
	
								WeChat weChat = new WeChat();
								weChat.setWxId(jsonData.getString("sender"));
								weChat.setClientId(login.getClientId());
								WebSocketMsg msg = new WebSocketMsg();
								msg.setType("im.get.wx.friend");
								msg.setData(weChat);
								String jsonMsg = JSONObject.toJSONString(msg);														
								WeChatServerPool.sendMessageToUser(WeChatServerPool.getWebSocketByUser(login.getTerminalId()), jsonMsg);
	
							 
							} else {
	
								if (content.indexOf("开启了朋友验证，你还不是他（她）朋友") > 0) {
									 //SysEbootLog log=new SysEbootLog();
								 	//sysEbootService.insert(log);
									custwx.setWxStat("2");// 好友微信状态，1:正常，2被删除,3，拉黑
									custwxService.update(custwx);
									if(StringUtils.isNotBlank(culist.get(0).getcId())){
										CustWx cust=new CustWx();
										cust.setcId(culist.get(0).getcId());
										cust.setWxStat("1");									
										List<CustWx> list=custwxService.getList(cust);
										if(list.size()==0){
										    SysEbootLog log=new SysEbootLog();
											log.setContent(culist.get(0).toString());
											log.setLogBusinessId(culist.get(0).getSysWxId().toString());
											log.setLogBusinessType("t_cust_wx");
											log.setLogStaties("删除");
											log.setCreateTime(new Date());
											log.setCreatePeoId(culist.get(0).getcId());
											sysEbootService.save(log);
											custwxService.updateCust("被删除",culist.get(0).getcId());
										}else{
											 custwxService.updateCust("正常",culist.get(0).getcId());
										}
									}
									return;
									 
								}
								if (content.indexOf("但被对方拒收了") > 0) {
									
									custwx.setWxStat("3");// 好友微信状态，1:正常，2被删除,3，拉黑
									custwxService.update(custwx);
									if(StringUtils.isNotBlank(culist.get(0).getcId())){
										CustWx cust=new CustWx();
										cust.setcId(culist.get(0).getcId());
										cust.setWxStat("1");									
										List<CustWx> list=custwxService.getList(cust);
										if(list.size()==0){
											custwxService.updateCust("被拉黑",culist.get(0).getcId());
											SysEbootLog log=new SysEbootLog();
											log.setContent(culist.get(0).toString());
											log.setLogBusinessId(culist.get(0).getSysWxId().toString());
											log.setLogBusinessType("t_cust_wx");
											log.setLogStaties("拉黑");
											log.setCreateTime(new Date());
											log.setCreatePeoId(culist.get(0).getcId());
											sysEbootService.save(log);
										}else{
											 custwxService.updateCust("正常",culist.get(0).getcId());
										}
									}
								  return;
								}
	
								if (content.indexOf("<msg>\\n\\t<img") > 0) {
									content = "[暂不支持图片，请在客户端查看！！！]";
								}
							 
								 
								if (content.indexOf("<msg><voicemsg") > 0) {
									content = "[收到一条语音，请在客户端查看！！！]";
								}
								if (content.indexOf("![CDATA[微信转账]]") > 0) {
									content = "[收到转账，请在客户端查看!!!]";
								}
								if (content.indexOf("<voipinvitemsg><roomid>") > 0) {
									content = "[通话已结束!!!]";
								}
								pd.put("USERNAME", jsonData.getString("sender")); // 发送者ID
								pd.put("NAME", custwx.getWxNickname()); // 发送者姓名
								pd.put("PHOTO", custwx.getWxAvatar()); // 发送者头像
							}
							
							//获取业务员和聊天人员微信关系
							Map<Object, Object> map=redisUtil.hmget(Constants.REDIS_CHAT_PEO_MANAGER);   
							Object cuObj=map.get(jsonData.getString("sender")+"-"+jsonData.getString("curwxid"));
						
							if(cuObj!=null){
								str=JSONObject.parseObject(cuObj.toString(), String[].class);
							}
							if(str!=null){//现根据用户 人员关系查询数据
								peo=str[1]; 
							}else{//如果查不到分配的数据  去查默认分配的微信号  将消息发送到业务员所归属的微信号
								Map<Object, Object> relMap=redisUtil.hmget(Constants.REDIS_CUST_MANAGER_REL); 
								if(relMap.get(jsonData.getString("curwxid"))!=null){
									peo=relMap.get(jsonData.getString("sender")).toString();
								}else{
									peo="1";
								}
							}
							 WebSocket socket = ChatServerPool.getWebSocketByUser(peo);//发送消息到人员
							 
							if (socket != null) {
								pd.put("DREAD", "1"); // 是否读取：0->未读；1->已读						
							}
							ChatServerPool.sendMessageToUser(socket, JSON.toJSONString(pd));	
							//将聊天消息追加到redis中		
							Map<Object, Object> histmap=redisUtil.hmget(Constants.REDIS_CHAT_HISTORY);
							Object obj=null;
							if(str!=null){
								  obj= histmap.get(str[0]);
							}
							
							List<HisWxMsg> list=new ArrayList<HisWxMsg>();	
							if(obj!=null){
								list=JSONArray.parseArray(obj.toString(), HisWxMsg.class) ;
							}
						 
							list.add(his);	
							if(str!=null){
								histmap.put(str[0], JSON.toJSONString(list));	
							}
											 
							redisUtil.hmset(Constants.REDIS_CHAT_HISTORY,histmap,-1);
							
						
							
 						}
  						else { // 群消息
 							pd.put("TOID", jsonData.getString("room"));             // 目标(群)
 							pd.put("TYPE", "group");                                // 类型(群)
 							WxGroupFriends groupFriends = new WxGroupFriends();
 							groupFriends.setWxId(jsonData.getString("sender"));
 							groupFriends.setWxRoom(jsonData.getString("room"));
 							groupFriends = wxGroupFriendsService.get(groupFriends);
 							if (null != groupFriends) {
 								pd.put("NAME", groupFriends.getWxNickname());		    // 发送者姓名
 								pd.put("PHOTO", groupFriends.getWxAvatar());		    // 发送者头像
 							}
 						}
 						//String owner = wxLoginService.getOwnerByWxId(jsonData.getString("curwxid"));

					
						
						// 保存聊天记录
						HismsgService hismsgService = (HismsgService) SpringUtil.getBean("hismsgServiceImpl");
						hismsgService.save(pd);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}	
			} else if (type.equals("wx.remove")) {
				WeChatServerPool.removeUser(conn);
			}else if (type.equals("wx.account.exit")) {//微信账号退出..新增消息
				
				JSONObject jsonData = JSONObject.parseObject(data);
				String clientId = jsonData.getString("clientid");
				System.out.println("账号推出clientId="+clientId);
				
				// 根据分组ID查询微信所属客户端
				WxLogin wxLogin = wxLoginService.getByCid(clientId);
				if (null == wxLogin) {
					return; 
				}

				wxLogin.setOnlineStat(Const.IM_ONLINE_STATUS_0);
				wxLoginService.edit(wxLogin);
			}
		}
		
	}
	//根据微信id获取管理人员的信息
	public String  get_check_user_name(){
	//	List<Object> list= redisUtil.lGet(Constants.REDIS_PREFIX_CONVERSATION + currentUser.getId(), 0, -1);
		return "";
	}
	
	
	
	
	/**
	 * 触发关闭事件
	 */
	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		WeChatServerPool.removeUser(conn);
	}
	
	/**
	 * 触发异常事件(废弃，用发消息类型代替)
	 */
	@Override
	public void onError( WebSocket conn, Exception ex ) {
		ex.printStackTrace();
		if( conn != null ) {
			//some errors like port binding failed may not be assignable to a specific websocket
		}
	}
	
	/**
	 * 触发连接事件(废弃，用发消息类型代替)
	 */
	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		//this.sendToAll( "new connection: " + handshake.getResourceDescriptor() );
		//System.out.println("===" + conn.getRemoteSocketAddress().getAddress().getHostAddress());
	}

	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}
	
	public WeChatServer(int port) throws UnknownHostException {
		super(new InetSocketAddress(port));
	}

	public WeChatServer(InetSocketAddress address) {
		super(address);
	}

 
	
}
