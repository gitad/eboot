package org.fh.util;

import org.fh.entity.wechat.WxFriends;

import com.alibaba.fastjson.JSONObject;

public class JsonUtil {

	public static String msg = "{\"fromSocketPort\":\"2980:\"\",\"fromWeChatId\":\"wxid_pu7gpgieeyvs21\"}";

	public static void main(String[] args) throws InstantiationException, IllegalAccessException {
		System.out.println("原始数据=" + msg);
		msg = jsonStringConvert(msg);
		System.out.println("转换后数据=" + msg);
		JSONObject jsonObj = JSONObject.parseObject(msg);
		System.out.println("JSON数据=" + jsonObj);
		WxFriends info = jsonObj.toJavaObject(WxFriends.class);
		System.out.println(info);
	}

	/**
	 * 将数据中的英文双引号 更改成中文的双引号以及转义字符处理 \r 转换成 /r 。。。
	 * 
	 * @param s
	 * @return
	 */
	public static String jsonStringConvert(String s) {
		// 转义字符处理
		char[] temp = s.replace("\r", "/r").replace("\t", "/t").replace("\n", "/n").replace("\f", "/f")
				.replace("\b", "/b").replace("\'", "/'").replace("\\", "//").toCharArray();
		int n = temp.length;
		for (int i = 1; i < n; i++) {
			if (temp[i] == ':' && temp[i + 1] == '"' && temp[i - 1] == '"') {
				for (int j = i + 2; j < n; j++) {
					if (temp[j] == '"') { // 如果是双引号，开始判断
						if (temp[j + 1] != ',' && temp[j + 1] != '}') {
							temp[j] = '”'; // 说明是数据中的双引号,将英文的双引号替换成中文的双引号
						} else {
							break;
						}
					}
				}
			}
		}
		return new String(temp);
	}
}
