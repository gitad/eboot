package org.fh.util;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationObjectSupport;

@Component
public class SpringMvcContextHolder extends WebApplicationObjectSupport {

	private static WebApplicationContext springMvcContext;

	@Override
	protected void initApplicationContext(ApplicationContext context) {
		super.initApplicationContext(context);
		if (context instanceof WebApplicationContext) {
			springMvcContext = (WebApplicationContext) context;
		}
	}

	public static WebApplicationContext getSpringMvcContext() {
		return springMvcContext;
	}

	public static Object getBean(String beanId) {
		return springMvcContext.getBean(beanId);
	}

	public static <T> T getBean(Class<T> clazz) {
		return springMvcContext.getBean(clazz);
	}
}
