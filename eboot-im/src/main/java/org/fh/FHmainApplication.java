package org.fh;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 */

@SpringBootApplication
@EnableEurekaClient
@MapperScan("org.fh.mapper")
@EnableCaching
public class FHmainApplication {

	public static void main(String[] args) {
		SpringApplication.run(FHmainApplication.class, args);
	}
	
	
}